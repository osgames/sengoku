-- Army.lua

-- Right now this only contains example data which specifies the file format.
-- Replace with real data.

bitmaps = {
	[1] = { filename = "Graphics\\Map\\idleHonganji.bmp", colorKey = { 0, 255, 0 } },
	[2] = { filename = "Graphics\\Map\\walkHonganji.bmp", colorKey = { 0, 255, 0} },
	[3] = { filename = "Graphics\\Map\\walkAshigaru.bmp", alphaBlend = true },
	[4] = { filename = "Graphics\\Map\\idleAshigaru.bmp", alphaBlend = true },
	[5] = { filename = "Graphics\\Map\\ArmySelection.bmp", alphaBlend = true }
}

frames = {
	[1] = {
		bitmap = 1,
		source = { 0, 0, 37, 58 }
	},
	[2] = {
		bitmap = 1,
		source = { 37, 0, 74, 58 }
	},
	[3] = {
		bitmap = 1,
		source = { 74, 0, 111, 58 }
	},
	[4] = {
		bitmap = 1,
		source = { 111, 0, 148, 58 }
	},
	[5] = {
		bitmap = 1,
		source = { 148, 0, 185, 58 }
	},
	[6] = {
		bitmap = 1,
		source = { 185, 0, 222, 58 }
	},
	[7] = {
		bitmap = 1,
		source = { 222, 0, 259, 58 }
	},
	[8] = {
		bitmap = 1,
		source = { 259, 0, 296, 58 }
	},
	[9] = {
		bitmap = 1,
		source = { 296, 0,333 , 58 }
	},
	[10] = {
		bitmap = 1,
		source = { 333, 0, 370, 58 }
	},
	[11] = {
		bitmap = 1,
		source = { 370, 0, 407, 58 }
	},
	[12] = {
		bitmap = 1,
		source = { 407, 0, 444, 58 }
	},
	[13] = {
		bitmap = 1,
		source = { 444, 0, 481, 58 }
	},
	[14] = {
		bitmap = 1,
		source = { 481, 0, 518, 58 }
	},
	[15] = {
		bitmap = 1,
		source = { 518, 0, 555, 58 }
	},
	[16] = {
		bitmap = 1,
		source = { 555, 0, 592, 58 }
	},
	[17] = {
		bitmap = 1,
		source = { 592, 0, 629, 58 }
	},
	[18] = {
		bitmap = 1,
		source = { 629, 0, 666, 58 }
	},
	[19] = {
		bitmap = 1,
		source = { 666, 0, 703, 58 }
	},
	[20] = {
		bitmap = 1,
		source = { 703, 0, 740, 58 }
	},
	[21] = {
		bitmap = 2,
		source = { 0, 0, 37, 58 }
	},
	[22] = {
		bitmap = 2,
		source = { 37, 0, 74, 58 }
	},
	[23] = {
		bitmap = 2,
		source = { 74, 0, 111, 58 }
	},
	[24] = {
		bitmap = 2,
		source = { 111, 0, 148, 58 }
	},
	[25] = {
		bitmap = 2,
		source = { 148, 0, 185, 58 }
	},
	[26] = {
		bitmap = 2,
		source = { 185, 0, 222, 58 }
	},
	[27] = {
		bitmap = 2,
		source = { 222, 0, 259, 58 }
	},
	[28] = {
		bitmap = 2,
		source = { 259, 0, 296, 58 }
	},
	[29] = {
		bitmap = 2,
		source = { 296, 0,333 , 58 }
	},
	[30] = {
		bitmap = 2,
		source = { 333, 0, 370, 58 }
	},
	[31] = {
		bitmap = 2,
		source = { 370, 0, 407, 58 }
	},
	[32] = {
		bitmap = 2,
		source = { 407, 0, 444, 58 }
	},
	[33] = {
		bitmap = 2,
		source = { 444, 0, 481, 58 }
	},
	[34] = {
		bitmap = 2,
		source = { 481, 0, 518, 58 }
	},
	[35] = {
		bitmap = 2,
		source = { 518, 0, 555, 58 }
	},
	[36] = {
		bitmap = 2,
		source = { 555, 0, 592, 58 }
	},
	[37] = {
		bitmap = 2,
		source = { 592, 0, 629, 58 }
	},
	[38] = {
		bitmap = 2,
		source = { 629, 0, 666, 58 }
	},
	[39] = {
		bitmap = 2,
		source = { 666, 0, 703, 58 }
	},
	[40] = {
		bitmap = 2,
		source = { 703, 0, 740, 58 }
	},
	[41] = {
		bitmap = 3,
		source = { 0, 0, 32, 104 }
	},
	[42] = {
		bitmap = 3,
		source = { 32, 0, 64, 104 }
	},
	[43] = {
		bitmap = 3,
		source = { 64, 0, 96, 104 }
	},
	[44] = {
		bitmap = 3,
		source = { 96, 0, 128, 104 }
	},
	[45] = {
		bitmap = 3,
		source = { 128, 0, 160, 104 }
	},
	[46] = {
		bitmap = 3,
		source = { 160, 0, 192, 104 }
	},
	[47] = {
		bitmap = 3,
		source = { 192, 0, 224, 104 }
	},
	[48] = {
		bitmap = 3,
		source = { 224, 0, 256, 104 }
	},
	[49] = {
		bitmap = 3,
		source = { 256, 0, 288, 104 }
	},
	[50] = {
		bitmap = 3,
		source = { 288, 0, 320, 104 }
	},
	[51] = {
		bitmap = 3,
		source = { 320, 0, 352, 104 }
	},
	[52] = {
		bitmap = 3,
		source = { 352, 0, 384, 104 }
	},
	[53] = {
		bitmap = 3,
		source = { 384, 0, 416, 104 }
	},
	[54] = {
		bitmap = 3,
		source = { 416, 0, 448, 104 }
	},
	[55] = {
		bitmap = 3,
		source = { 448, 0, 480, 104 }
	},
	[56] = {
		bitmap = 3,
		source = { 480, 0, 512, 104 }
	},
	[57] = {
		bitmap = 3,
		source = { 512, 0, 544, 104 }
	},
	[58] = {
		bitmap = 3,
		source = { 544, 0, 576, 104 }
	},
	[59] = {
		bitmap = 3,
		source = { 576, 0, 608, 104 }
	},
	[60] = {
		bitmap = 3,
		source = { 608, 0, 640, 104 }
	},
	[61] = {
		bitmap = 3,
		source = { 0, 104, 32, 208 }
	},
	[62] = {
		bitmap = 3,
		source = { 32, 104, 64, 208 }
	},
	[63] = {
		bitmap = 3,
		source = { 64, 104, 96, 208 }
	},
	[64] = {
		bitmap = 3,
		source = { 96, 104, 128, 208 }
	},
	[65] = {
		bitmap = 3,
		source = { 128, 104, 160, 208 }
	},
	[66] = {
		bitmap = 3,
		source = { 160, 104, 192, 208 }
	},
	[67] = {
		bitmap = 3,
		source = { 192, 104, 224, 208 }
	},
	[68] = {
		bitmap = 3,
		source = { 224, 104, 256, 208 }
	},
	[69] = {
		bitmap = 3,
		source = { 256, 104, 288, 208 }
	},
	[70] = {
		bitmap = 3,
		source = { 288, 104, 320, 208 }
	},
	[71] = {
		bitmap = 3,
		source = { 320, 104, 352, 208 }
	},
	[72] = {
		bitmap = 3,
		source = { 352, 104, 384, 208 }
	},
	[73] = {
		bitmap = 3,
		source = { 384, 104, 416, 208 }
	},
	[74] = {
		bitmap = 3,
		source = { 416, 104, 448, 208 }
	},
	[75] = {
		bitmap = 3,
		source = { 448, 104, 480, 208 }
	},
	[76] = {
		bitmap = 3,
		source = { 480, 104, 512, 208 }
	},
	[77] = {
		bitmap = 3,
		source = { 512, 104, 544, 208 }
	},
	[78] = {
		bitmap = 3,
		source = { 544, 104, 576, 208 }
	},
	[79] = {
		bitmap = 3,
		source = { 576, 104, 608, 208 }
	},
	[80] = {
		bitmap = 3,
		source = { 608, 104, 640, 208 }
	},
	[81] = {
		bitmap = 3,
		source = { 0, 208, 32, 312  }
	},
	[82] = {
		bitmap = 3,
		source = { 32, 208, 64, 312  }
	},
	[83] = {
		bitmap = 3,
		source = { 64, 208, 96, 312  }
	},
	[84] = {
		bitmap = 3,
		source = { 96, 208, 128, 312  }
	},
	[85] = {
		bitmap = 3,
		source = { 128, 208, 160, 312  }
	},
	[86] = {
		bitmap = 3,
		source = { 160, 208, 192, 312  }
	},
	[87] = {
		bitmap = 3,
		source = { 192, 208, 224, 312  }
	},
	[88] = {
		bitmap = 3,
		source = { 224, 208, 256, 312  }
	},
	[89] = {
		bitmap = 3,
		source = { 256, 208, 288, 312  }
	},
	[90] = {
		bitmap = 3,
		source = { 288, 208, 320, 312  }
	},
	[91] = {
		bitmap = 3,
		source = { 320, 208, 352, 312  }
	},
	[92] = {
		bitmap = 3,
		source = { 352, 208, 384, 312  }
	},
	[93] = {
		bitmap = 3,
		source = { 384, 208, 416, 312  }
	},
	[94] = {
		bitmap = 3,
		source = { 416, 208, 448, 312  }
	},
	[95] = {
		bitmap = 3,
		source = { 448, 208, 480, 312  }
	},
	[96] = {
		bitmap = 3,
		source = { 480, 208, 512, 312  }
	},
	[97] = {
		bitmap = 3,
		source = { 512, 208, 544, 312  }
	},
	[98] = {
		bitmap = 3,
		source = { 544, 208, 576, 312  }
	},
	[99] = {
		bitmap = 3,
		source = { 576, 208, 608, 312  }
	},
	[100] = {
		bitmap = 3,
		source = { 608, 208, 640, 312  }
	},
	[101] = {
		bitmap = 3,
		source = { 0, 312, 32, 416  }
	},
	[102] = {
		bitmap = 3,
		source = { 32, 312, 64, 416  }
	},
	[103] = {
		bitmap = 3,
		source = { 64, 312, 96, 416  }
	},
	[104] = {
		bitmap = 3,
		source = { 96, 312, 128, 416  }
	},
	[105] = {
		bitmap = 3,
		source = { 128, 312, 160, 416  }
	},
	[106] = {
		bitmap = 3,
		source = { 160, 312, 192, 416  }
	},
	[107] = {
		bitmap = 3,
		source = { 192, 312, 224, 416  }
	},
	[108] = {
		bitmap = 3,
		source = { 224, 312, 256, 416  }
	},
	[109] = {
		bitmap = 3,
		source = { 256, 312, 288, 416  }
	},
	[110] = {
		bitmap = 3,
		source = { 288, 312, 320, 416  }
	},
	[111] = {
		bitmap = 3,
		source = { 320, 312, 352, 416  }
	},
	[112] = {
		bitmap = 3,
		source = { 352, 312, 384, 416  }
	},
	[113] = {
		bitmap = 3,
		source = { 384, 312, 416, 416  }
	},
	[114] = {
		bitmap = 3,
		source = { 416, 312, 448, 416  }
	},
	[115] = {
		bitmap = 3,
		source = { 448, 312, 480, 416  }
	},
	[116] = {
		bitmap = 3,
		source = { 480, 312, 512, 416  }
	},
	[117] = {
		bitmap = 3,
		source = { 512, 312, 544, 416  }
	},
	[118] = {
		bitmap = 3,
		source = { 544, 312, 576, 416  }
	},
	[119] = {
		bitmap = 3,
		source = { 576, 312, 608, 416  }
	},
	[120] = {
		bitmap = 3,
		source = { 608, 312, 640, 416  }
	},
	[121] = {
		bitmap = 3,
		source = { 0, 416, 32, 520  }
	},
	[122] = {
		bitmap = 3,
		source = { 32, 416, 64, 520  }
	},
	[123] = {
		bitmap = 3,
		source = { 64, 416, 96, 520  }
	},
	[124] = {
		bitmap = 3,
		source = { 96, 416, 128, 520  }
	},
	[125] = {
		bitmap = 3,
		source = { 128, 416, 160, 520  }
	},
	[126] = {
		bitmap = 3,
		source = { 160, 416, 192, 520  }
	},
	[127] = {
		bitmap = 3,
		source = { 192, 416, 224, 520  }
	},
	[128] = {
		bitmap = 3,
		source = { 224, 416, 256, 520  }
	},
	[129] = {
		bitmap = 3,
		source = { 256, 416, 288, 520  }
	},
	[130] = {
		bitmap = 3,
		source = { 288, 416, 320, 520  }
	},
	[131] = {
		bitmap = 3,
		source = { 320, 416, 352, 520  }
	},
	[132] = {
		bitmap = 3,
		source = { 352, 416, 384, 520  }
	},
	[133] = {
		bitmap = 3,
		source = { 384, 416, 416, 520  }
	},
	[134] = {
		bitmap = 3,
		source = { 416, 416, 448, 520  }
	},
	[135] = {
		bitmap = 3,
		source = { 448, 416, 480, 520  }
	},
	[136] = {
		bitmap = 3,
		source = { 480, 416, 512, 520  }
	},
	[137] = {
		bitmap = 3,
		source = { 512, 416, 544, 520  }
	},
	[138] = {
		bitmap = 3,
		source = { 544, 416, 576, 520  }
	},
	[139] = {
		bitmap = 3,
		source = { 576, 416, 608, 520  }
	},
	[140] = {
		bitmap = 3,
		source = { 608, 416, 640, 520  }
	},
	[141] = {
		bitmap = 3,
		source = { 0, 520, 32, 624  }
	},
	[142] = {
		bitmap = 3,
		source = { 32, 520, 64, 624  }
	},
	[143] = {
		bitmap = 3,
		source = { 64, 520, 96, 624  }
	},
	[144] = {
		bitmap = 3,
		source = { 96, 520, 128, 624  }
	},
	[145] = {
		bitmap = 3,
		source = { 128, 520, 160, 624  }
	},
	[146] = {
		bitmap = 3,
		source = { 160, 520, 192, 624  }
	},
	[147] = {
		bitmap = 3,
		source = { 192, 520, 224, 624  }
	},
	[148] = {
		bitmap = 3,
		source = { 224, 520, 256, 624  }
	},
	[149] = {
		bitmap = 3,
		source = { 256, 520, 288, 624  }
	},
	[150] = {
		bitmap = 3,
		source = { 288, 520, 320, 624  }
	},
	[151] = {
		bitmap = 3,
		source = { 320, 520, 352, 624  }
	},
	[152] = {
		bitmap = 3,
		source = { 352, 520, 384, 624  }
	},
	[153] = {
		bitmap = 3,
		source = { 384, 520, 416, 624  }
	},
	[154] = {
		bitmap = 3,
		source = { 416, 520, 448, 624  }
	},
	[155] = {
		bitmap = 3,
		source = { 448, 520, 480, 624  }
	},
	[156] = {
		bitmap = 3,
		source = { 480, 520, 512, 624  }
	},
	[157] = {
		bitmap = 3,
		source = { 512, 520, 544, 624  }
	},
	[158] = {
		bitmap = 3,
		source = { 544, 520, 576, 624  }
	},
	[159] = {
		bitmap = 3,
		source = { 576, 520, 608, 624  }
	},
	[160] = {
		bitmap = 3,
		source = { 608, 520, 640, 624  }
	},
	[161] = {
		bitmap = 3,
		source = { 0, 624, 32, 728  }
	},
	[162] = {
		bitmap = 3,
		source = { 32, 624, 64, 728  }
	},
	[163] = {
		bitmap = 3,
		source = { 64, 624, 96, 728  }
	},
	[164] = {
		bitmap = 3,
		source = { 96, 624, 128, 728  }
	},
	[165] = {
		bitmap = 3,
		source = { 128, 624, 160, 728  }
	},
	[166] = {
		bitmap = 3,
		source = { 160, 624, 192, 728  }
	},
	[167] = {
		bitmap = 3,
		source = { 192, 624, 224, 728  }
	},
	[168] = {
		bitmap = 3,
		source = { 224, 624, 256, 728  }
	},
	[169] = {
		bitmap = 3,
		source = { 256, 624, 288, 728  }
	},
	[170] = {
		bitmap = 3,
		source = { 288, 624, 320, 728  }
	},
	[171] = {
		bitmap = 3,
		source = { 320, 624, 352, 728  }
	},
	[172] = {
		bitmap = 3,
		source = { 352, 624, 384, 728  }
	},
	[173] = {
		bitmap = 3,
		source = { 384, 624, 416, 728  }
	},
	[174] = {
		bitmap = 3,
		source = { 416, 624, 448, 728  }
	},
	[175] = {
		bitmap = 3,
		source = { 448, 624, 480, 728  }
	},
	[176] = {
		bitmap = 3,
		source = { 480, 624, 512, 728  }
	},
	[177] = {
		bitmap = 3,
		source = { 512, 624, 544, 728  }
	},
	[178] = {
		bitmap = 3,
		source = { 544, 624, 576, 728  }
	},
	[179] = {
		bitmap = 3,
		source = { 576, 624, 608, 728  }
	},
	[180] = {
		bitmap = 3,
		source = { 608, 624, 640, 728  }
	},
	[181] = {
		bitmap = 3,
		source = { 0, 728, 32, 832  }
	},
	[182] = {
		bitmap = 3,
		source = { 32, 728, 64, 832  }
	},
	[183] = {
		bitmap = 3,
		source = { 64, 728, 96, 832  }
	},
	[184] = {
		bitmap = 3,
		source = { 96, 728, 128, 832  }
	},
	[185] = {
		bitmap = 3,
		source = { 128, 728, 160, 832  }
	},
	[186] = {
		bitmap = 3,
		source = { 160, 728, 192, 832  }
	},
	[187] = {
		bitmap = 3,
		source = { 192, 728, 224, 832  }
	},
	[188] = {
		bitmap = 3,
		source = { 224, 728, 256, 832  }
	},
	[189] = {
		bitmap = 3,
		source = { 256, 728, 288, 832  }
	},
	[190] = {
		bitmap = 3,
		source = { 288, 728, 320, 832  }
	},
	[191] = {
		bitmap = 3,
		source = { 320, 728, 352, 832  }
	},
	[192] = {
		bitmap = 3,
		source = { 352, 728, 384, 832  }
	},
	[193] = {
		bitmap = 3,
		source = { 384, 728, 416, 832  }
	},
	[194] = {
		bitmap = 3,
		source = { 416, 728, 448, 832  }
	},
	[195] = {
		bitmap = 3,
		source = { 448, 728, 480, 832  }
	},
	[196] = {
		bitmap = 3,
		source = { 480, 728, 512, 832  }
	},
	[197] = {
		bitmap = 3,
		source = { 512, 728, 544, 832  }
	},
	[198] = {
		bitmap = 3,
		source = { 544, 728, 576, 832  }
	},
	[199] = {
		bitmap = 3,
		source = { 576, 728, 608, 832  }
	},
	[200] = {
		bitmap = 3,
		source = { 608, 728, 640, 832  }
	},
	[201] = {
		bitmap = 4,
		source = { 0, 0, 28, 90  }
	},
	[202] = {
		bitmap = 4,
		source = { 28, 0, 56, 90  }
	},
	[203] = {
		bitmap = 4,
		source = { 56, 0, 84, 90  }
	},
	[204] = {
		bitmap = 4,
		source = { 84, 0, 112, 90  }
	},
	[205] = {
		bitmap = 4,
		source = { 112, 0, 140, 90  }
	},
	[206] = {
		bitmap = 4,
		source = { 140, 0, 168, 90  }
	},
	[207] = {
		bitmap = 4,
		source = { 168, 0, 196, 90  }
	},
	[208] = {
		bitmap = 4,
		source = { 196, 0, 224, 90  }
	},
	[209] = {
		bitmap = 4,
		source = { 224, 0, 252, 90  }
	},
	[210] = {
		bitmap = 4,
		source = { 252, 0, 280, 90  }
	},
	[211] = {
		bitmap = 4,
		source = { 280, 0, 308, 90  }
	},
	[212] = {
		bitmap = 4,
		source = { 308, 0, 336, 90  }
	},
	[213] = {
		bitmap = 4,
		source = { 336, 0, 364, 90  }
	},
	[214] = {
		bitmap = 4,
		source = { 364, 0, 392, 90  }
	},
	[215] = {
		bitmap = 4,
		source = { 392, 0, 420, 90  }
	},
	[216] = {
		bitmap = 4,
		source = { 420, 0, 448, 90  }
	},
	[217] = {
		bitmap = 4,
		source = { 448, 0, 476, 90  }
	},
	[218] = {
		bitmap = 4,
		source = { 476, 0, 504, 90  }
	},
	[219] = {
		bitmap = 4,
		source = { 504, 0, 532, 90  }
	},
	[220] = {
		bitmap = 4,
		source = { 532, 0, 560, 90  }
	},
	[988] = {
		bitmap = 5,
		source = { 38, 0, 76, 25  },
		offset = { 1, 26 }
	},
	[989] = {
		bitmap = 5,
		source = { 76, 0, 114, 25  },
		offset = { 1, 26 }
	},
	[990] = {
		bitmap = 5,
		source = { 114, 0, 152, 25  },
		offset = { 1, 26 }
	},
	[991] = {
		bitmap = 5,
		source = { 152, 0, 190, 25  },
		offset = { 1, 26 }
	},
	[992] = {
		bitmap = 5,
		source = { 190, 0, 228, 25  },
		offset = { 1, 26 }
	},
	[993] = {
		bitmap = 5,
		source = { 228, 0, 266, 25  },
		offset = { 1, 26 }
	},
	[994] = {
		bitmap = 5,
		source = { 266, 0, 304, 25  },
		offset = { 1, 26 }
	},
	[995] = {
		bitmap = 5,
		source = { 304, 0, 342, 25  },
		offset = { 1, 26 }
	},
	[996] = {
		bitmap = 5,
		source = { 342, 0, 380, 25  },
		offset = { 1, 26 }
	},
	[997] = {
		bitmap = 5,
		source = { 380, 0, 418, 25  },
		offset = { 1, 26 }
	},
	[998] = {
		bitmap = 5,
		source = { 418, 0, 456, 25  },
		offset = { 1, 26 }
	},
	[999] = {
		bitmap = 5,
		source = { 0, 0, 38, 25  },
		offset = { 1, 26 }
	}
	
}

animations = {
	army1_idle = {
		sequence = {
			{ frame = 1, duration = 0.1 },
			{ frame = 2, duration = 0.1 },
			{ frame = 3, duration = 0.1 },
			{ frame = 4, duration = 0.1 },
			{ frame = 5, duration = 0.1 },
			{ frame = 6, duration = 0.1 },
			{ frame = 7, duration = 0.1 },
			{ frame = 8, duration = 0.1 },
			{ frame = 9, duration = 0.1 },
			{ frame = 10, duration = 0.1 },
			{ frame = 11, duration = 0.1 },
			{ frame = 12, duration = 0.1 },
			{ frame = 13, duration = 0.1 },
			{ frame = 14, duration = 0.1 },
			{ frame = 15, duration = 0.1 },
			{ frame = 16, duration = 0.1 },
			{ frame = 17, duration = 0.1 },
			{ frame = 18, duration = 0.1 },
			{ frame = 19, duration = 0.1 },
			{ frame = 20, duration = 0.1 }
		},
		whenFinishedDo = "army1_idle" -- This means the frame will repeat itself.
	},
	army1_moving = {
		sequence = {
			{ frame = 21, duration = 0.1 },
			{ frame = 22, duration = 0.1 },
			{ frame = 23, duration = 0.1 },
			{ frame = 24, duration = 0.1 },
			{ frame = 25, duration = 0.1 },
			{ frame = 26, duration = 0.1 },
			{ frame = 27, duration = 0.1 },
			{ frame = 28, duration = 0.1 },
			{ frame = 29, duration = 0.1 },
			{ frame = 30, duration = 0.1 },
			{ frame = 31, duration = 0.1 },
			{ frame = 32, duration = 0.1 },
			{ frame = 33, duration = 0.1 },
			{ frame = 34, duration = 0.1 },
			{ frame = 35, duration = 0.1 },
			{ frame = 36, duration = 0.1 },
			{ frame = 37, duration = 0.1 },
			{ frame = 38, duration = 0.1 },
			{ frame = 39, duration = 0.1 },
			{ frame = 40, duration = 0.1 }
		},
		whenFinishedDo = "army1_moving" -- This means the frame will repeat itself.
	},
	army_selection = {
		sequence = {
			{ frame = 999 }
		},
		whenFinishedDo = "army_selection"
	},
	army_selection_start = {
		sequence = {
			{ frame = 988, duration = 0.04 },
			{ frame = 989, duration = 0.04 },
			{ frame = 990, duration = 0.04 },
			{ frame = 991, duration = 0.04 },
			{ frame = 992, duration = 0.04 },
			{ frame = 993, duration = 0.04 },
			{ frame = 994, duration = 0.04 },
			{ frame = 995, duration = 0.04 },
			{ frame = 996, duration = 0.04 },
			{ frame = 997, duration = 0.04 },
			{ frame = 998, duration = 0.04 }
		},
		whenFinishedDo = "army_selection"
	},
	army2_idle = {
		sequence = {	
			{ frame = 201, duration = 1.6 },
			{ frame = 202, duration = 0.2 },
			{ frame = 203, duration = 0.2 },
			{ frame = 204, duration = 0.2 },
			{ frame = 205, duration = 0.2 },
			{ frame = 206, duration = 0.2 },
			{ frame = 207, duration = 0.2 },
			{ frame = 208, duration = 0.2 },
			{ frame = 209, duration = 0.2 },
			{ frame = 210, duration = 0.2 },
			{ frame = 211, duration = 0.2 },
			{ frame = 212, duration = 0.2 },
			{ frame = 213, duration = 0.2 },
			{ frame = 214, duration = 0.2 },
			{ frame = 215, duration = 0.2 },
			{ frame = 216, duration = 0.2 },
			{ frame = 217, duration = 0.2 },
			{ frame = 218, duration = 0.2 },
			{ frame = 219, duration = 0.2 },
			{ frame = 220, duration = 0.2 }
		},
		whenFinishedDo = "army2_idle" 
	},
	army2_moving1 = {
		sequence = {			
			{ frame = 41, duration = 0.1 },
			{ frame = 42, duration = 0.1 },
			{ frame = 43, duration = 0.1 },
			{ frame = 44, duration = 0.1 },
			{ frame = 45, duration = 0.1 },
			{ frame = 46, duration = 0.1 },
			{ frame = 47, duration = 0.1 },
			{ frame = 48, duration = 0.1 },
			{ frame = 49, duration = 0.1 },
			{ frame = 50, duration = 0.1 },
			{ frame = 51, duration = 0.1 },
			{ frame = 52, duration = 0.1 },
			{ frame = 53, duration = 0.1 },
			{ frame = 54, duration = 0.1 },
			{ frame = 55, duration = 0.1 },
			{ frame = 56, duration = 0.1 },
			{ frame = 57, duration = 0.1 },
			{ frame = 58, duration = 0.1 },
			{ frame = 59, duration = 0.1 },
			{ frame = 60, duration = 0.1 }
		},
		whenFinishedDo = "army2_moving1" 
	},
	army2_moving2 = {
		sequence = {			
			{ frame = 61, duration = 0.1 },
			{ frame = 62, duration = 0.1 },
			{ frame = 63, duration = 0.1 },
			{ frame = 64, duration = 0.1 },
			{ frame = 65, duration = 0.1 },
			{ frame = 66, duration = 0.1 },
			{ frame = 67, duration = 0.1 },
			{ frame = 68, duration = 0.1 },
			{ frame = 69, duration = 0.1 },
			{ frame = 70, duration = 0.1 },
			{ frame = 71, duration = 0.1 },
			{ frame = 72, duration = 0.1 },
			{ frame = 73, duration = 0.1 },
			{ frame = 74, duration = 0.1 },
			{ frame = 75, duration = 0.1 },
			{ frame = 76, duration = 0.1 },
			{ frame = 77, duration = 0.1 },
			{ frame = 78, duration = 0.1 },
			{ frame = 79, duration = 0.1 },
			{ frame = 80, duration = 0.1 }
		},
		whenFinishedDo = "army2_moving2" 
	},
	army2_moving3 = {
		sequence = {			
			{ frame = 81, duration = 0.1 },
			{ frame = 82, duration = 0.1 },
			{ frame = 83, duration = 0.1 },
			{ frame = 84, duration = 0.1 },
			{ frame = 85, duration = 0.1 },
			{ frame = 86, duration = 0.1 },
			{ frame = 87, duration = 0.1 },
			{ frame = 88, duration = 0.1 },
			{ frame = 89, duration = 0.1 },
			{ frame = 90, duration = 0.1 },
			{ frame = 91, duration = 0.1 },
			{ frame = 92, duration = 0.1 },
			{ frame = 93, duration = 0.1 },
			{ frame = 94, duration = 0.1 },
			{ frame = 95, duration = 0.1 },
			{ frame = 96, duration = 0.1 },
			{ frame = 97, duration = 0.1 },
			{ frame = 98, duration = 0.1 },
			{ frame = 99, duration = 0.1 },
			{ frame = 100, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving3" 
	},
	army2_moving4 = {
		sequence = {			
			{ frame = 101, duration = 0.1 },
			{ frame = 102, duration = 0.1 },
			{ frame = 103, duration = 0.1 },
			{ frame = 104, duration = 0.1 },
			{ frame = 105, duration = 0.1 },
			{ frame = 106, duration = 0.1 },
			{ frame = 107, duration = 0.1 },
			{ frame = 108, duration = 0.1 },
			{ frame = 109, duration = 0.1 },
			{ frame = 110, duration = 0.1 },
			{ frame = 111, duration = 0.1 },
			{ frame = 112, duration = 0.1 },
			{ frame = 113, duration = 0.1 },
			{ frame = 114, duration = 0.1 },
			{ frame = 115, duration = 0.1 },
			{ frame = 116, duration = 0.1 },
			{ frame = 117, duration = 0.1 },
			{ frame = 118, duration = 0.1 },
			{ frame = 119, duration = 0.1 },
			{ frame = 120, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving4" 
	},
	army2_moving5 = {
		sequence = {			
			{ frame = 121, duration = 0.1 },
			{ frame = 122, duration = 0.1 },
			{ frame = 123, duration = 0.1 },
			{ frame = 124, duration = 0.1 },
			{ frame = 125, duration = 0.1 },
			{ frame = 126, duration = 0.1 },
			{ frame = 127, duration = 0.1 },
			{ frame = 128, duration = 0.1 },
			{ frame = 129, duration = 0.1 },
			{ frame = 130, duration = 0.1 },
			{ frame = 131, duration = 0.1 },
			{ frame = 132, duration = 0.1 },
			{ frame = 133, duration = 0.1 },
			{ frame = 134, duration = 0.1 },
			{ frame = 135, duration = 0.1 },
			{ frame = 136, duration = 0.1 },
			{ frame = 137, duration = 0.1 },
			{ frame = 138, duration = 0.1 },
			{ frame = 139, duration = 0.1 },
			{ frame = 140, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving5" 
	},
	army2_moving6 = {
		sequence = {	
			{ frame = 141, duration = 0.1 },
			{ frame = 142, duration = 0.1 },
			{ frame = 143, duration = 0.1 },
			{ frame = 144, duration = 0.1 },
			{ frame = 145, duration = 0.1 },
			{ frame = 146, duration = 0.1 },
			{ frame = 147, duration = 0.1 },
			{ frame = 148, duration = 0.1 },
			{ frame = 149, duration = 0.1 },
			{ frame = 150, duration = 0.1 },
			{ frame = 151, duration = 0.1 },
			{ frame = 152, duration = 0.1 },
			{ frame = 153, duration = 0.1 },
			{ frame = 154, duration = 0.1 },
			{ frame = 155, duration = 0.1 },
			{ frame = 156, duration = 0.1 },
			{ frame = 157, duration = 0.1 },
			{ frame = 158, duration = 0.1 },
			{ frame = 159, duration = 0.1 },
			{ frame = 160, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving6" 
	},
	army2_moving7 = {
		sequence = {		
			{ frame = 161, duration = 0.1 },
			{ frame = 162, duration = 0.1 },
			{ frame = 163, duration = 0.1 },
			{ frame = 164, duration = 0.1 },
			{ frame = 165, duration = 0.1 },
			{ frame = 166, duration = 0.1 },
			{ frame = 167, duration = 0.1 },
			{ frame = 168, duration = 0.1 },
			{ frame = 169, duration = 0.1 },
			{ frame = 170, duration = 0.1 },
			{ frame = 171, duration = 0.1 },
			{ frame = 172, duration = 0.1 },
			{ frame = 173, duration = 0.1 },
			{ frame = 174, duration = 0.1 },
			{ frame = 175, duration = 0.1 },
			{ frame = 176, duration = 0.1 },
			{ frame = 177, duration = 0.1 },
			{ frame = 178, duration = 0.1 },
			{ frame = 179, duration = 0.1 },
			{ frame = 180, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving7" 
	},
	army2_moving8 = {
		sequence = {		
			{ frame = 181, duration = 0.1 },
			{ frame = 182, duration = 0.1 },
			{ frame = 183, duration = 0.1 },
			{ frame = 184, duration = 0.1 },
			{ frame = 185, duration = 0.1 },
			{ frame = 186, duration = 0.1 },
			{ frame = 187, duration = 0.1 },
			{ frame = 188, duration = 0.1 },
			{ frame = 189, duration = 0.1 },
			{ frame = 190, duration = 0.1 },
			{ frame = 191, duration = 0.1 },
			{ frame = 192, duration = 0.1 },
			{ frame = 193, duration = 0.1 },
			{ frame = 194, duration = 0.1 },
			{ frame = 195, duration = 0.1 },
			{ frame = 196, duration = 0.1 },
			{ frame = 197, duration = 0.1 },
			{ frame = 198, duration = 0.1 },
			{ frame = 199, duration = 0.1 },
			{ frame = 200, duration = 0.1 }	
		},
		whenFinishedDo = "army2_moving8" 
	}

}

sprites = {
	army1 = {
		animations = { "army1_idle", "army1_moving"}
	},
	army_selection = {
		animations = { "army_selection", "army_selection_start" }
	},
	army2 = {
		offset = { 24, 0 },
		animations = { "army2_idle", "army2_moving1", "army2_moving2", "army2_moving3",
                                "army2_moving4", "army2_moving5", "army2_moving6",  "army2_moving7",
                                "army2_moving8" }
	}
}
