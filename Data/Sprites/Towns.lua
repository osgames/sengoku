-- Towns.lua

-- Town sprites.

bitmaps = {
	[1] = { filename = "Graphics\\Map\\Towns.bmp", alphaBlend = true }
}

frames = {
	[1] = {
		bitmap = 1,
		source = { 0, 0, 48, 48 }
	},
	[2] = {
		bitmap = 1,
		source = { 48, 0, 96, 48 }
	}
}

animations = {
	town_L0_idle = {
		sequence = {
			{ frame = 1 }
		},
		whenFinishedDo = "town_L0_idle"
	},
	town_L1_idle = {
		sequence = {
			{ frame = 2 }
		},
		whenFinishedDo = "town_L1_idle"
	}
}

sprites = {
	town = {
		animations = { "town_L0_idle", "town_L1_idle" }
	}
}