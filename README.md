# Sengoku: Warring States of Japan

Originally developed at https://sourceforge.net/projects/sengdokuwsj/ by [matle](https://sourceforge.net/u/matle/) and [p_hansson](https://sourceforge.net/u/userid-1392011/) and published under the GPL and MIT license.