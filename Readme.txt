Sengdoku: Warring States of Japan, ver. 0.2

Copyright (C) 2006 Petter Hansson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
USA

See GPL.txt for details about the General Public License.

This program also uses the libraries ENet, SDL, SDL_Image, Lua and
Luabind. These libraries are licensed under their respective
licences and copyrighted by their authors.

---------------------------------------------------------------

Table of Contents:

B. Installation and Setting Up
B.1: Screen Resolutions
C. Multiplayer
D. Known Issues
E. Exiting the Game (Read this!!!)

---------------------------------------------------------------

Section A: Introduction

This is Sengoku: Warring States of Japan, a game set in 16th century
feudal Japan when warlords fought for control of the country.

This is an alpha demo release, only intended to generate interest,
and it's not playable at all (there's nothing to do).

---------------------------------------------------------------

Section B: Installation and Setting Up

Currently installation is quite simple. Unextract the archive into an
empty directory. Before you run the main executable Japan.exe, please
run ScreenConfig.exe and enter a valid screen resolution and bit
depth for your computer. See the list below for supported screen
resolutions.

A.1: Currently only 1024x768 is supported because the
game is developed using this resolution. There might be more in the
future.

---------------------------------------------------------------

Section C: Multiplayer

Currently this game only direct IP connections by UDP sockets. While
it is impractical from the point of remembering and entering IP
addresses, it is nontheless a powerful feature. You can set up a
server (which also participates in the game) and then connect either
via a local area network IP address or an external IP address.

Note that it is also possible to mix LAN and Internet connections.
This allows some NAT issues to be solved if the host is at the NAT
routed sub-network. The other clients can then connect to this host
through local IP addresses and anyone else may join externally.

---------------------------------------------------------------

Section D: Known Issues

When exiting (press Escape key at any time), the game will freeze for
some seconds before Windows appear. This is not really a problem as
such as it is deallocating resources, but if you don't expect it you
might think the program hung up.

When tabbing the program might experience program with reloading its
bitmaps, resulting in a corrupted interface. We're working on this
issue.

---------------------------------------------------------------

Section E: Exiting the Game

As there currently isn't a menu for exiting from within the game you
should just press Escape.