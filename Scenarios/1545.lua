-- 1545 scenario

header = {
	name = "1545: Warring States of Japan",
	date = { year = 1545, month = 12, day = 1 },

	selectable = { "AKIA", "AKIT", "AKIZ", "AMAK", "ANEG", "ASAI", "ASAK", "ASHI", "ASHN", "ASOU", "BESS", "CHOS", "DATE", "GOTO", "HAT2", "HAT3", "HATA", "HATN", "HOJO", "HORC", "HOSO", "ICHI", "IMAG", "ISSH", "ITOU", "JINB", "KAMA", "KIII", "KIKK", "KIKU", "KIMO", "KISO", "KITA", "KOBA", "KONO", "KUKI", "MATD", "MATS", "MATU", "MIMU", "MIYO", "MOGA", "MORI", "MOTO", "MURA", "NAGO", "NANB", "NIKI", "ODAA", "OMUR", "OTOM", "OUCH", "SAGA", "SAIT", "SATA", "SATO", "SHIM", "SHON", "SOGO", "SOUU", "TAK2", "TAKE", "TSUC", "TSUG", "TSUT", "UESU", "UKIT", "URAK", "UTSU", "UTS2", "YAMA" },

	clanDescriptors = "Scenarios\\Common\\Clans.lua",
	provinceDescriptors = "Scenarios\\Common\\Provinces.lua",
	townDescriptors = "Scenarios\\Common\\Towns.lua"
}

provinces = {
}

clans = {
	SHIM =
	{
  		ownedProvinces = { 1 },
  		controlledProvinces = { 1 },
  		alive = true,
  		koku = 200
	},

	KIMO =
	{
  		ownedProvinces = { 0 },
  		controlledProvinces = { 0 },
  		alive = true
	},

	ITOU =
	{
  		ownedProvinces = { 2 },
  		controlledProvinces = { 2 },
  		alive = true
	},
	SAGA =
	{
  		ownedProvinces = { 3 },
  		controlledProvinces = { 3 },
  		alive = true
	},
	OTOM =
	{
  		ownedProvinces = { 4 },
  		controlledProvinces = { 4 },
  		alive = true
	},
	KAMA =
	{
  		ownedProvinces = { 5 },
  		controlledProvinces = { 5 },
  		alive = true
	},
	KIII =
	{
  		ownedProvinces = { 6 },
  		controlledProvinces = { 6 },
  		alive = true
	},
	AKIZ =
	{
  		ownedProvinces = { 7 },
  		controlledProvinces = { 7 },
  		alive = true
	},
	SHON =
	{
  		ownedProvinces = { 8 },
  		controlledProvinces = { 8 },
  		alive = true
	},
	SOUU =
	{
  		ownedProvinces = { 9 },
  		controlledProvinces = { 9 },
  		alive = true
	},
	ICHI =
	{
  		ownedProvinces = { 11 },
  		controlledProvinces = { 11 },
  		alive = true
	},
	KONO =
	{
  		ownedProvinces = { 12 },
  		controlledProvinces = { 12 },
  		alive = true
	},
	MIYO =
	{
  		ownedProvinces = { 13, 31, 32 },
  		controlledProvinces = { 13, 31, 32 },
  		alive = true
	},
	SOGO =
	{
  		ownedProvinces = { 14 },
  		controlledProvinces = { 14 },
  		alive = true
	},
	OUCH =
	{
  		ownedProvinces = { 16, 17, 18 },
  		controlledProvinces = { 16, 17, 18 },
  		alive = true
	},
	MORI =
	{
  		ownedProvinces = { 19 },
  		controlledProvinces = { 19 },
  		alive = true
	},
	KIKK =
	{
  		ownedProvinces = { 20 },
  		controlledProvinces = { 20 },
  		alive = true
	},
	AMAK =
	{
  		ownedProvinces = { 21, 23, 70 },
  		controlledProvinces = { 21, 23, 70 },
  		alive = true
	},
	MIMU =
	{
  		ownedProvinces = { 22 },
  		controlledProvinces = { 22 },
  		alive = true
	},
	URAK =
	{
  		ownedProvinces = { 24 },
  		controlledProvinces = { 24 },
  		alive = true
	},
	UKIT =
	{
  		ownedProvinces = { 25 },
  		controlledProvinces = { 25 },
  		alive = true
	},
	YAMA =
	{
  		ownedProvinces = { 26, 28 },
  		controlledProvinces = { 26, 28 },
  		alive = true
	},
	BESS =
	{
  		ownedProvinces = { 27 },
  		controlledProvinces = { 27 },
  		alive = true
	},
	ISSH =
	{
  		ownedProvinces = { 29 },
  		controlledProvinces = { 29 },
  		alive = true
	},
	HATN =
	{
  		ownedProvinces = { 30 },
  		controlledProvinces = { 30 },
  		alive = true
	},
	HATA =
	{
  		ownedProvinces = { 33, 34 },
  		controlledProvinces = { 33, 34 },
  		alive = true
	},
	HORC =
	{
  		ownedProvinces = { 72 },
  		controlledProvinces = { 72 },
  		alive = true
	},
	TSUT =
	{
  		ownedProvinces = { 35 },
  		controlledProvinces = { 35 },
  		alive = true
	},
	KITA =
	{
  		ownedProvinces = { 36 },
  		controlledProvinces = { 36 },
  		alive = true
	},
	KUKI =
	{
  		ownedProvinces = { 37 },
  		controlledProvinces = { 37 },
  		alive = true
	},
	NIKI =
	{
  		ownedProvinces = { 38 },
  		controlledProvinces = { 38 },
  		alive = true
	},
	HOSO =
	{
  		ownedProvinces = { 39, 15 },
  		controlledProvinces = { 39, 15 },
  		alive = true
	},
	ASAI =
	{
  		ownedProvinces = { 40 },
  		controlledProvinces = { 40 },
  		alive = true
	},
	TAK2 =
	{
  		ownedProvinces = { 41 },
  		controlledProvinces = { 41 },
  		alive = true
	},
	ASAK =
	{
  		ownedProvinces = { 42 },
  		controlledProvinces = { 42 },
  		alive = true
	},
	SAIT =
	{
  		ownedProvinces = { 43 },
  		controlledProvinces = { 43 },
  		alive = true
	},
	ODAA =
	{
  		ownedProvinces = { 44 },
  		controlledProvinces = { 44 },
  		alive = true
	},
	MATD =
	{
  		ownedProvinces = { 45 },
  		controlledProvinces = { 45 },
  		alive = true
	},
	IMAG =
	{
  		ownedProvinces = { 47, 46 },
  		controlledProvinces = { 47, 46 },
  		alive = true
	},
	MURA =
	{
  		ownedProvinces = { 48 },
  		controlledProvinces = { 48 },
  		alive = true
	},
	ANEG =
	{
  		ownedProvinces = { 49 },
  		controlledProvinces = { 49 },
  		alive = true
	},
	IKKO =
	{
  		ownedProvinces = { 50 },
  		controlledProvinces = { 50 },
  		alive = true
	},
	JINB =
	{
  		ownedProvinces = { 51 },
  		controlledProvinces = { 51 },
  		alive = true
	},
	HAT2 =
	{
  		ownedProvinces = { 52 },
  		controlledProvinces = { 52 },
  		alive = true
	},
	HOJO =
	{
  		ownedProvinces = { 54, 53, 56 },
  		controlledProvinces = { 54, 53, 56 },
  		alive = true
	},
	TAKE =
	{
  		ownedProvinces = { 55 },
  		controlledProvinces = { 55 },
  		alive = true
	},
	UESU =
	{
  		ownedProvinces = { 57 },
  		controlledProvinces = { 57 },
  		alive = true
	},
	NAGO =
	{
  		ownedProvinces = { 58 },
  		controlledProvinces = { 58 },
  		alive = true
	},
	ASHI =
	{
  		ownedProvinces = { 61 },
  		controlledProvinces = { 61 },
  		alive = true
	},
	SATO =
	{
  		ownedProvinces = { 59, 60 },
  		controlledProvinces = { 59, 60 },
  		alive = true
	},
	SATA =
	{
  		ownedProvinces = { 62 },
  		controlledProvinces = { 62 },
  		alive = true
	},
	UTSU =
	{
  		ownedProvinces = { 63 },
  		controlledProvinces = { 63 },
  		alive = true
	},
	HAT3 =
	{
  		ownedProvinces = { 64 },
  		controlledProvinces = { 64 },
  		alive = true
	},
	TSUG =
	{
  		ownedProvinces = { 65 },
  		controlledProvinces = { 65 },
  		alive = true
	},
	NANB =
	{
  		ownedProvinces = { 66 },
  		controlledProvinces = { 66 },
  		alive = true
	},
	MOGA =
	{
  		ownedProvinces = { 67 },
  		controlledProvinces = { 67 },
  		alive = true
	},
	AKIT =
	{
  		ownedProvinces = { 68 },
  		controlledProvinces = { 68 },
  		alive = true
	},
	ASHN =
	{
  		ownedProvinces = { 71 },
  		controlledProvinces = { 71 },
  		alive = true
	},
	KISO =
	{
  		ownedProvinces = { 72 },
  		controlledProvinces = { 72 },
  		alive = true
	},
	DATE =
	{
  		ownedProvinces = { 73 },
  		controlledProvinces = { 73 },
  		alive = true
	}
}

towns = {
	{
		id = 0,
		castleLevel = 0
	},
	{
		id = 1,
		castleLevel = 0
	},
	{
		id = 2,
		castleLevel = 0
	},
	{
		id = 3,
		castleLevel = 0
	},
	{
		id = 4,
		castleLevel = 0
	},
	{
		id = 5,
		castleLevel = 0
	},
	{
		id = 6,
		castleLevel = 0
	},
	{
		id = 7,
		castleLevel = 0
	},
--	{
--		id = 8,
--		castleLevel = 0
--	},
--	{
--		id = 9,
--		castleLevel = 0
--	},
	{
		id = 10,
		castleLevel = 0
	},
	{
		id = 11,
		castleLevel = 0
	},
	{
		id = 12,
		castleLevel = 0
	},
	{
		id = 13,
		castleLevel = 0
	},
	{
		id = 14,
		castleLevel = 0
	},
--	{
--		id = 15,
--		castleLevel = 0
--	},
	{
		id = 16,
		castleLevel = 0
	},
	{
		id = 17,
		castleLevel = 0
	},
	{
		id = 18,
		castleLevel = 0
	},
	{
		id = 19,
		castleLevel = 0
	},
	{
		id = 20,
		castleLevel = 0
	},
	{
		id = 21,
		castleLevel = 0
	},
	{
		id = 22,
		castleLevel = 0
	},
	{
		id = 23,
		castleLevel = 0
	},
--	{
--		id = 24,
--		castleLevel = 0
--	},
	{
		id = 25,
		castleLevel = 0
	},
--	{
--		id = 26,
--		castleLevel = 0
--	},
	{
		id = 27,
		castleLevel = 0
	},
	{
		id = 28,
		castleLevel = 0
	},
	{
		id = 29,
		castleLevel = 0
	},
	{
		id = 30,
		castleLevel = 0
	},
	{
		id = 31,
		castleLevel = 0
	},
	{
		id = 32,
		castleLevel = 0
	},
	{
		id = 33,
		castleLevel = 0
	},
	{
		id = 34,
		castleLevel = 0
	},
	{
		id = 35,
		castleLevel = 0
	},
	{
		id = 36,
		castleLevel = 0
	},
	{
		id = 37,
		castleLevel = 0
	},
	{
		id = 38,
		castleLevel = 0
	},
	{
		id = 39,
		castleLevel = 0
	},
	{
		id = 40,
		castleLevel = 0
	},
	{
		id = 41,
		castleLevel = 0
	},
	{
		id = 42,
		castleLevel = 0
	},
	{
		id = 43,
		castleLevel = 0
	},
--	{
--		id = 44,
--		castleLevel = 0
--	},
	{
		id = 45,
		castleLevel = 0
	},
	{
		id = 46,
		castleLevel = 0
	},
	{
		id = 47,
		castleLevel = 0
	},
	{
		id = 48,
		castleLevel = 0
	},
	{
		id = 49,
		castleLevel = 0
	},
	{
		id = 50,
		castleLevel = 0
	},
	{
		id = 51,
		castleLevel = 0
	},
	{
		id = 52,
		castleLevel = 0
	},
--	{
--		id = 53,
--		castleLevel = 0
--	},
	{
		id = 54,
		castleLevel = 0
	},
	{
		id = 55,
		castleLevel = 0
	},
	{
		id = 56,
		castleLevel = 0
	},
	{
		id = 57,
		castleLevel = 0
	},
	{
		id = 58,
		castleLevel = 0
	},
	{
		id = 59,
		castleLevel = 0
	},
	{
		id = 60,
		castleLevel = 0
	},
	{
		id = 61,
		castleLevel = 0
	},
	{
		id = 62,
		castleLevel = 0
	},
	{
		id = 63,
		castleLevel = 0
	},
	{
		id = 64,
		castleLevel = 0
	},
	{
		id = 65,
		castleLevel = 0
	},
	{
		id = 66,
		castleLevel = 0
	},
	{
		id = 67,
		castleLevel = 0
	},
	{
		id = 68,
		castleLevel = 0
	},
	{
		id = 69,
		castleLevel = 0
	},
	{
		id = 70,
		castleLevel = 0
	},
	{
		id = 71,
		castleLevel = 0
	},
	{
		id = 72,
		castleLevel = 0
	},
	{
		id = 73,
		castleLevel = 0
	},
--	{
--		id = 74,
--		castleLevel = 0
--	},
	{
		id = 75,
		castleLevel = 0
	},
	{
		id = 76,
		castleLevel = 0
	},
	{
		id = 77,
		castleLevel = 0
	},
--	{
--		id = 78,
--		castleLevel = 0
--	},
	{
		id = 79,
		castleLevel = 0
	},
	{
		id = 80,
		castleLevel = 0
	},
--	{
--		id = 81,
--		castleLevel = 0
--	},
	{
		id = 82,
		castleLevel = 0
	},
	{
		id = 83,
		castleLevel = 0
	},
	{
		id = 84,
		castleLevel = 0
	},
	{
		id = 85,
		castleLevel = 0
	},
--	{
--		id = 86,
--		castleLevel = 0
--	},
	{
		id = 87,
		castleLevel = 0
	},
--	{
--		id = 88,
--		castleLevel = 0
--	},
	{
		id = 89,
		castleLevel = 0
	},
	{
		id = 90,
		castleLevel = 0
	},
	{
		id = 91,
		castleLevel = 0
	},
--	{
--		id = 92,
--		castleLevel = 0
--	},
--	{
--		id = 93,
--		castleLevel = 0
--	},
	{
		id = 94,
		castleLevel = 0
	},
	{
		id = 95,
		castleLevel = 0
	},
	{
		id = 96,
		castleLevel = 0
	},
--	{
--		id = 97,
--		castleLevel = 0
--	},
	{
		id = 98,
		castleLevel = 0
	},
	{
		id = 99,
		castleLevel = 0
	},
	{
		id = 100,
		castleLevel = 0
	},
	{
		id = 101,
		castleLevel = 0
	},
	{
		id = 102,
		castleLevel = 0
	},
--	{
--		id = 103,
--		castleLevel = 0
--	},
	{
		id = 104,
		castleLevel = 0
	},
	{
		id = 105,
		castleLevel = 0
	},
	{
		id = 106,
		castleLevel = 0
	},
--	{
--		id = 107,
--		castleLevel = 0
--	},
	{
		id = 108,
		castleLevel = 0
	},
	{
		id = 109,
		castleLevel = 0
	},
	{
		id = 110,
		castleLevel = 0
	},
--	{
--		id = 111,
--		castleLevel = 0
--	},
--	{
--		id = 112,
--		castleLevel = 0
--	},
	{
		id = 113,
		castleLevel = 0
	},
	{
		id = 114,
		castleLevel = 0
	},
	{
		id = 115,
		castleLevel = 0
	},
--	{
--		id = 116,
--		castleLevel = 0
--	},
--	{
--		id = 117,
--		castleLevel = 0
--	},
	{
		id = 118,
		castleLevel = 0
	},
	{
		id = 119,
		castleLevel = 0
	},
--	{
--		id = 120,
--		castleLevel = 0
--	},
	{
		id = 121,
		castleLevel = 0
	},
	{
		id = 122,
		castleLevel = 0
	},
	{
		id = 123,
		castleLevel = 0
	},
	{
		id = 124,
		castleLevel = 0
	},
	{
		id = 125,
		castleLevel = 0
	},
	{
		id = 126,
		castleLevel = 0
	},
	{
		id = 127,
		castleLevel = 0
	},
	{
		id = 128,
		castleLevel = 0
	},
	{
		id = 129,
		castleLevel = 0
	},
	{
		id = 130,
		castleLevel = 0
	},
	{
		id = 131,
		castleLevel = 0
	},
	{
		id = 132,
		castleLevel = 0
	},
--	{
--		id = 133,
--		castleLevel = 0
--	},
	{
		id = 134,
		castleLevel = 0
	},
	{
		id = 135,
		castleLevel = 0
	},
--	{
--		id = 136,
--		castleLevel = 0
--	},
--	{
--		id = 137,
--		castleLevel = 0
--	},
	{
		id = 138,
		castleLevel = 0
	},
	{
		id = 139,
		castleLevel = 0
	},
	{
		id = 140,
		castleLevel = 0
	},
	{
		id = 141,
		castleLevel = 0
	},
	{
		id = 142,
		castleLevel = 0
	},
	{
		id = 143,
		castleLevel = 0
	},
	{
		id = 144,
		castleLevel = 0
	},
	{
		id = 145,
		castleLevel = 0
	},
	{
		id = 146,
		castleLevel = 0
	},
	{
		id = 147,
		castleLevel = 0
	},
	{
		id = 148,
		castleLevel = 0
	},
	{
		id = 149,
		castleLevel = 0
	},
--	{
--		id = 150,
--		castleLevel = 0
--	},
	{
		id = 151,
		castleLevel = 0
	},
	{
		id = 152,
		castleLevel = 0
	},
	{
		id = 153,
		castleLevel = 0
	},
--	{
--		id = 154,
--		castleLevel = 0
--	},
	{
		id = 155,
		castleLevel = 0
	},
--	{
--		id = 156,
--		castleLevel = 0
--	},
	{
		id = 157,
		castleLevel = 0
	},
	{
		id = 158,
		castleLevel = 0
	},
	{
		id = 159,
		castleLevel = 0
	},
	{
		id = 160,
		castleLevel = 0
	},
--	{
--		id = 161,
--		castleLevel = 0
--	},
--	{
--		id = 162,
--		castleLevel = 0
--	},
	{
		id = 163,
		castleLevel = 0
	},
	{
		id = 164,
		castleLevel = 0
	},
	{
		id = 165,
		castleLevel = 0
	},
	{
		id = 166,
		castleLevel = 0
	},
	{
		id = 167,
		castleLevel = 0
	},
	{
		id = 168,
		castleLevel = 0
	},
	{
		id = 169,
		castleLevel = 0
	},
	{
		id = 170,
		castleLevel = 0
	},
	{
		id = 171,
		castleLevel = 0
	},
	{
		id = 172,
		castleLevel = 0
	},
	{
		id = 173,
		castleLevel = 0
	},
	{
		id = 174,
		castleLevel = 0
	},
--	{
--		id = 175,
--		castleLevel = 0
--	},
	{
		id = 176,
		castleLevel = 0
	},
	{
		id = 177,
		castleLevel = 0
	},
	{
		id = 178,
		castleLevel = 0
	},
--	{
--		id = 179,
--		castleLevel = 0
--	},
--	{
--		id = 180,
--		castleLevel = 0
--	},
	{
		id = 181,
		castleLevel = 0
	},
--	{
--		id = 182,
--		castleLevel = 0
--	},
	{
		id = 183,
		castleLevel = 0
	},
	{
		id = 184,
		castleLevel = 0
	},
	{
		id = 185,
		castleLevel = 0
	},
	{
		id = 186,
		castleLevel = 0
	},
	{
		id = 187,
		castleLevel = 0
	},
	{
		id = 188,
		castleLevel = 0
	},
	{
		id = 189,
		castleLevel = 0
	},
	{
		id = 190,
		castleLevel = 0
	},
	{
		id = 191,
		castleLevel = 0
	},
--	{
--		id = 192,
--		castleLevel = 0
--	},
	{
		id = 193,
		castleLevel = 0
	},
	{
		id = 194,
		castleLevel = 0
	},
	{
		id = 195,
		castleLevel = 0
	},
	{
		id = 196,
		castleLevel = 0
	},
	{
		id = 197,
		castleLevel = 0
	},
	{
		id = 198,
		castleLevel = 0
	},
	{
		id = 199,
		castleLevel = 0
	},
	{
		id = 200,
		castleLevel = 0
	},
	{
		id = 201,
		castleLevel = 0
	},
--	{
--		id = 202,
--		castleLevel = 0
--	},
	{
		id = 203,
		castleLevel = 0
	},
	{
		id = 204,
		castleLevel = 0
	},
	{
		id = 205,
		castleLevel = 0
	},
	{
		id = 206,
		castleLevel = 0
	},
	{
		id = 207,
		castleLevel = 0
	},
--	{
--		id = 208,
--		castleLevel = 0
--	},
	{
		id = 209,
		castleLevel = 0
	},
	{
		id = 210,
		castleLevel = 0
	},
	{
		id = 211,
		castleLevel = 0
	},
	{
		id = 212,
		castleLevel = 0
	},
	{
		id = 213,
		castleLevel = 0
	},
	{
		id = 214,
		castleLevel = 0
	},
	{
		id = 215,
		castleLevel = 0
	},
	{
		id = 216,
		castleLevel = 0
	},
	{
		id = 217,
		castleLevel = 0
	},
	{
		id = 218,
		castleLevel = 0
	},
--	{
--		id = 219,
--		castleLevel = 0
--	},
--	{
--		id = 220,
--		castleLevel = 0
--	},
--	{
--		id = 221,
--		castleLevel = 0
--	},
	{
		id = 222,
		castleLevel = 0
	},
	{
		id = 223,
		castleLevel = 0
	},
	{
		id = 224,
		castleLevel = 0
	},
	{
		id = 225,
		castleLevel = 0
	}
}

armies = {
	{
		id = 0,
		name = "The Testers!!!",
		location = 0,
		owner = "SHIM",
		regiments = {
			{
				homeTown = 0,
				quality = 128,
				morale = 255,
				gunners = 123,
				ashigaru = 502,
				samurai = 245,
				cavalry = 52,
				monks = 124
			}
		}
	},
	{
		id = 4,
		name = "15th Tosa Reg.",
		location = 200,
		owner = "CHOS",
		regiments = {
			{
				homeTown = 1,
				quality = 192,
				morale = 192,
				cavalry = 345
			}
		}
	},
	{
		id = 14,
		name = "Ronin.",
		location = 170,
		owner = "CHOS",
		regiments = {
			{
				homeTown = 43,
				quality = 192,
				morale = 192,
				cavalry = 345
			}
		}
	},
	{
		id = 15,
		name = "more Ronin.",
		location = 119,
		owner = "CHOS",
		regiments = {
			{
				homeTown = 43,
				quality = 192,
				morale = 192,
				cavalry = 345
			}
		}
	}
}