-- Clans.lua

-- All tables encountered in this file are assumed to be clan instances.

clans =
{
	AKIA =
	{
		name = "Aki",
		palette = "ownerDarkBlue"
	},



	AKIT =
	{
  		name = "Akita",
  		palette = "ownerGreen"
	},



        AKIZ =
        {
                name = "Akizuki",
                palette = "ownerBabyGreen"
        },



        AMAK =
        {
                name = "Amako",
                palette = "ownerLightGreen"
        },



        ANDO =
        {
                name = "Ando",
                palette = "ownerYellow"
        },



        ANEG =
        {
                name = "Anegakoji",
                palette = "ownerLightPurple"
        },



        ARIM =
        {
                name = "Arima",
                palette = "ownerYellow"
        },



        ASAI =
        {
                name = "Asai",
                palette = "ownerBabyGreen"
        },



        ASAK =
        {
                name = "Asakura",
                palette = "ownerOrange"
        },



        ASHI =
        {
                name = "Ashikaga",
                palette = "ownerWhite"
        },



        ASHN =
        {
                name = "Ashina",
                palette = "ownerLightPurple"
        },



        ASOU =
        {
                name = "Aso",
                palette = "ownerYellow"
        },



        BESS =
        {
                name = "Bessho",
                palette = "ownerGreen"
        },



        CHIB =
        {
                name = "Chiba",
                palette = "ownerYellow"
        },



        CHOS =
        {
                name = "Chosokabe",
                palette = "ownerLightBlue"
        },



        DATE =
        {
                name = "Date",
                palette = "ownerOrange"
        },



        FUKA =
        {
                name = "Fukahori",
                palette = "ownerYellow"
        },



        GAMO =
        {
                name = "Gamo",
                palette = "ownerGreen"
        },



        GOTO =
        {
                name = "Goto",
                palette = "ownerWhite"
        },



        HACH =
        {
                name = "Hachisuka",
                palette = "ownerGreen"
        },



        HATA =
        {
                name = "Hatakeyama",
                palette = "ownerDarkBlue"
        },



        HATN =
        {
                name = "Hatano",
                palette = "ownerLightBlue"
        },



        HETS =
        {
                name = "Hetsugi",
                palette = "ownerYellow"
        },



        HIJI =
        {
                name = "Hijitsuki",
                palette = "ownerGreen"
        },



        HOJO =
        {
                name = "Hojo",
                palette = "ownerPurple"
        },



        HONM =
        {
                name = "Honma",
                palette = "ownerGreen"
        },



        HORC =
        {
                name = "Horiuchi",
                palette = "ownerGreen"
        },



        HORI =
        {
                name = "Hori",
                palette = "ownerGreen"
        },



        HOSO =
        {
                name = "Hosokawa",
                palette = "ownerYellow"
        },



        ICHI =
        {
                name = "Ichijo",
                palette = "ownerBabyGreen"
        },



        IKED =
        {
                name = "Ikeda",
                palette = "ownerYellow"
        },



        IMAG =
        {
                name = "Imagawa",
                palette = "ownerSkyBlue"
        },



        INAB =
        {
                name = "Inaba",
                palette = "ownerYellow"
        },



        IRIK =
        {
                name = "Iriki-in",
                palette = "ownerRed"
        },



        ISHI =
        {
                name = "Ishida",
                palette = "ownerGreen"
        },



        ISHK =
        {
                name = "Ishikawa",
                palette = "ownerYellow"
        },



        ISSH =
        {
                name = "Isshiki",
                palette = "ownerLightPurple"
        },



        ITOU =
        {
                name = "Ito",
                palette = "ownerDarkBlue"
        },



        IWAK =
        {
                name = "Iwaki",
                palette = "ownerGreen"
        },



        JINB =
        {
                name = "Jinbo",
                palette = "ownerLightRed"
        },



        KAMA =
        {
                name = "Kamachi",
                palette = "ownerDarkYellow"
        },



        KANA =
        {
                name = "Kanamori",
                palette = "ownerYellow"
        },



        KANE =
        {
                name = "Kaneko",
                palette = "ownerYellow"
        },



        KASA =
        {
                name = "Kasai",
                palette = "ownerGreen"
        },



        KATO =
        {
                name = "Kato",
                palette = "ownerYellow"
        },



        KIII =
        {
                name = "Kii",
                palette = "ownerLightBlue"
        },



        KIKK =
        {
                name = "Kikkawa",
                palette = "ownerTurqois"
        },



        KIKU =
        {
                name = "Kikuchi",
                palette = "ownerGreen"
        },



        KINO =
        {
                name = "Kinoshita",
                palette = "ownerYellow"
        },



        KIMO =
        {
                name = "Kimotsuki",
                palette = "ownerYellow"
        },



        KISO =
        {
                name = "Kiso",
                palette = "ownerGreen"
        },



        KITA =
        {
                name = "Kitabatake",
                palette = "ownerLightGreen"
        },



        KOBA =
        {
                name = "Kobayakawa",
                palette = "ownerGreen"
        },



        KONI =
        {
                name = "Koni",
                palette = "ownerYellow"
        },



        KONO =
        {
                name = "Kono",
                palette = "ownerOrange"
        },



        KUKI =
        {
                name = "Kuki",
                palette = "ownerBrown"
        },



        KURA =
        {
                name = "Kuroda",
                palette = "ownerGreen"
        },



        KURO =
        {
                name = "Kuroki",
                palette = "ownerYellow"
        },



        KYOG =
        {
                name = "Kyogoku",
                palette = "ownerGreen"
        },



        MAED =
        {
                name = "Maeda",
                palette = "ownerYellow"
        },



        MATS =
        {
                name = "Matsuda",
                palette = "ownerGreen"
        },



        MATD =
        {
                name = "Matsudaira",
                palette = "ownerLightBrown"
        },



        MATU =
        {
                name = "Matsuura",
                palette = "ownerGreen"
        },



        MIYO =
        {
                name = "Miyoshi",
                palette = "ownerWhite"
        },



        MOGA =
        {
                name = "Mogami",
                palette = "ownerBabyGreen"
        },



        MORI =
        {
                name = "Mori",
                palette = "ownerDarkRed"
        },



        MOTO =
        {
                name = "Motoyama",
                palette = "ownerBabyGreen"
        },



        MUNA =
        {
                name = "Munakata",
                palette = "ownerYellow"
        },



        MURA =
        {
                name = "Murakami",
                palette = "ownerYellow"
        },



        NABE =
        {
                name = "Nabeshima",
                palette = "ownerYellow"
        },



        NAGA =
        {
                name = "Nagano",
                palette = "ownerGreen"
        },



        NAGO =
        {
                name = "Nagao",
                palette = "ownerBrown"
        },



        NAKA =
        {
                name = "Nakamura",
                palette = "ownerGreen"
        },



        NANB =
        {
                name = "Nanbu",
                palette = "ownerYellow"
        },



        NIKI =
        {
                name = "Niki",
                palette = "ownerGrey"
        },



        ODAA =
        {
                name = "Oda",
                palette = "ownerDarkYellow"
        },



        OMUR =
        {
                name = "Omura",
                palette = "ownerLightGreen"
        },



        ONOD =
        {
                name = "Ododera",
                palette = "ownerYellow"
        },



        ONOJ =
        {
                name = "Onoji",
                palette = "ownerGreen"
        },



        OTAN =
        {
                name = "Otani",
                palette = "ownerYellow"
        },



        OTOM =
        {
                name = "�tomo",
                palette = "ownerLightBrown"
        },



        OUCH =
        {
                name = "Ouchi",
                palette = "ownerYellow"
        },



        OURA =
        {
                name = "Oura",
                palette = "ownerGreen"
        },



        RYUU =
        {
                name = "Ryuuzoji",
                palette = "ownerBrown"
        },



        SAGA =
        {
                name = "Sagara",
                palette = "ownerDarkGrey"
        },



        SAIG =
        {
                name = "Saigo",
                palette = "ownerYellow"
        },



        SAIT =
        {
                name = "Saito",
                palette = "ownerDarkBlue"
        },



        SASA =
        {
                name = "Sasaki",
                palette = "ownerYellow"
        },



        SASS =
        {
                name = "Sassa",
                palette = "ownerGreen"
        },



        SATA =
        {
                name = "Satake",
                palette = "ownerDarkRed"
        },



        SATO =
        {
                name = "Satomi",
                palette = "ownerDarkBlue"
        },



        SHIB =
        {
                name = "Shibata",
                palette = "ownerGreen"
        },



        SHIM =
        {
                name = "Shimazu",
                palette = "ownerGreen"
        },



        SHON =
        {
                name = "Shoni",
                palette = "ownerLightRed"
        },



        SOUU =
        {
                name = "So",
                palette = "ownerWhite"
        },



        SOMA =
        {
                name = "Soma",
                palette = "ownerYellow"
        },



        SOGO =
        {
                name = "Sogo",
                palette = "ownerPurple"
        },



        SUWA =
        {
                name = "Suwa",
                palette = "ownerYellow"
        },



        TACH =
        {
                name = "Tachibana",
                palette = "ownerGreen"
        },



        TAKA =
        {
                name = "Takahasa",
                palette = "ownerYellow"
        },



        TAKH =
        {
                name = "Takahashi",
                palette = "ownerGreen"
        },



        TAKT =
        {
                name = "Takato",
                palette = "ownerYellow"
        },



        TAKE =
        {
                name = "Takeda",
                palette = "ownerTakedaGrey"
        },




        TANI =
        {
                name = "Tani",
                palette = "ownerYellow"
        },



        TOKI =
        {
                name = "Toki",
                palette = "ownerGreen"
        },



        TOKU =
        {
                name = "Tokugawa",
                palette = "ownerTurqois"
        },



        TOYO =
        {
                name = "Toyotomi",
                palette = "ownerYellow"
        },



        TSUC =
        {
                name = "Tsuchimochi",
                palette = "ownerYellow"
        },



        TSUG =
        {
                name = "Tsugaru",
                palette = "ownerYellow"
        },



        TSUT =
        {
                name = "Tsutsui",
                palette = "ownerLightRed"
        },



        UESU =
        {
                name = "Uesugi",
                palette = "ownerLightBlue"
        },



        UKIT =
        {
                name = "Ukita",
                palette = "ownerBrown"
        },



        URAK =
        {
                name = "Urakami",
                palette = "ownerDarkGrey"
        },



        UTSU =
        {
                name = "Utsunomiya",
                palette = "ownerLightGreen"
        },



        UTS2 =
        {
                name = "Utsunomiya",
                palette = "ownerYellow"
        },



        WAKI =
        {
                name = "Wakizaka",
                palette = "ownerYellow"
        },



        YAMA =
        {
                name = "Yamana",
                palette = "ownerOrange"
        },



        HAT2 =
        {
                name = "Hatakeyama",
                palette = "ownerDarkGrey"
        },



        HAT3 =
        {
        name = "Hatakeyama",
        palette = "ownerGrey"
        },



        IKKO =
        {
                name = "Ikko-Ikki",
                palette = "ownerWhite"
        },



        MATG =
        {
                name = "Matsunaga",
                palette = "ownerYellow"
        },



        MIMU =
        {
                name = "Mimura",
                palette = "ownerLightBlue"
        },



        NASU =
        {
                name = "Nasu",
                palette = "ownerYellow"
        },



        TAK2 =
        {
                name = "Takeda",
                palette = "ownerDarkGrey"
        },



        YUKI =
        {
                name = "Yuki",
                palette = "ownerYellow"
        } -- Remember to add a comma if another clan is added following this one
}
