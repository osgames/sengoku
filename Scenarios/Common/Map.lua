-- Map.lua

dofile("Scenarios\\Common\\Palettes.lua")

map = {
	physicalProvinceCount = 74,
	graphicalProvinceCount = 125,
	width = 3300,
	height = 2800,
	provincePath = "Graphics\\Provinces\\",
	overlays = {
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 610, 50, 1000, 315 }, destination = { 221, 1621 } },
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 436, 315, 1247, 1202 }, destination = { 47, 1886 } },
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 41, 1673, 288, 1870 }, destination = { 611, 1673 } },
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 288, 1280, 1242, 2192 }, destination = { 858, 1280 } },
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 1242, 1068, 2422, 1959 }, destination = { 1811, 1068 } },
		{ bitmap = "Graphics\\Provinces\\Overlay.bmp", source = { 1788, 0, 2682, 1068 }, destination = { 2357, 0 } }
	}
}

physicalProvinces = {
-- Kyushu:
	{
		id = 0,
		graphical = { 0, 1, 6, 7, 115 }
	},
	{
		id = 1,
		graphical = { 2, 3, 4, 5 }
	},
	{
		id = 2,
		graphical = { 9 }
	},
	{
		id = 3,
		graphical = { 10, 8, 11, 114, 113, 112 }
	},
	{
		id = 4,
		graphical = { 16 }
	},
	{
		id = 5,
		graphical = { 15 }
	},
	{
		id = 6,
		graphical = { 18 }
	},
	{
		id = 7,
		graphical = { 17 }
	},
	{
		id = 8,
		graphical = { 12, 13, 14, 120, 121, 119, 118, 117, 116 }
	},
	{
		id = 9,
		graphical = { 19, 20 }
	},
	{
		id = 10,
		graphical = { 21 }
	},
-- Shikoku:
	{
		id = 11,
		graphical = { 22, 109 }
	},
	{
		id = 12,
		graphical = { 23, 97, 98, 96, 93 }
	},
	{
		id = 13,
		graphical = { 24 }
	},
	{
		id = 14,
		graphical = { 25, 101, 111, 102, 103 }
	},
	{
		id = 15,
		graphical = { 40 }
	},
-- Honshu:
	{
		id = 16,
		graphical = { 27 }
	},
	{
		id = 17,
		graphical = { 26, 88, 89 }
	},
	{
		id = 18,
		graphical = { 29 }
	},
	{
		id = 19,
		graphical = { 28, 90, 91, 92, 94, 95, 99 }
	},
	{
		id = 20,
		graphical = { 30, 100 }
	},
	{
		id = 21,
		graphical = { 31 }
	},
	{
		id = 22,
		graphical = { 35, 110 }
	},
	{
		id = 23,
		graphical = { 34 }
	},
	{
		id = 24,
		graphical = { 36 }
	},
	{
		id = 25,
		graphical = { 37 }
	},
	{
		id = 26,
		graphical = { 38 }
	},
	{
		id = 27,
		graphical = { 39 }
	},
	{
		id = 28,
		graphical = { 47 }
	},
	{
		id = 29,
		graphical = { 48 }
	},
	{
		id = 30,
		graphical = { 46 }
	},
	{
		id = 31,
		graphical = { 45 }
	},
	{
		id = 32,
		graphical = { 44 }
	},
	{
		id = 33,
		graphical = { 43 }
	},
	{
		id = 34,
		graphical = { 41 }
	},
	{
		id = 35,
		graphical = { 42 }
	},
	{
		id = 36,
		graphical = { 52 }
	},
	{
		id = 37,
		graphical = { 53 }
	},
	{
		id = 38,
		graphical = { 51 }
	},
	{
		id = 39,
		graphical = { 50 }
	},
	{
		id = 40,
		graphical = { 58 }
	},
	{
		id = 41,
		graphical = { 49 }
	},
	{
		id = 42,
		graphical = { 57 }
	},
	{
		id = 43,
		graphical = { 56 }
	},
	{
		id = 44,
		graphical = { 54 }
	},
	{
		id = 45,
		graphical = { 55 }
	},
	{
		id = 46,
		graphical = { 59 }
	},
	{
		id = 47,
		graphical = { 62 }
	},
	{
		id = 48,
		graphical = { 63 }
	},
	{
		id = 49,
		graphical = { 64 }
	},
	{
		id = 50,
		graphical = { 65 }
	},
	{
		id = 51,
		graphical = { 66 }
	},
	{
		id = 52,
		graphical = { 67, 84 }
	},
	{
		id = 53,
		graphical = { 61, 33, 106, 107, 104, 105 }
	},
	{
		id = 54,
		graphical = { 71 }
	},
	{
		id = 55,
		graphical = { 83 }
	},
	{
		id = 56,
		graphical = { 70 }
	},
	{
		id = 57,
		graphical = { 60 }
	},
	{
		id = 58,
		graphical = { 69, 68 }
	},
	{
		id = 59,
		graphical = { 72 }
	},
	{
		id = 60,
		graphical = { 73 }
	},
	{
		id = 61,
		graphical = { 74 }
	},
	{
		id = 62,
		graphical = { 75 }
	},
	{
		id = 63,
		graphical = { 76 }
	},
	{
		id = 64,
		graphical = { 77 }
	},
	{
		id = 65,
		graphical = { 79 }
	},
	{
		id = 66,
		graphical = { 81 }
	},
	{
		id = 67,
		graphical = { 78 }
	},
	{
		id = 68,
		graphical = { 80 }
	},
	{
		id = 69,
		graphical = { 82, 108 }
	},
	{
		id = 70,
		graphical = { 32, 86, 85, 87 }
	},
	{
		id = 71,
		graphical = { 123 }
	},
	{
		id = 72,
		graphical = { 122 }
	},
	{
		id = 73,
		graphical = { 124 }
	}
}


