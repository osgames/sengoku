--[[
	All palettes essentially are built using the linear equations:

	r(x) = m1 * x + c1
	g(x) = m2 * x + c2
	b(x) = m3 * x + c3

	x is the palette index. This allows for a palette of gradual
	change. Keep in mind color components will be clamped between
	0 and 255 so it doesn't matter if the sum is too high or low
	at certain values of x.

	Added 2007-10-16: Try to avoid really high values of m, because
	that leads to a discontinuous color change for small values of x.
	(For instance, with m = 25, mx = 0, 25, 50, 75, 100 for x = 
	0, 1, 2, 3, 4.) If you want maximum color, make c have a high
	value instead.
]]--


builtPalettes =
{
	shading = { m1 = -1, c1 = 255, m2 = -1, c2 = 255, m3 = -1, c3 = 255 },
	debugGreen = { m1 = 0, c1 = 0, m2 = 0.75, c2 = 64, m3 = 0, c3 = 0 },
	debugRed = { m1 = 1, c1 = 0, m2 = 0, c2 = 0, m3 = 0, c3 = 0 },
	ownerLightRed = { m1 = 1, c1 = 0, m2 = 0.1, c2 = 0, m3 = 0.08, c3 = 0 },
	ownerDarkRed = { m1 = 0.8, c1 = 0, m2 = 0, c2 = 0, m3 = 0, c3 = 0 },
	ownerGreen = { m1 = 0, c1 = 35, m2 = 0.6, c2 = 0, m3 = 0, c3 = 35 },
	ownerLightGreen = { m1 = 0.3, c1 = 0, m2 = 0.75, c2 = 0, m3 = 0, c3 = 0 },
	ownerBabyGreen = { m1 = 0.55, c1 = 0, m2 = 0.92, c2 = 0, m3 = 0.6, c3 = 0 },
	ownerLightBlue = { m1 = 0, c1 = 10, m2 = 0.45, c2 = 0, m3 = 1, c3 = 0 },
	ownerDarkBlue = { m1 = 0, c1 = 5, m2 = 0, c2 = 5, m3 = 0.5, c3 = 0 },
	ownerSkyBlue = { m1 = 0, c1 = 25, m2 = 0.8, c2 = 0, m3 = 0.8, c3 = 0 },
	ownerYellow = { m1 = 1, c1 = 0, m2 = 1, c2 = 0, m3 = 0, c3 = 10 },
	ownerDarkYellow = { m1 = 0.8, c1 = 25, m2 = 0.6, c2 = 25, m3 = 0, c3 = 0 },
	ownerOrange = { m1 = 1, c1 = 0, m2 = 0.3, c2 = 10, m3 = 0, c3 = 0 },
	ownerPurple = { m1 = 0.6, c1 = 0, m2 = 0, c2 = 0, m3 = 0.5, c3 = 0 },
	ownerLightPurple = { m1 = 0.6, c1 = 0, m2 = 0.5, c2 = 0, m3 = 0.8, c3 = 0 },
	ownerTakedaGrey = { m1 = 0.4, c1 = 0, m2 = 0.25, c2 = 0, m3 = 0.3, c3 = 0 },
	ownerBrown = { m1 = 0.7, c1 = 0, m2 = 0.44, c2 = 0, m3 = 0.23, c3 = 0 },
	ownerLightBrown = { m1 = 1, c1 = 0, m2 = 0.5, c2 = 0, m3 = 0.25, c3 = 0 },
	ownerTurqois = { m1 = 0, c1 = 0, m2 = 0.5, c2 = 0, m3 = 0.45, c3 = 0 },
	ownerGrey = { m1 = 0.5, c1 = 0, m2 = 0.5, c2 = 0, m3 = 0.5, c3 = 0 },
	ownerDarkGrey = { m1 = 0.3, c1 = 0, m2 = 0.4, c2 = 0, m3 = 0.4, c3 = 0 },
	ownerWhite = { m1 = 1, c1 = 0, m2 = 1, c2 = 0, m3 = 1, c3 = 0 }
}
