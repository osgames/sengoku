-- Provinces.lua

-- In-game related static province information, shared by multiple scenarios.

provinces = {
-- Kyushu:
	{
		id = 0,
		name = "Osumi",
		farming = 175 -- Thousands of koku produced in 1598.
	},
	{
		id = 1,
		name = "Satsuma",
		farming = 283
	},
	{
		id = 2,
		name = "Hyuga",
		farming = 120
	},
	{
		id = 3,
		name = "Higo",
		farming = 341
	},
	{
		id = 4,
		name = "Bungo",
		farming = 418
	},
	{
		id = 5,
		name = "Chikugo",
		farming = 266
	},
	{
		id = 6,
		name = "Buzen",
		farming = 140
	},
	{
		id = 7,
		name = "Chikuzen",
		farming = 356
	},
	{
		id = 8,
		name = "Hizen",
		farming = 310
	},
	{
		id = 9,
		name = "Tsushima",
		farming = 25 -- Unknown and guessed, check this out later. They had large piracy and trading incomes,put in town?
	},
	{
		id = 10,
		name = "Iki",
		farming = 5 --samma som ovan.
	},
-- Shikoku:
	{
		id = 11,
		name = "Tosa",
		farming = 98
	},
	{
		id = 12,
		name = "Iyo",
		farming = 366
	},
	{
		id = 13,
		name = "Awa",
		farming = 184
	},
	{
		id = 14,
		name = "Sanuki",
		farming = 126
	},
	{
		id = 15,
		name = "Awaji",
		farming = 62
	},
-- Honshu:
	{
		id = 16,
		name = "Nagato",
		farming = 131
	},
	{
		id = 17,
		name = "Suo",
		farming = 168
	},
	{
		id = 18,
		name = "Iwami",
		farming = 112
	},
	{
		id = 19,
		name = "Aki",
		farming = 194
	},
	{
		id = 20,
		name = "Bingo",
		farming = 186
	},
	{
		id = 21,
		name = "Izumo",
		farming = 187
	},
	{
		id = 22,
		name = "Bitchu",
		farming = 177
	},
	{
		id = 23,
		name = "Hoki",
		farming = 101
	},
	{
		id = 24,
		name = "Mimasaka",
		farming = 186
	},
	{
		id = 25,
		name = "Bizen",
		farming = 224
	},
	{
		id = 26,
		name = "Inaba",
		farming = 89
	},
	{
		id = 27,
		name = "Harima",
		farming = 359
	},
	{
		id = 28,
		name = "Tajima",
		farming = 114
	},
	{
		id = 29,
		name = "Tango",
		farming = 111
	},
	{
		id = 30,
		name = "Tanba",
		farming = 264
	},
	{
		id = 31,
		name = "Settsu",
		farming = 356
	},
	{
		id = 32,
		name = "Izumi",
		farming = 142
	},
	{
		id = 33,
		name = "Kawatchi",
		farming = 242
	},
	{
		id = 34,
		name = "Kii",
		farming = 244
	},
	{
		id = 35,
		name = "Yamato",
		farming = 449
	},
	{
		id = 36,
		name = "Ise",
		farming = 567
	},
	{
		id = 37,
		name = "Shima",
		farming = 18 -- Hmmmm. fiskade ist�llet
	},
	{
		id = 38,
		name = "Iga",
		farming = 100
	},
	{
		id = 39,
		name = "Yamashiro",
		farming = 225
	},
	{
		id = 40,
		name = "Omi",
		farming = 775
	},
	{
		id = 41,
		name = "Wakasa",
		farming = 85
	},
	{
		id = 42,
		name = "Echizen",
		farming = 499
	},
	{
		id = 43,
		name = "Mino",
		farming = 540
	},
	{
		id = 44,
		name = "Owari",
		farming = 572
	},
	{
		id = 45,
		name = "Mikawa",
		farming = 291
	},
	{
		id = 46,
		name = "Totomi",
		farming = 255
	},
	{
		id = 47,
		name = "Suruga",
		farming = 150
	},
	{
		id = 48,
		name = "Shinano 1",
		farming = 204
	},
	{
		id = 49,
		name = "Hida",
		farming = 38
	},
	{
		id = 50,
		name = "Kaga",
		farming = 356
	},
	{
		id = 51,
		name = "Etchu",
		farming = 380
	},
	{
		id = 52,
		name = "Noto",
		farming = 210
	},
	{
		id = 53,
		name = "Izu",
		farming = 70
	},
	{
		id = 54,
		name = "Sagami",
		farming = 194
	},
	{
		id = 55,
		name = "Kai",
		farming = 228
	},
	{
		id = 56,
		name = "Musashi",
		farming = 667
	},
	{
		id = 57,
		name = "Kozuke",
		farming = 496
	},
	{
		id = 58,
		name = "Echigo",
		farming = 391
	},
	{
		id = 59,
		name = "Awa",
		farming = 45
	},
	{
		id = 60,
		name = "Kazusa",
		farming = 379
	},
	{
		id = 61,
		name = "Shimosa",
		farming = 393
	},
	{
		id = 62,
		name = "Hitachi",
		farming = 530
	},
	{
		id = 63,
		name = "Shimotsuke",
		farming = 374
	},
	{
		id = 64,
		name = "Iwaki",
		farming = 335
	},
	{
		id = 65,
		name = "Mutsu",
		farming = 335
	},
	{
		id = 66,
		name = "Rikuchu",
		farming = 335
	},
	{
		id = 67,
		name = "Ugen",
		farming = 159
	},
	{
		id = 68,
		name = "Uzen",
		farming = 159
	},
	{
		id = 69,
		name = "Ezo",
		farming = 0
	},
	{
		id = 70,
		name = "Oki",
		farming = 5
	},
	{
		id = 71,
		name = "Iwashiro",
		farming = 335
	},
	{
		id = 72,
		name = "Shinano 2",
		farming = 204
	},
	{
		id = 73,
		name = "Rikuzen",
		farming = 335
	}
}
