-- Towns.lua

towns = {
	{
		id = 0,
		name = "Tokushima",
		province = 13,
		x = 1444,
		y = 1902
	},
	{
		id = 1,
		name = "Aki Castle",
		province = 11,
		x = 1304,
		y = 2013
	},
	{
		id = 2,
		name = "Oko Castle",
		province = 11,
		x = 1210,
		y = 1986
	},
	{
		id = 3,
		name = "Asakura",
		province = 11,
		x = 1171,
		y = 2001
	},
	{
		id = 4,
		name = "Nakamura",
		province = 11,
		x = 1053,
		y = 2122
	},
	{
		id = 5,
		name = "Ozu",
		province = 12,
		x = 999,
		y = 1975
	},
	{
		id = 6,
		name = "Matsuyama",
		province = 12,
		x = 1034,
		y = 1916
	},
	{
		id = 7,
		name = "Doi",
		province = 12,
		x = 1165,
		y = 1889
	},
--	{
--		id = 8,
--		name = "Amagiri",
--		province = 14,
--		x = 1278,
--		y = 1816
--	},
--Takamatsu, later scenario
--	{
--		id = 9,
--		name = "Takamatsu",
--		province = 14,
--		x = 1336,
--		y = 1809
--	},
	{
		id = 10,
		name = "Sogo",
		province = 14,
		x = 1373,
		y = 1827
	},
	{
		id = 11,
		name = "Hofu",
		province = 17,
		x = 803,
		y = 1821
	},
	{
		id = 12,
		name = "Hagi",
		province = 16,
		x = 735,
		y = 1740
	},
	{
		id = 13,
		name = "Yamaguchi",
		province = 16,
		x = 760,
		y = 1788
	},
	{
		id = 14,
		name = "Tsuwano",
		province = 18,
		x = 836,
		y = 1708
	},
--	{
--		id = 15,
--		name = "Sakurao",
--		province = 19,
--		x = 933,
--		y = 1745
--	},
	{
		id = 16,
		name = "Kanayama",
		province = 19,
		x = 973,
		y = 1743
	},
	{
		id = 17,
		name = "Takehara",
		province = 19,
		x = 1067,
		y = 1764
	},
	{
		id = 18,
		name = "Koriyama",
		province = 19,
		x = 1029,
		y = 1689
	},
	{
		id = 19,
		name = "Oasa",
		province = 19,
		x = 956,
		y = 1670
	},
	{
		id = 20,
		name = "Yamabuki",
		province = 18,
		x = 1022,
		y = 1570
	},
	{
		id = 21,
		name = "Takase",
		province = 21,
		x = 1108,
		y = 1518
	},
	{
		id = 22,
		name = "Gassan-Toda",
		province = 21,
		x = 1176,
		y = 1523
	},
	{
		id = 23,
		name = "Akana",
		province = 21,
		x = 1095,
		y = 1563
	},
--	{
--		id = 24,
--		name = "Ueshi",
--		province = 23,
--		x = 1286,
--		y = 1518
--	},
	{
		id = 25,
		name = "Yonago",
		province = 23,
		x = 1223,
		y = 1518
	},
--	{
--		id = 26,
--		name = "Shikano",
--		province = 26,
--		x = 1364,
--		y = 1523
--	},
	{
		id = 27,
		name = "Tottori",
		province = 26,
		x = 1405,
		y = 1519
	},
	{
		id = 28,
		name = "Takiyama",
		province = 20,
		x = 1191,
		y = 1756
	},
	{
		id = 29,
		name = "Takamatsu",
		province = 22,
		x = 1264,
		y = 1724
	},
	{
		id = 30,
		name = "Matsuyama",
		province = 22,
		x = 1251,
		y = 1672
	},
	{
		id = 31,
		name = "Tsuyama",
		province = 24,
		x = 1356,
		y = 1622
	},
	{
		id = 32,
		name = "Katsuyama",
		province = 24,
		x = 1283,
		y = 1614
	},
	{
		id = 33,
		name = "Tenjinyama",
		province = 25,
		x = 1365,
		y = 1694
	},
	{
		id = 34,
		name = "Himeji",
		province = 27,
		x = 1495,
		y = 1684
	},
	{
		id = 35,
		name = "Kozuki",
		province = 27,
		x = 1437,
		y = 1661
	},
	{
		id = 36,
		name = "Miki",
		province = 27,
		x = 1560,
		y = 1716
	},
	{
		id = 37,
		name = "Itami",
		province = 31,
		x = 1648,
		y = 1704
	},
	{
		id = 38,
		name = "Ishiyama", --Osaka
		province = 31,
		x = 1673,
		y = 1734
	},
	{
		id = 39,
		name = "Takatsuki",
		province = 28,
		x = 1700,
		y = 1703
	},
	{
		id = 40,
		name = "Konosumi",
		province = 28,
		x = 1540,
		y = 1510
	},
	{
		id = 41,
		name = "Takeda",
		province = 28,
		x = 1538,
		y = 1575
	},
	{
		id = 42,
		name = "Yagami",
		province = 30,
		x = 1644,
		y = 1633
	},
	{
		id = 43,
		name = "Kuroi",
		province = 30,
		x = 1608,
		y = 1610
	},
--	{
--		id = 44,
--		name = "Momii",
--		province = 30,
--		x = 1667,
--		y = 1625
--	},
	{
		id = 45,
		name = "Miyazu",
		province =29 ,
		x = 1642,
		y = 1522
	},
	{
		id = 46,
		name = "Obama",
		province = 41,
		x = 1771,
		y = 1545
	},
	{
		id = 47,
		name = "Sakai",
		province = 34,
		x = 1659,
		y = 1798
	},
	{
		id = 48,
		name = "Takaya",
		province = 32,
		x = 1697,
		y = 1785
	},
	{
		id = 49,
		name = "Saiga",
		province = 34,
		x = 1600,
		y = 1872
	},
	{
		id = 50,
		name = "Kyoto",
		province = 39,
		x = 1743,
		y = 1667
	},
	{
		id = 51,
		name = "Nara",
		province = 35,
		x = 1771,
		y = 1757
	},
	{
		id = 52,
		name = "Takatori",
		province = 35,
		x = 1762,
		y = 1816
	},
--	{
--		id = 53,
--		name = "Shigi",
--		province = 35,
--		x = 1726,
--		y = 1782
--	},
	{
		id = 54,
		name = "Uyeno",
		province = 38,
		x = 1832,
		y = 1744
	},
	{
		id = 55,
		name = "Toba",
		province = 37,
		x = 1971,
		y = 1843
	},
	{
		id = 56,
		name = "Uji", --later Matsusaka
		province = 36,
		x = 1936,
		y = 1814
	},
	{
		id = 57,
		name = "Anotsu",
		province = 36,
		x = 1896,
		y = 1752
	},
	{
		id = 58,
		name = "Kanbe",
		province = 36,
		x = 1923,
		y = 1710
	},
	{
		id = 59,
		name = "Hino",
		province = 40,
		x = 1839,
		y = 1698
	},
	{
		id = 60,
		name = "Seta",
		province = 40,
		x = 1786,
		y = 1682
	},
	{
		id = 61,
		name = "Kannonji",
		province = 40,
		x = 1817,
		y = 1644
	},
	{
		id = 62,
		name = "Sawayama",
		province = 40,
		x = 1858,
		y = 1622
	},
	{
		id = 63,
		name = "Odani",
		province = 40,
		x = 1877,
		y = 1565
	},
	{
		id = 64,
		name = "Nagoya",
		province = 44,
		x = 1997,
		y = 1660
	},
	{
		id = 65,
		name = "Kiyosu",
		province = 44,
		x = 1999,
		y = 1621 
	},
	{
		id = 66,
		name = "Inabayama", --Gifu
		province = 43,
		x = 1981,
		y = 1582
	},
	{
		id = 67,
		name = "Iwamura",
		province = 43,
		x = 2120,
		y = 1583
	},
	{
		id = 68,
		name = "Akechi",
		province = 43,
		x = 2109,
		y = 1623
	},
	{
		id = 69,
		name = "Ogaki",
		province = 43,
		x = 1935,
		y = 1591
	},
	{
		id = 70,
		name = "Gujo",
		province = 43,
		x = 2032,
		y = 1492
	},
	{
		id = 71,
		name = "Okazaki",
		province = 45,
		x = 2064,
		y = 1718
	},
	{
		id = 72,
		name = "Yoshida",
		province = 45,
		x = 2107,
		y = 1754
	},
	{
		id = 73,
		name = "Nagashino",
		province = 45,
		x = 2162,
		y = 1698
	},
--	{
--		id = 74,
--		name = "Asuke",
--		province = 45,
--		x = 2095,
--		y = 1660
--	},
	{
		id = 75,
		name = "Ichijo-no-Dani",
		province = 42,
		x = 1877,
		y = 1421
	},
	{
		id = 76,
		name = "Tsuraga",
		province = 42,
		x = 1832,
		y = 1515
	},
	{
		id = 77,
		name = "Daishoji",
		province = 50,
		x = 1926,
		y = 1363
	},
--	{
--		id = 78,
--		name = "Komatsu",
--		province = 50,
--		x = 1950,
--		y = 1337
--	},
	{
		id = 79,
		name = "Oyama", --Kanazawa
		province = 50,
		x = 2003,
		y = 1280
	},
	{
		id = 80,
		name = "Nanao",
		province = 52,
		x = 2066,
		y = 1186
	},
--	{
--		id = 81,
--		name = "Anamizu",
--		province = 52,
--		x = 2066,
--		y = 1121
--	},
	{
		id = 82,
		name = "Moriyama",
		province = 51,
		x = 2065,
		y = 1252
	},
	{
		id = 83,
		name = "Toyama",
		province = 51,
		x = 2129,
		y = 1279
	},
	{
		id = 84,
		name = "Uzu",
		province = 51,
		x = 2175,
		y = 1243
	},
	{
		id = 85,
		name = "Takayama",
		province = 49,
		x = 2119,
		y = 1387
	},
--	{
--		id = 86,
--		name = "Tsumagome",
--		province = 72,
--		x = 2184,
--		y = 1546
--	},
	{
		id = 87,
		name = "Fukushima",
		province = 72,
		x = 2193,
		y = 1498
	},
--	{
--		id = 88,
--		name = "Iida?",
--		province = 72,
--		x = 2229,
--		y = 1600
--	},
	{
		id = 89,
		name = "Takato",
		province = 72,
		x = 2270,
		y = 1529
	},
	{
		id = 90,
		name = "Suwa",
		province = 72,
		x = 2296,
		y = 1479
	},
	{
		id = 91,
		name = "Matsumoto",
		province = 72,
		x = 2283,
		y = 1426
	},
--	{
--		id = 92,
--		name = "Shiojiri",
--		province = 7,
--		x = 2277,
--		y = 1442
--	},
--	{
--		id = 93,
--		name = "Iidani",
--		province = 46,
--		x = 2174,
--		y = 1744
--	},
	{
		id = 94,
		name = "Futamata",
		province = 46,
		x = 2197,
		y = 1729
	},
	{
		id = 95,
		name = "Hamamatsu",
		province = 46,
		x = 2185,
		y = 1779
	},
	{
		id = 96,
		name = "Kakegawa",
		province = 46,
		x = 2255,
		y = 1776
	},
--	{
--		id = 97,
--		name = "Takatenjin",
--		province = 46,
--		x = 2278,
--		y = 1774
--	},
	{
		id = 98,
		name = "Kofu",
		province = 55,
		x = 2392,
		y = 1561
	},
	{
		id = 99,
		name = "Nirasaki",
		province = 55,
		x = 2358,
		y = 1522
	},
	{
		id = 100,
		name = "Shimoyama",
		province = 55,
		x = 2356,
		y = 1621
	},
	{
		id = 101,
		name = "Sumpu",
		province = 14,
		x = 2344,
		y = 1721
	},
	{
		id = 102,
		name = "Kanbara",
		province = 14,
		x = 2383,
		y = 1689
	},
--	{
--		id = 103,
--		name = "Yamanaka",
--		province = 14,
--		x = 2449,
--		y = 1695
--	},
	{
		id = 104,
		name = "Nirayama",
		province = 53,
		x = 2469,
		y = 1720
	},
	{
		id = 105,
		name = "Kasugayama",
		province = 58,
		x = 2350,
		y = 1190
	},
	{
		id = 106,
		name = "Biwajima",
		province = 58,
		x = 2435,
		y = 1138
	},
--	{
--		id = 107,
--		name = "Kakizaki",
--		province = 58,
--		x = 2353,
--		y = 1190
--	},
	{
		id = 108,
		name = "Sakado",
		province = 58,
		x = 2474,
		y = 1213
	},
	{
		id = 109,
		name = "Tochio",
		province = 58,
		x = 2511,
		y = 1128
	},
	{
		id = 110,
		name = "Honjo",
		province = 58,
		x = 2655,
		y = 935
	},
--	{
--		id = 111,
--		name = "Yoita",
--		province = 58,
--		x = 2486,
--		y = 1140
--	},
--	{
--		id = 112,
--		name = "Kaizu",
--		province = 48,
--		x = 2355,
--		y = 1316
--	},
	{
		id = 113,
		name = "Iiyama",
		province = 48,
		x = 2387,
		y = 1255
	},
	{
		id = 114,
		name = "Katsurao",    --later scenarios Uyeda
		province = 48,
		x = 2342,
		y = 1350
	},
	{
		id = 115,
		name = "Uchiyama",
		province = 48,
		x = 2387,
		y = 1405
	},
--	{
--		id = 116,
--		name = "Umi no Kuchi",
--		province = 48,
--		x = 2398,
--		y = 1456
--	},
--	{
--		id = 117,
--		name = "Kuruisawa",
--		province = 48,
--		x = 2381,
--		y = 1410
--	},
	{
		id = 118,
		name = "Odawara",
		province = 54,
		x = 2521,
		y = 1666
	},
	{
		id = 119,
		name = "Kamakura",
		province = 54,
		x = 2607,
		y = 1665
	},
--	{
--		id = 120,
--		name = "Arai",
--		province = 54,
--		x = 2627,
--		y = 1692
--	},
	{
		id = 121,
		name = "Edo",
		province = 56,
		x = 2664,
		y = 1578
	},
	{
		id = 122,
		name = "Hachioji",
		province = 56,
		x = 2558,
		y = 1572
	},
	{
		id = 123,
		name = "Kawagoe",
		province = 56,
		x = 2599,
		y = 1525
	},
	{
		id = 124,
		name = "Hachigata",
		province = 56,
		x = 2544,
		y = 1465
	},
	{
		id = 125,
		name = "Kumagaya",
		province = 56,
		x = 2599,
		y = 1459
	},
	{
		id = 126,
		name = "Hirai",
		province = 57,
		x = 2518,
		y = 1399
	},
	{
		id = 127,
		name = "Minowa",
		province = 57,
		x = 2494,
		y = 1376
	},
	{
		id = 128,
		name = "Numata",
		province = 57,
		x = 2516,
		y = 1316
	},
	{
		id = 129,
		name = "Kanayama",
		province = 57,
		x = 2577,
		y = 1395
	},
	{
		id = 130,
		name = "Tateyama",
		province = 45,
		x = 2679,
		y = 1744
	},
	{
		id = 131,
		name = "Otaki",
		province = 61,
		x = 2763,
		y = 1691
	},
	{
		id = 132,
		name = "Shiizu",
		province = 61,
		x = 2732,
		y = 1643
	},
--	{
--		id = 133,
--		name = "Chiba",
--		province = 60,
--		x = 2734,
--		y = 1591
--	},
	{
		id = 134,
		name = "Usui",
		province = 60,
		x = 2766,
		y = 1574
	},
	{
		id = 135,
		name = "Konodai",
		province = 60,
		x = 2694,
		y = 1572
	},
--	{
--		id = 136,
--		name = "Sekiyado",
--		province = 60,
--		x = 2692,
--		y = 1526
--	},
--	{
--		id = 137,
--		name = "Sakura",
--		province = 60,
--		x = 2748,
--		y = 1580
--	},
	{
		id = 138,
		name = "Koga",
		province = 60,
		x = 2682,
		y = 1465
	},
	{
		id = 139,
		name = "Oyama",
		province = 63,
		x = 2682,
		y = 1412
	},
	{
		id = 140,
		name = "Utsunomiya",
		province = 63,
		x = 2709,
		y = 1359
	},
	{
		id = 141,
		name = "Karasuyama",
		province = 63,
		x = 2771,
		y = 1332
	},
	{
		id = 142,
		name = "Oda",
		province = 62,
		x = 2756,
		y = 1477
	},
	{
		id = 143,
		name = "Ota",
		province = 62,
		x = 2861,
		y = 1371
	},
	{
		id = 144,
		name = "Mito",
		province = 62,
		x = 2838,
		y = 1413
	},
	{
		id = 145,
		name = "Nihonmatsu",
		province = 64,
		x = 2834,
		y = 1118
	},
	{
		id = 146,
		name = "Fukushima", --Yanagawa
		province = 64,
		x = 2896,
		y = 1050
	},
	{
		id = 147,
		name = "Souma",
		province = 64,
		x = 2961,
		y = 1064
	},
	{
		id = 148,
		name = "Shirakawa",
		province = 64,
		x = 2789,
		y = 1203
	},
	{
		id = 149,
		name = "Iwaki",
		province = 64,
		x = 2943,
		y = 1268
	},
--	{
--		id = 150,
--		name = "Sukagawa",
--		province = 64,
--		x = 2818,
--		y = 1187
--	},
	{
		id = 151,
		name = "Shiroishi",
		province = 73,
		x = 2929,
		y = 970
	},
	{
		id = 152,
		name = "Osaki",
		province = 73,
		x = 3000,
		y = 892
	},
	{
		id = 153,
		name = "Teraike",
		province = 73,
		x = 3072,
		y = 824
	},
--	{
--		id = 154,
--		name = "Sendai",
--		province = 73,
--		x = 2971,
--		y = 961
--	},
	{
		id = 155,
		name = "Aizu",
		province = 71,
		x = 2738,
		y = 1119
	},
--	{
--		id = 156,
--		name = "Inawashiro",
--		province = 71,
--		x = 2782,
--		y = 1105
--	},
	{
		id = 157,
		name = "Yonezawa",
		province = 67,
		x = 2779,
		y = 1023
	},
	{
		id = 158,
		name = "Yamagata",
		province = 67,
		x = 2810,
		y = 943
	},
	{
		id = 159,
		name = "Sakata",
		province = 67,
		x = 2730,
		y = 809
	},
	{
		id = 160,
		name = "Tendo",
		province = 67,
		x = 2834,
		y = 897
	},
--	{
--		id = 161,
--		name = "Arakawa",
--		province = 58,
--		x = 2650,
--		y = 966
--	},
--	{
--		id = 162,
--		name = "Fujisawa",
--		province = 65,
--		x = 3071,
--		y = 760
--	},
	{
		id = 163,
		name = "Omori",
		province = 68,
		x = 2880,
		y = 682
	},
	{
		id = 164,
		name = "Kubota",
		province = 68,
		x = 2816,
		y = 551
	},
	{
		id = 165,
		name = "Kakunodake",
		province = 68,
		x = 2917,
		y = 593
	},
	{
		id = 166,
		name = "Hinai",
		province = 68,
		x = 2916,
		y = 437
	},
	{
		id = 167,
		name = "Hiyama",
		province = 68,
		x = 2839,
		y = 449
	},
	{
		id = 168,
		name = "Shizukuishi",
		province = 65,
		x = 3000,
		y = 544
	},
	{
		id = 169,
		name = "Toyagasaki",
		province = 65,
		x = 3040,
		y = 634
	},
	{
		id = 170,
		name = "Nabekura",
		province = 65,
		x = 3154,
		y = 646
	},
	{
		id = 171,
		name = "Ichinoseki",
		province = 65,
		x = 3035,
		y = 752
	},
	{
		id = 172,
		name = "Hirosaki",
		province = 66,
		x = 2924,
		y = 367
	},
	{
		id = 173,
		name = "Sannohe",
		province = 66,
		x = 3071,
		y = 423
	},
	{
		id = 174,
		name = "Kunohe",
		province = 66,
		x = 3124,
		y = 461
	},
--	{
--		id = 175,
--		name = "Kanagawa",
--		province = 56,
--		x = 2624,
--		y = 1624
--	},
	{
		id = 176,
		name = "Akamagaseki",
		province = 16,
		x = 638,
		y = 1818
	},
	{
		id = 177,
		name = "Okayama",
		province = 25,
		x = 1311,
		y = 1722
	},
	{
		id = 178,
		name = "Kumano",
		province = 34,
		x = 1774,
		y = 1962
	},
--	{
--		id = 179,
--		name = "Tanakura",
--		province = 62,
--		x = 2846,
--		y = 1305
--	},
--	{
--		id = 180,
--		name = "Niigata",
--		province = 58,
--		x = 2491,
--		y = 1059
--	},
	{
		id = 181,
		name = "Shibata",
		province = 58,
		x = 2585,
		y = 1028
	},
--	{
--		id = 182,
--		name = "Nagasaki",
--		province = 8,
--		x = 330,
--		y = 2111
--	},
--KYUSHU
	{
		id = 183,
		name = "Omura",
		province = 8,
		x = 375,
		y = 2079
	},
	{
		id = 184,
		name = "Hara",  --Shimabara
		province = 8,
		x = 450,
		y = 2134
	},
	{
		id = 185,
		name = "Hirado",
		province = 8,
		x = 283,
		y = 1965
	},
	{
		id = 186,
		name = "Karatsu",
		province = 8,
		x = 389,
		y = 1950
	},
	{
		id = 187,
		name = "Saga",
		province = 8,
		x = 458,
		y = 2002
	},
	{
		id = 188,
		name = "Yanagawa",
		province = 5,
		x = 488,
		y = 2033
	},
	{
		id = 189,
		name = "Yabe",
		province = 5,
		x = 553,
		y = 2022
	},
	{
		id = 190,
		name = "Tachibana",
		province = 7,
		x = 501,
		y = 1926
	},
	{
		id = 191,
		name = "Akizuki",
		province = 7,
		x = 556,
		y = 1964
	},
--	{
--		id = 192,
--		name = "Iwaya",
--		province = 7,
--		x = 517,
--		y = 1935
--	},
	{
		id = 193,
		name = "Moji",
		province = 6,
		x = 619,
		y = 1874
	},
	{
		id = 194,
		name = "Ganjaku",
		province = 6,
		x = 610,
		y = 1931
	},
	{
		id = 195,
		name = "Funai",
		province = 4,
		x = 749,
		y = 2041
	},
	{
		id = 196,
		name = "Usuki",
		province = 4,
		x = 800,
		y = 2065
	},
	{
		id = 197,
		name = "Oka",
		province = 4,
		x = 706,
		y = 2100
	},
	{
		id = 198,
		name = "Mori",
		province = 4,
		x = 649,
		y = 2005
	},
	{
		id = 199,
		name = "Matsuo",
		province = 2,
		x = 732,
		y = 2209
	},
	{
		id = 200,
		name = "Obi",
		province = 2,
		x = 645,
		y = 2431
	},
	{
		id = 201,
		name = "Sadowara",
		province = 2,
		x = 681,
		y = 2315
	},
	{
		id = 203,
		name = "Aya",
		province = 2,
		x = 615,
		y = 2342
	},
	{
		id = 204,
		name = "Koyama",
		province = 0,
		x = 530,
		y = 2496
	},
	{
		id = 205,
		name = "Kajiki",
		province = 0,
		x = 492,
		y = 2386
	},
	{
		id = 206,
		name = "Kagoshima",
		province = 1,
		x = 447,
		y = 2431
	},
	{
		id = 207,
		name = "Iriki-in",
		province = 1,
		x = 422,
		y = 2372
	},
--	{
--		id = 208,
--		name = "Kaseda",
--		province = 1,
--		x = 391,
--		y = 2470
--	},
	{
		id = 209,
		name = "Oguchi",
		province = 1,
		x = 497,
		y = 2313
	},
	{
		id = 210,
		name = "Izumi",
		province = 1,
		x = 433,
		y = 2305
	},
	{
		id = 211,
		name = "Minamata",
		province = 3,
		x = 451,
		y = 2262
	},
	{
		id = 212,
		name = "Hitoyoshi",
		province = 3,
		x = 536,
		y = 2264
	},
	{
		id = 213,
		name = "Yatsushiro",
		province = 3,
		x = 506,
		y = 2187
	},
	{
		id = 214,
		name = "Kumamoto",
		province = 3,
		x = 525,
		y = 2128
	},
	{
		id = 215,
		name = "Kikuchi",
		province = 3,
		x = 563,
		y = 2082
	},
	{
		id = 216,
		name = "Izuhara",
		province = 9,
		x = 254,
		y = 1728
	},
	{
		id = 217,
		name = "Sawata",
		province = 9,
		x = 2400,
		y = 962
	},
	{
		id = 218,
		name = "Sumoto",
		province = 15,
		x = 1521,
		y = 1814
	},

--Ikko Ikki/Warrior monks
--	{
--		id = 219,
--		name = "Negoroji",  
--		province = 15,
--		x = 1645,
--		y = 1842
--	},
--	{
--		id = 220,
--		name = "Enryakuji",  -- Mt Hiei
--		province = 15,
--		x = 1761,
--		y = 1644
--	},
--	{
--		id = 221,
--		name = "Nagashima", 
--		province = 15,
--		x = 1964,
--		y = 1668
--	},
--Gl�mda...
	{
		id = 222,
		name = "Hamada",
		province = 18,
		x = 911,
		y = 1635
	},
	{
		id = 223,
		name = "Ono",
		province = 42,
		x = 1931,
		y = 1439
	},
	{
		id = 224,
		name = "Kazuno",
		province = 66,
		x = 2960,
		y = 451
	},
	{
		id = 225,
		name = "Ichinohe",
		province = 66,
		x = 3053,
		y = 477
	}

--Islands
--	{
--		id = 226,
--		name = "Amakusa",
--		province = 15,
--		x = 362
--		y = 2222
--	},
--	{
--		id = 227,
--		name = "Fukue",  -- Goto islands
--		province = 15,
--		x = 94,
--		y = 2097
--	}
}

roads = {

	{
		a = 0,
		b = 1,
		dist = 12
	},
	{
		a = 1,
		b = 2,
		dist = 5
	},
	{
		a = 2,
		b = 3,
		dist = 2
	},
	{
		a = 3,
		b = 4,
		dist = 12
	},
	{
		a = 4,
		b = 5,
		dist = 12
	},
	{
		a = 5,
		b = 6,
		dist = 4
	},
	{
		a = 3,
		b = 5,
		dist = 10
	},
	{
		a = 6,
		b = 7,
		dist = 8
	},
	{
		a = 7,
		b = 2,
		dist = 10
	},
	{
		a = 7,
		b = 10,
		dist = 13
	},
	{
		a = 10,
		b = 0,
		dist = 6
	},
	{
		a = 176,
		b = 12,
		dist = 9
	},
	{
		a = 176,
		b = 13,
		dist = 8
	},
	{
		a = 11,
		b = 13,
		dist = 5
	},
	{
		a = 12,
		b = 14,
		dist = 7
	},
	{
		a = 11,
		b = 16,
		dist = 12
	},
	{
		a = 14,
		b = 16,
		dist = 6
	},
	{
		a = 16,
		b = 17,
		dist = 5
	},
	{
		a = 16,
		b = 19,
		dist = 7
	},
	{
		a = 16,
		b = 18,
		dist = 6
	},
	{
		a = 17,
		b = 18,
		dist = 6
	},
	{
		a = 17,
		b = 23,
		dist = 7
	},
	{
		a = 18,
		b = 19,
		dist = 4
	},
	{
		a = 18,
		b = 23,
		dist = 10
	},
	{
		a = 18,
		b = 222,
		dist = 11
	},
	{
		a = 19,
		b = 222,
		dist = 5
	},
	{
		a = 14,
		b = 222,
		dist = 5
	},
	{
		a = 222,
		b = 20,
		dist = 6
	},
	{
		a = 20,
		b = 21,
		dist = 5
	},
	{
		a = 21,
		b = 23,
		dist = 3
	},
	{
		a = 22,
		b = 25,
		dist = 3
	},
	{
		a = 25,
		b = 27,
		dist = 10
	},
	{
		a = 25,
		b = 30,
		dist = 11
	},
	{
		a = 27,
		b = 40,
		dist = 8
	},
	{
		a = 27,
		b = 41,
		dist = 9
	},
	{
		a = 28,
		b = 29,
		dist = 5
	},
	{
		a = 29,
		b = 30,
		dist = 4
	},
	{
		a = 29,
		b = 177,
		dist = 3
	},
	{
		a = 30,
		b = 32,
		dist = 6
	},
	{
		a = 31,
		b = 32,
		dist = 3
	},
	{
		a = 31,
		b = 35,
		dist = 5
	},
	{
		a = 31,
		b = 177,
		dist = 9 
	},
	{
		a = 33,
		b = 177,
		dist = 4
	},
	{
		a = 33,
		b = 34,
		dist = 7
	},
	{
		a = 33,
		b = 35,
		dist = 6
	},
	{
		a = 34,
		b = 35,
		dist = 3
	},
	{
		a = 34,
		b = 41,
		dist = 8
	},
	{
		a = 34,
		b = 36,
		dist = 3
	},
	{
		a = 36,
		b = 37,
		dist = 5
	},
	{
		a = 36,
		b = 38,
		dist = 7
	},
	{
		a = 36,
		b = 43,
		dist = 7
	},
	{
		a = 37,
		b = 38,
		dist = 3
	},
	{
		a = 37,
		b = 39,
		dist = 2
	},
	{
		a = 38,
		b = 39,
		dist = 3
	},
	{
		a = 38,
		b = 47,
		dist = 3
	},
	{
		a = 39,
		b = 48,
		dist = 5
	},
	{
		a = 39,
		b = 50,
		dist = 4
	},
	{
		a = 40,
		b = 41,
		dist = 4
	},
	{
		a = 40,
		b = 45,
		dist = 6
	},
	{
		a = 41,
		b = 43,
		dist = 4
	},
	{
		a = 42,
		b = 43,
		dist = 2
	},
	{
		a = 42,
		b = 50,
		dist = 7
	},
	{
		a = 45,
		b = 46,
		dist = 7
	},
	{
		a = 46,
		b = 76,
		dist = 4
	},
	{
		a = 47,
		b = 48,
		dist = 2
	},
	{
		a = 47,
		b = 49,
		dist = 8
	},
	{
		a = 48,
		b = 51,
		dist = 5
	},
	{
		a = 49,
		b = 52,
		dist = 12
	},
	{
		a = 49,
		b = 178,
		dist = 18
	},
	{
		a = 50,
		b = 51,
		dist = 5
	},
	{
		a = 50,
		b = 60,
		dist = 2
	},
	{
		a = 50,
		b = 76,
		dist = 13
	},
	{
		a = 51,
		b = 52,
		dist = 3 
	},
	{
		a = 51,
		b = 54,
		dist = 4
	},
	{
		a = 54,
		b = 57,
		dist = 4
	},
	{
		a = 55,
		b = 56,
		dist = 2
	},
	{
		a = 56,
		b = 57,
		dist = 5
	},
	{
		a = 56,
		b = 178,
		dist = 13
	},
	{
		a = 57,
		b = 58,
		dist = 3
	},
	{
		a = 58,
		b = 59,
		dist = 6
	},
	{
		a = 58,
		b = 64,
		dist = 5
	},
	{
		a = 59,
		b = 60,
		dist = 3
	},
	{
		a = 59,
		b = 61,
		dist = 2
	},
	{
		a = 60,
		b = 61,
		dist = 2
	},
	{
		a = 61,
		b = 62,
		dist = 3
	},
	{
		a = 62,
		b = 63,
		dist = 4
	},
	{
		a = 62,
		b = 69,
		dist = 4
	},
	{
		a = 63,
		b = 76,
		dist = 5
	},
	{
		a = 64,
		b = 65,
		dist = 3
	},
	{
		a = 64,
		b =71,
		dist = 5
	},
	{
		a = 65,
		b = 66,
		dist = 3
	},
	{
		a = 65,
		b = 68,
		dist = 7
	},
	{
		a = 66,
		b = 67,
		dist = 8
	},
	{
		a = 66,
		b = 69,
		dist = 3
	},
	{
		a = 66,
		b = 70,
		dist = 9
	},
	{
		a = 67,
		b = 68,
		dist = 2
	},
	{
		a = 67,
		b = 87,
		dist = 9
	},
	{
		a = 68,
		b = 71,
		dist =7 
	},
	{
		a = 68,
		b = 73,
		dist = 5
	},
	{
		a = 68,
		b = 89,
		dist = 13
	},
	{
		a = 70,
		b = 85,
		dist = 9
	},
	{
		a = 70,
		b = 223,
		dist = 8
	},
	{
		a = 71,
		b = 72,
		dist = 4
	},
	{
		a = 72,
		b = 73,
		dist = 5
	},
	{
		a = 72,
		b = 95,
		dist = 5
	},
	{
		a = 73,
		b = 89,
		dist = 12
	},
	{
		a = 73,
		b = 94,
		dist = 4
	},
	{
		a = 75,
		b = 76,
		dist = 6
	},
	{
		a = 75,
		b = 223,
		dist = 3
	},
	{
		a = 75,
		b = 77,
		dist = 6
	},
	{
		a = 77,
		b = 79,
		dist = 7
	},
	{
		a = 79,
		b = 80,
		dist = 7
	},
	{
		a = 79,
		b = 82,
		dist = 4
	},
	{
		a = 80,
		b = 82,
		dist = 4
	},
	{
		a = 82,
		b = 83,
		dist = 5
	},
	{
		a = 83,
		b = 84,
		dist = 3
	},
	{
		a = 84,
		b = 85,
		dist = 10
	},
	{
		a = 84,
		b = 105,
		dist = 10
	},
	{
		a = 85,
		b = 87,
		dist = 9
	},
	{
		a = 87,
		b = 91,
		dist = 6
	},
	{
		a = 89,
		b = 90,
		dist = 3
	},
	{
		a = 89,
		b = 94,
		dist = 13
	},
	{
		a = 90,
		b = 91,
		dist = 3
	},
	{
		a = 90,
		b = 99,
		dist = 5
	},
	{
		a = 90,
		b = 115,
		dist = 8
	},
	{
		a = 91,
		b = 114,
		dist = 9
	},
	{
		a = 94,
		b = 95,
		dist = 3
	},
	{
		a = 94,
		b = 96,
		dist = 6
	},
	{
		a = 95,
		b = 96,
		dist = 4
	},
	{
		a = 96,
		b = 101,
		dist = 8
	},
	{
		a = 98,
		b = 99,
		dist = 3
	},
	{
		a = 98,
		b = 100,
		dist = 4 
	},
	{
		a = 98,
		b = 122,
		dist = 11
	},
	{
		a = 99,
		b = 115,
		dist = 9
	},
	{
		a = 100,
		b = 102,
		dist = 6
	},
	{
		a = 101,
		b = 102,
		dist = 3
	},
	{
		a = 102,
		b = 104,
		dist = 6 
	},
	{
		a = 102,
		b = 118,
		dist = 9
	},
	{
		a = 104,
		b = 118,
		dist = 4
	},
	{
		a = 105,
		b = 106,
		dist = 5
	},
	{
		a = 105,
		b = 108,
		dist = 6
	},
	{
		a = 105,
		b = 113,
		dist = 7
	},
	{
		a = 106,
		b = 108,
		dist = 5 
	},
	{
		a = 106,
		b = 109,
		dist = 4
	},
	{
		a = 106,
		b = 181,
		dist = 10
	},
	{
		a = 108,
		b = 109,
		dist = 5
	},
	{
		a = 108,
		b = 113,
		dist = 9
	},
	{
		a = 108,
		b = 128,
		dist = 8
	},
	{
		a = 109,
		b = 181,
		dist = 7
	},
	{
		a = 110,
		b = 159,
		dist = 10
	},
	{
		a = 110,
		b = 181,
		dist = 7
	},
	{
		a = 113,
		b = 114,
		dist = 7
	},
	{
		a = 114,
		b = 115,
		dist = 4 
	},
	{
		a = 115,
		b = 126,
		dist = 8
	},
	{
		a = 115,
		b = 127,
		dist = 8
	},
	{
		a = 118,
		b = 119,
		dist = 4
	},
	{
		a = 118,
		b = 122,
		dist = 5
	},
	{
		a = 119,
		b = 121,
		dist = 6
	},
	{
		a = 121,
		b = 122,
		dist = 5
	},
	{
		a = 121,
		b = 123,
		dist = 4
	},
	{
		a = 121,
		b = 135,
		dist = 2
	},
	{
		a = 122,
		b = 123,
		dist = 3
	},
	{
		a = 123,
		b = 124,
		dist = 4
	},
	{
		a = 123,
		b = 125,
		dist = 4
	},
	{
		a = 124,
		b = 125,
		dist = 3
	},
	{
		a = 124,
		b = 126,
		dist = 4
	},
	{
		a = 125,
		b = 129,
		dist = 5
	},
	{
		a = 125,
		b = 138,
		dist = 4
	},
	{
		a = 125,
		b = 139,
		dist = 5
	},
	{
		a = 126,
		b = 127,
		dist = 2
	},
	{
		a = 126,
		b = 129,
		dist = 3
	},
	{
		a = 127,
		b = 128,
		dist = 4
	},
	{
		a = 128,
		b = 129,
		dist = 5
	},
	{
		a = 129,
		b = 139,
		dist = 5
	},
	{
		a = 130,
		b = 131,
		dist = 4
	},
	{
		a = 130,
		b = 132,
		dist = 5
	},
	{
		a = 131,
		b = 132,
		dist = 3
	},
	{
		a = 132,
		b = 134,
		dist = 7
	},
	{
		a = 134,
		b = 135,
		dist = 3
	},
	{
		a = 134,
		b = 142,
		dist = 6
	},
	{
		a = 135,
		b = 138,
		dist = 6
	},
	{
		a = 135,
		b = 142,
		dist = 7
	},
	{
		a = 138,
		b = 139,
		dist = 4
	},
	{
		a = 139,
		b = 140,
		dist = 4
	},
	{
		a = 140,
		b = 141,
		dist = 4
	},
	{
		a = 140,
		b = 142,
		dist = 8
	},
	{
		a = 140,
		b = 148,
		dist = 11
	},
	{
		a = 141,
		b = 143,
		dist = 5
	},
	{
		a = 142,
		b = 144,
		dist = 5
	},
	{
		a = 143,
		b = 144,
		dist = 2
	},
	{
		a = 144,
		b = 148,
		dist = 12
	},
	{
		a = 144,
		b = 149,
		dist = 8
	},
	{
		a = 145,
		b = 146,
		dist = 5
	},
	{
		a = 145,
		b = 148,
		dist = 5
	},
	{
		a = 145,
		b = 149,
		dist = 12
	},
	{
		a = 145,
		b = 155,
		dist = 5
	},
	{
		a = 146,
		b = 147,
		dist = 3
	},
	{
		a = 146,
		b = 151,
		dist = 5
	},
	{
		a = 146,
		b = 157,
		dist = 6
	},
	{
		a = 147,
		b = 149,
		dist = 14
	},
	{
		a = 147,
		b = 151,
		dist = 6
	},
	{
		a = 148,
		b = 155,
		dist = 6
	},
	{
		a = 151,
		b = 152,
		dist = 5
	},
	{
		a = 151,
		b = 158,
		dist = 7
	},
	{
		a = 152,
		b = 153,
		dist = 6
	},
	{
		a = 152,
		b = 171,
		dist = 8
	},
	{
		a = 153,
		b = 170,
		dist = 15
	},
	{
		a = 153,
		b = 171,
		dist = 5
	},
	{
		a = 155,
		b = 157,
		dist = 8
	},
	{
		a = 155,
		b = 181,
		dist = 11
	},
	{
		a = 157,
		b = 158,
		dist = 5
	},
	{
		a = 158,
		b = 159,
		dist = 11
	},
	{
		a = 158,
		b = 160,
		dist = 2
	},
	{
		a = 159,
		b = 160,
		dist = 10
	},
	{
		a = 159,
		b = 164,
		dist = 17
	},
	{
		a = 160,
		b = 163,
		dist = 12
	},
	{
		a = 163,
		b = 165,
		dist = 5
	},
	{
		a = 164,
		b = 165,
		dist = 4
	},
	{
		a = 164,
		b = 167,
		dist = 4
	},
	{
		a = 165,
		b = 168,
		dist = 7
	},
	{
		a = 166,
		b = 167,
		dist = 4
	},
	{
		a = 166,
		b = 172,
		dist = 7
	},
	{
		a = 166,
		b = 224,
		dist = 3
	},
	{
		a = 168,
		b = 169,
		dist = 5
	},
	{
		a = 168,
		b = 225,
		dist = 6
	},
	{
		a = 169,
		b = 170,
		dist = 5
	},
	{
		a = 169,
		b = 171,
		dist = 6
	},
	{
		a = 170,
		b = 174,
		dist = 15
	},
	{
		a = 172,
		b = 173,
		dist = 11
	},
	{
		a = 173,
		b = 174,
		dist = 4
	},
	{
		a = 173,
		b = 224,
		dist = 5
	},
	{
		a = 173,
		b = 225,
		dist = 3
	},
--Kyushu
	{
		a = 183,
		b = 184,
		dist = 5
	},
	{
		a = 183,
		b = 185,
		dist = 8
	},
	{
		a = 183,
		b = 187,
		dist = 8
	},
	{
		a = 185,
		b = 186,
		dist = 6
	},
	{
		a = 186,
		b = 187,
		dist = 4
	},
	{
		a = 186,
		b = 190,
		dist = 6
	},
	{
		a = 187,
		b = 188,
		dist = 3
	},
	{
		a = 187,
		b = 190,
		dist = 7
	},
	{
		a = 188,
		b = 189,
		dist = 4
	},
	{
		a = 188,
		b = 191,
		dist = 7
	},
	{
		a = 188,
		b = 214,
		dist = 7
	},
	{
		a = 189,
		b = 215,
		dist = 4
	},
	{
		a = 190,
		b = 191,
		dist = 4
	},
	{
		a = 190,
		b = 193,
		dist = 9
	},
	{
		a = 191,
		b = 194,
		dist = 4
	},
	{
		a = 191,
		b = 198,
		dist = 6
	},
	{
		a = 193,
		b = 194,
		dist = 4
	},
	{
		a = 193,
		b = 195,
		dist = 12
	},
	{
		a = 194,
		b = 198,
		dist = 6
	},
	{
		a = 195,
		b = 196,
		dist = 3
	},
	{
		a = 195,
		b = 197,
		dist = 4
	},
	{
		a = 195,
		b = 198,
		dist = 5
	},
	{
		a = 196,
		b = 197,
		dist = 5
	},
	{
		a = 196,
		b = 199,
		dist =10 
	},
	{
		a = 197,
		b = 199,
		dist = 8
	},
	{
		a = 199,
		b = 201,
		dist = 7
	},
	{
		a = 199,
		b = 214,
		dist = 15
	},
	{
		a = 200,
		b = 201,
		dist = 7
	},
	{
		a = 200,
		b = 204,
		dist = 7
	},
	{
		a = 201,
		b = 203,
		dist = 4
	},
	{
		a = 203,
		b = 205,
		dist = 8
	},
	{
		a = 203,
		b = 209,
		dist = 7
	},
	{
		a = 203,
		b = 212,
		dist = 7
	},
	{
		a = 204,
		b = 205,
		dist = 7
	},
	{
		a = 205,
		b = 206,
		dist = 4
	},
	{
		a = 205,
		b = 109,
		dist = 6
	},
	{
		a = 205,
		b = 210,
		dist = 7
	},
	{
		a = 206,
		b = 207,
		dist = 5
	},
	{
		a = 207,
		b = 210,
		dist = 6
	},
	{
		a = 209,
		b = 210,
		dist = 3
	},
	{
		a = 209,
		b = 212,
		dist = 7
	},
	{
		a = 210,
		b = 211,
		dist = 4
	},
	{
		a = 211,
		b = 212,
		dist = 4
	},
	{
		a = 211,
		b = 213,
		dist = 5
	},
	{
		a = 212,
		b = 213,
		dist = 6
	},
	{
		a = 213,
		b = 214,
		dist = 5
	},
	{
		a = 214,
		b = 215,
		dist = 3
	},
-- Waterways. tempor�ra, tills det blir best�mt hur de ska g�
   --Tsushima-Kyushu
	{
		a = 185,
		b = 216,
		dist = 6
	},
   -- Kyushu-Honshu
	{
		a = 176,
		b = 193,
		dist = 6
	},
   -- Awaji-Honshu
	{
		a = 47,
		b = 218,
		dist = 6
	},
   -- Awaji-Shikoku
	{
		a = 0,
		b = 218,
		dist = 6
	},
   -- Kyushu-Shikoku
	{
		a = 6,
		b = 195,
		dist = 6
	},
   -- Honshu-Shikoku
	{
		a = 10,
		b = 177,
		dist = 6
	},
   -- Honshu-Sado
	{
		a = 105,
		b = 217,
		dist = 12
	},
	{
		a = 159,
		b = 217,
		dist = 12
	}
}
