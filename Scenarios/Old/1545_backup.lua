-- 1545 scenario

header =
{
	name = "1545: Warring States of Japan",

	selectable = { "AKIA", "AKIT", "AKIZ", "AMAK", "ANEG", "ASAI", "ASAK", "ASHI", "ASHN", "ASOU", "BESS", "CHOS", "DATE", "GOTO", "HAT2", "HAT3", "HATA", "HATN", "HOJO", "HORC", "HOSO", "ICHI", "IMAG", "ISSH", "ITOU", "JINB", "KAMA", "KIII", "KIKK", "KIKU", "KIMO", "KISO", "KITA", "KOBA", "KONO", "KUKI", "MATD", "MATS", "MATU", "MIMU", "MIYO", "MOGA", "MORI", "MOTO", "MURA", "NAGO", "NANB", "NIKI", "ODAA", "OMUR", "OTOM", "OUCH", "SAGA", "SAIT", "SATA", "SATO", "SHIM", "SHON", "SOGO", "SOUU", "TAK2", "TAKE", "TSUC", "TSUT", "UESU", "UKIT", "URAK", "UTSU", "UTS2", "YAMA" },

	clanDescriptors = "Scenarios\\Common\\Clans.lua"
}

global =
{
	date = { year = 1545, month = 11, day = 0 }
}

clans =
{
	SHIM =
	{
  		ownedprovinces = { 1 },
  		controlledprovinces = { 1 },
  		alive = true,
  		koku = 200
	},

	KIMO =
	{
  		ownedprovinces = { 0 },
  		controlledprovinces = { 0 },
  		alive = true
	},

	ITOU =
	{
  		ownedprovinces = { 2 },
  		controlledprovinces = { 2 },
  		alive = true
	}
}

--[[ This means that everything after this is commented until two closing brackets are found.
clan =
{
  tag = SAGA
  ownedprovinces = { 3 }
  controlledprovinces = { 3 }
  alive = yes
}

clan =
{
  tag = OTOM
  ownedprovinces = { 4 }
  controlledprovinces = { 4 }
  alive = yes
}

clan =
{
  tag = KAMA
  ownedprovinces = { 5 }
  controlledprovinces = { 5 }
  alive = yes
}

clan =
{
  tag = KIII
  ownedprovinces = { 6 }
  controlledprovinces = { 6 }
  alive = yes
}

clan =
{
  tag = AKIZ
  ownedprovinces = { 7 }
  controlledprovinces = { 7 }
  alive = yes
}

clan =
{
  tag = SHON
  ownedprovinces = { 8 }
  controlledprovinces = { 8 }
  alive = yes
}

clan =
{
  tag = SOUU
  ownedprovinces = { 9 }
  controlledprovinces = { 9 }
  alive = yes
}

#clan =
#{
#  tag = 
#  ownedprovinces = { 10 }
#  controlledprovinces = { 10 }
#  alive = yes
#}

clan =
{
  tag = ICHI
  lord = 3
  ownedprovinces = { 11 }
  controlledprovinces = { 11 }
  ownedtowns = { 3 }
  controlledtowns =  { 3 }
  alive = yes
}

clan =
{
  tag = AKIA
  lord = 1
  capital = 1
  ownedtowns = { 1 }
  controlledtowns =  { 1 }
  alive = yes
}

clan =
{
  tag = CHOS
  lord = 2
  capital = 2
  ownedtowns = { 2 }
  controlledtowns =  { 2 }
  alive = yes
}

clan =
{
  tag = MOTO
  lord = 8
  capital = 10
  ownedtowns = { 10 }
  controlledtowns =  { 10 }
  alive = yes
}

clan =
{
  tag = KONO
  lord = 4
  capital = 4
  ownedprovinces = { 12 }
  controlledprovinces = { 12 }
  ownedtowns = { 4 7 }
  controlledtowns =  { 4 7 }
  alive = yes
}

clan =
{
  tag = UTS2
  lord = 6
  capital = 6
  ownedtowns = { 6 }
  controlledtowns =  { 6 }
  alive = yes
}

clan =
{
  tag = MIYO
  lord = 0
  capital = 0
  ownedprovinces = { 13 31 32 }
  controlledprovinces = { 13 31 32 }
  ownedtowns = { 0 }
  controlledtowns =  { 0 }
  alive = yes
}

clan =
{
  tag = SOGO
  lord = 5
  capital = 5
  ownedprovinces = { 14 }
  controlledprovinces = { 14 }
  ownedtowns = { 5 8 }
  controlledtowns =  { 5 8 }
  alive = yes
} 

clan =
{
  tag = OUCH
  ownedprovinces = { 16 17 18 }
  controlledprovinces = { 16 17 18 }
  alive = yes
}

clan =
{
  tag = MORI
  ownedprovinces = { 19 }
  controlledprovinces = { 19 }
  alive = yes
} 

clan =
{
  tag = KIKK
  ownedprovinces = { 20 }
  controlledprovinces = { 20 }
  alive = yes
} 

clan =
{
  tag = AMAK
  ownedprovinces = { 21 23 70 }
  controlledprovinces = { 21 23 70 }
  alive = yes
} 

clan =
{
  tag = MIMU
  ownedprovinces = { 22 }
  controlledprovinces = { 22 }
  alive = yes
} 

clan =
{
  tag = URAK
  ownedprovinces = { 24 }
  controlledprovinces = { 24 }
  alive = yes
} 

clan =
{
  tag = UKIT
  ownedprovinces = { 25 }
  controlledprovinces = { 25 }
  alive = yes
} 

clan =
{
  tag = YAMA
  ownedprovinces = { 26 28 }
  controlledprovinces = { 26 28 }
  alive = yes
} 

clan =
{
  tag = BESS
  ownedprovinces = { 27 }
  controlledprovinces = { 27 }
  alive = yes
} 

clan =
{
  tag = ISSH
  ownedprovinces = { 29 }
  controlledprovinces = { 29 }
  alive = yes
} 

clan =
{
  tag = HATN
  ownedprovinces = { 30 }
  controlledprovinces = { 30 }
  alive = yes
} 

clan =
{
  tag = HATA
  ownedprovinces = { 33 34 }
  controlledprovinces = { 33 34 }
  alive = yes
} 

clan =
{
  tag = HORC
  capital = 72
  ownedtowns = { 72 }
  controlledtowns =  { 72 }
  alive = yes
} 

clan =
{
  tag = TSUT
  ownedprovinces = { 35 }
  controlledprovinces = { 35 }
  alive = yes
} 

clan =
{
  tag = KITA
  ownedprovinces = { 36 }
  controlledprovinces = { 36 }
  alive = yes
} 

clan =
{
  tag = KUKI
  ownedprovinces = { 37 }
  controlledprovinces = { 37 }
  alive = yes
} 

clan =
{
  tag = NIKI
  ownedprovinces = { 38 }
  controlledprovinces = { 38 }
  alive = yes
} 

clan =
{
  tag = HOSO
  ownedprovinces = { 39 15 }
  controlledprovinces = { 39 15 }
  alive = yes
} 

clan =
{
  tag = ASAI
  ownedprovinces = { 40 }
  controlledprovinces = { 40 }
  alive = yes
} 

clan =
{
  tag = TAK2
  ownedprovinces = { 41 }
  controlledprovinces = { 41 }
  alive = yes
} 

clan =
{
  tag = ASAK
  ownedprovinces = { 42 }
  controlledprovinces = { 42 }
  alive = yes
} 

clan =
{
  tag = SAIT
  ownedprovinces = { 43 }
  controlledprovinces = { 43 }
  alive = yes
} 

clan =
{
  tag = ODAA
  ownedprovinces = { 44 }
  controlledprovinces = { 44 }
  alive = yes
} 

clan =
{
  tag = MATD
  ownedprovinces = { 45 }
  controlledprovinces = { 45 }
  alive = yes
} 

clan =
{
  tag = IMAG
  ownedprovinces = { 47 46 }
  controlledprovinces = { 47 46 }
  alive = yes
} 

clan =
{
  tag = MURA
  ownedprovinces = { 48 }
  controlledprovinces = { 48 }
  alive = yes
} 

clan =
{
  tag = ANEG
  ownedprovinces = { 49 }
  controlledprovinces = { 49 }
  alive = yes
} 

#Ikko-Ikki here
clan =
{
  tag = IKKO
  ownedprovinces = { 50 }
  controlledprovinces = { 50 }
  alive = yes
} 

clan =
{
  tag = JINB
  ownedprovinces = { 51 }
  controlledprovinces = { 51 }
  alive = yes
} 

clan =
{
  tag = HAT2
  ownedprovinces = { 52 }
  controlledprovinces = { 52 }
  alive = yes
} 

clan =
{
  tag = HOJO
  ownedprovinces = { 54 53 56 }
  controlledprovinces = { 54 53 56 }
  alive = yes
} 

clan =
{
  tag = TAKE
  ownedprovinces = { 55 }
  controlledprovinces = { 55 }
  alive = yes
}

clan =
{
  tag = UESU
  ownedprovinces = { 57 }
  controlledprovinces = { 57 }
  alive = yes
} 

clan =
{
  tag = NAGO
  ownedprovinces = { 58 }
  controlledprovinces = { 58 }
  alive = yes
} 

clan =
{
  tag = ASHI
  ownedprovinces = { 61 }
  controlledprovinces = { 61 }
  alive = yes
} 

clan =
{
  tag = SATO
  ownedprovinces = { 59 60 }
  controlledprovinces = { 59 60 }
  alive = yes
} 

clan =
{
  tag = SATA
  ownedprovinces = { 62 }
  controlledprovinces = { 62 }
  alive = yes
} 

clan =
{
  tag = UTSU
  ownedprovinces = { 63 }
  controlledprovinces = { 63 }
  alive = yes
} 

clan =
{
  tag = HAT3
  ownedprovinces = { 64 }
  controlledprovinces = { 64 }
  alive = yes
} 

clan =
{
  tag = NANB
  ownedprovinces = { 66 65}
  controlledprovinces = { 66 65 }
  alive = yes
} 

clan =
{
  tag = MOGA
  ownedprovinces = { 67 }
  controlledprovinces = { 67 }
  alive = yes
} 

clan =
{
  tag = AKIT
  ownedprovinces = { 68 }
  controlledprovinces = { 68 }
  alive = yes
} 

#Hokkaido
#clan =
#{
#  tag = 
#  ownedprovinces = { 69 }
#  controlledprovinces = { 69 }
#  alive = yes
#} 

clan =
{
  tag = ASHN
  ownedprovinces = { 71 }
  controlledprovinces = { 71 }
  alive = yes
} 

clan =
{
  tag = KISO
  ownedprovinces = { 72 }
  controlledprovinces = { 72 }
  alive = yes
} 

clan =
{
  tag = DATE
  ownedprovinces = { 73 }
  controlledprovinces = { 73 }
  alive = yes
} 

army =
{
  id = { type = 2 id = 0 }
  tag = CHOS
  general = 0
  status = standing
	
  active = yes
  location = 2

  unit = 
  {
    id = 0
    captain = 0
    morale = 1.0
    quality = 2
    
    troops =
    {
       gun = 0
       ash = 800
      sam = 300
       cav = 0
       mon = 0
    }
  }
} 
]]--
