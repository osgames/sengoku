#The required number of towns need to be estimated (in addition to their function).

town =
{
  id = { type = 1 id = 0 }
  name = "Tokushima Castle"
  x = 1441
  y = 1902
  fort = 1
  castellan = 0
  maxfort = 3
  province = 13
  roads = { 1 5 }

  income = {
	farming = 184
  }
}
town =
{
  id = { type = 1 id = 1 }
  name = "Aki Castle"
  x = 1304
  y = 2005
  fort = 1
  castellan = 1
  maxfort = 1
  province = 11
  roads = { 0 2 }


  income = {
	farming = 18
  }
}
town =
{
  id = { type = 1 id = 2 }
  name = "Oko Castle"  #Kochi
  x = 1168
  y = 1999
  fort = 1
  castellan = 2
  maxfort = 5
  province = 11
  roads = { 1 3 5 }


  income = {
	farming = 48
  }
}
town =
{
  id = { type = 1 id = 3 }
  name = "Nakamura"
  x = 1043
  y = 2116
  fort = 1
  castellan = 3
  maxfort = 4
  province = 11
  roads = { 2 6 }


  income = {
	farming = 38
  }
}
town =
{
  id = { type = 1 id = 4 }
  name = "Dogo Castle"
  x = 1033
  y = 1915
  fort = 1
  castellan = 4
  maxfort = 3
  province = 12
  roads = { 6 7 }


  income = {
	farming = 180
  }
}
town =
{
  id = { type = 1 id = 5 }
  name = "Takamatsu Castle"
  x = 1334
  y = 1806
  fort = 1
  castellan = 5
  maxfort = 3
  province = 14
  roads = { 0 8 }


  income = {
	farming = 86
  }
}
town =
{
  id = { type = 1 id = 6 }
  name = "Ouzo Castle"
  x = 970
  y = 1988
  fort = 1
  castellan = 6
  maxfort = 2
  province = 1
  roads = { 3 4 }


  income = {
	farming = 62
  }
}

town =
{
  id = { type = 1 id = 7 }
  name = "Niihama"
  x = 1159
  y = 1887
  fort = 0
  castellan = 7
  maxfort = 3
  province = 12
  roads = { 4 8 }


  income = {
	farming = 54
  }
}

town =
{
  id = { type = 1 id = 8 }
  name = "Marugame"
  x = 1272
  y = 1820
  fort = 0
  maxfort = 3
  province = 14
  roads = { 5 7 }


  income = {
	farming = 40
  }
}

town =
{
  id = { type = 1 id = 9 }
  name = "Kaseda"
  x = 387
  y = 2460
  fort = 0
  castellan = 9
  maxfort = 3
  province = 1


  income = {
	farming = 70
  }
}

town =
{
  id = { type = 1 id = 10 }
  name = "Asakura"
  x = 1145
  y = 1989
  fort = 1
  castellan = 8
  maxfort = 3
  province = 4

  income = {
	farming = 30
  }
}

