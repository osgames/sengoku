-- Actions.lua

-- This code is published under the General Public License.

-- Contains actions for GUI events.

------------------Includes:--------------------------

dofile("Scenarios\\Common\\Clans.lua")
dofile("Scripts\\Actions\\MenuActions.lua")
dofile("Scripts\\Actions\\ClientActions.lua")
dofile("Scripts\\Actions\\GameWindowActions.lua")
dofile("Scripts\\Actions\\MapActions.lua")

-------------Global "constants": --------------------

mouseButtonLeft = 1
mouseButtonMiddle = 2
mouseButtonRight = 3

--------------Global variables: ---------------------

scenarioFilename = ""
preloadClans = { }
players = { }
multiplayer = false --False = singleplayer, true = multiplayer. Needed for certain callbacks to know state of UI.
loading = false
isPlaying = false

-------------End of global vars----------------------

