--ClientActions.lua

-- This code is published under the General Public License.

function clientUserAdded(game, gui, id, name)
        if multiplayer then
                gui:selectWindow("lobby_menu/player_list")
        else
                gui:selectWindow("singleplayer_menu/player_list")
        end
        
        gui:addTextItem(name)
        -- Unfortunately we must index by tostring(id) instead of id because if we use integer
        -- keys table.remove will fuck up the id:s later.
        players[tostring(id)] = { ["name"] = name, clan = "", index = gui.itemCount }
end

function clientUserRemoved(game, gui, id)
        if multiplayer then
                selectClan(gui, game, id, "", "lobby_menu/player_list", "lobby_menu/clan_list")
        else
                selectClan(gui, game, id, "", "singleplayer_menu/player_list", "singleplayer_menu/clan_list")
        end
        
        local index = players[tostring(id)].index
        table.remove(players, tostring(id))
        
        if multiplayer then
                gui:selectWindow("lobby_menu/player_list")
        else
                gui:selectWindow("singleplayer_menu/player_list")
        end
        
        gui:removeItems(index, index)
        
        -- Adjust other players' indices:
        for i, v in pairs(players) do
                if v.index > index then
                        v.index = v.index - 1
                end
        end
end

function clientConnectionFailed(game, gui)
        game:disconnect()
        if multiplayer then
                showScreen(gui, "network_menu")
        else
                showScreen(gui, "main_menu")
        end
        isPlaying = false
end

function clientConnected(game, gui)
        if multiplayer then
                showScreen(gui, "lobby_menu")
        end
end

function clientDisconnected(game, gui)
        game:disconnect()
        if multiplayer then
                showScreen(gui, "network_menu")
        else
                showScreen(gui, "main_menu")
        end
        isPlaying = false
end

function clientChatMessage(game, gui, fromUser, message)
        if isPlaying then
                gameChatMessage(gui, game, fromUser, message)
        else
                lobbyChatMessage(gui, game, fromUser, message)
        end
end

function clientScenarioChange(game, gui, name)
        if multiplayer then
                gui:selectWindow("lobby_menu/scenario_name")
                gui:setText(name)
                selectScenario(gui, game, name, "lobby_menu/clan_list");
        else
                gui:selectWindow("singleplayer_menu/scenario_name")
                gui:setText(name)
                selectScenario(gui, game, name, "singleplayer_menu/clan_list");
        end
end

function clientClanChange(game, gui, user, tag)
        if multiplayer then
                selectClan(gui, game, user, tag, "lobby_menu/player_list", "lobby_menu/clan_list")
        else
                selectClan(gui, game, user, tag, "singleplayer_menu/player_list",
                        "singleplayer_menu/clan_list")
        end
end

function clientStartPlay(game, gui)
        loading = true
        if game:loadScenario(scenarioFilename) then
                isPlaying = true
                showScreen(gui, "game_window")
        else
                print("Couldn't load scenario!") -- Change to something more informative.
        end
        loading = false
end
