--GameWindowActions.lua

-- This code is published under the General Public License.

function action_gameWindowOnClick(gui, game, x, y, button)
        
end

function action_gameWindowShown(gui, game, visible)
        if visible then
                gui:selectWindow("game_window")
                gui:setAsKeyhandler()
        else
                gui:removeKeyhandler()
        end
end

function action_gameWindowKeyPress(gui, game, keycode, charcode)
        if multiplayer and charcode == 9 then --tab
                gui:selectWindow("game_window/chat_box")
                local vis = not gui:isVisible()
                gui:setVisible(vis)
                gui:setText("")
                gui:selectWindow("game_window/chat_selection_box")
                gui:setVisible(vis)
        end
end

function gameChatMessage(gui, game, fromUser, message)
        gui:selectWindow("game_window/chat_list")
        gui:addTextItem(players[tostring(fromUser)].name .. ": " .. message)
        table.insert(chatDurations, maxChatDuration)
        if gui.itemCount > gui:calculateVisibleItems() then
                gui:removeItems(1, 1) -- Means the first row will be removed.
                table.remove(chatDurations, 1)
        end
end

function action_gameSendChatMessage(gui, game, text)
        if multiplayer then
                game:chatPublic(text)
                gui:selectWindow("game_window/chat_box")
                gui:setVisible(false)
                gui:selectWindow("game_window/chat_selection_box")
                gui:setVisible(false)
        end
end
