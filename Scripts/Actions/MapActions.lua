--MapActions.lua

-- This code is published under the General Public License.

function changePalette(game, gui, palette, province)
        gui:selectWindow("game_window/map")
        gui:setPalette(palette, province)
end

function action_minimapClick(gui, game, x, y, button)
        if button == mouseButtonLeft then
                -- Get parameters:
                gui:selectWindow("game_window/mini_map")
                local miniWidth = gui:getWidth()
                local miniHeight = gui:getHeight()
                gui:selectWindow("game_window/map")
                local realWidth = gui:getMapWidth()
                local realHeight = gui:getMapHeight()
                local viewedWidth = gui:getWidth()
                local viewedHeight = gui:getHeight()
                
                gui:setMapScrolling((x / miniWidth) * realWidth - (viewedWidth / 2),
                        (y / miniHeight) * realHeight - (viewedHeight / 2))
        end
end

function action_mainmapSpriteClick(gui, game, sprite, button)
end

function mapClickOnArmy(gui, game, id, button)
end

function mapClickOnTown(gui, game, id, button)
end
