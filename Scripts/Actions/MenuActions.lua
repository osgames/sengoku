--MenuActions.lua

-- This code is published under the General Public License.

function isClanSelected(clan)
        for k, v in pairs(players) do
                if v.clan == clan then
                        return true
                end
        end
        
        return false
end

function showScreen(gui, name)
	gui:hideScreens()
	gui:selectWindow(name)
	gui:setVisible(true)
end

function selectScenario(gui, game, name, clan_list)
        -- Save scenario name:
	scenarioFilename = "Scenarios\\" .. name .. ".lua"
	
        -- Attempt to preload scenario:
	if not game:preloadScenario(scenarioFilename) then
		-- Todo: Change so that this notice appears in GUI instead.
		print("Scenario '" .. scenarioFilename .. "' not found.")
		return
	end

	-- Save preload clans in our own global table copy:
	for i = 1,game.preloadClanCount do
		table.insert(preloadClans, { tag = game:getPreloadClanTag(i) })
	end
	
	-- Sort preload clans in alphabetic order:
	table.sort(preloadClans, function(a, b) return clans[a.tag].name < clans[b.tag].name end)
	
	-- Add preload clans to list of selectable clans:
        gui:selectWindow(clan_list)
        gui:clearItems()
        
	for i = 1,table.getn(preloadClans) do
	       gui:addTextItem(clans[preloadClans[i].tag].name)
	end
end

function selectClan(gui, game, user, tag, player_list, clan_list)
        function setClan(playerName, clanTag)
                -- Find new index (if we need one):
                if clanTag ~= "" then
                        local index = 0
                        for i, v in ipairs(preloadClans) do
                                if v.tag == clanTag then
                                        index = i
                                        break
                                end
                        end
                        if index >= 1 then
			     if playerName == nil then
				    gui:setTextItem(clans[clanTag].name, index)
			     else
				    gui:setTextItem(clans[clanTag].name .. " - " .. playerName, index)
			     end
                        end
                end
	end
	
	gui:selectWindow(clan_list)

	-- Update old clan in clan list:
        if players[tostring(user)].clan ~= "" then
                setClan(nil, players[tostring(user)].clan)
	end

	-- Update new clan in clan list:
        if tag ~= "" then
                setClan(players[tostring(user)].name, tag)
	end
	
	-- Update player list:
        gui:selectWindow(player_list)
        if tag == "" then
                gui:setTextItem(players[tostring(user)].name, players[tostring(user)].index)
        else
                gui:setTextItem(players[tostring(user)].name ..  " - " ..
                        clans[tag].name, players[tostring(user)].index)
        end
	
	-- Set player's clan:
	players[tostring(user)].clan = tag
end

function action_singleplayerMenuShown(gui, game, visible)
        if visible then
                players = { }
                preloadClans = { }
                gui:selectWindow("singleplayer_menu/scenario_name")
                gui:setText("1545")
                gui:selectWindow("singleplayer_menu/player_list")
                gui:clearItems()
                gui:selectWindow("singleplayer_menu/clan_list")
                gui:clearItems()
        end
end

function action_lobbyMenuShown(gui, game, visible)
        if visible then
                players = { }
                preloadClans = { }
                gui:selectWindow("lobby_menu/scenario_name")
                if hosting then
                        gui:setText("1545")
                else
                        gui:setText("")
                end
                gui:selectWindow("lobby_menu/player_list")
                gui:clearItems()
                gui:selectWindow("lobby_menu/clan_list")
                gui:clearItems()
                gui:selectWindow("lobby_menu/chat_list")
                gui:clearItems()
                gui:selectWindow("lobby_menu/chat_box")
                gui:setText("")
        end
end

function action_showSingleplayerMenu(gui, game, x, y, button)
        if game:initSingleServerAndClient() then
                hosting = true
                multiplayer = false
                showScreen(gui, "singleplayer_menu")
        else
                print("More refined error messaged here perhaps?")
        end
end

function action_showMainMenu(gui, game, x, y, button)
	showScreen(gui, "main_menu")
end

function action_showNetworkMenu(gui, game, x, y, button)
	showScreen(gui, "network_menu")
end

function action_quit(gui, game, x, y, button)
	gui:quitASAP()
end

function action_startPlaying(gui, game, x, y, button)
        if scenarioFilename ~= "" then
                game:startPlaying()
        end
end

function action_selectSingleplayerScenario(gui, game, x, y, button)
	gui:selectWindow("singleplayer_menu/scenario_name")
        game:selectScenario(gui:getText())
end

function action_scrollSingleplayerClansUp(gui, game, x, y, button)
	gui:selectWindow("singleplayer_menu/clan_list")
	gui:scrollItems(-10)
end

function action_scrollSingleplayerClansDown(gui, game, x, y, button)
	gui:selectWindow("singleplayer_menu/clan_list")
	gui:scrollItems(10)
end

function action_selectSingleplayerClan(gui, game, index)
        if players["1"].clan == preloadClans[index].tag then
                game:selectClan("")
        elseif not isClanSelected(preloadClans[index].tag) then
                game:selectClan(preloadClans[index].tag)
	end
end

function action_goHost(gui, game, x, y, button)
        --Get parameters:
        gui:selectWindow("network_menu/username")
        local username = gui:getText()
        gui:selectWindow("network_menu/password")
        local password = gui:getText()
        gui:selectWindow("network_menu/host_port")
        local port = gui:getText()
        
        -- Attempt to host if parameters are (somewhat) valid:
        if username ~= "" and port ~= "" then
                if game:initMultiServerAndClient(port, password, username) then
                        hosting = true
                        multiplayer = true
                        showScreen(gui, "connectionwait_menu")
                else
                        print("a more refined error message here perhaps?")
                end
        end
end

function action_goConnect(gui, game, x, y, button)
        --Get parameters:
        gui:selectWindow("network_menu/username")
        local username = gui:getText()
        gui:selectWindow("network_menu/password")
        local password = gui:getText()
        gui:selectWindow("network_menu/connect_ip")
        local ip = gui:getText()
        gui:selectWindow("network_menu/connect_port")
        local port = gui:getText()
        
        -- Attempt to connect if parameters are (somewhat) valid:
        if username ~= "" and port ~= "" and ip ~= "" then
                if game:initConnectedClient(ip, port, password, username) then
                        hosting = false
                        multiplayer = true
                        showScreen(gui, "connectionwait_menu")
                else
                        print("a more refined error message here perhaps?")
                end
        end
end

function action_scrollLobbyPlayersUp(gui, game, x, y, button)
        gui:selectWindow("lobby_menu/player_list")
        gui:scrollItems(-2)
end

function action_scrollLobbyPlayersDown(gui, game, x, y, button)
        gui:selectWindow("lobby_menu/player_list")
        gui:scrollItems(-2)
end

function action_scrollLobbyClansUp(gui, game, x, y, button)
	gui:selectWindow("lobby_menu/clan_list")
	gui:scrollItems(-10)
end

function action_scrollLobbyClansDown(gui, game, x, y, button)
	gui:selectWindow("lobby_menu/clan_list")
	gui:scrollItems(10)
end

function action_selectLobbyClan(gui, game, index)
        if players["1"].clan == preloadClans[index].tag then
                game:selectClan("")
        elseif not isClanSelected(preloadClans[index].tag) then
                game:selectClan(preloadClans[index].tag)
	end
end

function action_lobbySendChatMessage(gui, game, x, y, button)
        gui:selectWindow("lobby_menu/chat_box")
        if gui:getText() ~= "" then
                game:chatPublic(gui:getText())
                gui:setText("")
        end
end

function action_lobbySendChatMessage2(gui, game, text)
        game:chatPublic(text)
        -- Box automatically cleared.
end

function lobbyChatMessage(gui, game, fromUser, message)
        --Can only happen in multiplayer.
        gui:selectWindow("lobby_menu/chat_list")
        gui:addTextItem(players[tostring(fromUser)].name .. ": " .. message)
        if gui.itemCount > gui:calculateVisibleItems() then
                gui:removeItems(1, 1) -- Means the first row will be removed.
        end
end

function action_selectLobbyScenario(gui, game, x, y, button)
        if hosting then
                gui:selectWindow("lobby_menu/scenario_name")
                game:selectScenario(gui:getText())
        end
end
