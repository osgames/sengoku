-- Game.lua

-- This code is published under the General Public License.

-- Contains scripts that will be called during life time of game.

---------------------------------- Global "constants":----------------------------------------

mapModeNothing = 0
mapModeProvinces = 1
mapModeDebugShading = 2
mapModeTerrain = 3

scrollSpeed = { x = 600, y = 600 }

dayLengths = { 0.125, 0.25, 0.5, 1.0, 2.0, 4.0 } -- How long a day takes in seconds.
maxDayLength = 10.0
monthNames = { "January", "February", "March", "April", "May", "June", "July", "August",
        "September", "October", "November", "December" }

maxChatDuration = 5

----------------------------------- Global variables:-----------------------------------------

hosting = false -- Used to easily remember whether we're the server in multiplayer. True for singleplayer.
armies = { } -- Army info we need to remember.
timeSinceLastDay = 0
gameSpeed = 4 -- Used to index dayLengths.
chatDurations = { }

---------------------------------- Utility functions: ----------------------------------------

function updateDate(gui, world)
        gui:selectWindow("game_window/title_bar/date")
        gui:setTextItem(world.day .. " " .. monthNames[world.month] .. " " .. world.year, 1)
end

----------------------------------- Game functions: ------------------------------------------

-- Game scripts called when scenario finishes loading.
function onLoadScenario(game, gui, world)
        -- Arguments:
        -- game: CGame object.
        -- gui: CGUI object.
        
        mapSelection = { type = "", id = 0 }
        timeSinceLastDay = 0
        chatDurations = { }
        
	gui:selectWindow("game_window/map")
	gui:setMapScrolling(1600, 1400)
	gui:setMapMode(mapModeProvinces)
	gui:forceRedraw()
	updateDate(gui, world)
end

-- Game update script only called with a loaded scenario, called when internal game updates have finished.
function onUpdate(game, gui, dt)
        -- Arguments:
        -- game: CGame object.
        -- gui: CGUI object.
        -- dt: Delta time, time since last update.
	
	-- Scroll map if cursor is in appropiate locations:
        gui:selectWindow("game_window/map")
	if gui.cursorX <= 0 then
	       gui:scrollMap(-scrollSpeed.x * dt, 0)
	end -- Do not elseif this because it's possible the cursor is in the left, top corner for instance.
	if gui.cursorY <= 0 then
	       gui:scrollMap(0, -scrollSpeed.y * dt)
	end
	if gui.cursorX >= gui.screenWidth - 1 then
	       gui:scrollMap(scrollSpeed.x * dt, 0)
	end
	if gui.cursorY >= gui.screenHeight - 1 then
	       gui:scrollMap(0, scrollSpeed.y * dt)
	end
	
	if hosting and game:isGreen() then
                if timeSinceLastDay > dayLengths[gameSpeed] then
	               timeSinceLastDay = timeSinceLastDay - dayLengths[gameSpeed]
	               game:proposeNextDay()
	        end
	        
	        timeSinceLastDay = timeSinceLastDay + dt
	        
	        if timeSinceLastDay > maxDayLength then
	               timeSinceLastDay = maxDayLength
	        end
	end
	
	if mapSelection.changedPos then
	       gui:selectWindow("game_window/map")
	       moveMapSelectionSprites(gui)
	       mapSelection.changedPos = false
	end
	
	if isPlaying then
	       table.foreachi(chatDurations, function(i, v) chatDurations[i] = v - dt end)
	       
	       gui:selectWindow("game_window/chat_list")
	       while chatDurations[1] ~= nil and chatDurations[1] < 0 do
	               gui:removeItems(1, 1) -- Means the first row will be removed.
	                table.remove(chatDurations, 1)
	       end
	end
end

function onNewDay(game, gui, world)
	updateDate(gui, world)
end

function calcLimitedAngle(x, y, add) --Do not add more than [-pi, pi]
        if x == 0 and y == 0 then
                return nil
        end
        
        local result = math.atan2(y, x) + add
        if result < 0 then
                return result + 2 * math.pi
        else
                return result
        end
end
