-- Interface.lua

-- This code is published under the General Public License.

-- Called by CGUI to build interface for menu and game.

function buildInterface(gui)
	-- "gui" is a reference to a CGUI object.

	if(gui.screenWidth == 1024 and gui.screenHeight == 768) then
		dofile("Scripts\\Interface\\Menu_1024x768.lua")
	else
		print("Unsupported screen resolution.")
		return
	end

	-- Main menu:
        
	gui:createWindow("main_menu", "CWindow", {})
	gui:setBitmap("background", "Graphics\\Menu\\Main_1024x768.bmp", { })
	
	gui:createWindow("main_menu/singleplayer", "CWindow", {})
	gui:setArea(unpack(main_menu_singleplayer.area))
	gui:registerAction("onClick", "action_showSingleplayerMenu")
	
	gui:createWindow("main_menu/multiplayer", "CWindow", {})
	gui:setArea(unpack(main_menu_multiplayer.area))
	gui:registerAction("onClick", "action_showNetworkMenu")
	
	gui:createWindow("main_menu/quit", "CWindow", {})
	gui:setArea(unpack(main_menu_quit.area))
	gui:registerAction("onClick", "action_quit")

	-- Singleplayer menu:
        
	gui:createWindow("singleplayer_menu", "CWindow", { visible = false })
	gui:setBitmap("background", "Graphics\\Menu\\Singleplayer_1024x768.bmp", { })
	gui:registerAction("shown", "action_singleplayerMenuShown")
	
	gui:createWindow("singleplayer_menu/player_list", "CMultilineLabel", {})
	gui:setArea(unpack(singleplayer_menu_player_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("singleplayer_menu/scenario_name", "CTextBox", {})
	gui:setArea(unpack(singleplayer_menu_scenario_name.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("singleplayer_menu/cancel", "CWindow", {})
	gui:setArea(unpack(singleplayer_menu_cancel.area))
	gui:registerAction("onClick", "action_showMainMenu")
	
	gui:createWindow("singleplayer_menu/select", "CWindow", {})
	gui:setArea(unpack(singleplayer_menu_select.area))
	gui:registerAction("onClick", "action_selectSingleplayerScenario")
	
	gui:createWindow("singleplayer_menu/clan_list", "CMultilineLabel", {})
	gui:setArea(unpack(singleplayer_menu_clan_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	gui:registerAction("onSelect", "action_selectSingleplayerClan")
	
	gui:createWindow("singleplayer_menu/clan_up", "CWindow", {})
	gui:setArea(unpack(singleplayer_menu_clan_up.area))
	gui:registerAction("onClick", "action_scrollSingleplayerClansUp")
	
	gui:createWindow("singleplayer_menu/clan_down", "CWindow", {})
	gui:setArea(unpack(singleplayer_menu_clan_down.area))
	gui:registerAction("onClick", "action_scrollSingleplayerClansDown")
	
	gui:createWindow("singleplayer_menu/start", "CWindow", {})
	gui:setArea(unpack(singleplayer_menu_start.area))
	gui:registerAction("onClick", "action_startPlaying")

	-- Network menu (multiplayer menu 1):
        
	gui:createWindow("network_menu", "CWindow", { visible = false })
	gui:setBitmap("background", "Graphics\\Menu\\Multiplayer1_1024x768.bmp", { })
	
	gui:createWindow("network_menu/cancel", "CWindow", {})
	gui:setArea(unpack(network_menu_cancel.area))
	gui:registerAction("onClick", "action_showMainMenu")
	
	gui:createWindow("network_menu/username", "CTextBox", { maxLength = 31 })
	gui:setArea(unpack(network_menu_username.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("network_menu/password", "CTextBox", { maxLength = 31 })
	gui:setArea(unpack(network_menu_password.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("network_menu/host_port", "CTextBox", {})
	gui:setArea(unpack(network_menu_host_port.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("network_menu/host_go", "CWindow", {})
	gui:setArea(unpack(network_menu_host_go.area))
	gui:registerAction("onClick", "action_goHost")
	
	gui:createWindow("network_menu/connect_ip", "CTextBox", {})
	gui:setArea(unpack(network_menu_connect_ip.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("network_menu/connect_port", "CTextBox", {})
	gui:setArea(unpack(network_menu_connect_port.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("network_menu/connect_go", "CWindow", {})
	gui:setArea(unpack(network_menu_connect_go.area))
	gui:registerAction("onClick", "action_goConnect")

	-- Lobby (multiplayer menu 2):
        
	gui:createWindow("lobby_menu", "CWindow", { visible = false })
	gui:setBitmap("background", "Graphics\\Menu\\Multiplayer2_1024x768.bmp", { })
	gui:registerAction("shown", "action_lobbyMenuShown")
	
	gui:createWindow("lobby_menu/player_list", "CMultilineLabel", {})
	gui:setArea(unpack(lobby_menu_player_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	-- Add select action here for player list.
	
	gui:createWindow("lobby_menu/player_up", "CWindow", {})
	gui:setArea(unpack(lobby_menu_player_up.area))
	gui:registerAction("onClick", "action_scrollLobbyPlayersUp")
	
	gui:createWindow("lobby_menu/player_down", "CWindow", {})
	gui:setArea(unpack(lobby_menu_player_down.area))
	gui:registerAction("onClick", "action_scrollLobbyPlayersDown")
	
	gui:createWindow("lobby_menu/chat_box", "CTextBox", { maxLength = 32, clearOnEnter = true })
	gui:setArea(unpack(lobby_menu_chat_box.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	gui:registerAction("onEnter", "action_lobbySendChatMessage2")
	
	gui:createWindow("lobby_menu/chat_send", "CWindow", {})
	gui:setArea(unpack(lobby_menu_chat_send.area))
	gui:registerAction("onClick", "action_lobbySendChatMessage")
	
	gui:createWindow("lobby_menu/chat_list", "CMultilineLabel", { limitWidth = true })
	gui:setArea(unpack(lobby_menu_chat_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("lobby_menu/scenario_name", "CTextBox", {})
	gui:setArea(unpack(lobby_menu_scenario_name.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	
	gui:createWindow("lobby_menu/select", "CWindow", {})
	gui:setArea(unpack(lobby_menu_select.area))
	gui:registerAction("onClick", "action_selectLobbyScenario")
	
	gui:createWindow("lobby_menu/clan_list", "CMultilineLabel", {})
	gui:setArea(unpack(lobby_menu_clan_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	gui:registerAction("onSelect", "action_selectLobbyClan")
	
	gui:createWindow("lobby_menu/clan_up", "CWindow", {})
	gui:setArea(unpack(lobby_menu_clan_up.area))
	gui:registerAction("onClick", "action_scrollLobbyClansUp")
	
	gui:createWindow("lobby_menu/clan_down", "CWindow", {})
	gui:setArea(unpack(lobby_menu_clan_down.area))
	gui:registerAction("onClick", "action_scrollLobbyClansDown")
	
	gui:createWindow("lobby_menu/start", "CWindow", {})
	gui:setArea(unpack(lobby_menu_start.area))
	gui:registerAction("onClick", "action_startPlaying")
	
	-- Waiting for connection screen (multiplayer menu 3):
        
        gui:createWindow("connectionwait_menu", "CWindow", { visible = false })
        gui:setBitmap("background", "Graphics\\Menu\\Multiplayer3_1024x768.bmp", { })
	
	-- Game window:
        
        gui:createWindow("game_window", "CWindow", { visible = false })
        gui:setArea(0, 0, gui.screenWidth, gui.screenHeight)
        gui:registerAction("shown", "action_gameWindowShown")
        gui:registerAction("keyPress", "action_gameWindowKeyPress")
        gui:registerAction("onClick", "action_gameWindowOnClick")
        
        gui:createWindow("game_window/map", "CProvinceMap", { 
                mapFile = "Scenarios\\Common\\Map.lua",
                initialArea = game_window_map.area })
        gui:setBitmap("background", "Graphics\\Provinces\\Background.bmp", { })
        gui:registerAction("spriteClick", "action_mainmapSpriteClick")
        
        gui:createWindow("game_window/title_bar", "CWindow", { })
        gui:setBitmap("background", "Graphics\\GameUI\\TitleBar_1024.bmp", { })
        gui:setArea(unpack(game_window_title_bar.area))
        
        gui:createWindow("game_window/title_bar/date", "CMultilineLabel",
                { halign = "center" })
        gui:setArea(unpack(game_window_title_bar_date.area))
        gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
        gui:addTextItem("No date")
        
        gui:createWindow("game_window/command_menu", "CWindow", { })
        gui:setBitmap("background", "Graphics\\GameUI\\CommandBackground_768.bmp", { })
        gui:setArea(unpack(game_window_command_menu.area))
        
        gui:createWindow("game_window/mini_map", "CWindow", { })
        gui:setBitmap("background", "Graphics\\GameUI\\Minimap.bmp", { })
        gui:setArea(unpack(game_window_mini_map.area))
        gui:registerAction("onClick", "action_minimapClick")
        
        gui:createWindow("game_window/chat_box", "CTextBox", { visible = false, maxLength = 127, clearOnEnter = true })
        gui:setBitmap("background", "Graphics\\GameUI\\ChatBox.bmp", { alphaBlend = true })
        gui:setArea(unpack(game_window_chat_box.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
	gui:registerAction("onEnter", "action_gameSendChatMessage")
	
	gui:createWindow("game_window/chat_selection_box", "CWindow", { visible = false })
	gui:setBitmap("background", "Graphics\\GameUI\\ChatSelectionBox.bmp", { alphaBlend = true })
	gui:setArea(unpack(game_window_chat_selection_box.area))
	
	gui:createWindow("game_window/chat_list", "CMultilineLabel", { clickable = false, limitWidth = true })
	gui:setArea(unpack(game_window_chat_list.area))
	gui:setFont("text", "Graphics\\Fonts\\MenuSmall")
end
