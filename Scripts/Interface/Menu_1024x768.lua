-- This code is published under the General Public License.

-- Specific properties for 1024x768 resolution.

main_menu_singleplayer = { area = { 493, 223, 601, 240 } }
main_menu_multiplayer = { area = { 447, 559, 468, 664 } }
main_menu_quit = { area = { 179, 674, 216, 692 } }

singleplayer_menu_player_list = { area = { 37, 218, 247, 694 } }
singleplayer_menu_scenario_name = { area = { 384, 219, 567, 242 } }
singleplayer_menu_cancel = { area = { 934, 721, 977, 736 } }
singleplayer_menu_select = { area = { 578, 226, 655, 243 } }
singleplayer_menu_clan_list = { area = { 713, 218, 922, 694 } }
singleplayer_menu_clan_up = { area = { 924, 227, 973, 281 } }
singleplayer_menu_clan_down = { area = { 924, 627, 973, 681 } }
singleplayer_menu_start = { area = { 49, 721, 155, 736 } }

network_menu_cancel = { area = { 144, 627, 205, 641 } }
network_menu_username = { area = { 818, 223, 904, 247 } }
network_menu_password = { area = { 814, 325, 900, 349 } }
network_menu_host_port = { area = { 716, 552, 802, 576 } }
network_menu_host_go = { area = { 812, 554, 843, 568 } }
network_menu_connect_ip = { area = { 158, 224, 321, 248 } }
network_menu_connect_port = { area = { 332, 224, 418, 248 } }
network_menu_connect_go = { area = { 429, 226, 460, 240 } }

lobby_menu_player_list = { area = { 37, 218, 247, 694 } }
lobby_menu_player_up = { area = { 249, 227, 298, 281 } }
lobby_menu_player_down = { area = { 249, 627, 298, 681 } }
lobby_menu_chat_box = { area = { 383, 291, 566, 314 } }
lobby_menu_chat_send = { area = { 578, 298, 623, 313 } }
lobby_menu_chat_list = { area = { 344, 335, 677, 694 } }
lobby_menu_scenario_name = { area = { 384, 219, 567, 242 } }
lobby_menu_select = { area = { 578, 226, 655, 243 } }
lobby_menu_clan_list = { area = { 713, 218, 922, 694 } }
lobby_menu_clan_up = { area = { 924, 227, 973, 281 } }
lobby_menu_clan_down = { area = { 924, 627, 973, 681 } }
lobby_menu_start = { area = { 49, 721, 155, 736 } }

game_window_title_bar = { area = { 0, 0, 768, 24 } }
game_window_title_bar_date = { area = { 384, 3, 384, 3 } }
game_window_map = { area = { 0, 24, 768, 768 } }
game_window_command_menu = { area = { 768, 0, 1024, 768 } }
game_window_mini_map = { area = { 777, 561, 1015, 761 } }
game_window_chat_box = { area = { 4, 28, 500, 52 } }
game_window_chat_selection_box = { area = { 508, 28, 764, 412 } }
game_window_chat_list = { area = { 4, 56, 500, 312 } }
