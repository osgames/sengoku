//b2.h

#ifndef BORDERIZER2_H
#define BORDERIZER2_H

#define APP_TITLE       "Borderizer 2.0"

#include <string>
#include <list>
#include <vector>
#include <SDL.h>
#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_core\r2_log.h"

#define BSOPTYPE_PROVGENERATION         1

//these two pixel modification routines assumes the surfaces are locked
u8 getPixel8bit(const SDL_Surface* surface, u32 x, u32 y);
void putPixel8bit(SDL_Surface* surface, u32 x, u32 y, u8 pixel);

class CBorderizer
{
public: //types
        struct COperation
        {
        protected: //attributes
                u8 opType;
                bool success;
        public:
                bool succeeded() const {return success;}
                virtual void commit() = 0;
        };
        struct CProvGeneration : public COperation
        {
        protected: //attributes
                std::string defBitmap;
                std::string outputDir;
                struct {
                        std::vector<std::string> bitmaps;
                        std::vector<Si32_rect> src;
                        std::vector<Si32_point> dest;
                } shading;
                std::list<Si32_point> startPoints;
                
                u8 minColor;
                u8 maxColor;
                u16 width;
                
                bool emitBMP;
        protected: //methods
                SDL_Surface* generateProvince(const Si32_point& start, Si32_rect& limits,
                        std::map<std::string, SDL_Surface*>& bitmaps, f32* distances,
                        const SDL_Color palette[], SDL_Surface * const * shadingIndices);
                static bool loadBitmapIfNecessary(std::map<std::string, SDL_Surface*>& bitmaps,
                        const std::string& filename);
                u8 CBorderizer::CProvGeneration::getShadingValueAtPoint(u32 x, u32 y,
                        SDL_Surface * const * shadingIndices);
                static void unloadBitmaps(std::map<std::string, SDL_Surface*>& bitmaps);
        public: //methods
                CProvGeneration(luabind::object table);
                void commit();
        };
private: //attributes
        std::list<COperation*> ops;
        bool success;
private: //methods
        bool init(const std::string& filename);
public: //methods
        CBorderizer(const std::string& filename) {success = init(filename);}
        ~CBorderizer();
        bool succeeded() const {return success;}
        void commit();
};

#endif //!BORDERIZER2_H
