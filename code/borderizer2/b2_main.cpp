//b2_main.cpp

//Written by Petter Hansson

//Major improvement from the original Borderizer program.

//Win32 library headers
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <cstdlib>
#include <iostream>
#include "b2.h"

//the reason why I must use WinMain instead of main is to prevent SDL from eating up the console window
INT WINAPI WinMain(HINSTANCE thisInst, HINSTANCE prevInst, LPSTR argv,
    INT show)
{
        CLogStreams::open();
        
        std::cout << APP_TITLE << std::endl;
        
        if(SDL_Init(SDL_INIT_VIDEO) != 0) {
                return EXIT_FAILURE;
        }
        
        //initialize settings from command line filename if available, or manually entered filename
        CBorderizer* program;
        if(strlen(argv) > 0) {
                program = new CBorderizer(argv);
        } else {
                std::string filename;
                std::cout << "Enter filename: ";
                std::cin >> filename;
                program = new CBorderizer(filename);
        }
        if(program == 0) {
                SDL_Quit();
                return EXIT_FAILURE;
        }
        if(!program->succeeded()) {
                delete program;
                SDL_Quit();
                return EXIT_FAILURE;
        }
        
        //perform operations
        program->commit();
        
        //clean up
        delete program;
        SDL_Quit();
        
        CLogStreams::close();
        
        return EXIT_SUCCESS;
}

u8 getPixel8bit(const SDL_Surface* surface, u32 x, u32 y)
{
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x;

    return *p;
}

void putPixel8bit(SDL_Surface* surface, u32 x, u32 y, u8 pixel)
{
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x;

    *p = pixel;
}


