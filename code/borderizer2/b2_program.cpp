//b2_program.cpp

#include "b2.h"

bool CBorderizer::init(const std::string& filename)
{
        using namespace luabind;
        r2::CLua lua;
        
        if(!lua.doFile(filename)) {
                LOG_ERROR("Couldn't read instruction file, quitting");
                return false;
        }
        
        ASSERT(lua.state() != 0);
        
        object operations = globals(lua.state())["operations"];
        if(operations && type(operations) == LUA_TTABLE) {
                for(iterator i(operations), end; i != end; ++i) {
                        if(type(*i) == LUA_TTABLE && type((*i)["type"]) == LUA_TSTRING) {
                                COperation* op = 0;
                                std::string opType = object_cast<std::string>((*i)["type"]);
                                if(opType == "provinceGeneration") {
                                        op = new CProvGeneration(*i);
                                } else {
                                        LOG_WARNING("Unknown operation '" << opType << "'");
                                }
                                if(op != 0) {
                                        if(op->succeeded()) {
                                                ops.push_back(op);
                                        } else {
                                                delete op;
                                        }
                                }
                        }
                }
        }
        
        return true;
}

CBorderizer::~CBorderizer()
{
        for(std::list<COperation*>::const_iterator i = ops.begin(); i != ops.end(); ++i) {
                ASSERT(*i != 0);
                delete *i;
        }
}

void CBorderizer::commit()
{
        for(std::list<COperation*>::const_iterator i = ops.begin(); i != ops.end(); ++i) {
                ASSERT(*i != 0);
                (*i)->commit();
        }
}
