//b2_provgen.cpp

#include <boost/lexical_cast.hpp>
#include <map>
#include <algorithm>
#include <iostream>
#include <cfloat>
#include <cmath>
#include <fstream>
#include <SDL_image.h>
#include "b2.h"

std::string toLowercase(const std::string& str);

CBorderizer::CProvGeneration::CProvGeneration(luabind::object table)
{
        using namespace luabind;
        
        success = false;
        opType = BSOPTYPE_PROVGENERATION;
        
        if(table && type(table) == LUA_TTABLE) {
                if(type(table["definitionBitmap"]) == LUA_TSTRING) {
                        defBitmap = toLowercase(object_cast<std::string>(table["definitionBitmap"]));
                } else {
                        LOG_ERROR("No definition bitmap; province generation operation will be ignored");
                        return;
                }
                        
                outputDir = type(table["outputDirectory"]) == LUA_TSTRING ? object_cast<std::string>
                        (table["outputDirectory"]) : "";
                if(outputDir != "" && *outputDir.rbegin() != '\\' && *outputDir.rbegin() != '/') {
                        outputDir += "\\";
                }
                
                emitBMP = type(table["emitBMP"]) == LUA_TBOOLEAN ? object_cast<bool>(table["emitBMP"])
                        : false;
                
                object borders(table["borders"]);
                if(borders && type(borders) == LUA_TTABLE) {
                        minColor = type(borders["minColor"]) == LUA_TNUMBER ?
                                object_cast<u8>(borders["minColor"]) : 1;
                        maxColor = type(borders["maxColor"]) == LUA_TNUMBER ?
                                object_cast<u8>(borders["maxColor"]) : 254;
                        width = type(borders["width"]) == LUA_TNUMBER ?
                                object_cast<u16>(borders["width"]) : 16;
                }
                
                if(type(table["shading"]) == LUA_TTABLE) {
                        for(iterator i(table["shading"]), end; i != end; ++i) {
                                if(type(*i) == LUA_TTABLE && type((*i)["bitmap"]) == LUA_TSTRING) {
                                        shading.bitmaps.push_back(toLowercase(object_cast<std::string>
                                                ((*i)["bitmap"])));
                                        Si32_rect src;
                                        if(r2::CLua::readRectangle((*i)["source"], src)) {
                                                shading.src.push_back(src);
                                        }
                                        Si32_point dest;
                                        if(r2::CLua::readPoint((*i)["destination"], dest)) {
                                                shading.dest.push_back(dest);
                                        }
                                }
                        }
                }
                
                if(type(table["startPoints"]) == LUA_TTABLE) {
                        for(iterator i(table["startPoints"]), end; i != end; ++i) {
                                Si32_point p;
                                if(r2::CLua::readPoint(*i, p)) {
                                        startPoints.push_back(p);
                                }
                        }
                }
                
        } else {
                return;
        }
        
        if(emitBMP) { //if more emission types are added, OR them here
                success = true;
        }
}

void CBorderizer::CProvGeneration::commit()
{
        //start by loading bitmaps
        std::map<std::string, SDL_Surface*> bitmaps;
        if(!loadBitmapIfNecessary(bitmaps, defBitmap)) {
                LOG_ERROR("Failed to load an 8-bit definition bitmap, aborting.");
                return;
        }
        for(std::vector<std::string>::const_iterator i = shading.bitmaps.begin(); i != shading.bitmaps.end();
                ++i) {
                if(!loadBitmapIfNecessary(bitmaps, *i)) {
                        LOG_WARNING("Failed to load an 8-bit shading bitmap, removing from shading list");
                }
        }
        
        //remove unloaded bitmaps
        std::map<std::string, SDL_Surface*>::iterator j = bitmaps.end();
        for(std::map<std::string, SDL_Surface*>::iterator i = bitmaps.begin(); i != bitmaps.end(); ++i) {
                if(j != bitmaps.end()) {
                        bitmaps.erase(j);
                        j = bitmaps.end();
                }
                if(i->second == 0) {
                        j = i;
                }
        }
        if(j != bitmaps.end()) {
                bitmaps.erase(j);
        }
        
        //create indices to shading surface pointers for efficiency reasons
        SDL_Surface** shadingIndices = new SDL_Surface*[shading.bitmaps.size()];
        ASSERT(shadingIndices != 0); //not that large so being out of mem is highly abnormal
        for(u32 i = 0; i < shading.bitmaps.size(); ++i) {
                shadingIndices[i] = bitmaps.count(shading.bitmaps[i]) > 0 ? bitmaps[shading.bitmaps[i]] : 0;
        }
        
        //palette of grays
        SDL_Color grays[256];
        for(u16 i = 0; i < 256; ++i) {
                grays[i].r = grays[i].g = grays[i].b = i;
                grays[i].unused = 0;
        }
        
        //create a distance array where each value corresponds to a pixel in the definition bitmap
        f32* distances = new f32[bitmaps[defBitmap]->w * bitmaps[defBitmap]->h];
        if(distances == 0) {
                LOG_ERROR("Unable to create distance array (not enough memory is likely)");
                unloadBitmaps(bitmaps);
                delete[] shadingIndices;
        }
        
        std::cout << "\nGenerating province graphics...\n";
        //main loop
        u32 count = 1;
        std::vector<Si32_rect> provRects;
        for(std::list<Si32_point>::const_iterator i = startPoints.begin(); i != startPoints.end(); ++i) {
                if(i->x >= 0 && i->y >= 0 && i->x < bitmaps[defBitmap]->w && i->y < bitmaps[defBitmap]->h) {
                        Si32_rect limits;
                        SDL_Surface* provBitmap = generateProvince(*i, limits, bitmaps, distances, grays,
                                shadingIndices);
                        if(provBitmap != 0) {
                                std::string filename = outputDir +
                                        boost::lexical_cast<std::string>(count);
                                if(emitBMP) {
                                        SDL_SaveBMP(provBitmap, (filename + ".bmp").c_str());
                                }
                                SDL_FreeSurface(provBitmap);
                        } else {
                                LOG_ERROR("Failed to generate bitmap for point (" << i->x << ", "
                                        << i->y << ")");
                        }
                        provRects.push_back(limits);
                        
                } else {
                        LOG_WARNING("Point (" << i->x << ", " << i->y << ") is out of bounds");
                        provRects.push_back(Si32_rect(0, 0, 0, 0));
                }
                ++count;
        }
        
        std::ofstream os;
        os.open((outputDir + "Rects.lua").c_str(), std::ios::out | std::ios::trunc);
        os << "-------- Automatically generated Lua-file: --------" << std::endl;
        os << "-- Created by " << APP_TITLE << std::endl << std::endl;
        os << "provinceRects = {" << std::endl;
        for(u32 i = 0; i < provRects.size(); ++i) {
                const Si32_rect& rc = provRects[i];
                os << "\t[" << i + 1 << "] = { " << rc.left << ", " << rc.top << ", " << rc.right << ", "
                        << rc.bottom << " }," << std::endl;
        }
        os << "}" << std::endl;
        os.close();
        
        //clean up
        unloadBitmaps(bitmaps);
        if(distances != 0) {
                delete[] distances;
        }
        delete[] shadingIndices;
}

SDL_Surface* CBorderizer::CProvGeneration::generateProvince(const Si32_point& start, Si32_rect& limits,
        std::map<std::string, SDL_Surface*>& bitmaps, f32* distances, const SDL_Color palette[],
        SDL_Surface * const * shadingIndices)
{
        const f32 c_veryShortDist = 0.0f;
        
        ASSERT(distances != 0);
        ASSERT(shadingIndices != 0);
        
        std::cout << start.x << " " << start.y << std::endl;
        
        SDL_Surface* defSurf = bitmaps[defBitmap]; //because else the compiler makes apeshit slow code;
        //it doesn't realize it needs to put this pointer away into memory or register
        
        //set distances as negative = non-computed
        u32 size = defSurf->w * defSurf->h;
        for(u32 j = 0; j < size; ++j) {
                distances[j] = -1.0f;
        }
        
        //reset limits that record how wide the province is
        limits.left = limits.right = start.x;
        limits.top = limits.bottom = start.y;
        
        u8 ourColor = getPixel8bit(defSurf, start.x, start.y);
        std::list<Si32_point> stack;
        stack.push_back(start);
        
        static u32 pixelsComputed = 0;
        while(!stack.empty()) {
                /*pixelsComputed++;
                if((pixelsComputed % 10000) == 0) {
                        std::cout << pixelsComputed << std::endl;
                }*/
                Si32_point p = *stack.begin();
                stack.pop_front();
                
                u16 maxRadius = defSurf->w > defSurf->h ?
                        defSurf->w : defSurf->h;
                maxRadius = maxRadius > width ? width : maxRadius;
                
                //calculates shortest distance to point
                f32 shortest = FLT_MAX;
                for(u16 radius = 1; radius <= maxRadius; ++radius) {
                        //left side and right side
                        for(i32 y = p.y - radius; y < p.y + radius; ++y) {
                                if(y >= 0 && y < defSurf->h) {
                                        //left
                                        if(p.x - radius >= 0 &&
                                                getPixel8bit(defSurf, p.x - radius, y) != ourColor) {
                                                f32 d = sqrt(radius * radius +
                                                        (p.y - y ) * (p.y - y));
                                                if(d < shortest) {
                                                        shortest = d;
                                                        maxRadius = u32(shortest) + 1;
                                                }
                                        }
                                        //right
                                        if(p.x + radius < defSurf->w &&
                                                getPixel8bit(defSurf, p.x + radius, y) != ourColor) {
                                                f32 d = sqrt(radius * radius +
                                                        (p.y - y ) * (p.y - y));
                                                if(d < shortest) {
                                                        shortest = d;
                                                        maxRadius = u32(shortest) + 1;
                                                }
                                        }
                                }
                        }
                        //top side and bottom side
                        for(i32 x = p.x - radius; x < p.x + radius; ++x) {
                                if(x >= 0 && x < defSurf->w) {
                                        //top
                                        if(p.y - radius >= 0 &&
                                                getPixel8bit(defSurf, x, p.y - radius) != ourColor) {
                                                f32 d = sqrt(radius * radius +
                                                        (p.x - x ) * (p.x - x));
                                                if(d < shortest) {
                                                        shortest = d;
                                                        maxRadius = u32(shortest) + 1;
                                                }
                                        }
                                        //bottom
                                        if(p.y + radius < defSurf->h &&
                                                getPixel8bit(defSurf, x, p.y + radius) != ourColor) {
                                                f32 d = sqrt(radius * radius +
                                                        (p.x - x ) * (p.x - x));
                                                if(d < shortest) {
                                                        shortest = d;
                                                        maxRadius = u32(shortest) + 1;
                                                }
                                        }
                                }
                        }
                } //end of radius loop
                
                distances[p.y * defSurf->w + p.x] = shortest;
                
                limits.left = p.x < limits.left ? p.x : limits.left;
                limits.right = p.x > limits.right ? p.x : limits.right;
                limits.top = p.y < limits.top ? p.y : limits.top;
                limits.bottom = p.y > limits.bottom ? p.y : limits.bottom;
                
                //see if we should add neighbour points to stack
                //left
                u32 index = p.y * defSurf->w + (p.x - 1);
                if(p.x - 1 >= 0 && distances[index] < 0.0f) {
                        if(getPixel8bit(defSurf, p.x - 1, p.y) == ourColor) {
                                Si32_point p2(p.x - 1, p.y);
                                stack.push_back(p2);
                        }
                        distances[index] = c_veryShortDist; //this will regardless indicate that this point
                        //has been considered... distance will be overwritten if point was pushed on stack,
                        //but it will make sure it's not pushed on stack more times
                }
                //right
                index = p.y * defSurf->w + (p.x + 1);
                if(p.x + 1 < defSurf->w && distances[index] < 0.0f) {
                        if(getPixel8bit(defSurf, p.x + 1, p.y) == ourColor) {
                                Si32_point p2(p.x + 1, p.y);
                                stack.push_back(p2);
                        }
                        distances[index] = c_veryShortDist;
                }
                //top
                index = (p.y - 1) * defSurf->w + p.x;
                if(p.y - 1 >= 0 && distances[index] < 0.0f) {
                        if(getPixel8bit(defSurf, p.x, p.y - 1) == ourColor) {
                                Si32_point p2(p.x, p.y - 1);
                                stack.push_back(p2);
                        }
                        distances[index] = c_veryShortDist;
                }
                //bottom
                index = (p.y + 1) * defSurf->w + p.x;
                if(p.y + 1 < defSurf->h && distances[index] < 0.0f) {
                        if(getPixel8bit(defSurf, p.x, p.y + 1) == ourColor) {
                                Si32_point p2(p.x, p.y + 1);
                                stack.push_back(p2);
                        }
                        distances[index] = c_veryShortDist;
                }
        } //end of point stack loop - we got distances calculated
        
        //make a final limit correction
        limits.left = --limits.left < 0 ? 0 : limits.left;
        limits.right = ++limits.right >= defSurf->w ? defSurf->w - 1 : limits.right;
        limits.top = --limits.top < 0 ? 0 : limits.top;
        limits.bottom = ++limits.bottom >= defSurf->h ? defSurf->h - 1 : limits.bottom;
        
        SDL_Surface* provSurf = SDL_CreateRGBSurface(SDL_SWSURFACE, limits.right - limits.left,
                limits.bottom - limits.top, 8, 0, 0, 0, 0);
        if(provSurf == 0) {
                return 0;
        }
        SDL_SetPalette(provSurf, SDL_LOGPAL | SDL_PHYSPAL, (SDL_Color*&) palette, 0, 256);
        if(SDL_LockSurface(provSurf) != 0) {
                SDL_FreeSurface(provSurf);
                return 0;
        }
        
        //rendering loop
        for(u32 y = limits.top; y < limits.bottom; ++y) {
                for(u32 x = limits.left; x < limits.right; ++x) {
                        
                        f32 dist = distances[y * defSurf->w + x];
                        if(dist < 0.0f) {
                                putPixel8bit(provSurf, x - limits.left, y - limits.top, 0);
                        } else {
                                u8 shadingColor = getShadingValueAtPoint(x, y, shadingIndices);
                                f32 borderColor = dist / f32(width);
                                borderColor = borderColor > 1.0f ? 1.0f : borderColor;
                                u8 finalBorderColor = u8(borderColor * f32(maxColor - minColor) + 0.5f) +
                                        minColor;
                                u8 finalColor = finalBorderColor < shadingColor ?
                                        finalBorderColor : shadingColor;
                                putPixel8bit(provSurf, x - limits.left, y - limits.top, finalColor);
                        }
                }
        }
        
        SDL_UnlockSurface(provSurf);
        
        return provSurf;
}

bool CBorderizer::CProvGeneration::loadBitmapIfNecessary(std::map<std::string, SDL_Surface*>& bitmaps,
        const std::string& filename)
{
        if(bitmaps.count(filename) > 0) {
                return true;
        }
        
        bitmaps[filename] = IMG_Load(filename.c_str()); //add to map even if it returns 0
        if(bitmaps[filename] != 0 && (bitmaps[filename]->format->BytesPerPixel != 1 ||
                SDL_LockSurface(bitmaps[filename]) != 0)) {
                //reject non-8 bit surfaces or unlocked ones
                SDL_FreeSurface(bitmaps[filename]);
                bitmaps[filename] = 0;
        }
        return bitmaps[filename] != 0;
}

u8 CBorderizer::CProvGeneration::getShadingValueAtPoint(u32 x, u32 y,
        SDL_Surface * const * shadingIndices)
{
        for(u32 i = 0; i < shading.bitmaps.size(); ++i) {
                if(shadingIndices[i] != 0) {
                        const Si32_point& dest = shading.dest[i];
                        const Si32_rect& src = shading.src[i];
                        i32 transX = x - dest.x;
                        i32 transY = y - dest.y;
                        if(transX >= 0 && transY >= 0 && transX < (src.right - src.left) &&
                                transY < (src.bottom - src.top)) {
                                u8 color = getPixel8bit(shadingIndices[i], transX + src.left, transY + src.top);
                                return color == 0 ? 1 : color; //make sure there's no transparency in shading
                        }
                }
        }
        
        return 0;
}

void CBorderizer::CProvGeneration::unloadBitmaps(std::map<std::string, SDL_Surface*>& bitmaps)
{
        for(std::map<std::string, SDL_Surface*>::const_iterator i = bitmaps.begin(); i != bitmaps.end();
                ++i) {
                if(i->second != 0) {
                        SDL_UnlockSurface(i->second);
                        SDL_FreeSurface(i->second);
                }
        }
}

std::string toLowercase(const std::string& str)
{
        std::string lowercase = str;
        std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), (int(*)(int)) std::tolower);
        return str;
}
