/** 
 @file  list.h
 @brief ENet list management 
*/
#ifndef __ENET_LIST_H__
#define __ENET_LIST_H__

#include <stdlib.h>

//Modified heavily to comply with MingW requirements.

typedef struct _ENetListNode
{
   struct _ENetListNode * next;
   struct _ENetListNode * previous;
} ENetListNode;

typedef ENetListNode * ENetListIterator;

typedef struct _ENetList
{
   ENetListNode sentinel;
} ENetList;

//I had to add "C" to the extern:s below...

extern "C" void enet_list_clear (ENetList *);

extern "C" ENetListIterator enet_list_insert (ENetListIterator, void *);
extern "C" void * enet_list_remove (ENetListIterator);

extern "C" size_t enet_list_size (ENetList *);

/*#define enet_list_begin(list) ((list) -> sentinel.next)
#define enet_list_end(list) (& (list) -> sentinel)

#define enet_list_empty(list) (enet_list_begin (list) == enet_list_end (list))

#define enet_list_next(iterator) ((iterator) -> next)
#define enet_list_previous(iterator) ((iterator) -> previous)

#define enet_list_front(list) ((void *) (list) -> sentinel.next)
#define enet_list_back(list) ((void *) (list) -> sentinel.previous)*/

//I had to rewrite these macros as function because MingW couldn't deal
//with them...

inline ENetListIterator enet_list_begin(ENetList* list)
{
  return list -> sentinel.next;
}

inline ENetListIterator enet_list_end(ENetList* list)
{
  return &list -> sentinel;
}

inline bool enet_list_empty(ENetList* list)
{
  return enet_list_begin(list) == enet_list_end(list);
}

inline ENetListIterator enet_list_next(ENetListIterator iterator)
{
  return iterator -> next;
}

inline ENetListIterator enet_list_previous(ENetListIterator iterator)
{
  return iterator -> previous;
}

inline void* enet_list_front(ENetList* list)
{
  return (void *) list -> sentinel.next;
}

inline void* enet_list_back(ENetList* list)
{
  return (void *) list -> sentinel.previous;
}

#endif /* __ENET_LIST_H__ */

