//r2_createclasses.cpp

//This code is published under the General Public License.

#include "r2_createclasses.h"
#include "r2_creator.h"

#include "..\r2_gui\r2_gui.h"
#include "..\r2_gui\r2_window.h"
#include "..\r2_gui\r2_textbox.h"
#include "..\r2_gui\r2_provmap.h"
#include "..\r2_gui\r2_slider.h"

using namespace r2;

void r2::registerCreatableClasses()
{
        //CWindow classes:
        R2_REGISTER_CREATE(CWindow, CWindow);
        R2_REGISTER_CREATE(CWindow, CScreenBackground);
        R2_REGISTER_CREATE(CWindow, CTextBox);
        R2_REGISTER_CREATE(CWindow, CMultilineLabel);
        R2_REGISTER_CREATE(CWindow, CSlider);
        R2_REGISTER_CREATE(CWindow, CProvinceMap);
}
