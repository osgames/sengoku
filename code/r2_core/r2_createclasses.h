//r2_createclasses.h

//This code is published under the General Public License.

#ifndef R2_CREATECLASSES_H
#define R2_CREATECLASSES_H

namespace r2
{

void registerCreatableClasses();

}

#endif //!R2_CREATECLASSES_H
