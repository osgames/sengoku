//r2_creator.h

//This code is published under the General Public License.

#ifndef R2_CREATOR_H
#define R2_CREATOR_H

#include <string>
#include <map>
#include "r2_singleton.h"

#ifndef R2_REGISTER_EXISTS
#define R2_REGISTER_EXISTS(B, k) r2::CCreator<B>::exists(k)
#endif

#ifndef R2_CREATE
#define R2_CREATE(B, k) r2::CCreator<B>::create(k)
#endif

#ifndef R2_REGISTER_CREATE
#define R2_REGISTER_CREATE(B, T) r2::CCreator<B>::registerCreate(&r2::CNew<B, T>::create, std::string(#T))
#endif

namespace r2
{

template<class B>
class CCreator : public r2::CSingleton<CCreator<B> >
{
friend class r2::CSingleton<CCreator<B> >;
protected:
        CCreator() {}
        std::map<std::string, B* (*)(void)> map;
public:
        static CCreator& getInstance() {return CSingleton<CCreator<B> >::getInstance();}
        static void registerCreate(B* (*createFunc)(void), const std::string& key);
        static B* create(const std::string& key);
        static bool exists(const std::string& key)
        {
                return getInstance().map.count(key) > 0;
        }
};

template<class B, class T>
class CNew
{
public:
        static B* create();
};

template<class B>
void CCreator<B>::registerCreate(B* (*createFunc)(void), const std::string& key)
{
        CCreator& creator = getInstance();
        creator.map[key] = createFunc;
}

template<class B>
B* CCreator<B>::create(const std::string& key)
{
        CCreator& creator = getInstance();
        if(creator.map.count(key) > 0) {
                return creator.map[key]();
        } else {
                return 0;
        }
}

template<class B, class T>
B* CNew<B, T>::create()
{
        return new T();
}

} //end of namespace

#endif //!R2_CREATOR_H
