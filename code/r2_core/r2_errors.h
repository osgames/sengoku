//r2_errors.h

//This code is published under the General Public License.

//By the way, this file should be removed... It's useless anyhow.

#ifndef R2_ERRORS_H
#define R2_ERRORS_H

//SDL related errors
#define ERRORMSG_SDLINIT    "\nSDL could not be initialized. (SDL.dll may be missing.)"
#define ERRORMSG_SDLVIDEO "\nSDL video could not be initialized."
#define ERRORMSG_SDLINPUT "Input initialization function called multiple times."

//video related messages
#define ERRORMSG_VIDEOMODEHARDWARE   "\nThe specified hardware video mode could not be initialized."
#define ERRORMSG_VIDEOMODESOFTWARE   "\nThe specified software video mode could not be initialized."

#endif //!R2_ERRORS_H
