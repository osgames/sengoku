//r2_events.cpp

//This code is published under the General Public License.

#include "r2_events.h"
#include "r2_log.h"

struct SMessageEntry
{
  IRecipent* rec;
} g_msgRegistry[MSG_COUNT];

bool IRecipent::Register(MESSAGE msg)
{
  if(msg < MSG_COUNT)
  {
    //don't care if we overwrite someone else, that's one of the ideas with
    //this system, to be able to select a recipent
    regMessages.push_back(msg);
    g_msgRegistry[msg].rec = this;
    return true;
  }
  else
  {
    Log_Print("IRecipent::Register(): Attempted to register invalid message type:");
    Log_Integer(msg);
    return false;
  }
}

void IRecipent::UnregisterHandlers(void)
{
  //unregister message handlers
  for(std::vector<MESSAGE>::iterator it = regMessages.begin();
    it != regMessages.end(); it++)
  {
    //only unregister if it's actually we that are registered, that is,
    //don't mess with someone else's registration
    if(g_msgRegistry[*it].rec == this)
      g_msgRegistry[*it].rec = 0;
  }
  regMessages.clear();
}

bool IRecipent::ReceivesMessage(MESSAGE msg)
{
  return g_msgRegistry[msg].rec == this;
}

void MSG_Post(MESSAGE msg, IMessageData* data)
{
  //data* may be null
  
  if(msg < MSG_COUNT)
  {
    if(g_msgRegistry[msg].rec != 0)
    {
      g_msgRegistry[msg].rec->Receive(msg, data);
    }
  }
  else
  {
    Log_Print("MSG_Post(): Attempted to post invalid message type:");
    Log_Integer(msg);
  }
}
