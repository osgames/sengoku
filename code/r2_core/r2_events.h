//r2_events.h

//This code is published under the General Public License.

//This file is called r2_events.h for legacy reasons... I'm to lazy to go
//through the trouble of changing it to something more suitable now.

#ifndef R2_EVENTS_H
#define R2_EVENTS_H

#include "typedef.h"
#include "r2_msgs.h"
#include <vector>

class IRecipent
{
protected:
  std::vector<MESSAGE> regMessages;
  bool Register(MESSAGE msg);
public:
  virtual void Receive(MESSAGE msg, IMessageData* data) = 0;
  virtual void RegisterHandlers(void) = 0;
  void UnregisterHandlers(void);
  bool ReceivesMessage(MESSAGE msg);
};

void MSG_Post(MESSAGE msg, IMessageData* data);

#endif
