//r2_filepaths.h

//This code is published under the General Public License.

//Central header to specify all file paths instead of littering them around in code.
//Not all file paths in code have been moved here yet.

#ifndef R2_FILEPATHS_H
#define R2_FILEPATHS_H

#define FILE_SCRIPTS_INTERFACE          "Scripts\\Interface.lua"
#define FILE_SCRIPTS_ACTIONS            "Scripts\\Actions.lua"
#define FILE_SCRIPTS_GAME               "Scripts\\Game.lua"

#define FILE_SPRITES_TOWNS              "Data\\Sprites\\Towns.lua"
#define FILE_SPRITES_ARMIES             "Data\\Sprites\\Armies.lua"

#endif //!R2_FILEPATHS_H
