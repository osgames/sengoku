//r2_gamestate.cpp

//this is old crap code, remove when possible

//This code is published under the General Public License.

#include "r2_gamestate.h"

i32 g_gameStates[GS_COUNT];

u16 GetGameState(gamestate type)
{
  return g_gameStates[type];
}

void SetGameState(gamestate type, i32 value)
{
  g_gameStates[type] = value;
}
