//r2_gamestate.h

//this is old crap code, remove when possible

//This code is published under the General Public License.

#ifndef R2_GAMESTATE_H
#define R2_GAMESTATE_H

#include "typedef.h"

//Rather simple for the moment, but easy to access from anywhere and
//is expandable.

//different kinds of gamestates
#define GS_MAINSTATE 0
#define GS_SCREENWIDTH 2
#define GS_SCREENHEIGHT 3
#define GS_INTERFACEMEMORY 4
#define GS_SCREENBPP 5
#define GS_LOOPDELAY 6  //boolean

#define GS_COUNT 7

//global state of game - "mainstate" constants
#define MAINSTATE_LOADING 0
#define MAINSTATE_MENU 1
#define MAINSTATE_PLAYING 2
#define MAINSTATE_UNLOADING 3

typedef u8 gamestate;

u16 GetGameState(gamestate type);
void SetGameState(gamestate type, i32 value);

#endif //!R2_GAMESTATE_H
