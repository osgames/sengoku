//r2_geometry.h

//This code is published under the General Public License.

#ifndef R2_GEOMETRY_H
#define R2_GEOMETRY_H

#include <cmath>
#include "util.h"

namespace r2 {

template<class T>
class TVector2D
{
public:
        T x;
        T y;
        
        TVector2D() {x = 0; y = 0;}
        TVector2D(const T& x, const T& y) {TVector2D::x = x; TVector2D::y = y;}
        TVector2D(const TVector2D& v) {x = v.x; y = v.y;}
        
        template<class S> S length() const {return sqrt(x * x + y * y);}
        template<class S> S dotProduct(const TVector2D& v) const {return x * v.x + y * v.y;}
        
        TVector2D mul(const T& f) const {TVector2D temp(x * f, y * f); return temp;}
        TVector2D div(const T& f) const {TVector2D temp(x / f, y / f); return temp;}
        TVector2D add(const T& f) const {TVector2D temp(x + f, y + f); return temp;}
        TVector2D sub(const T& f) const {TVector2D temp(x - f, y - f); return temp;}
        TVector2D add(const TVector2D& v) const {TVector2D temp(x + v.x, y + v.y); return temp;}
        TVector2D sub(const TVector2D& v) const {TVector2D temp(x - v.x, y - v.y); return temp;}
        TVector2D neg() const {TVector2D temp(-x, -y); return temp;}
        
        bool equals(const TVector2D& v) const {return x == v.x && y == v.y;}
        
        template<class S> TVector2D<S> cast() const {TVector2D<S> temp((S)x, (S)y); return temp;}
};

template<class T>
class TRect
{
public:
        T left;
        T top;
        T right;
        T bottom;
        
        TRect() {left = top = right = bottom = 0;}
        TRect(const T& left, const T& top, const T& right, const T& bottom) {TRect::left = left;
                TRect::top = top; TRect::right = right; TRect::bottom = bottom;}
        TRect(const TRect& r) {left = r.left; top = r.top; right = r.right; bottom = r.bottom;}
        TRect(const TVector2D<T>& p1, const TVector2D<T>& p2) {left = p1.x; top = p1.y; right = p2.x;
                bottom = p2.y;}
        
        T width() const {return right - left;}
        T height() const {return bottom - top;}
        
        TRect add(const TVector2D<T>& v) const {TRect temp(left + v.x, top + v.y, right + v.x, bottom + v.y);
                return temp;}
        TRect sub(const TVector2D<T>& v) const {TRect temp(left - v.x, top - v.y, right - v.x, bottom - v.y);
                return temp;}
        
        TVector2D<T> center() const {TVector2D<T> temp(left + right / 2, top + bottom / 2); return temp;}
        
        void normalize() {T temp; if(left > right) {temp = left; left = right; right = temp;}
                if(top > bottom) {temp = top; top = bottom; bottom = temp;}}
        bool isNormal() const {return left <= right && top <= bottom;}
        
        bool contains(const TVector2D<T>& p) const {return p.x >= left && p.x <= right && p.y >= top &&
                p.y <= bottom;}
        //!Calculates and intersection rectangle. Provided the two tested rectangles are normal(ized), a normal result indicates intersection.
        TRect intersect(const TRect& r) const {TRect temp(MAX(left, r.left), MAX(top, r.top),
                MIN(right, r.right), MIN(bottom, r.bottom)); return r;}
        
        template<class S> TRect<S> cast() const {TRect<S> temp((S)left, (S)top, (S)right, (S)bottom);
                return temp;}
};

}; //!namespace r2

#endif //!GEOMETRY_H
