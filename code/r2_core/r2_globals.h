//r2_globals.h

//This code is published under the General Public License.

#ifndef R2_GLOBALS_H
#define R2_GLOBALS_H

#include "typedef.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_graphics\r2_video.h"
#include "..\r2_graphics\r2_font.h"
#include "..\r2_graphics\r2_provrle.h"
#include "..\r2_core\r2_filepaths.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_gui\r2_sprite.h"

namespace r2
{

struct SGlobals
{
        CLua commonLua;
        
        CResourceManager<CBitmap> bitmaps;
        CResourceManager<CFont> fonts;
        CResourceManager<CProvinceRLESet> provrlesets;
        CResourceManager<CSpriteSet> sprites;
        
        u32 screenWidth;
        u32 screenHeight;
        u8 screenBPP;
        bool loopDelay;
        bool running; //is condition for main loop
};

extern SGlobals globals;

}

#endif

