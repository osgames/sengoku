//r2_inflatable.cpp

//This code is published under the General Public License.

#include "r2_inflatable.h"
#include <string.h>
#include <memory.h>
#include "r2_log.h"

void IInflatable::RawData(u8* pointer, u32 size)
{
  //this is pretty straightforward
  SCommand com;
  com.pointer = pointer;
  com.actualSize = size;
  com.mode = MODE_RAW;
  
  commands.push_back(com);
}

void IInflatable::FixedString(char* str, u32 maxSize)
{
  //not as straightforward but good enough
  SCommand com;
  com.pointer = str;
  com.maxSize = maxSize;
  com.mode = MODE_FIXEDSTRING;
  
  //calculate size of string (including null char)
  u32 size = strlen((const char*) str) + 1;
  
  //make sure it won't exceed maximum
  if(size > maxSize)
    com.actualSize = maxSize;
  else
    com.actualSize = size;
  
  commands.push_back(com);
}

void IInflatable::StdString(std::string* str)
{
  SCommand com;
  com.pointer = str;
  com.actualSize = strlen(str->c_str()) + 1;
  com.mode = MODE_STDSTRING;
  
  commands.push_back(com);
}

u32 IInflatable::Size(void)
{
  //clear just to be sure
  commands.clear(); 
  
  Serialize();
  
  //calculate required length of buffer
  u32 reqLen = 0;
  for(std::vector<SCommand>::iterator it = commands.begin();
    it != commands.end(); it++)
  {
    reqLen += it->actualSize;
  }
  
  return reqLen;
}

void IInflatable::Flatten(u8* buffer)
{
  u32 reqSize = Size(); //this also serializes
  
  //write contents to buffer
  u32 pos = 0;
  for(std::vector<SCommand>::iterator it = commands.begin();
    it != commands.end(); it++)
  {
    switch(it->mode)
    {
    case MODE_RAW:
      {
        memcpy(&buffer[pos], it->pointer, it->actualSize);
        break;
      }
    case MODE_FIXEDSTRING:
      {
        memcpy(&buffer[pos], it->pointer, it->actualSize);
        buffer[pos + it->actualSize - 1] = 0; //overwrite last char with zero
        //in the event of maximum size restricting its size
        break;
      }
    case MODE_STDSTRING:
      {
        memcpy(&buffer[pos], ((std::string*) it->pointer)->c_str(),
          it->actualSize);
        break;
      }
    }
    pos += it->actualSize;
  }
  
  //don't leave dirt in memory
  commands.clear();
}

void IInflatable::Inflate(u8* buffer)
{
  //clear just to be sure
  commands.clear(); 
  
  Serialize();
  
  //copy contents of buffer
  u32 pos = 0;
  for(std::vector<SCommand>::iterator it = commands.begin();
    it != commands.end(); it++)
  {
    switch(it->mode)
    {
    case MODE_RAW:
      {
        memcpy(it->pointer, &buffer[pos], it->actualSize);
        pos += it->actualSize;
        break;
      }
    case MODE_FIXEDSTRING:
      {
        u32 size = strlen((const char*) &buffer[pos]) + 1;
        if(size > it->maxSize)
          size = it->maxSize;
        memcpy(it->pointer, &buffer[pos], size);
        buffer[pos + size - 1] = 0; //null terminate
        pos += size;
        break;
      }
    case MODE_STDSTRING:
      {
        u32 size = strlen((const char*) &buffer[pos]) + 1;
        *((std::string*) it->pointer) = (char*) &buffer[pos];
        pos += size;
        break;
      }
    }
  }
  
  //don't leave dirt in memory
  commands.clear();
}
