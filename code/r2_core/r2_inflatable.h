//r2_inflatable.h

//This code is published under the General Public License.

#ifndef R2_INFLATABLE_H
#define R2_INFLATABLE_H

#include "typedef.h"
#include <vector>
#include <string>

#define RAWDATA(x) (RawData((u8*) &x, sizeof(x)))

#define MODE_RAW 0
#define MODE_FIXEDSTRING 1
#define MODE_STDSTRING 2

class IInflatable
{
private:
  struct SCommand
  {
    void* pointer;
    u32 actualSize;
    u32 maxSize;
    u8 mode; //null terminated
  };
  std::vector<SCommand> commands;
protected:
  virtual void Serialize(void) = 0;
  void RawData(u8* pointer, u32 size);
  void FixedString(char* str, u32 maxSize);
  void StdString(std::string* str);
public:
  u32 Size(void);
  void Flatten(u8* buffer);
  void Inflate(u8* buffer);
};

#endif //!R2_INFLATABLE_H
