//r2_log.cpp

//This code is published under the General Public License.

//Corresponding .h file: r2_log.h

#include "r2_log.h"
#include <time.h>

void CLogStreams::open()
{
        std::ofstream general;
        std::ofstream error;
        
        general.open("general.log", std::ios::out | std::ios::trunc);
        error.open("error.log", std::ios::out | std::ios::trunc);
        
        time_t time_s = time(0);
        tm* tm_s = localtime(&time_s);
        general << "Logging started " << asctime(tm_s);
        error << "Logging started " << asctime(tm_s);
        
        general.close();
        error.close();
}

void CLogStreams::close()
{
        std::ofstream general;
        std::ofstream error;
        
        general.open("general.log", std::ios::out | std::ios::app);
        error.open("error.log", std::ios::out | std::ios::app);
        
        time_t time_s = time(0);
        tm* tm_s = localtime(&time_s);
        general << "Logging ended " << asctime(tm_s);
        error << "Logging ended " << asctime(tm_s);
        
        general.close();
        error.close();
}
