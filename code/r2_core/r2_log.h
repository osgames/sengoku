//r2_log.h

//This code is published under the General Public License.

/*Revision history:
2007-09-07: Totally redesigned logging system to use standard C++ output streams. Obsolete functions are
        represented by macros to provide backwards compability.
*/

//Corresponding .cpp file: r2_log.cpp

#ifndef R2_LOG_H
#define R2_LOG_H

#include <string>
#include <iostream>
#include <fstream>
#include "typedef.h"
#include "r2_debug_def.h"

#ifndef DEBUG
//!Asserts a statement and prints an error message to the error log file if it's false. Is only evaluated in debug mode.
#define ASSERT(x)
//!Prints a warning message to the error log file. Is only evaluated in debug mode.
#define LOG_WARNING(x)
#else
//!Asserts a statement and prints an error message to the error log file if it's false. Is only evaluated in debug mode.
#define ASSERT(x) if(!(x)) { \
                std::ofstream s; \
                s.open("error.log", std::ios::out | std::ios::app); \
                s << "ASSERT failed in file " << __FILE__ << " on line " << __LINE__ << std::endl; \
                s.close(); \
        }
//!Prints a warning message to the error log file. Is only evaluated in debug mode.
#define LOG_WARNING(x) { std::ofstream s; \
        s.open("error.log", std::ios::out | std::ios::app); \
        s << "Warning: '" << x << "' in file " << __FILE__ << " on line " << __LINE__ << std::endl; \
        s.close(); }
#endif //!DEBUG

//!Unconditionally prints a message to the general log file.
#define LOG_PRINT(x) { std::ofstream s; \
        s.open("general.log", std::ios::out | std::ios::app); \
        s << x << std::endl; \
        s.close(); }
//!Unconditionally prints an error message to the error log file.
#define LOG_ERROR(x) { std::ofstream s; \
        s.open("error.log", std::ios::out | std::ios::app); \
        s << "Error: '" << x << "' in file " << __FILE__ << " on line " << __LINE__ << std::endl; \
        s.close(); }

class CLogStreams
{
protected:
        CLogStreams() {}
public:
        static void open();
        static void close();
};

//!This macro is provided for backwards compability. The goal is to eventually remove it. Do not use it.
#define Log_Print(x) LOG_PRINT("Old Log_Print: " << x)
//!This macro is provided for backwards compability. The goal is to eventually remove it. Do not use it.
#define Log_Integer(x) LOG_PRINT("Old Log_Integer: " << x)

#endif //!R2_LOG_H
