//r2_lua_manager.cpp

#include "r2_log.h"
#include "r2_lua_manager.h"
#include <cstdarg>

using namespace r2;

CLua::CLua()
{
        lua = 0;
        
        //initialize lua
        lua = lua_open();
  
        if(lua == 0) {
                LOG_ERROR("Failed to initialize LUA");
                return;
        }
        
        luaopen_base(lua);
        luaopen_table(lua);
        luaopen_string(lua);
        luaopen_math(lua);
        //luaopen_package(lua); //this isn't working for some reason and causing crashes, check out what the cause is
        
        try {
                luabind::open(lua);
        } catch(luabind::error& e) {
                LOG_ERROR("Exception: " << e.what());
        }
}

CLua::~CLua()
{
        if(lua != 0) {
                lua_close(lua);
                lua = 0;
        }
}

bool CLua::doFile(const std::string& filename)
{
        ASSERT(lua != 0);
        
        if(lua_dofile(lua, filename.c_str()) != 0) {
                LOG_ERROR("Failed to do LUA file '" << filename << "'");
                return false;
        }
        
        return true;
}

bool CLua::doString(const std::string& str)
{
        ASSERT(lua != 0);
        
        if(lua_dostring(lua, str.c_str()) != 0) {
                LOG_ERROR("Failed to do LUA string '" << str << "'");
                return false;
        }
        
        return true;
}

void CLua::printError(luabind::error& e)
{
        using namespace luabind;
        object errorMsg(from_stack(e.state(), -1));
        LOG_ERROR("Lua error: " << errorMsg);
}

bool CLua::readRGB(luabind::object table, u8& r, u8& g, u8& b)
{
        using namespace luabind;
        
        if(table && type(table) == LUA_TTABLE) {
                r = type(table["red"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["red"]) : type(table["r"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["r"]) : type(table[1]) == LUA_TNUMBER ?
                        object_cast<u8>(table[1]) : 0;
                g = type(table["green"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["green"]) : type(table["g"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["g"]) : type(table[2]) == LUA_TNUMBER ?
                        object_cast<u8>(table[2]) : 0;
                b = type(table["blue"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["blue"]) : type(table["b"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["b"]) : type(table[3]) == LUA_TNUMBER ?
                        object_cast<u8>(table[3]) : 0;
                
                return true;
        }
        
        return false;
}

bool CLua::readRGBA(luabind::object table, u8& r, u8& g, u8& b, u8& a)
{
        using namespace luabind;
        
        if(table && type(table) == LUA_TTABLE) {
                readRGB(table, r, g, b);
                a = type(table["alpha"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["alpha"]) : type(table["a"]) == LUA_TNUMBER ?
                        object_cast<u8>(table["a"]) : type(table[4]) == LUA_TNUMBER ?
                        object_cast<u8>(table[4]) : 0;
                
                return true;
        }
        
        return false;
}

bool CLua::readRectangle(luabind::object table, Si32_rect& r)
{
        /*!A Lua rectangle table is specified by either containing the named number elements
        left, top, right and bottom, or four indexed numbers in order (ie. table[1] = table["left"]).
        Named elements have higher priority than indexed elements.*/
        
        using namespace luabind;
        
        if(table && type(table) == LUA_TTABLE) {
                r.left = type(table["left"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["left"]) : type(table[1]) == LUA_TNUMBER ?
                        object_cast<i32>(table[1]) : 0;
                r.top = type(table["top"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["top"]) : type(table[2]) == LUA_TNUMBER ?
                        object_cast<i32>(table[2]) : 0;
                r.right = type(table["right"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["right"]) : type(table[3]) == LUA_TNUMBER ?
                        object_cast<i32>(table[3]) : 0;
                r.bottom = type(table["bottom"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["bottom"]) : type(table[4]) == LUA_TNUMBER ?
                        object_cast<i32>(table[4]) : 0;
                
                return true;
        }
        
        return false;
}

bool CLua::readPoint(luabind::object table, Si32_point& p)
{
        /*!A Lua point table is specified by either containing the named number elements
        x and y, or two index numbers in order (ie. table[1] = table["x"]. Named elements
        have higher priority than index elements.*/
        
        using namespace luabind;
        
        if(table && type(table) == LUA_TTABLE) {
                p.x = type(table["x"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["x"]) : type(table[1]) == LUA_TNUMBER ?
                        object_cast<i32>(table[1]) : 0;
                p.y = type(table["y"]) == LUA_TNUMBER ?
                        object_cast<i32>(table["y"]) : type(table[2]) == LUA_TNUMBER ?
                        object_cast<i32>(table[2]) : 0;
                return true;
        }
        
        return false;
}
