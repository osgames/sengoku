//lua_manager.h

//This code is published under the General Public License.

#ifndef LUA_MANAGER_H
#define LUA_MANAGER_H

#include <memory>

#include "lua\lua.hpp"
#include "luabind\luabind.hpp"
#include "typedef.h"

//!The following macro takes 5 params: (table, property, luatype, cpptype, defaultval)
#define R2LUA_SAFELOAD(a, b, c, d, e) (luabind::type(a[b]) == c ? luabind::object_cast<d>(a[b]) : e)

namespace r2 {

class CLua
{
protected:
        lua_State* lua;
public:
        //!Initializes the LUA state pointer and opens appropiate libraries.
        CLua();
        //!Closes LUA.
        ~CLua();
        
        //!Returns the LUA state pointer. Note this will be required to register and call functions/objects with luabind.
        lua_State* state() {return lua;}
        
        //!Loads and runs LUA file.
        bool doFile(const std::string& filename);
        //!Loads and runs LUA string.
        bool doString(const std::string& str);
        
        //static help functions:
        //!Helps priting a luabind error.
        static void printError(luabind::error& e);
        //!Utility function to read r = red, g = green, b = blue out of a table. Returns success or failure.
        static bool readRGB(luabind::object table, u8& r, u8& g, u8& b);
        //!Utility function to read r = red, g = green, b = blue, a = alpha out of a table. Returns success or failure.
        static bool readRGBA(luabind::object table, u8& r, u8& g, u8& b, u8& a);
        //!Utility function which reads a rectangle. Returns whether table was found.
        static bool readRectangle(luabind::object table, Si32_rect& r);
        //!Utility function which reads a point. Returns whether table was found.
        static bool readPoint(luabind::object table, Si32_point& p);
};

} //end of namespace r2

#endif //!LUA_MANAGER_H
