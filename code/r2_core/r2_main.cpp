//r2_main.cpp - entry & lifetime of program

//This code is published under the General Public License.

/*Revision history:
2007-09-07: Started to clean up the code, and there are lots of WTF??? moments.
*/

#include <SDL.h>
#include <iostream>
#include <fstream>
#include "..\r2_graphics\r2_video.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_input\r2_input.h"
#include "..\r2_core\r2_errors.h"
#include "..\r2_core\r2_gamestate.h"
#include "..\r2_core\typedef.h"
#include "..\r2_core\util.h"
#include "..\r2_graphics\r2_cursor.h"
#include "..\r2_game\r2_game.h"
#include "..\r2_network\r2_network.h"
#include "..\r2_core\r2_tree.h"
#include "..\r2_gui\r2_gui.h"
#include "..\r2_core\r2_createclasses.h"
#include "..\r2_core\r2_globals.h"

#define APP_TITLE "Sengoku: Warring States of Japan, ver. 0.2.0"

namespace r2
{
        r2::SGlobals globals;
}

using namespace r2;

u32 Time_GetTicks(void)
{
  return SDL_GetTicks();
}

void ReadScreenConfig(void)
{
        i32 width = 1024, height = 768, bpp = 16;
  
        std::ifstream is;
        is.open("Screen.cfg", std::ios::binary);
  
        if(is.is_open())
        {
        is.read((char*) &width, sizeof(width));
        is.read((char*) &height, sizeof(height));
        is.read((char*) &bpp, sizeof(bpp));
        }
  
        is.close();
  
        r2::globals.screenWidth = width;
        r2::globals.screenHeight = height;
        r2::globals.screenBPP = bpp;
  
        //the idea is to remove game states eventually; they're just guised globals
        SetGameState(GS_SCREENWIDTH, width);
        SetGameState(GS_SCREENHEIGHT, height);
        SetGameState(GS_SCREENBPP, bpp);
}

void RenderTask(CSurface& screen) //does rendering chunk of main loop
{
        CGUI::getInstance().render(screen);
        Cursor_Render(screen);
        Video_Flip();
}

void UpdateTask(f32 dt)
{
        Input_Refresh();
        Cursor_Update();
        
        CGUI::getInstance().update(dt);
        CGame::getInstance().update(dt);
}

int main(int, char**)
{
        CLogStreams::open();
        
        try 
        {
                if(globals.commonLua.state() == 0) {
                        LOG_ERROR("Error initializing Lua. There's no chance the game will run without Lua.");
                        return 0;
                }
                
                //register all classes that can be excplitly created
                registerCreatableClasses();
    
                //start SDL without any subsystems
                if(SDL_Init(0) < 0)
                {
                        Log_Print(ERRORMSG_SDLINIT);
                        return 0;
                }
  
                ReadScreenConfig();
    
                //attempt to initialize video
                if(!Video_Init(r2::globals.screenWidth, r2::globals.screenHeight, r2::globals.screenBPP, true))
                {
                        //do not proceed since video is vital
                        SDL_Quit();
                        return 0;
                }
                SDL_WM_SetCaption(APP_TITLE, 0);
  
                //get back buffer for graphics
                CSurface screen;
                Video_GetScreenSurface(screen);
  
                //attempt to initialize input capabilities
                if(!Input_Init()) {
                        r2::globals.running = false;
                }
  
                //attempt to initialize Enet
                if(enet_initialize() != 0) {
                        r2::globals.running = false;
                }
    
                //the SDL events we receive in this main function we want to be character encoded
                //TODO: move to input initialization function
                SDL_EnableUNICODE(1);
                SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  
                //set up cursor
                Cursor_LoadAll();
                Cursor_SetIcon("normal");
                Cursor_SetPos(100, 100);
                Cursor_Show(true);
                
                CGUI& gui = CGUI::getInstance();
                if(!gui.init()) {
                        r2::globals.running = false;
                }
                
                CGame& game = CGame::getInstance();
                //initialize game object (doesn't mean a game is started)
                if(!game.init()) {
                        r2::globals.running = false;
                }
  
                //set old ticks to get 0 time difference at first
                u32 ticksOld = SDL_GetTicks();
                bool activeOld = true; //we start in normal window state
    
                //main loop
                r2::globals.running = true;
                r2::globals.loopDelay = true;
                while(r2::globals.running)
                {
                try
                {
                        //calculate time delta
                        f32 dt = f32(SDL_GetTicks() - ticksOld) * 0.001f;
                        ticksOld = SDL_GetTicks();
    
                        //poll SDL events
                        SDL_Event event;
                        while(SDL_PollEvent(&event))
                        {
                                switch(event.type) {
                                case SDL_KEYUP: {
                                        //don't remove this last "escape hatch" until game is stable... lol
                                        if(event.key.keysym.sym == SDLK_ESCAPE)
                                        {
                                                r2::globals.running = false;
                                        }
                                        break; //do NOT comment out this line...
                                } case SDL_KEYDOWN: {
                                        //send a message containing the key code and the character
                                        //code (only unicode 32 through 255)
                                        u8 charcode = event.key.keysym.unicode & 0xff; //TODO: see if this is correct
                                        //maybe we need a comparison <= 255 instead...
                                        if(charcode >= 32)
                                        {
                                                charcode = 0;
                                        }
                                        gui.keyPress(event.key.keysym.sym, event.key.keysym.unicode & 0xff);
                                        break;
                                } case SDL_MOUSEBUTTONUP: {
                                        Cursor_Fix();
                                        Si32_point p(Cursor_GetX(), Cursor_GetY());
                                        gui.mouseClick(p, Input_MouseButton2(event.button.button), false);
                                        break;
                                } case SDL_MOUSEBUTTONDOWN: {
                                        Cursor_Fix();
                                        Si32_point p(Cursor_GetX(), Cursor_GetY());
                                        gui.mouseClick(p, Input_MouseButton2(event.button.button), true);
                                        break;
                                } case SDL_MOUSEMOTION: {
                                        Si32_point p;
                                        Cursor_GetOffset(p);
                                        gui.mouseMove(p, event.motion.state == SDL_PRESSED);
                                        break;
                                }
                        }
                }
    
                //check whether we're in normal or minimized mode
                bool active = SDL_GetAppState() & SDL_APPACTIVE;
                if(active != activeOld)
                {
                        if(active)
                        {
                                //don't need to restore surfaces nowadays
                        }
                        activeOld = active;
                }
                
                UpdateTask(dt);
                
                if(active)
                {
                        //only render when non-minimized
                        RenderTask(screen);
      
                        if(globals.loopDelay)
                                SDL_Delay(20);
                                globals.loopDelay = true;
                        }
                        else
                        {
                                SDL_Delay(5); //nicer to CPU and my CPU fan than 0
                        }
                        
                }
                catch(char *str)
                {           
                        LOG_ERROR("Exception in main loop: '" << str << "'\n");
                }
                }

                //MSG_Post(MSG_DISCONNECT, 0); //handles destruction of server and client
                game.destroy();
                gui.destroy();
                globals.sprites.unbindAll();
                globals.provrlesets.unbindAll();
                globals.fonts.unbindAll();
                globals.bitmaps.unbindAll();
                enet_deinitialize();
                Input_Exit();
  
                //quit SDL
                SDL_Quit();
        }
        catch(char *str)
        {           
                LOG_ERROR("Exception in main() outside main loop: '" << str << "'\n");
        }
        
        CLogStreams::close();
        
        return 1;
}
