//r2_msgs.cpp

//This code is published under the General Public License.

#include "r2_msgs.h"

void CMsgSender::sendMsg(i32 type, i32 id)
{
        sendCntr++;
        for(std::list<IMsgReceiver*>::const_iterator it = registry.begin(); it != registry.end(); it++) {
                (*it)->receiveMsg(type, id, this);
        }
        sendCntr--;
}

void CMsgSender::addReceiver(IMsgReceiver* recv)
{
        registry.push_back(recv);
}

bool CMsgSender::removeReceiver(IMsgReceiver* recv)
{
        if(sendCntr > 0) {
                return false;
        }
        
        registry.remove(recv);
        
        return true;
}
