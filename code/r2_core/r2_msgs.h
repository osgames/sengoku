//r2_msgs.h

//This code is published under the General Public License.

#ifndef R2_MSGS_H
#define R2_MSGS_H

#include "typedef.h"
#include <list>

//we need to issue messages to let GUI know of army activities
#define MSG_WORLD_ARMYMOVED     1
#define MSG_WORLD_ARMYADDED     2
#define MSG_WORLD_ARMYREMOVED   3

#define MSG_WORLD_TOWNADDED     4
#define MSG_WORLD_TOWNREMOVED   5

class IMsgReceiver;

//!This class maintains a registry of receivers, which all receive any message sent.
class CMsgSender
{
private:
        std::list<IMsgReceiver*> registry;
        i32 sendCntr;
public:
        //!Sends a message from this class to a receiver.
        void sendMsg(i32 type, i32 id);
        
        //!Adds a receiver to registry. May be called at any time.
        void addReceiver(IMsgReceiver* recv);
        
        //!Removes a receiver from registry. Fails (returns false) if sendMsg is executing.
        bool removeReceiver(IMsgReceiver* recv);
};

class IMsgReceiver
{
public:
        virtual void receiveMsg(i32 type, i32 id, CMsgSender* sender) = 0;
};

#endif //R2_MSGS_H
