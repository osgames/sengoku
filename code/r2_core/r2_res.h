//r2_res.h

//This code is published under the General Public License.

/*Changlog:
        2007-09-23: Implemented a templated version expected to be slightly easier to handle.*/

#ifndef R2_RES_H
#define R2_RES_H

#include <string>
#include <map>
#include <memory>
#include <cctype>
#include <algorithm>

#include "typedef.h"
#include "r2_log.h"
#include "r2_singleton.h"

namespace r2
{

//I wanted this class to be a singleton, but it crashed as such, regardless of whether getInstance()
//was implemented as an instanciated CSingleton or put directly in this class...
template<class T>
class CResourceManager
{
//friend class CSingleton<CResourceManager<T> >;
protected:
        struct SRes
        {
                T* res;
                u32 refCount;
        };
        
        std::map<std::string, SRes> map;
public:
        ~CResourceManager() {unbindAll();}
        
        T* bind(const std::string& filename, i32 param);
        T* get(const std::string& filename);
        void unbind(const std::string& filename);
        void unbind(T* res);
        void unbindAll();
};

template<class T>
T* CResourceManager<T>::bind(const std::string& filename, i32 param)
{
        std::string lowstr = filename;
        std::transform(lowstr.begin(), lowstr.end(), lowstr.begin(), (int(*)(int)) std::tolower);

        if(map.count(lowstr) == 0) {
                map[lowstr].res = new T(lowstr, param);
                
                if(map[lowstr].res == 0) {
                        LOG_ERROR("Allocation of resource '" << filename << "' failed");
                        map.erase(lowstr);
                        return 0;
                }
                
                map[lowstr].refCount = 1;
        } else {
                map[lowstr].refCount++;
        }
        
        return map[lowstr].res;
}

template<class T>
T* CResourceManager<T>::get(const std::string& filename)
{
        std::string lowstr = filename;
        std::transform(lowstr.begin(), lowstr.end(), lowstr.begin(), (int(*)(int)) std::tolower);
        
        if(map.count(lowstr) == 0) {
                return 0;
        }
        
        return map[lowstr].res;
}

template<class T>
void CResourceManager<T>::unbind(const std::string& filename)
{
        std::string lowstr = filename;
        std::transform(lowstr.begin(), lowstr.end(), lowstr.begin(), (int(*)(int)) std::tolower);
        
        if(map.count(lowstr) != 0) {
                map[lowstr].refCount--;
                
                if(map[lowstr].refCount == 0) {
                        ASSERT(map[lowstr].res != 0);
                        delete map[lowstr].res;
                        map.erase(lowstr);
                }
        }
}

template<class T>
void CResourceManager<T>::unbind(T* res)
{    
        if(res == 0) {
                return;
        }
        
        //linear search
        for(class std::map<std::string, SRes >::iterator it = map.begin();
                it != map.end(); ++it) {
                if(it->second.res == res) {
                        it->second.refCount--;
                        if(it->second.refCount == 0) {
                                ASSERT(it->second.res != 0);
                                delete it->second.res;
                                map.erase(it);
                        }
                        break;
                }
        }
}

template<class T>
void CResourceManager<T>::unbindAll()
{
        for(class std::map<std::string, SRes>::iterator it = map.begin();
                it != map.end(); ++it) {
                ASSERT(it->second.res != 0);
                delete it->second.res;
        }
        
        map.clear();
}

} //end of namespace

#endif //!R2_RES_H
