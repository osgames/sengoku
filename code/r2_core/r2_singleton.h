//r2_singleton.h

//This code is published under the General Public License.

#ifndef R2_SINGLETON_H
#define R2_SINGLETON_H

#include "r2_log.h"

namespace r2
{

template <class T>
class CSingleton
{
public:
        static T& getInstance()
        {
                static T instance;
                //the following line is needed to prevent this buggy compiler from periodically
                //return a 0 reference: :angry: I had hell debugging it so don't remove it.
                static T* instancePtr = &instance;
                return *instancePtr;
        }
protected:
        CSingleton() {}
        ~CSingleton() {}
        CSingleton(CSingleton const&);
        CSingleton& operator=(CSingleton const&);
};
        
} //end of namespace

#endif //!R2_SINGLETON_H
