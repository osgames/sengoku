//r2_tree.h

//This code is published under the General Public License.

/*Changelog:
2007-09-19: Finished preliminary implementation.
2007-09-18: Started working on the tree class.*/

#ifndef R2_TREE_H
#define R2_TREE_H

#include <vector>

#include "typedef.h"
#include "r2_log.h"

namespace r2 {

const u16 gc_maxTreeDepth = 0xffff;
        
template<class T>
class CListTreeNodeIterator;

////////////////////////////////TEMPLATE CLASS DEFINITIONS:////////////////////////////////////////

template<class L, class R>
class IComparator
{
public:
        virtual i8 compare(L& lhs, R& rhs) = 0;
        bool equal(L& lhs, R& rhs) {return compare(lhs, rhs) == 0;}
};

//!List tree node.
/*!There is no set root, that's up for user to define. Each node keeps a circular double-linked list
        to store its children, however it retains the concepts of head and tail. The head is
        the first child pointed to by the node, the tail is the previous sibling of the head.*/
template<class T>
class CListTreeNode
{
friend class CListTreeNodeIterator<T>;
protected:
        T e;
        CListTreeNode<T>* parent;
        CListTreeNode<T>* next;
        CListTreeNode<T>* prev;
        CListTreeNode<T>* firstChild;

public:
        CListTreeNode(const T& element) {e = element; next = 0; prev = 0; firstChild = 0; parent = 0;}
        ~CListTreeNode() {removeChildren();}
        
        bool insertChildFront(const T& element); //do not call during iteration
        bool insertChildBack(const T& element); //do not call during iteration
        template<class C>
        void removeChild(C& element, u16 maxDepth = gc_maxTreeDepth, IComparator<T, C>& comp); //do not call during iteration
        void removeChildren(); //do not call during iteration
        template<class C>
        CListTreeNode<T>* findChild(C& element, u16 maxDepth = gc_maxTreeDepth, IComparator<T, C>& comp);
        void set(const T& element) {e = element;}
        T& get() {return e;}
        CListTreeNode<T>* getParent() {return parent;}
        CListTreeNode<T>* findRoot();
};

//!List tree node iterator.
template<class T>
class CListTreeNodeIterator
{
protected:
        CListTreeNode<T>* curr;
        CListTreeNode<T>* currChild;
        CListTreeNode<T>* endChild; //never dereference, only assign to or use for comparisons, not necessarily valid when != 0
public:
        CListTreeNodeIterator(CListTreeNode<T>& root) { curr = &root; currChild = 0;}
        
        bool hasNextChild(); //has another child that can be iterated
        bool hasPrevChild(); //has another child that can be iterated
        CListTreeNode<T>* nextChild(); //goes to next child, if any, else wraps around to first child
        CListTreeNode<T>* prevChild(); //goes to next child, if any, else wraps around to first child
        void insertBefore(const T& element);
        void insertAfter(const T& element);
        CListTreeNode<T>* removeAndNextChild(); //remove current child and go to next
        CListTreeNode<T>* removeAndPrevChild(); //remove current child and go to previous
};

///////////////////////////////////TEMPLATE FUNCTION IMPLEMENTATIONS:////////////////////////////////

template<class T>
bool CListTreeNode<T>::insertChildFront(const T& element)
{
        //do not call during iteration
        
        CListTreeNode<T>* n = new CListTreeNode<T>(element);
        ASSERT(n);
        n->parent = this;
        
        if(firstChild == 0) {
                n->next = n;
                n->prev = n;
        } else {
                n->next = firstChild;
                n->prev = firstChild->prev;
                n->prev->next = n;
                n->next->prev = n;
        }
        
        firstChild = n;
}

template<class T>
bool CListTreeNode<T>::insertChildBack(const T& element)
{
        //do not call during iteration
        
        CListTreeNode<T>* n = new CListTreeNode<T>(element);
        ASSERT(n);
        n->parent = this;
        
        if(firstChild == 0) {
                n->next = n;
                n->prev = n;
                firstChild = n;
        } else {
                n->next = firstChild;
                n->prev = firstChild->prev;
                n->prev->next = n;
                n->next->prev = n;
        }
}

template<class T>
template<class C>
void CListTreeNode<T>::removeChild(C& element, u16 maxDepth, IComparator<T, C>& comp)
{
        //do not call during iteration
        
        CListTreeNode<T>* child = findChild(element, maxDepth, comp);
        
        if(child != 0) {
                child->prev->next = child->next;
                child->next->prev = child->prev;
                
                if(child->parent->firstChild == child) {
                        if(child->next == child) {
                                child->parent->firstChild = 0;
                        } else {
                                child->parent->firstChild = child->next;
                        }
                }
                
                ASSERT(child);
                delete child;
        }
}

template<class T>
void CListTreeNode<T>::removeChildren()
{
        //do not call during iteration
        
        if(firstChild == 0) {
                return;
        }
        
        CListTreeNode<T>* it = firstChild;
        
        do {
                CListTreeNode<T>* nextNode = it->next;
                ASSERT(it);
                delete it;
                it = nextNode;
        } while(it != firstChild);
        
        firstChild = 0;
}

template<class T>
template<class C>
CListTreeNode<T>* CListTreeNode<T>::findChild(C& element, u16 maxDepth, IComparator<T, C>& comp)
{
        if(firstChild == 0 || maxDepth == 0) {
                return 0;
        }
        
        CListTreeNode<T>* it = firstChild;
        
        do {
                if(comp.equal(it->e, element)) { //TODO: see if this comparison with the == operator is well
                        return it;
                }
                
                CListTreeNode<T>* n = it->findChild(element, maxDepth - 1, comp);
                if(n != 0) {
                        return n;
                }
                
                it = it->next;
        } while(it != firstChild);
        
        return 0;
}

template<class T>
CListTreeNode<T>* CListTreeNode<T>::findRoot()
{
        CListTreeNode<T>* it = this;
        
        while(it->parent != 0) {
                it = it->parent;
        }
        
        return it;
}

template<class T>
bool CListTreeNodeIterator<T>::hasNextChild()
{
        if(currChild == 0) {
                return curr->firstChild != 0;
        } else {
                return currChild->next != endChild;
        }
}

template<class T>
bool CListTreeNodeIterator<T>::hasPrevChild()
{
        if(currChild == 0) {
                return curr->firstChild == 0 ? 0 : curr->firstChild->prev != 0;
        } else {
                return currChild->prev != endChild;
        }
}

template<class T>
CListTreeNode<T>* CListTreeNodeIterator<T>::nextChild()
{
        if(currChild == 0) {
                currChild = endChild = curr->firstChild;
        } else {
                currChild = currChild->next;
        }
        return currChild;
}

template<class T>
CListTreeNode<T>* CListTreeNodeIterator<T>::prevChild()
{
        if(currChild == 0) {
                if(curr->firstChild != 0) {
                        currChild = curr->firstChild->prev;
                }
                endChild = currChild;
        } else {
                currChild = currChild->prev;
        }
        return currChild;
}

template<class T>
void CListTreeNodeIterator<T>::insertBefore(const T& element)
{
        if(currChild != 0) {
                CListTreeNode<T>* n = new CListTreeNode<T>(element);
                ASSERT(n);
                n->parent = curr;
                n->prev = currChild->prev;
                n->next = currChild;
                
                currChild->prev->next = n;
                currChild->prev = n;
                
                //the following sets new node as first child if it is before (as opposed to last)
                if(currChild == curr.firstChild) {
                        if(endChild == curr->firstChild) {
                                endChild = n;
                        }
                        
                        curr->firstChild = n;
                }
        } else if(curr->firstChild == 0) {
                CListTreeNode<T>* n = new CListTreeNode<T>(element);
                ASSERT(n);
                n->parent = curr;
                n->prev = n;
                n->next = n;
                
                curr->firstChild = n;
        }
}

template<class T>
void CListTreeNodeIterator<T>::insertAfter(const T& element)
{
        if(currChild != 0) {
                CListTreeNode<T>* n = new CListTreeNode<T>(element);
                ASSERT(n);
                n->parent = curr;
                n->prev = currChild;
                n->next = currChild->next;
                
                currChild->next->prev = n;
                currChild->next = n;
        } else if(curr->firstChild == 0) {
                CListTreeNode<T>* n = new CListTreeNode<T>(element);
                ASSERT(n);
                n->parent = curr;
                n->prev = n;
                n->next = n;
                
                curr->firstChild = n;
        }
}

template<class T>
CListTreeNode<T>* CListTreeNodeIterator<T>::removeAndNextChild()
{
        if(currChild != 0) {
                if(currChild->next == currChild) {
                        ASSERT(currChild);
                        delete currChild;
                        curr->firstChild = 0;
                        currChild = 0;
                } else {
                        CListTreeNode<T>* next = currChild->next;
                        currChild->next->prev = currChild->prev;
                        currChild->prev->next = currChild->next;
                        if(currChild == curr->firstChild) {
                                //the reason for checking though the list is circular is so that the
                                //concepts of start and end are retained; else the head of list will move
                                curr->firstChild = next;
                        }
                        if(currChild == endChild) {
                                endChild = next;
                        }
                        ASSERT(currChild);
                        delete currChild;
                        currChild = next;
                }
                
                return currChild;
        }
        
        return 0;
}

template<class T>
CListTreeNode<T>* CListTreeNodeIterator<T>::removeAndPrevChild()
{
        if(currChild != 0) {
                if(currChild->prev == currChild) {
                        ASSERT(currChild);
                        delete currChild;
                        curr->firstChild = 0;
                        currChild = 0;
                } else {
                        CListTreeNode<T>* prev = currChild->prev;
                        currChild->next->prev = currChild->prev;
                        currChild->prev->next = currChild->next;
                        if(currChild == curr->firstChild) {
                                //the reason for checking though the list is circular is so that the
                                //concepts of start and end are retained; else the head of list will move
                                curr->firstChild = prev;
                        }
                        if(currChild == endChild) {
                                endChild = prev;
                        }
                        ASSERT(currChild);
                        delete currChild;
                        currChild = prev;
                }
                
                return currChild;
        }
        
        return 0;
}

} //end of namespace

#endif
