//r2_types.h

//This code is published under the General Public License.

#ifndef R2_TYPES_H
#define R2_TYPES_H

typedef i16 prov_id; //negative = nonvalid province
typedef i16 town_id;
typedef i32 army_id;
typedef i32 char_id; //negative = nonvalid character

#endif //!R2_TYPES_H
