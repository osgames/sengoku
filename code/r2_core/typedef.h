//typedef.h

//This code is published under the General Public License.

#ifndef TYPEDEF_H
#define TYPEDEF_H

#include "r2_geometry.h"

typedef unsigned long u32;
typedef signed long i32;
typedef unsigned short u16;
typedef signed short i16;
typedef unsigned char u8;
typedef signed char i8;
typedef double f64;
typedef float f32;

typedef long ERRNUM;

//here for legacy reasons, write TVector2D<x> and TRect<x> in new code instead
typedef r2::TVector2D<i32> Si32_point;
typedef r2::TVector2D<f32> Sf32_point;
typedef r2::TRect<i32> Si32_rect;
typedef r2::TRect<f32> Sf32_rect;

typedef Si32_point Si32_vector;
typedef Sf32_point Sf32_vector;

/*struct Si32_point
{
	i32 x;
	i32 y;
	
	Si32_point()
	{
                x = 0;
                y = 0;
        }
        
        Si32_point(i32 x, i32 y)
        {
                Si32_point::x = x;
                Si32_point::y = y;
        }
};

struct Sf32_point
{
	f32 x;
	f32 y;
	
	Sf32_point()
	{
                x = 0.0f;
                y = 0.0f;
        }
        
        Sf32_point(f32 x, f32 y)
        {
                Sf32_point::x = x;
                Sf32_point::y = y;
        }
};

struct Si32_rect
{
	i32 left;
	i32 top;
	i32 right;
	i32 bottom;
	
	Si32_rect()
	{
                left = 0;
                top = 0;
                right = 0;
                bottom = 0;
        }
        
        Si32_rect(i32 left, i32 top, i32 right, i32 bottom)
        {
                Si32_rect::left = left;
                Si32_rect::top = top;
                Si32_rect::right = right;
                Si32_rect::bottom = bottom;
        }
};

struct Sf32_rect
{
	f32 left;
	f32 top;
	f32 right;
	f32 bottom;
	
	Sf32_rect()
	{
                left = 0.0f;
                top = 0.0f;
                right = 0.0f;
                bottom = 0.0f;
        }
        
        Sf32_rect(f32 left, f32 top, f32 right, f32 bottom)
        {
                Sf32_rect::left = left;
                Sf32_rect::top = top;
                Sf32_rect::right = right;
                Sf32_rect::bottom = bottom;
        }
};*/

#endif //!TYPEDEF_H
