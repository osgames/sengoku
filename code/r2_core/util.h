//util.h

//This code is published under the General Public License.

#ifndef UTIL_H
#define UTIL_H

#ifndef MAX
#define MAX(x, y)       (x > y? x : y)
#endif

#ifndef MIN
#define MIN(x, y)       (x < y? x : y)
#endif

#endif //!UTIL_H
