//eug_file.cpp

//avoid using this code, we use Lua nowadays

//This code is published under the General Public License.

#include "..\r2_file\eug_file.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\r2_core\r2_log.h"

//the macro below is only used in CNode::SaveChildren()
#define WRITE_INDENT for(int j = 0; j < real_level; j++) \
                     { \
                        stream->write(c_INDENT, c_INDENT_COUNT); \
                     }

namespace EUG
{

CNode::CNode(void)
{
    parent = 0;
    single = false; //this is the default
}

CNode::~CNode(void)
{
    //DO NOT ERASE CHILDREN HERE
}

void CNode::SaveChildren(std::ofstream* stream, int level)
{
    char buffer[0x100];
    double* double_ptr;
    CNode* node_ptr;
    std::string* str_ptr;
    int i, j; //j is used in WRITE_INDENT macro
    int real_level;
    short length;
    //char base_digits;
    bool has_tag;
    
    //evaluate start and ending tags (parts of test removed)
    has_tag = ((children_type == c_NODE) || (!single));

    //adjust indent
    real_level = level;

    //see if the node has a name to write
    if(name != "")
    {
        //write name of node
        WRITE_INDENT;
        stream->write(name.c_str(), strlen(name.c_str()));
        
        //write equal sign
        stream->write(" = ", 3);
    }
    
    //write start tag (if required)
    if(has_tag)
    {
        stream->write("\n", 1);
        WRITE_INDENT;
        stream->write("{\n", 2);
    }
    
    //make sure there are children to save
    if(children.size() != 0)
    {
        //adjust indent level
        real_level = level + 1;
        
        //check what type of children there are
        switch(children_type)
        {
        case c_NODE:
            {
                for(i = 0; i < children.size(); i++)
                {
                    node_ptr = (CNode*) children[i];
                    node_ptr->SaveChildren(stream, real_level);
                }
                break;
            }
        case c_NUMBER:
            {
                for(i = 0; i < children.size(); i++)
                {
                    double_ptr = (double*) children[i];
                    
                    //only write indent if array data
                    if(has_tag)
                        WRITE_INDENT;
                    
                    //convert number to string
                    if(*double_ptr == double(int(*double_ptr)))
                    {
                        //this is an integer
                        sprintf(buffer, "%i", int(*double_ptr));
                        stream->write(buffer, strlen(buffer));
                    }
                    else
                    {
                        //this is a floating point number
                        sprintf(buffer, "%.3f", *double_ptr);
                        stream->write(buffer, strlen(buffer));
                    }
                    
                    //write new line
                    stream->write("\n", 1);
                }
                break;
            }
        case c_ULSTRING:
        {
                for(i = 0; i < children.size(); i++)
                {
                    str_ptr = (std::string*) children[i];
                    
                    //only write indent if array data
                    if(has_tag)
                        WRITE_INDENT;
                    
                    //write string
                    stream->write(str_ptr->c_str(),
                        strlen(str_ptr->c_str()));
                    stream->write("\n", 1);
                }
                break;
        }
        case c_DLSTRING:
            {
                for(i = 0; i < children.size(); i++)
                {
                    str_ptr = (std::string*) children[i];
                    
                    //only write indent if array data
                    if(has_tag)
                        WRITE_INDENT;
                    
                    //write string enclosed between ""
                    stream->write("\"", 1);
                    stream->write(str_ptr->c_str(),
                        strlen(str_ptr->c_str()));
                    stream->write("\"\n", 2);
                }
                break;
            }
        }
    }
    
    real_level = level;
    
    //write end tag
    if(has_tag)
    {
        WRITE_INDENT;
        stream->write("}\n", 2);
    }
}

void CNode::EraseChildren(void)
{
    double* double_ptr;
    CNode* node_ptr;
    std::string* str_ptr;
    int i;
    
    //exit if there are no children
    if(children.size() == 0)
        return;
    
    //check what type of children there are
    switch(children_type)
    {
    case c_NODE:
        {
            for(i = 0; i < children.size(); i++)
            {
                node_ptr = (CNode*) children[i];
                node_ptr->EraseChildren(); //the recursive part
                delete node_ptr;
            }
            break;
        }
    case c_NUMBER:
        {
            for(i = 0; i < children.size(); i++)
            {
                double_ptr = (double*) children[i];
                delete double_ptr;
            }
            break;
        }
    case c_ULSTRING: //is used the same way as c_DLSTRING
    case c_DLSTRING:
        {
            for(i = 0; i < children.size(); i++)
            {
                str_ptr = (std::string*) children[i];
                str_ptr->clear();
                delete str_ptr;
            }
            break;
        }
    }
        
    //clear the vector
    children.clear();
}

void CNode::CopyChildren(CNode* dest_parent, int levels_left)
{
    //this function does not do anything to the parent but to add
    //child nodes
    
    double* double_ptr;
    CNode* node_ptr;
    CNode* child_ptr;
    std::string* str_ptr;
    int i;
    
    //error checking
    if(dest_parent == 0)
        return;
    
    //check what kind of copying should take place
    switch(children_type)
    {
    case c_NODE:
        {
            //make sure we should copy child nodes
            if(levels_left > 0)
            {
                for(i = 0; i < children.size(); i++)
                {
                    //get a pointer to the child of THIS node
                    child_ptr = (CNode*) children[i];
                    
                    //allocate new memory and set characteristics
                    node_ptr = new CNode;
                    node_ptr->parent = dest_parent;
                    node_ptr->name = child_ptr->name;
                    node_ptr->children_type = child_ptr->children_type;
                    node_ptr->loading_state = child_ptr->loading_state;
                    node_ptr->single = child_ptr->single;
                    
                    //add child to parent
                    dest_parent->children.push_back(node_ptr);
                    
                    
                    //the recursive part is based on whether it shall decrease
                    if(levels_left == c_COPYUNLIMITED)
                    {
                        child_ptr->CopyChildren(node_ptr, levels_left);
                    }
                    else
                    {
                        child_ptr->CopyChildren(node_ptr, levels_left - 1);
                    }
                }
            }
            break;
        }
    case c_NUMBER:
        {
            for(i = 0; i < children.size(); i++)
            {
                //create a new number and copy the value of the old to it,
                double_ptr = new double;
                *double_ptr = *(double*) children[i];
                
                //add child to parent
                dest_parent->children.push_back(double_ptr);
            }
            break;
        }
    case c_ULSTRING:
    case c_DLSTRING:
        {
            for(i = 0; i < children.size(); i++)
            {
                //create a new string and copy the value of the old to it
                str_ptr = new std::string;
                *str_ptr = *(std::string*) children[i];
                
                //add child to parent
                dest_parent->children.push_back(str_ptr);
            }
            break;
        }
    }
}

void CNode::SearchChildren(double number,
    std::vector<SSearchResult>* results,
    int levels_left, int level, bool force_integer)
{
    double* double_ptr;
    CNode* node_ptr;
    int i;
    SSearchResult sr;
    
    //error check
    if(results == 0)
        return;
    
    //change behaviour depending on child types
    switch(children_type)
    {
    case c_NODE:
        {
            //loop through the children and note occurances
            for(i = 0; i < children.size(); i++)
            {
                //obtain pointer to child
                node_ptr = (CNode*) children[i];
                
                //check recursive behaviour (is there a limit of depth?)
                if(levels_left == c_SEARCHUNLIMITED)
                {
                    //no limit
                    node_ptr->SearchChildren(number, results,
                        levels_left, level + 1, force_integer);
                }
                else if(levels_left > 0)
                {
                    //limited
                    node_ptr->SearchChildren(number, results,
                        levels_left - 1, level + 1, force_integer);
                }
            }
            break;
        }
    case c_NUMBER:
        {
            //loop through children and note occurances
            for(i = 0; i < children.size(); i++)
            {
                //obtain number pointer
                double_ptr = (double*) children[i];
                
                //check for match with number
                if(force_integer)
                {
                    if(int(*double_ptr) == int(number))
                    {
                        sr.parent = this;
                        sr.child_index = i;
                        sr.level = level;
                        results->push_back(sr);
                    }
                }
                else
                {
                    if(*double_ptr == number)
                    {
                        sr.parent = this;
                        sr.child_index = i;
                        sr.level = level;
                        results->push_back(sr);
                    }
                }
            }
            break;
        }
    } //!switch
}

void CNode::SearchChildren(std::string str,
    std::vector<SSearchResult>* results,
    int levels_left, int level)
{
    CNode* node_ptr;
    std::string* str_ptr;
    int i;
    SSearchResult sr;
    
    //error check
    if(results == 0)
        return;
    
    //change behaviour depending on child types
    switch(children_type)
    {
    case c_NODE:
        {
            //loop through the children and note occurances
            for(i = 0; i < children.size(); i++)
            {
                //obtain pointer to child
                node_ptr = (CNode*) children[i];
                
                //check for match with name
                if(node_ptr->name == str)
                {
                    //add to results
                    sr.parent = node_ptr;
                    sr.child_index = -1;
                    sr.level = level;
                    results->push_back(sr);
                }
                
                //check recursive behaviour (is there a limit of depth?)
                if(levels_left == c_SEARCHUNLIMITED)
                {
                    //no limit
                    node_ptr->SearchChildren(str, results,
                        levels_left, level + 1);
                }
                else if(levels_left > 0)
                {
                    //limited
                    node_ptr->SearchChildren(str, results,
                        levels_left - 1, level + 1);
                }
            }
            break;
        }
    case c_ULSTRING:
    case c_DLSTRING:
        {
            //loop through children and note occurances
            for(i = 0; i < children.size(); i++)
            {
                //obtain string pointer
                str_ptr = (std::string*) children[i];
                
                //check for match with string
                if(*str_ptr == str)
                {
                    sr.parent = this;
                    sr.child_index = i;
                    sr.level = level;
                    results->push_back(sr);
                }
            }
            break;
        }
    }//!switch
}

int CNode::FindFirstNode(std::string str)
{
  if(children_type == c_NODE)
  {
    //find first child with designated name
    for(int i = 0; i < children.size(); i++)
    {
      CNode* currNode = (CNode*) children[i];
      
      //return index of found child
      if(currNode->name == str)
        return i;
    }
  }
  
  //no children found (of node type)
  return -1;
}

CNode* CNode::MakeCopy(void)
{
    //allocate memory and assign characteristcs
    CNode* node_ptr = new CNode;
    node_ptr->parent = parent;
    node_ptr->name = name;
    node_ptr->children_type = children_type;
    node_ptr->loading_state = loading_state;
    node_ptr->single = single;
    
    //copy the children
    CopyChildren(node_ptr, c_COPYUNLIMITED);
    
    return node_ptr;
}

CNode* CNode::CreateChild(std::string name, char new_state)
{
    CNode* new_node;
    
    //create new node and set old one as parent
    new_node = new CNode;
    new_node->parent = this;
    children.push_back((void*) new_node);
    children_type = c_NODE;
    
    //set name
    new_node->name = name;
                                
    //set characteristics
    new_node->children_type = c_UNDETERMINED;
    new_node->loading_state = new_state;
    
    return new_node;
}

void CNode::AddString(char type, std::string str)
{
    std::string* str_ptr;
    
    //allocate memory for string and link it to node
    str_ptr = new std::string;
    children.push_back((void*) str_ptr);
    *str_ptr = str; //this copies the actual string data
                        
    //set characteristics (set regardless if already set)
    children_type = type;
}

void CNode::AddNumber(double number)
{
    double* double_ptr;
    
    //allocate memory for number
    double_ptr = new double;
    *double_ptr = number;
    children.push_back((void*) double_ptr);
                    
    //set child type regardless whether it already is set
    children_type = c_NUMBER;
}

void CTreeReader::SetTree(CNode* treeRoot)
{
  root = treeRoot;
  Reset();
}

void CTreeReader::Reset(void)
{
  actualLevel = 0;
  desiredLevel = 0;
  lockedLevel = -1;
  curr = root;
}
  
u32 CTreeReader::Count(std::string nodeName)
{
  u32 count = 0;
  
  //make sure children are nodes
  if(curr->children_type == c_NODE)
  {
    //step through every child
    for(std::vector<void*>::iterator it = curr->children.begin();
      it != curr->children.end(); it++)
    {
      //increment count if child node's name equals argument
      if(((CNode*) *it)->name == nodeName)
        count++;
    }
  }
  
  return count;
}

bool CTreeReader::In(std::string nodeName, u32 index)
{
  u32 count = 0;
  desiredLevel++;
  
  //make sure children are nodes
  if(curr->children_type == c_NODE)
  {
    //step through every child
    for(std::vector<void*>::iterator it = curr->children.begin();
      it != curr->children.end(); it++)
    {
      //increment count if child node's name equals argument
      CNode* child = (CNode*) *it;
      
      //precaution
      if(child != 0)
      {
        //does it have the right name?
        if(child->name == nodeName)
        {
          //this might be he node we look for
          if(index == count)
          {
            curr = child;
            actualLevel++;
            return true;
          }
          
          //important this is after index check but inside name check!
          count++;
        }
      }
    }
  }
  
  //nothing was found
  return false;
}

void CTreeReader::Out(i32 levels)
{
  if(levels <= 0)
    return;
  
  i32 levelsLeft = levels;
  
  //decrease the number of levels left to skip by an excess of desired levels
  levelsLeft -= desiredLevel - actualLevel;
  if(levelsLeft < 0)
    levelsLeft = 0; //but we shouldn't decrease more than we can
    
  //indicate we lost a number of desired levels
  desiredLevel -= levels;
  if(desiredLevel < 0)
    desiredLevel = 0; //once again, clamp
  
  //calculate the actual target level
  i32 targetLevel = actualLevel - levelsLeft;
  if(targetLevel < 0)
    targetLevel = 0; //clamp at 0
  
  //continue if we need to traverse actual (real) levels
  for(; actualLevel > targetLevel; actualLevel--)
    curr = curr->parent;
}

u32 CTreeReader::CountChildren(char type)
{
  //if type = c_UNDETERMINED we want any non-nodes to be counted
  if(type == c_UNDETERMINED)
  {
    if(curr->children_type != c_NODE)
      return curr->children.size();
    else
      return 0;
  }
  //we want children of a specific type
  else
  {
    if(curr->children_type == type)
      return curr->children.size();
    else
      return 0;
  }
}

void CTreeReader::Lock(void)
{
  lockedLevel = actualLevel;
}

bool CTreeReader::Unlock(void)
{
  bool synched = actualLevel == lockedLevel;
  
  lockedLevel = -1;
  
  return synched;
}

double CTreeReader::GetNumber(u32 index)
{
  //ensure children are of right type and number
  if(curr->children_type == c_NUMBER && curr->children.size() > index)
  {
    //only dereference if non zero...
    if(curr->children[index] != 0)
      return *((double*) curr->children[index]);
  }

  return 0.0; //default
}

std::string CTreeReader::GetULS(u32 index)
{
  //ensure children are of right type and number
  if(curr->children_type == c_ULSTRING && curr->children.size() > index)
  {
    //only dereference if non zero...
    if(curr->children[index] != 0)
      return *((std::string*) curr->children[index]);
  }

  return ""; //default
}

std::string CTreeReader::GetDLS(u32 index)
{
  //ensure children are of right type and number
  if(curr->children_type == c_DLSTRING && curr->children.size() > index)
  {
    //only dereference if non zero...
    if(curr->children[index] != 0)
      return *((std::string*) curr->children[index]);
  }

  return ""; //default
}

bool IParsable::SafeParse(CTreeReader& r, std::string nodeID)
{
  r.Lock();
  bool retval = Parse(r);
  if(!r.Unlock())
  {
    std::string error = "Unsynchronized tree parse at node identifier '";
    error += nodeID;
    error += "'";
    Log_Print(error);
    Log_Print("It could be a code error but it can also be a script error.");
  }
  
  return retval;
}

CFile::CFile(void)
{
    m_root = 0;
}

CFile::~CFile(void)
{
    Destroy();
}

bool CFile::Load(std::string filename)
{
    CNode* current_node;
    
    //notify about loading
    std::cout << "\nLoading " << filename << ".\n";

    //abort if file open fails
    if(!OpenInStream(filename))
        return false;
        
    //zero file-reading variables
    ZeroReadingVars();
    
    //allocate memory for root node
    m_root = new CNode;
    current_node = m_root;
    
    //set up base for root nodes
    current_node->parent = 0;
    current_node->children_type = c_NODE;
    current_node->loading_state = c_HASNOTAG;
    
    //read one character to start with
    GetNextChar();
    
    //reading loop (per line mainly)
    while(m_look != 0)
    {
        m_line++;
        current_node = ReadLine(current_node);
        SkipNewLine();
    }
    
    CloseInStream();
    
    //Tell some things about the current state:
    std::cout << "There were " << m_errors <<
        " errors during loading.\n";
    std::cout << "A total of " << m_bytes_read <<
        " bytes were read.\n";
    
    return true;
}

bool CFile::Save(std::string filename, std::string app_name)
{
    CNode *current_node;
    int i;
    
    //error check
    if(m_root == 0)
        return false;
    
    //abort if file opening fails
    if(!OpenOutStream(filename))
        return false;
    
    //write file name
    m_outs.write("#", 1);
    m_outs.write(filename.c_str(), strlen(filename.c_str()));
    m_outs.write("\n#File generated by ", 20);
    m_outs.write(app_name.c_str(), strlen(app_name.c_str()));
    m_outs.write("\n", 1);
    
    //loop through roots nodes and save them
    for(i = 0; i < m_root->children.size(); i++)
    {
        current_node = (CNode*) m_root->children[i];
        m_outs.write("\n", 1);
        current_node->SaveChildren(&m_outs, 0);
    }
    
    CloseOutStream();
    
    return true;
}

void CFile::Destroy(void)
{
    //only destructs if there is something to destroy
    if(m_root != 0)
    {
        //destroy root node
        m_root->EraseChildren();
        delete m_root;
        m_root = 0;
    }
}

CNode* CFile::ReleaseRootNode(void)
{
    //this basically means the file goes back to a NULL state
    //the generated tree is not stored in memory however
    CNode* save = m_root;
    m_root = 0;
    return save;
}

void CFile::ConstructRootNode(CNode* node)
{
    //kill off any existing tree if present
    Destroy();
    
    //this copies the parameter node and assigns it as a root
    m_root = node->MakeCopy();
}

CNode* CFile::ReadLine(CNode* current)
{
    double number;
    CNode* current_node;
    std::string str;
    
    //link up with the current node
    current_node = current;
    
    //this loop expects that the next character has been read
    while(!IsNewLine(m_look) && (m_look != 0))
    {
        //reason not to use switch here is comparisons are not always constant
        if(IsWhite(m_look))
        {
            SkipWhite();
        }
        else if(m_look == '#')
        {
            SkipComment();
            return current_node; //guaranteed end of line
        }
        else if(IsAlpha(m_look))
        {
            //different allowed state combinations for ALPHA char
            switch(current_node->loading_state)
            {
            case c_HASNOTAG:
                {
                    //the new node derives directly from the current one
                    
                    //read node name and create a new node from it
                    str = ReadULString();
                    current_node =
                        current_node->CreateChild(str, c_OPEN);
                    str.clear();
                    
                    break;
                }
            case c_CLOSED:
                {
                    //this implies a new current node based on last one's
                    //parent should be created

                    //read node name and create a new node from it
                    str = ReadULString();
                    current_node =
                        current_node->parent->CreateChild(str, c_OPEN);
                    str.clear();
                    
                    break;
                }
            case c_HASTAG:
                {
                    //read string data
                    str = ReadULString();
                
                    //check whether we know this is a node/ULS or not
                    switch(current_node->children_type)
                    {
                    case c_UNDETERMINED:
                        {
                            //skip all non-sense characters in search of
                            //an equal sign
                            if(EvaluateNextChar('='))
                            {
                                //if the first valid char was an equal sign
                                //then this was a new node
                                current_node =
                                    current_node->CreateChild(str, c_HASEQUAL);
                                str.clear();
                                GetNextChar();
                            }
                            else
                            {
                                //there was no equal sign so this is UL data
                                current_node->AddString(c_ULSTRING, str);
                                str.clear();
                            }
                            break;
                        }
                    case c_NODE:
                        {
                            //create a node and mark it as open for an equal
                            //sign
                            current_node =
                                current_node->CreateChild(str, c_OPEN);
                            str.clear();
                            break;
                        }
                    case c_ULSTRING:
                        {
                            //create an undefined length string
                            current_node->AddString(c_ULSTRING, str);
                            str.clear();
                            break;
                        }
                    default: //make improved error message later
                        {
                            UnexpectedChar(m_look);
                            GetNextChar();
                            break;
                        }
                    }
                    break;
                }
            case c_HASEQUAL:
                {
                    //this implies the current node links to a simple UL string
                    str = ReadULString();
                    current_node->AddString(c_ULSTRING, str);
                    current_node->single = true;
                    str.clear();
                    
                    //close the node
                    current_node->loading_state = c_CLOSED;
                    current_node = current_node->parent;
                    
                    break;
                }
            default: //make better error msg later
                {
                    UnexpectedChar(m_look);
                    GetNextChar();
                }
            }
        }
        else if(IsDigit(m_look))
        {
            //number must either be single data, or array data and then
            //it must also be of either undetermined or number type
            switch(current_node->loading_state)
            {
            case c_HASEQUAL:
                {
                    //read number from file and add it to tree
                    current_node->AddNumber(ReadNumber());
                    current_node->single = true;
                    
                    //close node
                    current_node->loading_state = c_CLOSED;
                    current_node = current_node->parent;
                    break;
                }
            case c_HASTAG:
                {
                    //interpret as number(but save string)
                    number = ReadNumber(&str);
                    
                    //add any fitting characters to the string
                    str += ReadULString();
                    
                    //behaviour is dependent on what kind of children there are
                    switch(current_node->children_type)
                    {
                    case c_UNDETERMINED:
                        {
                            //skip all non-sense characters in search of
                            //an equal sign
                            if(EvaluateNextChar('='))
                            {
                                //if the first valid char was an equal sign
                                //then this was a new node
                                current_node =
                                    current_node->CreateChild(str, c_HASEQUAL);
                                GetNextChar();
                            }
                            else
                            {
                                //add number
                                current_node->AddNumber(number);
                            }
                            break;
                        }
                    case c_NODE:
                        {
                            //create a node and mark it as open for an equal
                            //sign
                            current_node =
                                current_node->CreateChild(str, c_OPEN);
                            break;
                        }
                    case c_NUMBER:
                        {     
                            //read number, and let node remain open really
                            current_node->AddNumber(number);
                            break;
                        }
                    default:
                        {
                            //emit error message and flush number
                            ErrorMessage("No number expected.");
                            m_errors++;
                            ReadNumber();
                            break;
                        }
                    }
                    str.clear();
                    break;
                }
            case c_HASNOTAG:
                {
                    //the new node below is a child of the current one
                    
                    //read string data (this must be a node on this level)
                    str = ReadULString();
                    
                    //create node, since no data can appear without a tag
                    current_node = current_node->CreateChild(str, c_OPEN);
                    str.clear();
                    
                    break;
                }
            case c_CLOSED:
                {
                    //this implies a new current node based on last one's
                    //parent should be created
                    
                    //read string data (this must be a node on this level)
                    str = ReadULString();
                    
                    //create node, since no data can appear without a tag
                    current_node =
                        current_node->parent->CreateChild(str, c_OPEN);
                    str.clear();
                    
                    break;
                }
            default:
                {
                    //emit error message and flush number
                    ErrorMessage("No number expected.");
                    m_errors++;
                    ReadNumber();
                    break;
                }
            }
        }
        else if(m_look == '-') //negative number
        {
            //number must either be single data, or array data and then
            //it must also be of either undetermined or number type
            if((current_node->loading_state == c_HASEQUAL) ||
                ((current_node->loading_state == c_HASTAG) &&
                ((current_node->children_type == c_UNDETERMINED) ||
                (current_node->children_type == c_NUMBER))))
            {
                //kill minus sign
                GetNextChar();
                
                //make sure a number followed it
                if(IsDigit(m_look))
                {
                    //read number and create it
                    current_node->AddNumber(-ReadNumber());
                    
                    //close the node if it is single data
                    if(current_node->loading_state == c_HASEQUAL)
                    {
                        current_node->single = true;
                        current_node->loading_state = c_CLOSED;
                        current_node = current_node->parent;
                    }
                }
                else
                {
                    ErrorMessage("A number was expected.");
                    m_errors++;
                }
            }
            else
            {
                UnexpectedChar(m_look);
                GetNextChar();
            }
        }
        else if(m_look == '=')
        {
            //this is only valid to open up a node
            if(current_node->loading_state == c_OPEN)
                current_node->loading_state = c_HASEQUAL;
            else
                UnexpectedChar(m_look);
            
            GetNextChar();
        }
        else if(m_look == '{')
        {
            //behaviour is state dependent
            switch(current_node->loading_state)
            {
            case c_HASEQUAL:
                {
                    //after an equal sign the loading state is changed
                    current_node->loading_state = c_HASTAG;
                    break;
                }
            case c_HASTAG:
                {
                    //make sure that this node has nodes as children
                    if((current_node->children_type == c_NODE) ||
                        (current_node->children_type == c_UNDETERMINED))
                    {
                        //if a node a nameless node is created
                        current_node = current_node->CreateChild("", c_HASTAG);
                    }
                    else
                        UnexpectedChar(m_look);
                    break;
                }
            default:
                {
                    UnexpectedChar(m_look);
                    break;
                }
            }
            
            //skip opening tag
            GetNextChar();
        }
        else if(m_look == '}')
        {
            //require that closing tag must come after opening tag
            if(current_node->loading_state == c_HASTAG)
            {
                current_node->loading_state = c_CLOSED;
                current_node = current_node->parent;
            }
            else
                UnexpectedChar(m_look);
            
            //skip end tag
            GetNextChar();
        }
        else if(m_look == '"')
        {
            //DL string must either be single data, or array data and then
            //it must also be of either undetermined or DLS type
            if((current_node->loading_state == c_HASEQUAL) ||
                ((current_node->loading_state == c_HASTAG) &&
                ((current_node->children_type == c_UNDETERMINED) ||
                (current_node->children_type == c_DLSTRING))))
            {
                //read one char to take away the first "
                GetNextChar();
                
                //allocate memory and copy data
                str = ReadDLString();
                current_node->AddString(c_DLSTRING, str);
                str.clear();
                
                //close the node if it is single data
                if(current_node->loading_state == c_HASEQUAL)
                {
                    current_node->single = true;
                    current_node->loading_state = c_CLOSED;
                    current_node = current_node->parent;
                }
                
                //remove the last "
                if(m_look == '"')
                    GetNextChar();
                else
                {
                    ErrorMessage("There is no ending \" of the string");
                    m_errors++;
                }
            }
            else
            {
                UnexpectedChar(m_look);
                GetNextChar();
            }
        }
        else
        {
            UnexpectedChar(m_look);
            GetNextChar();
        }
    }
    
    return current_node;
}

std::string CFile::ReadDLString(void)
{
    std::string result;
    
    result = "";
    
    //important notice - this string does not handle the "" of it
    while((m_look != '"') && !IsNewLine(m_look) && (m_look != 0))
    {
        result += m_look;
        GetNextChar();
    }
    
    return result;
}

std::string CFile::ReadULString(void)
{
    std::string result;
    
    result = "";
    
    //this loop requires that first character of string is already loaded
    while((IsAlpha(m_look) || (m_look == '_') || IsDigit(m_look)) &&
        (m_look != 0))
    {
        result += m_look;
        GetNextChar();
    }
    
    return result;
}

double CFile::ReadNumber(std::string* copy_str)
{
    double number;
    std::string number_str;
    bool decimal_point; //has decimal point been encountered?
    bool add;
    
    //set some vars
    number_str = "";
    decimal_point = false;
    add = true;
    
    //this loop requires that first digit is already loaded
    while((IsDigit(m_look) || (m_look == '.')) && (m_look != 0))
    {
        //check if decimal point is valid
        if(m_look == '.')
            if(decimal_point)
            {
                //add a zero a ignore rest of digits
                number_str += '0';
                add = false;
                break;
            }
            else
            {
                //add zero if there are no digits before point
                if(number_str == "")
                    number_str = '0';
                decimal_point = true;
            }
        
        //add digit or decimal point to string
        if(add)
            number_str += m_look;
        
        //get next digit
        GetNextChar();
    }
    
    //send the string as a copy if needed
    if(copy_str != 0)
        *copy_str = number_str;
    
    //convert from string to double
    number = atof(number_str.c_str());
    number_str.clear();
    
    return number;
}

bool CFile::EvaluateNextChar(char desired)
{
    SkipWhite();
    
    //this tries to emulate the main reading loop,
    //though it has other conditions                 
    while(IsNewLine(m_look) && (m_look != 0))
    {
        SkipNewLine();
        m_line++;
        SkipWhite();
        
        //skip evil comments
        if(m_look == '#')
            SkipComment();
    }
                            
    //return the validity of the final character
    return m_look == desired;
}

} //!namespace EUG
