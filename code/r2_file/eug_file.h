//eug_file.h

//avoid using this code, we use Lua nowadays

//This code is published under the General Public License.

#ifndef EUG_FILE_H
#define EUG_FILE_H

#include "..\r2_core\typedef.h"
#include <string>
#include <vector>
#include "..\r2_file\sc_file.h"

namespace EUG
{

//constants for node children
const char c_UNDETERMINED = 0; //after equal sign
const char c_NODE = 1; //Node
const char c_NUMBER = 2; //double
const char c_ULSTRING = 3; //std::string
const char c_DLSTRING = 4; //std::string

//constants for loading states
const char c_OPEN = 0; //node did just appear
const char c_HASEQUAL = 1; //after equal sign is found
const char c_HASTAG = 2; //used for array data and child nodes
const char c_CLOSED = 3; //everything related to node is finished
const char c_HASNOTAG = 4; //only used for m_roots

//constants for saving
const char c_INDENT[] = "    ";
const char c_INDENT_COUNT = 4;

//constants for node operations
const int c_COPYUNLIMITED = 0x7fffffff;
const int c_SEARCHUNLIMITED = 0x7fffffff;

//class protypes for cross referencing
class CNode;

struct SSearchResult
{
public:
    CNode* parent;
    int child_index; //-1 indicates parent node itself
    int level; //what level related to first node is this
};

class CNode
{
//consider to make some of these protected later
public:
    CNode* parent;
    std::vector<void*> children;
    std::string name;
    char children_type; //c_NODE or others
    char loading_state; //used during loading function
    char single; //force single data
    char fill; //not used
public:
    //construction destruction
    CNode(void);
    ~CNode(void);
    
    //recursive functions
    void SaveChildren(std::ofstream* stream, int level); //level is indentation
    void EraseChildren(void);
    void CopyChildren(CNode* dest_parent,
        int levels_left = c_COPYUNLIMITED); //levels left to copy
    void SearchChildren(double number,
        std::vector<SSearchResult>* results,
        int levels_left = c_SEARCHUNLIMITED,
        int level = 0, bool force_integer = false);
    void SearchChildren(std::string str,
        std::vector<SSearchResult>* results,
        int levels_left = c_SEARCHUNLIMITED,
        int level = 0); //use same algorithm for nodes and strings
    int FindFirstNode(std::string str); //quick node finder
    
    //tree operations
    CNode* MakeCopy(void);
    
    //data manipulation functions
    CNode* CreateChild(std::string name, char new_state = c_CLOSED);
    void AddString(char type, std::string str);
    void AddNumber(double number);
};

/*class CTreeWriter*/

//CEmittable is a base class for classes that use CTreeWriter to represent
//themselves
/*class CEmittable
{
}*/

class CTreeReader
{
protected:
  CTreeReader(void) {} //nobody may touch my balls!
  
  //keep data protected as changing can screw things up majorly
  CNode* root;
  CNode* curr;
  i32 desiredLevel; //must always be >= actualLevel
  i32 actualLevel; //must always be <= desiredLevel
  i32 lockedLevel; //locked at specified level if >= 0

public:
  //setup
  CTreeReader(CNode* treeRoot) {SetTree(treeRoot);}
  void SetTree(CNode* treeRoot); //calls Reset implicitly
  void Reset(void);
  
  //node navigation
  u32 Count(std::string nodeName); //count number of child nodes with name
  bool In(std::string nodeName, u32 index = 0);
  bool Next(std::string nodeName, u32 index = 0)
  {
    Out();
    return In(nodeName, index);
  }
  void Out(i32 levels = 1);
  CNode* CurrentNode(void) {return curr;}
  void ToRoot(void)
  {
    Out(desiredLevel);
  }
  
  //locking prevents leaving a specified level through Out
  void Lock(void);
  bool Unlock(void); //return false if unauthorized leaving was attempted,
   //or if node was left open
  inline bool IsLocked(void) {return lockedLevel >= 0;}
  
  //value extraction
  u32 CountChildren(char type = c_UNDETERMINED); //c_UNDETERMINED = anything
  double GetNumber(u32 index = 0);
  std::string GetULS(u32 index = 0);
  std::string GetDLS(u32 index = 0);
};

//inherting IParsable specifies you can initialize that object from the
//currently open node in the CTreeReader
class IParsable
{
protected:
  virtual bool Parse(CTreeReader& r) = 0;

public:
  bool SafeParse(CTreeReader& r, std::string nodeID);
    //calls Parse but keeps track of starting node and ending node and expects
    //both to be the same, currently doesn't stop errors but logs them,
    //which is useful when program throwing segment faults
};

class CFile : public CSingleCharFile
{
protected:
    CNode* m_root; //this "virtual" node's children are the "real" roots
public:
    CFile(void);
    ~CFile(void);
    bool Load(std::string filename);
    bool Save(std::string filename, std::string app_name);
    void Destroy(void);
    
    //access methods (only use Release/Construct or Get/Append together!)
    CNode* GetRootNode(void) {return m_root;}
    void AppendRootNode(CNode* node) {m_root = node;}
    CNode* ReleaseRootNode(void);
    void ConstructRootNode(CNode* node);
protected:
    //loading routines
    CNode* ReadLine(CNode* current);
    std::string ReadDLString(void); //"*" string
    std::string ReadULString(void); //*delimiter string
    double ReadNumber(std::string* copy_str = 0);
    bool EvaluateNextChar(char desired); //skips all white and new lines
};

} //!namespace EUG

#endif //!EUG_FILE_H
