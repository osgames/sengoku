//sc_file.cpp

//This code is published under the General Public License.

//avoid using this code

#include "sc_file.h"
#include <iostream>
#include <stdlib.h>

bool CSingleCharFile::OpenInStream(std::string filename)
{
    //try to open input file
    m_ins.open(filename.c_str(), std::ios::in | std::ios::binary);
    
    //check if file was opened
    if(m_ins.is_open())
    {
        return true;
    }
    else
    {
        std::cout << "File error: " << filename <<
            "\nThis file could not be opened for input.\n";
        return false;
    }
}

bool CSingleCharFile::OpenOutStream(std::string filename)
{
    //try to open output file
    m_outs.open(filename.c_str(), std::ios::out |
        std::ios::binary | std::ios::trunc);
    
    //check if file was opened
    if(m_outs.is_open())
    {
        return true;
    }
    else
    {
        std::cout << "File error: " << filename <<
            "\nThis file could not be opened for output.\n";
        return false;
    }
}

void CSingleCharFile::GetNextChar(void)
{
    //make a first attempt to read
    m_ins.read(&m_look, sizeof(m_look));
    m_bytes_read++;
    
    //try to get a char
    if(m_ins.eof())
    {
        m_look = 0;
    }
    else if(m_ins.fail() || m_ins.bad())
    {
        //print error message
        std::cout << "File input error at " << m_bytes_read <<
            " bytes.\n";
    }
}

void CSingleCharFile::ErrorMessage(std::string msg)
{
    //prevent too many errors from hiding the initial, often
    //important ones in the console screen
    if(m_errors <= 10)
        std::cout << "Line " << m_line << ": " << msg << "\n";
}

void CSingleCharFile::UnexpectedChar(char c)
{
    std::string str;
    
    //create error message
    str = "Unexpected character \'";
    str += c;
    str += "\'";
            
    //print error and clean up
    ErrorMessage(str);
    str.clear();
            
    //add to errors
    m_errors++;
}
