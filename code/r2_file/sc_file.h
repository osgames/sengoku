//sc_file.h

//This code is published under the General Public License.

//avoid using this code

#ifndef SC_FILE_H
#define SC_FILE_H

#include <string.h>
#include <fstream>

class CSingleCharFile
{
protected:
    unsigned long m_line;
    unsigned long m_errors;
    unsigned long m_bytes_read;
    std::ifstream m_ins;
    std::ofstream m_outs;
    char m_look; //look-ahead character
public:
    //file handling
    bool OpenInStream(std::string filename);
    bool OpenOutStream(std::string filename);
    inline void CloseInStream(void) {m_ins.close();}
    inline void CloseOutStream(void) {m_outs.close();}
    inline void ZeroReadingVars(void)
    {
        m_line = 0;
        m_errors = 0;
        m_bytes_read = 0;
    }
    
    //the most important function
    void GetNextChar(void);
    
    //error routines
    void ErrorMessage(std::string msg);
    void UnexpectedChar(char c);
    
    //static inline utility functions
    static inline bool IsAlpha(char c)
    {
        char str[2];
        
        //create string
        str[0] = c;
        str[1] = 0;
        
        //convert to uppercase
        strupr(str);
        
        return (str[0] >= 'A') && (str[0] <= 'Z');
    }
    
    static inline bool IsDigit(char c)
    {
        return (c >= '0') && (c <= '9');
    }
    
    static inline bool IsAlNum(char c)
    {
        return IsAlpha(c) || IsDigit(c);
    }
    
    static inline bool IsWhite(char c)
    {
        return (c == ' ') || (c == '\t');
    }
    
    static inline bool IsNewLine(char c)
    {
        return (c == '\n') || (c == '\r');
    }
    //!static inline utility functions
    
    //inline utility functions
    inline void SkipWhite(void)
    {
        while(IsWhite(m_look))
            GetNextChar();
    }

    inline void SkipAllNewLines(void) //this used to be SkipNewLine
    {
        while(IsNewLine(m_look))
            GetNextChar();
    }
    inline void SkipNewLine(void)
    {
        bool n_skipped = false;
        bool r_skipped = false;
        
        //only go on while at least 1 new line component is missing
        while(!(n_skipped && r_skipped))
        {
            if(m_look == '\n')
                n_skipped = true;
            else if(m_look == '\r')
                r_skipped = true;
            else
                return; //exit if not a newline characters
            GetNextChar();
        }
    }
    
    inline void SkipComment(void)
    {
        while(!IsNewLine(m_look) && (m_look != 0))
            GetNextChar();
    }
    //!inline utility functions
};

#endif //!SC_FILE_H
