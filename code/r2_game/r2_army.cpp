//r2_army.cpp

//This code is published under the General Public License.

#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_game\r2_game.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_gui\r2_gui.h"

using namespace r2;

CRegiment::CRegiment(luabind::object table)
{
        using namespace luabind;
        
        ASSERT(table);
        ASSERT(type(table) == LUA_TTABLE);
        
        leader = type(table["leader"]) == LUA_TNUMBER ? object_cast<char_id>(table["leader"]) : -1;
        homeTown = type(table["homeTown"]) == LUA_TNUMBER ? object_cast<town_id>(table["homeTown"]) : -1;
        morale = type(table["morale"]) == LUA_TNUMBER ? object_cast<u8>(table["morale"]) : 255;
        quality = type(table["quality"]) == LUA_TNUMBER ? object_cast<u8>(table["quality"]) : 0;
        
        gunners = type(table["gunners"]) == LUA_TNUMBER ? object_cast<u32>(table["gunners"]) : 0;
        ashigaru = type(table["ashigaru"]) == LUA_TNUMBER ? object_cast<u32>(table["ashigaru"]) : 0;
        samurai = type(table["samurai"]) == LUA_TNUMBER ? object_cast<u32>(table["samurai"]) : 0;
        cavalry = type(table["cavalry"]) == LUA_TNUMBER ? object_cast<u32>(table["cavalry"]) : 0;
        monks = type(table["monks"]) == LUA_TNUMBER ? object_cast<u32>(table["monks"]) : 0;
}

CRegiment::CRegiment(char_id leader, town_id home, u8 quality, u32 gun, u32 ash, u32 sam, u32 cav, u32 monk)
{
        CRegiment::leader = leader;
        homeTown = home;
        morale = 10; //consider switching to game constant later
        CRegiment::quality = quality;
        
        gunners = gun;
        ashigaru = ash;
        samurai = sam;
        cavalry = cav;
        monks = monk;
}

CRegiment::~CRegiment()
{
}

void CArmy::indicateChange(CTownDescriptors& descs)
{
        using namespace luabind;
        
        CTownDescriptors::STown* loc = descs.getTown(location);
        ASSERT(loc != 0);
        
        std::string action = !path.empty() ? "moving" : "idle";
        Si32_point facing;
        facingPoint(descs, facing);
        
        /*try {
                call_function<void>(r2::globals.commonLua.state(), "changeArmy",
                        boost::ref(CGame::getInstance()), boost::ref(CGUI::getInstance()),
                        myId, action, loc->mapPos.x, loc->mapPos.y, facing.x, facing.y);
        } catch(error& e) {
                CLua::printError(e);
        }*/
}

void CArmy::facingPoint(CTownDescriptors& descs, Si32_point& p)
{
        if(!path.empty()) {
                CTownDescriptors::STown* next = descs.getTown(*path.begin());
                ASSERT(next != 0);
                
                p = next->mapPos;
        } else {
                CTownDescriptors::STown* loc = descs.getTown(location);
                ASSERT(loc != 0);
        
                p = loc->mapPos;
        }
}

CArmy::CArmy(army_id id, luabind::object table, CTownDescriptors& descs)
{
        using namespace luabind;
        
        ASSERT(table);
        ASSERT(type(table) == LUA_TTABLE);
        
        myId = id;
        battleCount = 0; //could be loaded but it's easier to rebuild when loading battles
        
        name = type(table["name"]) == LUA_TSTRING ? object_cast<std::string>(table["name"]) : "???";
        owner = type(table["owner"]) == LUA_TSTRING ? object_cast<std::string>(table["owner"]) : "";
        leader = type(table["leader"]) == LUA_TNUMBER ? object_cast<char_id>(table["leader"]) : -1;
        location = type(table["location"]) == LUA_TNUMBER ? object_cast<town_id>(table["location"]) : -1;
        daysMoved = type(table["daysMoved"]) == LUA_TNUMBER ? object_cast<u16>(table["daysMoved"]) : 0;
        
        if(type(table["regiments"]) == LUA_TTABLE) {
                for(iterator i(table["regiments"]), end; i != end; ++i) {
                        if(type(*i) == LUA_TTABLE) {
                                units.push_back(new CRegiment(*i));
                                ASSERT(*units.end() != 0);
                        }
                }
        }
        
        indicateChange(descs);
}

CArmy::CArmy(army_id id, const std::string& name, const std::string& owner, town_id location,
        const std::list<CRegiment*>& newUnits, CTownDescriptors& descs)
{
        myId = id;
        CArmy::name = name;
        CArmy::owner = owner;
        CArmy::location = location;
        leader = -1;
        units = newUnits;
        daysMoved = 0;
        battleCount = 0;
        
        indicateChange(descs);
}

CArmy::~CArmy()
{
        for(std::list<CRegiment*>::const_iterator it = units.begin(); it != units.end(); ++it) {
                if(*it != 0) {
                        delete *it;
                }
        }
}

void CArmy::nextDay(CTownDescriptors& descs)
{
        CTownDescriptors::STown* t = descs.getTown(location);
        if(t == 0) {
                return;
        }
        
        //are we moving?
        if(path.size() > 0) {
                daysMoved++;
                
                //check if we moved to new town
                town_id target = *path.begin();
                u16 dist = t->roads.count(target) > 0 ? t->roads[target].dist : 1;
                if(daysMoved >= dist) {
                        daysMoved = 0;
                        location = target;
                        path.pop_front();
                        indicateChange(descs);
                }
        }
}

bool CArmy::verifyMove(town_id toTown, const std::string& clan, CTownDescriptors& descs, CTownPathfinder& pf)
{
        return owner == clan && pf.bestPath(location, toTown, descs, 0, 0);
}

bool CArmy::orderMove(town_id toTown, CTownDescriptors& descs, CTownPathfinder& pf, bool overwritePath)
{
        if(overwritePath) {
                path.clear();
        }
        
        bool retval = pf.bestPath(location, toTown, descs, 0, &path);
        
        indicateChange(descs);
        
        return retval;
}
