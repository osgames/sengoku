//r2_character.h

//needs a cleanup

//This code is published under the General Public License.

#ifndef R2_CHARACTER_H
#define R2_CHARACTER_H

#include <string>
#include <list>
#include "..\r2_core\typedef.h"
#include "..\r2_game\r2_world.h"

class CWorld;

class CCharacter: public EUG::IParsable
{
friend class CWorld;
protected:
  bool Parse(EUG::CTreeReader& r);
  
protected:    
  i32 myID;  
  CWorld* myWorld;
   
  std::string name; 
  i16 clan; 
 // CWorldDate birth;
//  CWorldDate death;
  
  //indice to town of residence or Clan court -NOT DECIDED
  i16 liege; //must be an index to an ACTIVE(alive) clan
  std::vector<i32> sons;
  
  i16 honour;
  f32 storedKoku;
  //designated fief size for retainers
  f32 fiefIncome;
  
  //variable for religion
  
  i16 charisma;
  f32 loyalty;  //0-1
  
  
  i16 adm;
  i16 mil;
  i16 cov;
  i16 dip;
  
  
  
  
  //should the person carry a personal standard (flavour!).
  //appears when the person leads an army. 
  std::string banner_path;
  
  /*
  struct SSkill
  {
    i16 charisma; 
    i16 adm;
    i16 mil;
    i16 cov;
    i16 dip;    
         
  };
  SSkill skills; */
  //traits vector
public:
  CCharacter( void ) { Init();}
  ~CCharacter() {Destroy();} 
  void Init();
  void Destroy();
  //AddTrait
  //RemoveTrat(trait)
    
  
  std::string GetName() const { return name; }
  std::string GetFullName();
  i32 GetID() const { return myID; } 
  void AddFullIncome(void); 
  void AddIncome(f32 koku); 
  f32 GetIncome() const { return fiefIncome; }
  i16 GetLiege() const { return liege; }
  i16 GetClan() const { return clan; }
      
};




#endif
