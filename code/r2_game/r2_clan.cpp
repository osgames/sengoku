//r2_clan.cpp

//This code is published under the General Public License.

#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_log.h"

using namespace r2;

void CClanDescriptors::init(CLua& lua)
{
        using namespace luabind;
        
        ASSERT(lua.state() !=  0);
        
        object clans(luabind::globals(lua.state())["clans"]);
        if(type(clans) != LUA_TTABLE) {
                LOG_ERROR("Clan table missing");
                return;
        }
        
        //iterate through all items inside "clans" table
        for(iterator i(clans), end; i != end; ++i) {
                //clan needs to be table and have a name to be valid
                if(type(*i) == LUA_TTABLE && type((*i)["name"]) == LUA_TSTRING) {
                        std::string tag = object_cast<std::string>(i.key());
                        if(clanDescs.count(tag) > 0) {
                                LOG_ERROR("Duplicate clan tag detected: " << tag);
                                return;
                        }
                        
                        SClan* desc = new SClan;
                        ASSERT(desc != 0);
                                
                        desc->name = object_cast<std::string>((*i)["name"]);
                        desc->palette = type((*i)["palette"]) == LUA_TSTRING?
                                object_cast<std::string>((*i)["palette"]) : "";
                                        
                        clanDescs[tag] = desc;
                }
        }
}

CClanDescriptors::CClanDescriptors(const std::string& filename)
{
        CLua lua;
        if(!lua.doFile(filename)) {
                return;
        }
        
        init(lua);
}

CClanDescriptors::~CClanDescriptors()
{
        for(std::map<std::string, SClan*>::const_iterator it = clanDescs.begin(); it != clanDescs.end();
                ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
}

CClanDescriptors::SClan* CClanDescriptors::getClan(const std::string& clan)
{
        return clanDescs.count(clan) > 0 ? clanDescs[clan] : 0;
}

CClan::CClan(luabind::object table)
{
        using namespace luabind;
        
        //defaults
        liege = "";
        lord = -1;
        capital = -1;
        
        if(table && type(table) == LUA_TTABLE) {
                for(iterator j(table["ownedProvinces"]), end; j != end; ++j) {
                        if(type(*j) == LUA_TNUMBER) {
                                prov_id id = object_cast<prov_id>(*j);
                                if(id >= 0) {
                                        ownedProvinces.push_back(id);
                                }
                        }
                }
                for(iterator i(table["controlledProvinces"]), end; i != end; ++i) {
                        if(type(*i) == LUA_TNUMBER) {
                                prov_id id = object_cast<prov_id>(*i);
                                if(id >= 0) {
                                        controlledProvinces.push_back(id);
                                }
                        }
                }
        }
}

CClan::~CClan()
{
}

prov_id CClan::getOwnedProvince(u32 index)
{
        ASSERT(index >= 0 && index < ownedProvinces.size());
        return ownedProvinces[index];
}

prov_id CClan::getControlledProvince(u32 index)
{
        ASSERT(index >= 0 && index < controlledProvinces.size());
        return controlledProvinces[index];
}

/*bool CClan::Parse(EUG::CTreeReader& r)
{
  if(r.In("name"))
  {
    name = r.GetDLS();
  }
  if(r.Next("alive"))
  {
    alive = r.GetULS() == "yes";
  }
  if(r.Next("palette"))
  {
    paletteName = r.GetDLS();
  }
  if(r.Next("ownedprovinces"))
  {
    u32 ownedCount = r.CountChildren();
    for(u32 owned = 0; owned < ownedCount; owned++)
    {
      i16 provIndex = (i16) r.GetNumber(owned);
      if(provIndex >= 0 && provIndex < GAMEPROVINCE_COUNT)
      {
        myWorld->provs[provIndex].owner = myID;
        ownedProvinces.push_back(provIndex);
        //it may be possible more should be added at an appropiate location later
      }
    }
  }
  if(r.Next("controlledprovinces"))
  {
    u32 controlledCount = r.CountChildren();
    for(u32 controlled = 0; controlled < controlledCount; controlled++)
    {
      i16 provIndex = (i16) r.GetNumber(controlled);
      if(provIndex >= 0 && provIndex < GAMEPROVINCE_COUNT)
      {
        myWorld->provs[provIndex].controller = myID;
        controlledProvinces.push_back(provIndex);
        //it may be possible more should be added at an appropiate location later
      }
    }
  }
  if(r.Next("ownedtowns"))
  {
    u32 ownedCount = r.CountChildren();
    for(u32 owned = 0; owned < ownedCount; owned++)
    {
      i16 townIndex = (i16) r.GetNumber(owned);
      if(townIndex >= 0 && townIndex < GAMEPROVINCE_COUNT)
      {
        //myWorld->towns[townIndex].owner = myID;
        
        //HAVE NOT ADDED ALL FLAGS YET, USE THIS TEMPORARILY
        i32 ii=7;
        if (myID<20)
        {     
            ii = 8;  
        }
        else if (myID<40)
        {     
            ii = 9;  
        }
        else if (myID<60)
        {     
            ii = 10;  
        }
        else if (myID<80)
        {     
            ii = 11;  
        }
        else if (myID<100)
        {     
            ii = 12;  
        }
        //myWorld->towns[townIndex].flagSprite = ii;
        
        //myWorld->towns[townIndex].flagSprite = myID+FLAGID_START;
        
        ownedTowns.push_back(townIndex);
        //it may be possible more should be added at an appropiate location later
      }
    }
  }
  if(r.Next("controlledtowns"))
  {
    u32 controlledCount = r.CountChildren();
    for(u32 controlled = 0; controlled < controlledCount; controlled++)
    {
      i16 townIndex = (i16) r.GetNumber(controlled);
      if(townIndex >= 0 && townIndex < GAMEPROVINCE_COUNT)
      {
        //myWorld->towns[townIndex].controller = myID;
        controlledTowns.push_back(townIndex);
        //it may be possible more should be added at an appropiate location later
      }
    }
  }
  if(r.Next("koku"))
  {
    storedKoku = (f32) r.GetNumber();
  }
  if(r.Next("capital"))
  {
    capital = (i16) r.GetNumber();
  }
  if(r.Next("lord"))
  {
    lord = (i32) r.GetNumber();
  }
  
  r.Out();
        
  return true;
}*/

