//r2_game.cpp

//This code is published under the General Public License.

#include "..\r2_game\r2_game.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_graphics\r2_cursor.h"
#include "..\r2_input\r2_input.h"
#include "..\r2_core\r2_gamestate.h"
#include <stdlib.h>
#include "..\r2_core\r2_globals.h"
#include "..\r2_core\r2_filepaths.h"
#include "..\r2_gui\r2_gui.h"

using namespace r2;

bool CGame::init(void)
{
        ASSERT(!inited);
  
        inited = true;
  
        return initLua();
}

bool CGame::initLua()
{
        using namespace luabind;
        
        if(r2::globals.commonLua.state() == 0) {
                return false;
        }
        
        bindFunctionsWithLua(r2::globals.commonLua);
        world.bindFunctionsWithLua(r2::globals.commonLua);
        
        if(!r2::globals.commonLua.doFile(FILE_SCRIPTS_GAME)) {
                return false;
        }
        
        return true;
}

void CGame::destroy(void)
{
        disconnect();
}

void CGame::processServerChatNetcom(CNetcom& netcom)
{
        switch(netcom.getType()) {
                case NETCOMTYPE_CHAT_PUBLIC: {
                        //check length send pass on message
                        std::string sentence;
                        netcom.readString(sentence);
                        if(sentence.size() <= CHAT_MAX_LEN) {
                                //send a response which includes source user
                                CNetcom* netcom2;
                                server.createWritableNetcom(&netcom2, NETCOMTYPE_CHAT_PUBLIC,
                                        1 + (sentence.size() + 1));
                                netcom2->writeInteger(netcom.fromUser());
                                netcom2->writeString(sentence);
                                server.sendNetcom(&netcom2, 0, 0, NETCHANNEL_CHAT);
                        }
                        break;
                }
        }
}

void CGame::processClientChatNetcom(CNetcom& netcom)
{
        using namespace luabind;
        
        switch(netcom.getType()) {
                case NETCOMTYPE_CHAT_PUBLIC: {
                        u8 user;
                        netcom.readInteger(user);
                        std::string sentence;
                        netcom.readString(sentence);
                        
                        try {
                                call_function<void>(r2::globals.commonLua.state(), "clientChatMessage",
                                        boost::ref(*this), boost::ref(CGUI::getInstance()),
                                        user + 1, sentence); //to Lua base 1
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                        break;
                }
        }
}

void CGame::processServerSelectClan(u8 user, const std::string& tag)
{
        if(tag != "") {
                //only continue if tag (clan) isn't already selected
                for(u8 i = 0; i < NETUSER_MAX; i++) {
                        if(server.isUserAuthorized(i) && serverVars.clanSelections[i] == tag) {
                                return;
                        }
                }
                
                //check that tag is valid
                bool valid = false;
                for(std::vector<std::string>::iterator it = preloadClans.begin();
                                it != preloadClans.end(); it++) {
                        if(*it == tag) {
                                valid = true;
                                break;
                        }
                }
                
                if(!valid) {
                        return;
                }
        } else if(serverVars.clanSelections[user] == "") { //&& tag == "" <=> clanSels[user] == tag
                //tag is a "deactivation tag", but it means we don't want spam if a client sends unnecessary
                //deactivations
                return;
        }
        
        serverVars.clanSelections[user] = tag;
        
        //send tag to everyone (whether activation or deactivation)
        CNetcom* netcom;
        server.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_SELECTCLAN, 1 + (tag.size() + 1));
        netcom->writeInteger(user);
        netcom->writeString(tag);
        server.sendNetcom(&netcom, 0, 0, NETCHANNEL_WORLD);
}

void CGame::processServerMoveArmies(u8 user, town_id toTown, const std::list<army_id>& armies,
        bool replacePath)
{
        std::list<army_id> verified;
        
        for(std::list<army_id>::const_iterator it = armies.begin(); it != armies.end(); ++it) {
                if(world.verifyArmyMove(serverVars.clanSelections[user], toTown, *it)) {
                        verified.push_back(*it);
                }
        }
        
        if(verified.size() == 0) {
                return;
        }
        
        CNetcom* netcom;
        server.createWritableNetcom(&netcom, replacePath ? NETCOMTYPE_WORLD_REPLACEMOVEARMIES :
                NETCOMTYPE_WORLD_ADDMOVEARMIES,
                sizeof(town_id) + sizeof(army_id) * verified.size());
        netcom->writeInteger(toTown);
        for(std::list<army_id>::const_iterator it = verified.begin(); it != verified.end(); ++it) {
                netcom->writeInteger(*it);
        }
        server.sendNetcom(&netcom, 0, 0, NETCHANNEL_WORLD);
}

void CGame::processServerWorldNetcom(CNetcom& netcom)
{
        switch(netcom.getType()) {
                case NETCOMTYPE_WORLD_SELECTCLAN: {
                        std::string clan;
                        netcom.readString(clan);
                        processServerSelectClan(netcom.fromUser(), clan);
                        break;
                } case NETCOMTYPE_WORLD_ACKNOWLEDGE: {
                        serverVars.ack[netcom.fromUser()] = true;
                        
                        if(!serverVars.isGreen) {
                                serverVars.isGreen = everyoneAcknowledged() && scenarioLoaded;
                        }
                        break;
                } case NETCOMTYPE_WORLD_ADDMOVEARMIES:
                case NETCOMTYPE_WORLD_REPLACEMOVEARMIES: {
                        if(scenarioLoaded) {
                                town_id town;
                                netcom.readInteger(town);
                                std::list<army_id> armyList;
                                
                                while(netcom.bytesLeft() > 0) {
                                        army_id army;
                                        netcom.readInteger(army),
                                        armyList.push_back(army);
                                }
                                processServerMoveArmies(netcom.fromUser(), town, armyList,
                                        netcom.getType() == NETCOMTYPE_WORLD_REPLACEMOVEARMIES);
                        }
                        break;
                }
        }
}

void CGame::processClientWorldNetcom(CNetcom& netcom)
{
        using namespace luabind;
        
        switch(netcom.getType()) {
                case NETCOMTYPE_WORLD_SELECTSCENARIO: {
                        std::string scen;
                        netcom.readString(scen);
                        try {
                                call_function<void>(r2::globals.commonLua.state(), "clientScenarioChange",
                                        boost::ref(*this), boost::ref(CGUI::getInstance()), scen);
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                        break;
                } case NETCOMTYPE_WORLD_SELECTCLAN: {
                        try {
                                u8 user;
                                netcom.readInteger(user);
                                std::string tag;
                                netcom.readString(tag);
                                call_function<void>(r2::globals.commonLua.state(), "clientClanChange",
                                        boost::ref(*this), boost::ref(CGUI::getInstance()),
                                        user + 1, tag); //to Lua base 1
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                        break;
                } case NETCOMTYPE_WORLD_STARTPLAY: {
                        try {
                                call_function<void>(r2::globals.commonLua.state(), "clientStartPlay",
                                        boost::ref(*this), boost::ref(CGUI::getInstance()));
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                        break;
                } case NETCOMTYPE_WORLD_NEXTDAY: {
                        if(scenarioLoaded) {
                                world.nextDay();
                                try {
                                        call_function<void>(r2::globals.commonLua.state(), "onNewDay",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()),
                                                boost::ref(world));
                                } catch(error& e) {
                                        CLua::printError(e);
                                }
                        } else {
                                LOG_ERROR("Server tries to make client go to next day without init");
                        }
                        break;
                } case NETCOMTYPE_WORLD_REPLACEMOVEARMIES:
                case NETCOMTYPE_WORLD_ADDMOVEARMIES: {
                        if(scenarioLoaded) {
                                town_id town;
                                netcom.readInteger(town);
                                
                                while(netcom.bytesLeft() > 0) {
                                        army_id army;
                                        netcom.readInteger(army),
                                        world.orderArmyMove(town, army,
                                                netcom.getType() == NETCOMTYPE_WORLD_REPLACEMOVEARMIES);
                                }
                                
                        }
                        break;
                }
        }
}

void CGame::resetServerVars()
{
        serverVars.scenarioName = "";
        for(u8 i = 0; i < NETUSER_MAX; i++) {
                serverVars.clanSelections[i] = "";
        }
        serverVars.isGreen = false;
}

void CGame::update(f32 dt)
{
        using namespace luabind;
        
        server.run(dt);
        client.run(dt);
        
        //server events
        for(SNetworkEvent* ev; (ev = server.currentEvent()) != 0; server.nextEvent()) {
                switch(ev->type) {
                        case NETEVENTTYPE_SERVER_CLIENTCONNECTED: {
                                //set server vars regarding user
                                serverVars.clanSelections[ev->id] = "";
                                
                                //send state info of server (ie. state of scenario selection and game settings)
                                if(serverVars.scenarioName != "") {
                                        CNetcom* netcom;
                                        server.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_SELECTSCENARIO,
                                                serverVars.scenarioName.size() + 1);
                                        netcom->writeString(serverVars.scenarioName);
                                        server.sendNetcom(&netcom, &ev->id, 1, NETCHANNEL_WORLD);
                                        
                                }
                                for(u8 i = 0; i < NETUSER_MAX; i++) {
                                        if(server.isUserAuthorized(i) && i != ev->id) {
                                                CNetcom* netcom;
                                                server.createWritableNetcom(&netcom,
                                                NETCOMTYPE_WORLD_SELECTCLAN,
                                                1 + (serverVars.clanSelections[i].size() + 1));
                                                netcom->writeInteger(i);
                                                netcom->writeString(serverVars.clanSelections[i]);
                                                server.sendNetcom(&netcom, &ev->id, 1, NETCHANNEL_WORLD);
                                        }
                                }
                                break;
                        }
                }
        }
        
        //client events
        for(SNetworkEvent* ev; (ev = client.currentEvent()) != 0; client.nextEvent()) {
                try {
                        switch(ev->type) {
                                case NETEVENTTYPE_CLIENT_CONNECTIONFAILED: {
                                        call_function<void>(r2::globals.commonLua.state(), "clientConnectionFailed",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()));
                                        break;
                                } case NETEVENTTYPE_CLIENT_DISCONNECTED: {
                                        call_function<void>(r2::globals.commonLua.state(), "clientDisconnected",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()));
                                        break;
                                } case NETEVENTTYPE_CLIENT_CONNECTED: {
                                        call_function<void>(r2::globals.commonLua.state(), "clientConnected",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()));
                                        break;
                                } case NETEVENTTYPE_CLIENT_OTHERCONNECTED: {
                                        call_function<void>(r2::globals.commonLua.state(), "clientUserAdded",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()),
                                                ev->id + 1, ev->name); //to lua base 1
                                        break;
                                } case NETEVENTTYPE_CLIENT_OTHERDISCONNECTED: {
                                        call_function<void>(r2::globals.commonLua.state(), "clientUserRemoved",
                                                boost::ref(*this), boost::ref(CGUI::getInstance()),
                                                ev->id + 1); //to lua base 1
                                        break;
                                }
                        }
                } catch(error& e) {
                        CLua::printError(e);
                }
        }
        
        //server netcoms
        while(server.countNetcoms() > 0) {
                CNetcom* netcom = server.receiveNextNetcom();
                ASSERT(netcom != 0);
                
                switch(netcom->onChannel()) {
                        case NETCHANNEL_CHAT:
                                processServerChatNetcom(*netcom);
                                break;
                        case NETCHANNEL_WORLD:
                                processServerWorldNetcom(*netcom);
                                break;
                }
                
                server.destroyNetcom(&netcom);
        }
        
        //client netcoms
        while(client.countNetcoms() > 0) {
                CNetcom* netcom = client.receiveNextNetcom();
                ASSERT(netcom != 0);
                
                switch(netcom->onChannel()) {
                        case NETCHANNEL_CHAT:
                                processClientChatNetcom(*netcom);
                                break;
                        case NETCHANNEL_WORLD:
                                processClientWorldNetcom(*netcom);
                                break;
                }
                
                client.destroyNetcom(&netcom);
        }
  
        if(scenarioLoaded) {
                //Game.lua is already loaded by now (done in init), so we only need to call appropiate function:
                try {
                        call_function<void>(r2::globals.commonLua.state(), "onUpdate", boost::ref(*this),
                                boost::ref(CGUI::getInstance()), dt);
                } catch(error& e) {
                        CLua::printError(e);
                }
        }
}

void CGame::bindFunctionsWithLua(CLua& l)
{
        using namespace luabind;
        
        if(l.state() == 0) {
                return;
        }
        
        module(l.state())
        [
                class_ <CGame>("CGame")
                .def("preloadScenario", &CGame::preloadScenario)
                .property("preloadClanCount", &CGame::preloadClanCount)
                .def("getPreloadClanTag", &CGame::getPreloadClanTag)
                .def("selectClan", &CGame::selectClan)
                .def("initSingleServerAndClient", &CGame::initSingleServerAndClient)
                .def("initMultiServerAndClient", &CGame::initMultiServerAndClient)
                .def("initConnectedClient", &CGame::initConnectedClient)
                .def("disconnect", &CGame::disconnect)
                .def("startPlaying", &CGame::startPlaying)
                .def("proposeNextDay", &CGame::proposeNextDay)
                .def("isGreen", &CGame::isGreen)
                .def("requestArmyMoves", &CGame::requestArmyMoves)
                .def("chatPublic", &CGame::chatPublic)
                .def("selectScenario", &CGame::selectScenario)
                .def("loadScenario", &CGame::loadScenario)
        ];
}

bool CGame::preloadScenario(const std::string& filename)
{
        using namespace luabind;
        
        CLua lua;
        
        ASSERT(lua.state() != 0);
        
        if(!lua.doFile(filename)) {
                return false;
        }
        
        object header(luabind::globals(lua.state())["header"]);
        if(type(header) == LUA_TTABLE && type(header["selectable"]) == LUA_TTABLE) {
                //iterate through all selectable clans in header
                for(iterator it(header["selectable"]), end; it != end; ++it) {
                        if(type(*it) == LUA_TSTRING) {
                                preloadClans.push_back(object_cast<std::string>(*it));
                        }
                }
        }
        
        return true;
}

void CGame::selectClan(const std::string& tag)
{
        if(client.isInited()) {
                CNetcom* netcom;
                client.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_SELECTCLAN, tag.size() + 1);
                netcom->writeString(tag);
                client.sendNetcom(&netcom, NETCHANNEL_WORLD);
        }
}

bool CGame::initSingleServerAndClient()
{
        if(server.isInited() || client.isInited()) {
                return false;
        }
        
        if(!server.initSingle(client) || !client.initSingle(server)) {
                return false;
        }
        
        resetServerVars();
        
        return true;
}

bool CGame::initMultiServerAndClient(const std::string& port, const std::string& password,
        const std::string& userName)
{
        if(server.isInited() || client.isInited()) {
                return false;
        }
        
        if(!server.initMulti(port, password, userName)) {
                return false;
        }
        
        std::string localhost("localhost");
        if(!client.initConnect(localhost, port, password, userName)) {
                server.destroy();
                return false;
        }
        
        resetServerVars();
        
        return true;
}

bool CGame::initConnectedClient(const std::string& ip, const std::string& port,
        const std::string& password, const std::string& userName)
{
        if(server.isInited() || client.isInited()) {
                return false;
        }
        
        return client.initConnect(ip, port, password, userName);
}

void CGame::disconnect()
{
        client.destroy();
        server.destroy();
}

bool CGame::startPlaying()
{
        if(!server.isInited()) {
                return false;
        }
        
        for(u8 i = 0; i < NETUSER_MAX; ++i) {
                if(server.isUserAuthorized(i) && serverVars.clanSelections[i] == "") {
                        return false;
                }
        }
        
        CNetcom* netcom;
        server.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_STARTPLAY, 0);
        server.sendNetcom(&netcom, 0, 0, NETCHANNEL_WORLD);
        
        for(u8 i = 0; i < NETUSER_MAX; ++i) {
                serverVars.ack[i] = false;
        }
        
        return true;
}

bool CGame::proposeNextDay()
{
        if(!server.isInited() || !serverVars.isGreen || !scenarioLoaded || !everyoneAcknowledged()) {
                return false;
        }
        
        CNetcom* netcom;
        server.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_NEXTDAY, 0); //consider changing this to include a timestamp
        server.sendNetcom(&netcom, 0, 0, NETCHANNEL_WORLD);
        
        return true;
}

bool CGame::isGreen()
{
        return server.isInited() && serverVars.isGreen;
}

bool CGame::requestArmyMoves(town_id toTown, luabind::object armyIds, bool replacePath)
{
        using namespace luabind;
        
        if(client.isInited() && armyIds && type(armyIds) == LUA_TTABLE) {
                std::list<army_id> ids;
                //count id:s
                for(iterator i(armyIds), end; i != end; ++i) {
                        if(type(*i) == LUA_TNUMBER) {
                                ids.push_back(object_cast<army_id>(*i));
                        }
                }
                
                CNetcom* netcom;
                client.createWritableNetcom(&netcom, replacePath ? NETCOMTYPE_WORLD_REPLACEMOVEARMIES :
                        NETCOMTYPE_WORLD_ADDMOVEARMIES,
                        sizeof(town_id) + sizeof(army_id) * ids.size());
                netcom->writeInteger(toTown);
                for(std::list<army_id>::const_iterator it = ids.begin(); it != ids.end(); ++it) {
                        netcom->writeInteger(*it);
                }
                client.sendNetcom(&netcom, NETCHANNEL_WORLD);
        }
        
        return false;
}

bool CGame::everyoneAcknowledged()
{
        for(u8 i = 0; i < NETUSER_MAX; ++i) {
                if(server.isUserAuthorized(i) && !serverVars.ack[i]) {
                        return false;
                }
        }
        return true;
}

void CGame::chatPublic(const std::string& sentence)
{
        if(client.isInited() && sentence != "") {
                CNetcom* netcom;
                client.createWritableNetcom(&netcom, NETCOMTYPE_CHAT_PUBLIC, sentence.size() + 1);
                netcom->writeString(sentence);
                client.sendNetcom(&netcom, NETCHANNEL_CHAT);
        }
}

void CGame::selectScenario(const std::string& name)
{
        if(server.isInited() && name != "") {
                serverVars.scenarioName = name;
                
                CNetcom* netcom;
                server.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_SELECTSCENARIO, name.size() + 1);
                netcom->writeString(name);
                server.sendNetcom(&netcom, 0, 0, NETCHANNEL_WORLD);
        }
}

bool CGame::loadScenario(const std::string& filename)
{
        using namespace luabind;
        
        if(!world.loadFromFile(filename)) {
                //send drop to server here
                return false;
        }
        
        //Game.lua is already loaded by now (done in init), so we only need to call appropiate function:
        try {
                call_function<void>(r2::globals.commonLua.state(), "onLoadScenario", boost::ref(*this),
                        boost::ref(CGUI::getInstance()), boost::ref(world));
        } catch(error& e) {
                CLua::printError(e);
                return false;
        }
        
        //send acknowledgement to server (if applicable)
        if(client.isInited()) {
                CNetcom* netcom;
                client.createWritableNetcom(&netcom, NETCOMTYPE_WORLD_ACKNOWLEDGE, 0);
                client.sendNetcom(&netcom, NETCHANNEL_WORLD);
        }
        
        scenarioLoaded = true;
        
        return true;
}

u32 CGame::preloadClanCount() const
{
        return preloadClans.size();
}

std::string CGame::getPreloadClanTag(u32 index)
{
        if(index >= 1 && index <= preloadClans.size()) {
                return preloadClans[index - 1];
        }
        
        return "INVALID INDEX";
}
