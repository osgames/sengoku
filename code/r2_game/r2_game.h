//r2_game.h

//This code is published under the General Public License.

#ifndef R2_GAME_H
#define R2_GAME_H

#include "..\r2_core\typedef.h"
#include <string>
#include <map>
#include <list>
#include <memory>

#include "..\r2_graphics\r2_provrle.h"
#include "..\r2_gui\r2_provmap.h"
#include "..\r2_graphics\r2_font.h"
#include "..\r2_game\r2_world.h"
#include "..\r2_gui\r2_textbox.h"
#include "..\r2_network\r2_network.h"
#include "..\r2_core\r2_singleton.h"
#include "..\r2_core\r2_lua_manager.h"

#define NETCOMTYPE_CHAT_PUBLIC                  0

#define NETCOMTYPE_WORLD_SELECTSCENARIO         0
#define NETCOMTYPE_WORLD_SELECTCLAN             1
#define NETCOMTYPE_WORLD_STARTPLAY              2
#define NETCOMTYPE_WORLD_ACKNOWLEDGE            3
#define NETCOMTYPE_WORLD_NEXTDAY                4
#define NETCOMTYPE_WORLD_ADDMOVEARMIES          5
#define NETCOMTYPE_WORLD_REPLACEMOVEARMIES      6

#define CHAT_MAX_LEN    127

namespace r2 {

class CGame : public r2::CSingleton<CGame>
{
friend class r2::CSingleton<CGame>;
private: //attributes
        CServer server;
        CClient client;
        CWorld world;
        
        struct
        {
                std::string scenarioName;
                std::string clanSelections[NETUSER_MAX];
                bool ack[NETUSER_MAX];
                bool isGreen;
        } serverVars;
        
        std::vector<std::string> preloadClans;
  
        bool inited;
        bool scenarioLoaded;

private: //functions
        //contructor hidden because of singleton
        CGame() {inited = false;}
        
        //!Inits the Lua VM.
        bool initLua();
        
        //Various general netcom processing routines:
        void processServerChatNetcom(CNetcom& netcom);
        void processClientChatNetcom(CNetcom& netcom);
        void processServerWorldNetcom(CNetcom& netcom);
        void processClientWorldNetcom(CNetcom& netcom);
        
        //!Simple function which determines if all users have sent acknowledgements.
        bool everyoneAcknowledged();
        //!Sets all server vars to initial state.
        void resetServerVars();
        
        //Specific processing routines:
        //!Processes a user request to change clan.
        void processServerSelectClan(u8 user, const std::string& tag);
        //!Processes a user request to move armies.
        void processServerMoveArmies(u8 user, town_id toTown, const std::list<army_id>& armies,
                bool replacePath);
  
        //palette handling
        r2::SPalette& GetRegularPalette(u16 prov);
        void ModifyToSelectionPalette(r2::SPalette& pal);
        void UpdateAllPalettes(void);
        void SetProvincialPalette(u16 gameProv, r2::SPalette& pal);
public:
        ~CGame() {destroy();}
  
        //Init and Destroy mostly bind/unbind relevant resources and initialize objects:
        bool init(void);
        void destroy(void);
        
        inline CWorld& getWorld() {return world;}
  
        //!The update function.
        void update(f32 dt);
public: //lua functions
        //!Exposes functions to be accessible during run time to a Lua VM.
        static void bindFunctionsWithLua(r2::CLua& l);
        
        //Preload scenario routines:
        //!Attempts to preload scenario data.
        bool preloadScenario(const std::string& filename);
        //!Returns the number of clans preloaded.
        u32 preloadClanCount() const;
        //!Returns tag of a preloaded clan. Index is 1 <= index <= count.
        std::string getPreloadClanTag(u32 index);
        //!Sends a request from client to server to client clan.
        void selectClan(const std::string& tag);
        
        //Network management routines:
        //!Creates a local server and a local client.
        bool initSingleServerAndClient();
        //!Creates a multiplayer server and a client which connects locally to it.
        bool initMultiServerAndClient(const std::string& port, const std::string& password,
                const std::string& userName);
        //!Creates a client which connects to a remote multiplayer server.
        bool initConnectedClient(const std::string& ip, const std::string& port,
                const std::string& password, const std::string& userName);
        //!Disconnects client and/or server.
        void disconnect();
        //!Initiates play on server. Only works if all users have a clan.
        bool startPlaying();
        //!Tries to move ahead to next day. Will fail if not all users have sent acknowledgements.
        bool proposeNextDay();
        //!Have all clients acknowledged to server that they've loaded?
        bool isGreen();
        
        //Gameplay request routines:
        //!Requests a number of armies to move to a town. Lua bound.
        bool requestArmyMoves(town_id toTown, luabind::object armyIds, bool replacePath);
        
        //Chat routines:
        //!Sends a chat to all request to server.
        void chatPublic(const std::string& sentence);
        
        //Scenario handling:
        //!Sends a server order to select scenario.
        void selectScenario(const std::string& name);
        //!Loads a scenario and initializes associated game world.
        bool loadScenario(const std::string& filename);
};

} //!namespace r2

#endif //!R2_GAME_H
