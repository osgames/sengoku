//r2_province.cpp

//This code is published under the General Public License.

#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_log.h"

using namespace r2;

void CProvinceDescriptors::init(CLua& lua)
{
        using namespace luabind;
        
        ASSERT(lua.state() != 0);
        
        object provs(luabind::globals(lua.state())["provinces"]);
        if(type(provs) != LUA_TTABLE) {
                LOG_ERROR("Province table missing");
                return;
        }
        
        //foreach province
        for(luabind::iterator i(provs), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type((*i)["id"]) == LUA_TNUMBER) {
                        prov_id id = object_cast<prov_id>((*i)["id"]);
                        if(provDescs.count(id) > 0) {
                                LOG_ERROR("Duplicate province ID detected: " << id);
                                return;
                        }
                        
                        SProvince* desc = new SProvince;
                        ASSERT(desc != 0);
                        
                        desc->name = type((*i)["name"]) == LUA_TSTRING ? object_cast<std::string>
                                ((*i)["name"]) : "??unnamed??";
                        desc->farming = type((*i)["farming"]) == LUA_TNUMBER ? object_cast<i32>
                                ((*i)["farming"]) : 0;
                        provDescs[id] = desc;
                }
        }
}

CProvinceDescriptors::CProvinceDescriptors(const std::string& filename)
{
        CLua lua;
        if(!lua.doFile(filename)) {
                return;
        }
        
        init(lua);
}

CProvinceDescriptors::~CProvinceDescriptors()
{
        for(std::map<prov_id, SProvince*>::const_iterator it = provDescs.begin(); it != provDescs.end();
                ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
}

CProvinceDescriptors::SProvince* CProvinceDescriptors::getProvince(prov_id prov)
{
        return provDescs.count(prov) > 0 ? provDescs[prov] : 0;
}

CProvince::CProvince(luabind::object table)
{
        using namespace luabind;

        //defaults
        owner = "",
        controller = "";
        happiness = 80;
        taxRate = 50;
        
        if(table && type(table) == LUA_TTABLE) {
        }
}

CProvince::~CProvince()
{
}
