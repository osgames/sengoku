//r2_town.h

//This code is published under the General Public License.

#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_log.h"
#include <set>
#include <queue>

using namespace r2;

void CTownDescriptors::init(CLua& lua)
{
        using namespace luabind;
        
        ASSERT(lua.state() != 0);
        
        object towns(luabind::globals(lua.state())["towns"]);
        object roads(luabind::globals(lua.state())["roads"]);
        if(type(towns) != LUA_TTABLE || type(roads) != LUA_TTABLE) {
                LOG_ERROR("Town and/or road tables missing");
                return;
        }
        
        //towns
        for(iterator i(towns), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type((*i)["id"]) == LUA_TNUMBER) {
                        //town entry
                        town_id id = object_cast<town_id>((*i)["id"]);
                        if(townDescs.count(id) > 0) {
                                LOG_ERROR("Duplicate town ID detected: " << id);
                                return;
                        }
                        STown* t = new STown;
                        ASSERT(t != 0);
                        
                        t->name = type((*i)["name"]) == LUA_TSTRING ?
                                object_cast<std::string>((*i)["name"]) : "???";
                        t->mapPos.x = type((*i)["x"]) == LUA_TNUMBER ?
                                object_cast<i32>((*i)["x"]) : 0;
                        t->mapPos.y = type((*i)["y"]) == LUA_TNUMBER ?
                                object_cast<i32>((*i)["y"]) : 0;
                        townDescs[id] = t;
                }
        }
        
        //road network (can be viewn as a directed graph where towns = nodes, roads = edges)
        for(iterator i(roads), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type((*i)["a"]) == LUA_TNUMBER &&
                                type((*i)["b"]) == LUA_TNUMBER) {
                        //road entry
                        town_id a = object_cast<town_id>((*i)["a"]);
                        town_id b = object_cast<town_id>((*i)["b"]);
                        
                        if(townDescs.count(a) == 0) {
                                LOG_ERROR("Town A is non-existant ID: " << a);
                                return;
                        }
                        if(townDescs.count(b) == 0) {
                                LOG_ERROR("Town B is non-existant ID: " << b);
                                return;
                        }
                        if(a == b) {
                                LOG_ERROR("Town A and B are equal");
                                return;
                        }
                        
                        SRoad r(type((*i)["dist"]) == LUA_TNUMBER ? object_cast<u16>((*i)["dist"]) : 1);
                        if(townDescs[a]->roads.count(b) > 0 || townDescs[b]->roads.count(a) > 0) {
                                LOG_ERROR("Road between town " << a << " and town " << b << " already exists");
                                return;
                        }
                        townDescs[a]->roads[b] = r;
                        townDescs[b]->roads[a] = r;
                }
        }
}

CTownDescriptors::CTownDescriptors(const std::string& filename)
{
        CLua lua;
        if(!lua.doFile(filename)) {
                return;
        }
        
        init(lua);
}

CTownDescriptors::~CTownDescriptors()
{
        for(std::map<town_id, STown*>::iterator it = townDescs.begin(); it != townDescs.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
}

CTownDescriptors::STown* CTownDescriptors::getTown(town_id town)
{
        std::map<town_id, STown*>::iterator it = townDescs.find(town);
        
        if(it == townDescs.end()) {
                LOG_ERROR("Town with ID " << town << " doesn't have descriptor!");
                return 0;
        } else {
                return it->second;
        }
}

/*CTownDescriptors::SRoad* CTownDescriptors::getRoad(town_id from, town_id to)
{
        SRoadKey k(from, to);
        return roads.count(k) > 0 ? &roads[k] : 0;
}*/

CTown::CTown(luabind::object table)
{
        using namespace luabind;
        
        if(table && type(table) == LUA_TTABLE) {
        }
}

CTown::~CTown()
{
}

/*f32 CTown::CalcEffectiveIncome(void)
{
        //return (farmingBaseRate + fishingBaseRate) * taxRate + tradeBaseRate + miningBaseRate; //simple for now
}*/

bool CTownPathfinder::bestPath(town_id start, town_id goal, CTownDescriptors& desc,
        u8 costFunctionMode, std::list<town_id>* path)
{
        std::priority_queue<SNode, std::vector<SNode>, SNodeComparator> open;
        std::map<town_id, town_id> prev; //also acts as visited set
        
        prev[start] = -1;
        SNode n(0, start);
        open.push(n);
        
        while(!open.empty())
        {
                n = open.top();
                open.pop();
                
                if(n.town == goal) {
                        if(path != 0) {
                                //backtrack patch from goal to start
                                for(town_id i = goal; i != start; i = prev[i]) {
                                        path->push_front(i);
                                }
                        }
                        return true;
                }
                
                //get town of node
                CTownDescriptors::STown* currTown = desc.getTown(n.town);
                ASSERT(currTown != 0);
                
                //examine unvisited neighbours
                for(std::map<town_id, CTownDescriptors::SRoad>::const_iterator it = currTown->roads.begin();
                                it != currTown->roads.end(); ++it) {
                        if(prev.count(it->first) == 0) {
                                prev[it->first] = n.town;
                                u32 cost = costFunction(costFunctionMode, n.town, it->first);
                                if(cost != ULONG_MAX) {
                                        SNode neighbourNode(cost + n.cost, it->first);
                                        open.push(neighbourNode);
                                }
                        }
                        
                }
        }
        
        return false;
}
