//r2_world.cpp

//This code is published under the General Public License.

#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_log.h"
#include <cstdlib>
#include "..\r2_core\r2_globals.h"
#include "..\r2_game\r2_game.h"
#include "..\r2_gui\r2_gui.h"
#include <climits>

using namespace r2;

std::string CWorldDate::toString() const
{
        char buffer[16];
  
        //generate month name (might want to localize this later by the way)
        std::string month;
  
        switch(getMonth()) {
                case 0:
                        month = "January";
                        break;
                case 1:
                        month = "February";
                        break;
                case 2:
                        month = "March";
                        break;
                case 3:
                        month = "April";
                        break;
                case 4:
                        month = "May";
                        break;
                case 5:
                        month = "June";
                        break;
                case 6:
                        month = "July";
                        break;
                case 7:
                        month = "August";
                        break;
                case 8:
                        month = "September";
                        break;
                case 9:
                        month = "October";
                        break;
                case 10:
                        month = "November";
                        break;
                case 11:
                        month = "December";
                        break;
                default:
                month = "InvalidMonth";
                break;
        }
  
        //generate day
        itoa(getDay() + 1, buffer, 10);
        std::string day = buffer;
  
        //generate year
        itoa(getYear(), buffer, 10);
        std::string year = buffer;
  
        return month + " " + day + ", " + year;
}

void CWorldDate::set(luabind::object table)
{
        using namespace luabind;
        
        totalDays = 0;
        
        if(table && type(table) == LUA_TTABLE) {
                u8 day = type(table["day"]) == LUA_TNUMBER ? object_cast<u8>(table["day"]) : 0;
                u8 month = type(table["month"]) == LUA_TNUMBER ? object_cast<u8>(table["month"]) : 0;
                u32 year = type(table["year"]) == LUA_TNUMBER ? object_cast<u32>(table["year"]) : 0;
                
                set(day, month, year);
        }
}

using namespace r2;

void r2::CWorld::bindFunctionsWithLua(r2::CLua& l)
{
        using namespace luabind;
        
        if(l.state() == 0) {
                return;
        }
        
        module(l.state())
        [
                class_ <CWorld>("CWorld")
                .property("year", &CWorld::getYear)
                .property("month", &CWorld::getMonth)
                .property("day", &CWorld::getDay)
        ];
}

void r2::CWorld::indicatePaletteChange(prov_id province)
{
        using namespace luabind;
        
        ASSERT(clanDescs != 0);
        ASSERT(provs.count(province) > 0);
        
        CClanDescriptors::SClan* desc = clanDescs->getClan(provs[province]->getOwner());
        if(desc == 0) {
                return;
        }
        
        try {
                call_function<void>(r2::globals.commonLua.state(), "changePalette",
                        boost::ref(CGame::getInstance()), boost::ref(CGUI::getInstance()),
                        desc->palette, province + 1); //to Lua base 1
        } catch(error& e) {
                CLua::printError(e);
        }
}

void r2::CWorld::indicateTownChange(town_id town)
{
        using namespace luabind;
        
        ASSERT(townDescs != 0);
        
        CTownDescriptors::STown* desc = townDescs->getTown(town);
        
        if(desc == 0) {
                return;
        }
        
        /*try {
                call_function<void>(r2::globals.commonLua.state(), "changeTown",
                        boost::ref(CGame::getInstance()), boost::ref(CGUI::getInstance()),
                        town, desc->mapPos.x, desc->mapPos.y);
        } catch(error& e) {
                CLua::printError(e);
        }*/
}

u32 r2::CWorld::costFunction(u8 mode, town_id from, town_id to)
{
        ASSERT(townDescs != 0);
        r2::CTownDescriptors::STown* t = townDescs->getTown(from);
        ASSERT(t != 0);
        
        if(towns.count(from) == 0 || towns.count(to) == 0) {
                return ULONG_MAX;
        }
        if(mode == 0) {
                if(t->roads.count(to) > 0) {
                        return t->roads[to].dist;
                } else {
                        return ULONG_MAX;
                }
        }
        
        return 1;
}

r2::CWorld::CWorld()
{
        loaded = false;
        clanDescs = 0;
        townDescs = 0;
}

r2::CWorld::~CWorld()
{
        unload();
}

void r2::CWorld::notifyAddAllObjects()
{
        for(std::map<town_id, CTown*>::const_iterator it = towns.begin(); it != towns.end(); it++) {
                if(it->second != 0) {
                        sendMsg(MSG_WORLD_TOWNADDED, it->first);
                }
        }
        
        for(std::map<army_id, CArmy*>::const_iterator it = armies.begin(); it != armies.end(); it++) {
                if(it->second != 0) {
                        sendMsg(MSG_WORLD_ARMYADDED, it->first);
                }
        }
}

void r2::CWorld::notifyRemoveAllObjects()
{
        for(std::map<town_id, CTown*>::const_iterator it = towns.begin(); it != towns.end(); it++) {
                if(it->second != 0) {
                        sendMsg(MSG_WORLD_TOWNREMOVED, it->first);
                }
        }
        
        for(std::map<army_id, CArmy*>::const_iterator it = armies.begin(); it != armies.end(); it++) {
                if(it->second != 0) {
                        sendMsg(MSG_WORLD_ARMYREMOVED, it->first);
                }
        }
}

void r2::CWorld::unload()
{
        notifyRemoveAllObjects();
        
        for(std::map<std::string, CClan*>::const_iterator it = clans.begin(); it != clans.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
        clans.clear();
        
        for(std::map<prov_id, CProvince*>::const_iterator it = provs.begin(); it != provs.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
        provs.clear();
        
        for(std::map<town_id, CTown*>::const_iterator it = towns.begin(); it != towns.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
        towns.clear();
        
        if(provDescs != 0) {
                delete provDescs;
                provDescs = 0;
        }
        if(clanDescs != 0) {
                delete clanDescs;
                clanDescs = 0;
        }
        
        if(townDescs != 0) {
                delete townDescs;
                townDescs = 0;
        }
        
        loaded = false; 
        
}

bool r2::CWorld::loadFromFile(const std::string& filename)
{
        using namespace luabind;
        
        ASSERT(!loaded);
        
        CLua lua;
        
        ASSERT(lua.state() != 0);
        
        if(!lua.doFile(filename)) {
                return false;
        }
        
        object header(luabind::globals(lua.state())["header"]);
        if(type(header) != LUA_TTABLE) {
                LOG_ERROR("Header missing");
                return false;
        }
        
        if(type(header["date"]) == LUA_TTABLE) {
                date.set(header["date"]);
        }
        
        if(type(header["clanDescriptors"]) == LUA_TSTRING) {
                clanDescriptorFile = object_cast<std::string>(header["clanDescriptors"]);
                
                clanDescs = new CClanDescriptors(clanDescriptorFile);
                ASSERT(clanDescs != 0);
        } else {
                clanDescriptorFile = "";
        }
        
        if(type(header["townDescriptors"]) == LUA_TSTRING) {
                townDescriptorFile = object_cast<std::string>(header["townDescriptors"]);
                
                townDescs = new CTownDescriptors(townDescriptorFile);
                ASSERT(townDescs != 0);
        } else {
                townDescriptorFile = "";
        }
        
        if(type(header["provinceDescriptors"]) == LUA_TSTRING) {
                provDescriptorFile = object_cast<std::string>(header["provinceDescriptors"]);
                
                provDescs = new CProvinceDescriptors(provDescriptorFile);
                ASSERT(provDescs != 0);
        } else {
                provDescriptorFile = "";
        }
        
        //province instances
        object provT(luabind::globals(lua.state())["provinces"]);
        if(provDescs != 0 && type(provT) == LUA_TTABLE) {
                for(iterator i(provT), end; i != end; ++i) {
                        if(type(*i) == LUA_TTABLE && type((*i)["id"]) == LUA_TNUMBER) {
                                prov_id id = object_cast<prov_id>((*i)["id"]);
                                
                                if(provs.count(id) == 0) {
                                        provs[id] = new CProvince(*i);
                                } else {
                                        LOG_WARNING("Duplicate province ID: " << id);
                                }
                        }
                }
        } else {
                LOG_WARNING("No province descriptors or province table missing");
        }
        
        //init provinces to default present in descriptor that weren't present in file
        if(provDescs != 0) {
                for(CProvinceDescriptors::CIterator it = provDescs->iterator(); it.hasNext(); it.next()) {
                        if(provs.count(it.key()) == 0) {
                                object o; //invalid object - signals province will be inited to default
                                provs[it.key()] = new CProvince(o);
                        }
                }
        }
        
        //clan instances
        object clanT(luabind::globals(lua.state())["clans"]);
        if(type(clanT) != LUA_TTABLE) {
                LOG_ERROR("Clans missing");
                unload();
                return false;
        }
        for(iterator i(clanT), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE) {
                        std::string id = object_cast<std::string>(i.key());
                        if(clans.count(id) == 0) {
                                clans[id] = new CClan(*i);
                                
                                for(u32 j = 0; j < clans[id]->countOwnedProvinces(); ++j) {
                                        prov_id owned = clans[id]->getOwnedProvince(j);
                                        if(provs.count(owned) > 0) {
                                                provs[owned]->setOwner(id);
                                                indicatePaletteChange(owned);
                                        } else {
                                                LOG_WARNING("Non-existing owned province ID: " << owned);
                                        }
                                }
                                for(u32 j = 0; j < clans[id]->countControlledProvinces(); ++j) {
                                        prov_id ctrl = clans[id]->getControlledProvince(j);
                                        if(provs.count(ctrl) > 0) {
                                                provs[ctrl]->setController(id);
                                        } else {
                                                LOG_WARNING("Non-existing controlled province ID: " << ctrl);
                                        }
                                }
                        } else {
                                LOG_WARNING("Duplicate clan ID: " << id);
                        }
                }
        }
        
        //town instances
        object townT(luabind::globals(lua.state())["towns"]);
        if(townDescs != 0 && type(townT) == LUA_TTABLE) {
                for(iterator i(townT), end; i != end; ++i) {
                        if(type(*i) == LUA_TTABLE && type((*i)["id"]) == LUA_TNUMBER) {
                                town_id id = object_cast<town_id>((*i)["id"]);
                                if(towns.count(id) == 0) {
                                        towns[id] = new CTown(*i);
                                        indicateTownChange(id);
                                } else {
                                        LOG_WARNING("Duplicate town ID: " << id);
                                }
                        }
                }
        } else {
                LOG_WARNING("Town table missing or no town descriptors are available");
        }
        
        //armies
        object armyT(luabind::globals(lua.state())["armies"]);
        if(type(armyT) == LUA_TTABLE) {
                for(iterator i(armyT), end; i != end; ++i) {
                        if(type(*i) == LUA_TTABLE && type((*i)["id"]) == LUA_TNUMBER) {
                                army_id id = object_cast<army_id>((*i)["id"]);
                                if(armies.count(id) == 0) {
                                        armies[id] = new CArmy(id, *i, *townDescs);
                                } else {
                                        LOG_WARNING("Duplicate army ID: " << id);
                                }
                        }
                }
        }
        
        notifyAddAllObjects();
        
        loaded = true;
        
        return true;
}

void r2::CWorld::nextDay()
{
        date.increment();
        
        //do army updates
        for(std::map<army_id, CArmy*>::const_iterator it = armies.begin(); it != armies.end(); ++it) {
                ASSERT(it->second != 0);
                it->second->nextDay(*townDescs);
        }
}

void r2::CWorld::getArmyRenderInfo(army_id id, const std::string& clan, SArmyRender& info)
{
        ASSERT(townDescs != 0);
        CArmy* army = armies[id];
        ASSERT(army != 0);
        
        CTownDescriptors::STown* td = townDescs->getTown(army->getLocation());
        ASSERT(td != 0);
                
        info.mapPos = td->mapPos;
        info.clan = army->getOwner();
        info.visible = true; //no fog of war for now
}

void r2::CWorld::getTownRenderInfo(town_id id, STownRender& info)
{
        ASSERT(townDescs != 0);
        CTown* town = towns[id];
        ASSERT(town != 0);
        
        CTownDescriptors::STown* td = townDescs->getTown(id);
        ASSERT(td != 0);
        
        info.mapPos = td->mapPos;
}

bool r2::CWorld::verifyArmyMove(const std::string& clan, town_id toTown, army_id army)
{
        if(armies.count(army) == 0 || towns.count(toTown) == 0) {
                return false;
        }
        
        ASSERT(armies[army] != 0);
        ASSERT(townDescs != 0);
        
        return armies[army]->verifyMove(toTown, clan, *townDescs, *this);
}

bool r2::CWorld::orderArmyMove(town_id toTown, army_id army, bool clearPath)
{
        if(armies.count(army) == 0 || towns.count(toTown) == 0) {
                return false;
        }
        
        ASSERT(armies[army] != 0);
        ASSERT(townDescs != 0);
        
        if(armies[army]->orderMove(toTown, *townDescs, *this, clearPath)) {
                sendMsg(MSG_WORLD_ARMYMOVED, army);
                return true;
        }
        
        return false;
}

/*void CWorld::AddAnnualIncomes(void)
{
  u32 i;
  
  //calculate waste of koku from last year
  for(i = 0; i < GAMECLAN_COUNT; i++)
  {
    if(clans[i].alive)
    {
      clans[i].storedKoku *= GAMEPLAY_KOKUWASTE;
    }
  }
  
  //calculate incomes from provinces
  for(i = 0; i < GAMETOWN_COUNT; i++)
  {
    if(towns[i].owner >= 0 && towns[i].controller == towns[i].owner)
    {
      clans[towns[i].owner].storedKoku += towns[i].CalcEffectiveIncome();
    }
  }
  
  //calculate retainer incomes 
  for(i = 0; i < GAMECLAN_COUNT; i++)
  {
    if(clans[i].alive)
    {
      
      std::vector<CCharacter*>::iterator it;  
      for(it = clans[i].retainers.begin(); it !=  clans[i].retainers.end(); it++)
      {
     //        Log_Print("found a retainer");
        //     Log_Print((*it)->name);
      //       Log_Integer((*it)->GetIncome());
          if (clans[i].storedKoku > (*it)->GetIncome())
          {
             (*it)->AddFullIncome();  
             clans[i].storedKoku -= (*it)->GetIncome();                    
          }
          else if (clans[i].storedKoku > 0)
          {
             (*it)->AddIncome(clans[i].storedKoku);      
             clans[i].storedKoku = 0;        
          }
          else 
          {
             //set some Unpaid flag, lower loyalty etc              
          }
      }  
    }
  }
}*/

u8 r2::CWorld::getDay() const
{
        return date.getDay() + 1;
}

u8 r2::CWorld::getMonth() const
{
        return date.getMonth() + 1;
}

u32 r2::CWorld::getYear() const
{
        return date.getYear();
}
