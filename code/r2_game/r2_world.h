//r2_world.h

//This code is published under the General Public License.

/*Changelog:
        2007-10-15: Added clan descriptors.
        */

#ifndef R2_WORLD_H
#define R2_WORLD_H

#include "..\r2_core\typedef.h"
#include <string>
#include <list>
#include <map>
#include <vector>
#include "..\r2_file\eug_file.h"
#include "..\r2_game\r2_character.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_core\r2_types.h"
#include "..\r2_core\r2_msgs.h"

#define DATE_DAYSPERMONTH       30 //sorry how unrealistic :P
#define DATE_MONTHSPERYEAR      12

namespace r2
{

//!Loads a set of clan tags and invariant clan properties (descriptions) from a Lua state.
class CClanDescriptors
{
public: //types
        struct SClan
        {
                std::string name;
                std::string palette;
        };
protected: //attributes
        std::map<std::string, SClan*> clanDescs;
protected: //methods
        void init(CLua& lua);
public: //methods
        //!Creates a number of clan descriptors from the table "clans" in a Lua state.
        CClanDescriptors(CLua& lua) {init(lua);}
        CClanDescriptors(const std::string& filename);
        ~CClanDescriptors();
        
        //!Returns a clan descriptor. Returns 0 if not found.
        SClan* getClan(const std::string& clan);
};

//!Loads a set of towns & roads and their invariant properties from a Lua state.
class CTownDescriptors
{
protected: //types
public: //types
        struct SRoad
        {
                u16 dist;
                SRoad() {dist = 0;}
                SRoad(u32 dist) { SRoad::dist = dist; }
        };
        struct STown
        {
                std::string name;
                Si32_point mapPos;  
                std::map<town_id, SRoad> roads;
        };
protected: //attributes
        std::map<town_id, STown*> townDescs;
protected: //methods
        void init(CLua& lua);
public: //methods
        //!Creates a number of town descriptors from the table "towns" in a Lua state.
        CTownDescriptors(CLua& lua) {init(lua);}
        CTownDescriptors(const std::string& filename);
        ~CTownDescriptors();
        
        //!Returns a town descriptor. Returns 0 if not found.
        STown* getTown(town_id town);
};

//!Class which finds the shortest path between two towns if one exists.
class CTownPathfinder
{
        //TODO: implement an iterator interface to make independent of town descriptors
protected: //types
        struct SNode
        {
                u32 cost;
                town_id town;
                SNode() {cost = 0; town = -1;}
                SNode(u32 cost, town_id town) {SNode::cost = cost; SNode::town = town;}
        };
        struct SNodeComparator
        {
                bool operator()(const SNode& a, const SNode& b)
                {
                        return a.cost >= b.cost;
                }
        };
protected:
        //!Cost function, default returns distance cost. An edge (road) must exist between two towns.
        virtual u32 costFunction(u8 mode, town_id from, town_id to) = 0;
public:
        //!Uses uniform cost search to find the shortest path from one town to another.
        bool bestPath(town_id start, town_id goal, CTownDescriptors& desc, u8 costFunctionMode,
                std::list<town_id>* path);
};

//!Loads a set of static province properties from a Lua state.
class CProvinceDescriptors
{
public: //types
        struct SProvince
        {
                std::string name;
                i32 farming;
        };
        class CIterator
        {
                friend class CProvinceDescriptors;
        protected:
                std::map<prov_id, SProvince*>::iterator it;
                std::map<prov_id, SProvince*>::iterator end;
                CIterator(std::map<prov_id, SProvince*>& descs)
                {
                        it = descs.begin();
                        end = descs.end();
                }
        public:
                bool hasNext() {return it != end;}
                void next() {if(hasNext()) ++it;}
                prov_id key() {return it->first;}
                SProvince* val() {return it->second;}
        };
protected: //attri.
        std::map<prov_id, SProvince*> provDescs;
protected: //methods
        void init(CLua& lua);
public: //methods
        //!Creates static province properties from a Lua state.
        CProvinceDescriptors(CLua& lua) {init(lua);}
        CProvinceDescriptors(const std::string& filename);
        ~CProvinceDescriptors();
        
        SProvince* getProvince(prov_id prov);
        
        CIterator iterator() {CIterator it(provDescs); return it;}
};

class CWorldDate
{
protected:
        u32 totalDays; //sorry, no pre-AD years
public:
        //constructor
        CWorldDate(void) {totalDays = 0;}
  
        //evaluation functions
        inline u32 getTotalDays() const  {return totalDays;}
        inline u32 getTotalMonths() const  {return totalDays / DATE_DAYSPERMONTH;}
        inline u32 getTotalYears() const  {return totalDays / (DATE_DAYSPERMONTH *
                DATE_MONTHSPERYEAR);}
        inline u32 getDay() const  {return totalDays % DATE_DAYSPERMONTH;}
        inline u32 getMonth() const  {return getTotalMonths() % DATE_MONTHSPERYEAR;}
        inline u32 getYear() const {return getTotalYears();}
  
        //!Set date explicitly.
        void set(u8 day, u8 month, u32 year)
        {
                totalDays = (day - 1) + ((month - 1) + year * DATE_MONTHSPERYEAR) * DATE_DAYSPERMONTH;
        }
        
        //!Set date from a Lua table.
        void set(luabind::object table);
        
        void increment() {totalDays++;}
  
        std::string toString() const;
        
        
};

//sort out class below, the fact I indented it doesn't mean it's good...
class CProvince
{
protected: //attributes
        std::string owner;
        std::string controller;
        
        u8 happiness; //0-100
        u8 taxRate; //0-80 or maybe 90
public:
        CProvince(luabind::object table);
        ~CProvince();
        
        void setOwner(const std::string& owner) {CProvince::owner = owner;}
        void setController(const std::string& controller) {CProvince::controller = controller;}
        std::string getOwner() {return owner;}
        std::string getController() {return controller;}
};

class CClan
{
protected: //attributes
        //directly owned and controlled land
        std::vector<prov_id> ownedProvinces;
        std::vector<prov_id> controlledProvinces;
        
        //see if attributes below are needed, may be a bit premature
        prov_id capital;
        char_id lord;
        std::string liege;
public: //methods
        //constructor
        CClan(luabind::object table);
        ~CClan();
        
        prov_id getOwnedProvince(u32 index);
        prov_id getControlledProvince(u32 index);
        u32 countOwnedProvinces() {return ownedProvinces.size();}
        u32 countControlledProvinces() {return controlledProvinces.size();}
};

class CTown
{
protected: //attributes
        u8 landType; //hilltop, plain, mountain
        u8 fortLevel;
        std::string owner;
        std::string controller;
public: //methods
        CTown(luabind::object);
        ~CTown();
};

class CRegiment
{
protected:
        
        
        u32 gunners;   //ashigaru arqubisiers
        u32 ashigaru;  //ashigaru spearmen
        u32 samurai;   //samurai spearmen
        u32 cavalry;
        u32 monks; 
        
        char_id leader;
        town_id homeTown;
   
        u8 morale;
        u8 quality;
public:
        CRegiment(luabind::object table);
        CRegiment(char_id leader, town_id home, u8 quality, u32 gun, u32 ash, u32 sam, u32 cav, u32 monk);
        ~CRegiment();
};

class CArmy
{
protected:
        army_id myId;
        std::string name;
        std::string owner;
        char_id leader;
        town_id location;
        std::list<CRegiment*> units; //a regiment is represented by its pointer, so there is no ID
        std::list<town_id> path;
        u16 daysMoved;
        u16 battleCount; //number of battles current participating in
protected: //methods
        //!Send an indication that the army moved to global Lua space.
        void indicateChange(CTownDescriptors& descs);
        //!Specifies a point which the army faces.
        void facingPoint(CTownDescriptors& descs, Si32_point& p);
public:
        CArmy(army_id id, luabind::object table, CTownDescriptors& descs);
        CArmy(army_id id, const std::string& name, const std::string& owner, town_id location,
                const std::list<CRegiment*>& newUnits, CTownDescriptors& descs);
        ~CArmy();
        
        //!Performs daily army calculations (such as movement and attrition).
        void nextDay(CTownDescriptors& descs);
        
        //!Checks if army can move to a town.
        bool verifyMove(town_id toTown, const std::string& clan, CTownDescriptors& descs,
                CTownPathfinder& pf);
        //!Orders the army to go to a town.
        bool orderMove(town_id toTown, CTownDescriptors& descs, CTownPathfinder& pf, bool overwritePath);
        
        //accessors
        town_id getLocation() const {return location;}
        std::string getOwner() const {return owner;}
        
        //Used to indicate army entered a battle.
        void startBattle() {battleCount++;}
        //!Indicates this army left a battle.
        void endBattle() {if(battleCount > 0) battleCount--;}
        
        //allows externals to set path - not sure how much I like this, see what can be done
        std::list<town_id>& getPath() {return path;}
};

class CBattle
{
protected:
        std::list<army_id> attackers;
        std::list<army_id> defenders;
};

class CWorld : public CTownPathfinder, public CMsgSender
{
public: //structures
        struct SArmyRender {
                Si32_point mapPos;
                std::string clan;
                bool visible;
        };
        struct STownRender {
                Si32_point mapPos;
        };
protected: //attributes
        CClanDescriptors* clanDescs;
        std::string clanDescriptorFile;
        
        CTownDescriptors* townDescs;
        std::string townDescriptorFile;
        
        CProvinceDescriptors* provDescs;
        std::string provDescriptorFile;
        
        CWorldDate date;
        std::map<prov_id, CProvince*> provs;
        std::map<town_id, CTown*> towns;
        std::map<std::string, CClan*> clans;
        std::map<army_id, CArmy*> armies;
        
        bool loaded;
protected: //methods
        //!Calls a Lua function to indicate a palette changed.
        void indicatePaletteChange(prov_id province);
        //!Calls a Lua function to indicate a town changed.
        void indicateTownChange(town_id town);
        //!Implementation of cost function for pathfinding.
        u32 costFunction(u8 mode, town_id from, town_id to);
        
        //!Send addition messages for all world objects.
        void notifyAddAllObjects();
        //!Send removal messages for all world objects.
        void notifyRemoveAllObjects();
public: //methods
        CWorld();
        ~CWorld();
        
        //!Exposes functions to be accessible during run time to a Lua VM.
        static void bindFunctionsWithLua(r2::CLua& l);
  
        //initialization and resetting
        //!Loads a "world" from file (can be either scenario or save game).
        bool loadFromFile(const std::string& filename);
        
        //!Releases all resources used when loaded.
        void unload();
  
        //!Goes to the next day.
        void nextDay();
        
        //!Gets information on how/if to render an army, where & how based on perspective from a clan.
        void getArmyRenderInfo(army_id id, const std::string& clan, SArmyRender& info);
        //!Gets information on how to render a town.
        void getTownRenderInfo(town_id id, STownRender& info);
public: //lua bound methods
        //!Determines whether an army can move to a town or not, and if the supposed clan is allowed to order it.
        bool verifyArmyMove(const std::string& clan, town_id toTown, army_id army);
        //!Orders an army to move to a town. Returns whether the order was successful.
        bool orderArmyMove(town_id toTown, army_id army, bool clearPath);
        
        //!Access functions.
        //!Gets current day. Lua bound.
        u8 getDay() const;
        //!Gets current month. Lua bound.
        u8 getMonth() const;
        //!Gets current year. Lua bound.
        u32 getYear() const;
};

} //!namespace r2

#endif //!R2_WORLD_H
