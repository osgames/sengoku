//r2_bitmap.h

//This code is published under the General Public License.

#ifndef R2_BITMAP_H
#define R2_BITMAP_H

#define BITMAP_FLAG_ALPHA           0x1
#define BITMAP_FLAG_VIDEOFORMAT     0x2

#include "r2_surface.h"

namespace r2
{

//!Loads from a bitmap file into a CSurface in software memory.
class CBitmap : public r2::CSurface
{
protected:
        std::string loadedFile; //for restoration
        bool succeeded;
public:
        CBitmap(std::string& filename, i32 flags)
        {
                succeeded = loadImage(filename, flags & BITMAP_FLAG_VIDEOFORMAT,
                        flags & BITMAP_FLAG_ALPHA);
        }
        ~CBitmap() {unload();}
        
        bool hasSucceeded() {return succeeded;}
};

} //!namespace r2

#endif //!R2_BITMAP_H
