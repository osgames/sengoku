//r2_cursor.cpp

//replace at some point with a proper class

//This code is published under the General Public License.

#include "..\r2_graphics\r2_cursor.h"
#include "..\r2_graphics\r2_surface.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_input\r2_input.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_gamestate.h"
#include "..\r2_file\eug_file.h"
#include <map>
#include <string>

using namespace r2;

static Si32_point g_cursorCoords;
static Si32_point g_fixCoords;
static std::string g_cursorCurrent = "";
struct SCursor
{
  CSurface* bitmap;
  Si32_point offset;
};
static std::map<std::string, SCursor> g_cursors;
static bool g_cursorVisible = false;

bool Cursor_LoadAll(void)
{
  EUG::CFile eug;
  
  //attempt to load our fine file
  if(!eug.Load("Data\\Interface\\Cursors.txt"))
    return false;
  
  EUG::CTreeReader r(eug.GetRootNode());
  
  //find all cursor entries
  u32 cursorCount = r.Count("cursor");
  for(u32 i = 0; i < cursorCount; i++)
  {
    r.In("cursor", i); //1
    SCursor newCursor;
    
    //retrieve ID, important because it's a key into the map
    r.In("id"); //2
    std::string id = r.GetDLS();
    if(id != "") //no point in creating a cursor without a key ID
    {
      //load bitmap
      r.Next("bitmap"); //still 2
      std::string bitmapFilename = r.GetDLS();
      if(bitmapFilename != "")
      {
        newCursor.bitmap = globals.bitmaps.bind(bitmapFilename, 0);
        if(newCursor.bitmap != 0)
        {
          //apply color key
          r.Next("colorkey");
          newCursor.bitmap->setColorKey(u8(r.GetNumber(0)), u8(r.GetNumber(1)),
            u8(r.GetNumber(2))); //0 0 0 if not found, but is good default
          
          //retrieve offset coordinates, (0, 0) default is fine
          r.Next("clickx");
          newCursor.offset.x = i32(r.GetNumber());
          r.Next("clicky");
          newCursor.offset.y = i32(r.GetNumber());
          
          //add to map
          g_cursors[id] = newCursor;
        }
      }
    }
    r.Out(); //1
    
    r.Out(); //0
  }
  
  //we're finished
  eug.Destroy();
}

void Cursor_SetIcon(std::string name)
{
  if(g_cursors.count(name) > 0)
    g_cursorCurrent = name;
  else
    g_cursorCurrent = "";
}

void Cursor_Update(void)
{
  if(g_cursorVisible)
  {
    //make it simple
    Si32_point translation;
    Input_MouseMovement(translation);
    g_cursorCoords.x += translation.x;
    g_cursorCoords.y += translation.y;
  
    //bounds check
    Si32_rect rc;
    Cursor_CalcLimits(rc);
    
    if(g_cursorCoords.x < rc.left)
      g_cursorCoords.x = rc.left;
    if(g_cursorCoords.y < rc.top)
      g_cursorCoords.y = rc.top;
    if(g_cursorCoords.x > rc.right)
      g_cursorCoords.x = rc.right;
    if(g_cursorCoords.y > rc.bottom)
      g_cursorCoords.y = rc.bottom;
  }
}

void Cursor_Render(CSurface& dest)
{
  //make sure it should be displayed
  if(g_cursorVisible && g_cursorCurrent != "")
  {
    SCursor& cursor = g_cursors[g_cursorCurrent];
    Si32_rect rcDest;
    rcDest.left = g_cursorCoords.x;
    rcDest.top = g_cursorCoords.y;
    rcDest.right = rcDest.left + cursor.bitmap->getWidth();
    rcDest.bottom = rcDest.top + cursor.bitmap->getHeight();
    dest.blit(*cursor.bitmap, 0, &rcDest);
  }
}

void Cursor_Show(bool show)
{
  if(g_cursorCurrent != "")
    g_cursorVisible = show;
  else
    g_cursorVisible = false;
}

void Cursor_SetPos(i32 x, i32 y)
{
  if(g_cursorCurrent != "")
  {
    SCursor& cursor = g_cursors[g_cursorCurrent];
    g_cursorCoords.x = x - cursor.offset.x;
    g_cursorCoords.y = y - cursor.offset.y;
  }
}

i32 Cursor_GetX(void)
{
  if(g_cursorCurrent != "")
  {
    SCursor& cursor = g_cursors[g_cursorCurrent];
    return g_cursorCoords.x + cursor.offset.x;
  }
  
  return g_cursorCoords.x;
}

i32 Cursor_GetY(void)
{
  if(g_cursorCurrent != "")
  {
    SCursor& cursor = g_cursors[g_cursorCurrent];
    return g_cursorCoords.y + cursor.offset.y;
  }
  
  return g_cursorCoords.y;
}

void Cursor_Fix()
{
        g_fixCoords.x = Cursor_GetX();
        g_fixCoords.y = Cursor_GetY();
}

void Cursor_GetOffset(Si32_point& p)
{
        p.x = Cursor_GetX() - g_fixCoords.x;
        p.y = Cursor_GetY() - g_fixCoords.y;
}

void Cursor_CalcLimits(Si32_rect& fillRect)
{
  fillRect.left = 0;
  fillRect.top = 0;
  fillRect.right = GetGameState(GS_SCREENWIDTH);
  fillRect.bottom = GetGameState(GS_SCREENHEIGHT);
}
