//r2_cursor.h

//replace at some point with a proper class

//This code is published under the General Public License.

#ifndef R2_CURSOR_H
#define R2_CURSOR_H

#include "..\r2_core\typedef.h"
#include "..\r2_graphics\r2_surface.h"

bool Cursor_LoadAll(void);
void Cursor_SetIcon(std::string name);
void Cursor_Update(void);
void Cursor_Render(r2::CSurface& dest);
void Cursor_Show(bool show);
void Cursor_SetPos(i32 x, i32 y);
i32 Cursor_GetX();
i32 Cursor_GetY();
void Cursor_Fix();
void Cursor_GetOffset(Si32_point& p);
void Cursor_CalcLimits(Si32_rect& fillRect);

#endif //!R2_CURSOR_H
