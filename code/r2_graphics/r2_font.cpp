//r2_font.cpp

//This code is published under the General Public License.

#include "..\r2_graphics\r2_font.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_globals.h"

using namespace r2;

std::string CFont::readString()
{
        std::string result = "";
  
        //this loop requires that first character of string is already loaded
        while(IsAlpha(m_look) && (m_look != 0))
        {
                result += m_look;
                GetNextChar();
        }
  
        return result;
}

i32 CFont::readInteger()
{
        std::string intStr = "";
    
        //handle negation sign
        if(m_look == '-') {
                intStr = "-";
                GetNextChar();
        }
  
        //this loop requires that first digit is already loaded
        while(IsDigit(m_look) && (m_look != 0)) {
                //add digit to string
                intStr += m_look;
    
                //get next digit
                GetNextChar();
        }
        
        if(intStr == "" || intStr == "-") {
                //default to zero if we didn't find anything of value
                return 0;
        } else {
                //convert from string to integer
                return atoi(intStr.c_str());
        }
}

void CFont::readLine()
{
        std::string mode = "";
        std::string lastLabel = "";
        u8 id = 0;
  
        //character reading loop
        while(!IsNewLine(m_look) && (m_look != 0)) {
                if(IsWhite(m_look)) {
                        SkipWhite();
                } else if(m_look == '=') {
                        GetNextChar();
                        SkipWhite(); //in case any afterwards
      
                        //this implies we have an integer to extract, but first check
                        //who should receive it
                        if(lastLabel == "lineHeight" && mode == "common") {
                                lineHeight = readInteger();
                        } else if(lastLabel == "base" && mode == "common") {
                                base = readInteger();
                        } else if(lastLabel == "id" && mode == "char") {
                                //unfortunately it is assumed the ID will come first...
                                id = readInteger();
                                chars[id].valid = true;
                        } else if(lastLabel == "x" && mode == "char") {
                                chars[id].srcRect.left = readInteger();
                        } else if(lastLabel == "y" && mode == "char"){
                                chars[id].srcRect.top = readInteger();
                        } else if(lastLabel == "width" && mode == "char") {
                                chars[id].srcRect.right = chars[id].srcRect.left + readInteger();
                        } else if(lastLabel == "height" && mode == "char") {
                                chars[id].srcRect.bottom = chars[id].srcRect.top + readInteger();
                        } else if(lastLabel == "xoffset" && mode == "char") {
                                chars[id].offset.x = readInteger();
                        } else if(lastLabel == "yoffset" && mode == "char") {
                                chars[id].offset.y = readInteger();
                        } else if(lastLabel == "xadvance" && mode == "char") {
                                chars[id].advanceX = readInteger();
                        } else if(lastLabel == "first" && mode == "kerning") {
                                id = readInteger(); //ID of first kerning character
                                chars[id].kerning.push_back(SChar::SKerning()); //push inited element
                        } else if(lastLabel == "second" && mode == "kerning") {
                                chars[id].kerning[chars[id].kerning.size() - 1].secondChar = (u8) readInteger();
                        } else if(lastLabel == "amount" && mode == "kerning") {
                                chars[id].kerning[chars[id].kerning.size() - 1].modifyX = readInteger();
                        } else {
                                //we have no label to assign it to so just skip it
                                readInteger();
                        }
      
                        //this implies the label was handled
                        lastLabel = "";
                } else if(IsAlpha(m_look)) {
                        //will this set the mode or not
                        if(mode == "") {
                                //yes read the mode
                                mode = readString();
        
                                //deal with the info area: skip rest of line
                                if(mode == "info") {
                                        while(!IsNewLine(m_look) && (m_look != 0)) GetNextChar();
                                        return;
                                }
                        } else {
                                //no, this is a label
                                lastLabel = readString();
                        }
                } else {
                        UnexpectedChar(m_look);
                        GetNextChar();
                }
        } //end of while
}

/*!The filename argument should be without extension since a font has a data file and a
        corresponding bitmap.*/
CFont::CFont(const std::string& filename, i32 param)
{
        succeeded = false;
  
        //generate file paths
        std::string fontName = filename + ".fnt";
        std::string bitmapName = filename + ".bmp";
  
        //load the bitmap and ensure its creation
        bitmap = globals.bitmaps.bind(bitmapName, 0);
        if(bitmap == 0) {
                return;
        }
        
        //set pink as color key for fonts
        bitmap->setColorKey(255, 0, 255);
  
        //abort if file open fails
        if(!OpenInStream(fontName))
        {
                LOG_ERROR("Failed to open font file \"" << fontName << "\"");
                return;
        }
        
        //zero file-reading variables
        ZeroReadingVars();
        for(i16 i = 0; i <= 255; i++) {
                chars[i].valid = false;
        }
    
        //read one character to start with
        GetNextChar();
  
        //reading loop (per line mainly)
        while(m_look != 0) {
                m_line++;
                readLine();
                SkipNewLine();
        }
  
        CloseInStream();
        
        //find largest width
        largestWidth = 0;
        for(u16 i = 0; i < 256; i++) {
                i32 width = chars[i].srcRect.right - chars[i].srcRect.left;
                if(width > largestWidth) {
                        largestWidth = width;
                }
        }
    
        succeeded = true;
}

CFont::~CFont()
{
        if(bitmap != 0) {
                globals.bitmaps.unbind(bitmap);
                bitmap = 0;
        }
}

i32 CFont::calcStringWidth(const std::string& str)
{
        i32 lineWidth = 0;
  
        //step through all characters in string
        for(u32 i = 0; i < str.size(); i++) {
                u8 c = str[i];
    
                //only calculate for valid characters
                if(chars[c].valid) {
                        //add width to total width
                        lineWidth += chars[c].advanceX;
      
                        //TODO: include kerning effects here
                }
        }
  
         return lineWidth;
}

i32 CFont::calcStringFit(const std::string& str, i32 width)
{
        i32 lineWidth = 0;
  
        //step through all characters in string
        u32 i; //this must be outside for loop to allow return statement outside of loop
        for(i = 0; i < str.size(); i++) {
                u8 c = str[i];
    
                //deal with some special characters first
                if(c == '\n') {
                        lineWidth = 0;
                } else if(chars[c].valid) {
                        //quit when we exceed the specified width
                        if(lineWidth + chars[c].srcRect.right - chars[c].srcRect.left > width) {
                                return i;
                        }
        
                        lineWidth += chars[c].advanceX;
      
                        //TODO: include kerning effects here
                }
        }
  
        return i;
}

i32 CFont::calcStringIndex(const std::string& str, i32 offset, i32 maxWidth, i32 x, bool mode)
{
        /*!Calculates the index of the character for a certain x coordinate. Aborts upon newline.
        
        Arguments:
                - str: The string referred to.
                - offset: Offset into string (str[offset] is the leftmost character.
                - maxWidth: Maximum value of x coordinate.
                - x: Horizontal coordinate for which an index into string is attempted to be found.
                x is expected to satisfy the relation 0 <= x <= maxWidth.
                - mode: If true, the calculation will only index absolutely correctly such that
                0 <= index < size, if false there output index is extended to 0 <= index <= size,
                and well return size instead of -1 when the string or width is exceeded.
        
        Return value: Returns an index into the string. Returns a negative value if operation was
        aborted due to a new line. Otherwise, see the mode argumention description.*/
        
        ASSERT(x >= 0 && x <= maxWidth);
        ASSERT(offset >= 0);
        
        i32 stepX = 0;
        for(i32 index = offset; index < str.size(); index++) {
                u8 c = str[index];
                
                if(c == '\n') {
                        return -1;
                } else if(chars[c].valid) {
                        stepX += chars[c].advanceX;
                        
                        if(stepX > maxWidth) {
                                return mode ? -1 : index;
                        } else if(x <= stepX) {
                                //this works because we already checked characters to the left
                                return index;
                        }
                }
        }
        
        return mode ? -1 : (i32) str.size();
}
