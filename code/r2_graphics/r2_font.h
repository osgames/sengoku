//r2_font.h

//This code is published under the General Public License.

#ifndef R2_FONT_H
#define R2_FONT_H
/*namespace r2
{
        class CFont;
}*/

#include "..\r2_core\typedef.h"
#include "..\r2_file\sc_file.h"
#include "..\r2_core\r2_res.h"
#include <vector>
#include <string>

namespace r2
{
        
class CBitmap;

//the CFont class loads the properties of a font; it doesn't draw it

class CFont : public CSingleCharFile
{
public: //classes
        struct SChar
        {
                Si32_rect srcRect;
                Si32_point offset;
                i32 advanceX;
    
                //kerning
                struct SKerning
                {
                        i32 modifyX; //known as "amount" in file
                        u8 secondChar; //character index
                 };
                std::vector<SKerning> kerning;
    
                //this is a flag which determines if the character was recognized
                bool valid;
         };
protected: //attributes
        i32 lineHeight;
        i32 base;
        i32 largestWidth;
  
        //ignore the texture scale and pages members of the .fnf files
        //because we will assume there is only a single texture anyhow
  
        SChar chars[256];

        r2::CBitmap* bitmap;
        bool succeeded;
protected: //functions
        std::string readString();
        i32 readInteger();
        void readLine();
public:
        CFont(const std::string& filename, i32 param);
        ~CFont();
        bool hasSucceded() {return succeeded;}
  
        i32 calcStringWidth(const std::string& str);
        i32 calcStringFit(const std::string& str, i32 width); //returns number of character that fit
        i32 calcStringIndex(const std::string& str, i32 offset, i32 maxWidth, i32 x, bool mode);
        
        //accessors
        i32 getLineHeight() {return lineHeight;}
        i32 getBase() {return base;}
        SChar& getChar(u8 index) {return chars[index];}
        CBitmap* getBitmap() {return bitmap;}
        i32 getLargestWidth() {return largestWidth;}
};

//!This is a utility interface class which aids class in owning fonts.
class IFontOwner
{
public:
        /*!Should return true if the usage was found. Any classes that interit it should call thei parent's
        setFont(), if its parent's setFont returns true the child should avoid processing its own setFont
        call further.*/
        virtual bool setFont(const std::string& usage, const std::string& filepath) = 0;
        /*!Should release a font with the specified usage, if usage is found it returns true. Works the
        same way with base classes as IFontOwner::setFont.*/
        virtual bool releaseFont(const std::string& usage) = 0;
};

} //end of namespace r2

#endif //!R2_FONT_H
