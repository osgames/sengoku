//r2_palette.cpp

//This code is published under the General Public License.

#include "..\r2_graphics\r2_palette.h"
#include "..\r2_graphics\r2_surface.h"

using namespace r2;

void SPalette::build(f32 m1, f32 c1, f32 m2, f32 c2, f32 m3, f32 c3)
{
        //calculate colors for every entry in palette
        for(u16 i = 0; i <= 255; i++) {
                i32 r = i32(m1 * f32(i) + c1);
                i32 g = i32(m2 * f32(i) + c2);
                i32 b = i32(m3 * f32(i) + c3);
    
                //clamp them within byte range
                if(r < 0)
                r = 0;
                if(r > 255)
                r = 255;
                if(g < 0)
                g = 0;
                if(g > 255)
                g = 255;
                if(b < 0)
                b = 0;
                if(b > 255)
                b = 255;
    
                entries[i].r = r;
                entries[i].g = g;
                entries[i].b = b;
        }
}

/*void SPalette::map(const CSurface& format)
{
        for(u16 i = 0; i <= 255; i++) {
                 entries[i] = SDL_MapRGB(format.surface->format, entries[i].r,
                        entries[i].g, entries[i].b);
        }
}*/
