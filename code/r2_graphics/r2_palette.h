//r2_palette.h

//This code is published under the General Public License.

#ifndef R2_PALETTE_H
#define R2_PALETTE_H

#include <string>
#include <map>
#include <SDL.h>
#include "..\r2_file\eug_file.h"

namespace r2
{
        
//forward declaration
class CSurface;

//like for CSurface the API specific details shouldn't be used directly
class SPalette
{
public: //don't touch this or you may create API specific code
        SDL_Color entries[256]; //we will never use anything but 8 bit palettes
public:
        inline void setColor(u8 index, u8 r, u8 g, u8 b)
        {
                entries[index].r = r;
                entries[index].g = g;
                entries[index].b = b;
        }
        inline u8 getR(u8 index) const {return entries[index].r;}
        inline u8 getG(u8 index) const {return entries[index].g;}
        inline u8 getB(u8 index) const {return entries[index].b;}
  
        void build(f32 m1, f32 c1, f32 m2, f32 c2, f32 m3, f32 c3);
        //void map(const CSurface& format);
};

} //!namespace r2

#endif //!R2_PALETTE_H
