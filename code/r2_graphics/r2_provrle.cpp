//r2_provrle.cpp

//This code is published under the General Public License.

#include <fstream>

#include "..\r2_graphics\r2_provrle.h"
#include "..\r2_core\r2_log.h"

using namespace r2;

bool CProvinceRLE::MoveIndexByPixels(u32 num, u32& pixel_i, u32& entry_i)
{
    u32 pixels_left = num;
    
    //remove pixels at the current entry
    if(pixel_i > 0)
    {
        if(pixels_left >= (entries[entry_i].length - pixel_i))
        {
            pixels_left -= entries[entry_i].length - pixel_i;
            pixel_i = 0;
            entry_i++;
            
            if(entry_i >= count)
                return false; //we're no longer inside the RLE data
        }
        else
        {
            pixel_i += pixels_left;
            pixels_left = 0;
        }
    }
    
    //this loop makes the assumption pixel_i = 0 at start
    while(pixels_left > 0)
    {
        if(pixels_left >= entries[entry_i].length)
        {
            pixels_left -= entries[entry_i].length;
            entry_i++;
            
            if(entry_i >= count)
                return false; //we're no longer inside the RLE data
        }
        else
        {
            pixel_i = pixels_left; //since pixel_i = 0
            pixels_left = 0;
        }
    }
    
    return true; //we managed to move the desired number of pixels
}

CProvinceRLE::CProvinceRLE(const std::string& filename)
{
        succeeded = false;
    
        palette2 = 0;
        width = 0;
        height = 0;
        count = 0;
        isDrawn = false;
        
        std::ifstream is;
    
        //open .rlep file
        is.open(filename.c_str(), std::ios::binary);
    
        if(!is.is_open()) {
                LOG_ERROR("Failed to open " << filename);
                return;
        }
    
        //read header
        is.read((char*) &width, sizeof(width));
        is.read((char*) &height, sizeof(height));
        is.read((char*) &count, sizeof(count));
    
        if(width == 0 || height == 0 || count == 0) {
                LOG_PRINT("RLE header was corrupt in " << filename);
                is.close();
                return;
        }
    
        //allocate memory for RLE entries and row indices
        entries = new SRLE_Entry[count];
        row_entry = new u32[height];
        row_pixel = new u8[height];
    
        if(!entries && !row_entry && ! row_pixel) {
                LOG_PRINT("Failed to initialize enough memory for RLE data.");
                is.close();
        
                return;
        }
    
        //read RLE data entries
        for(u32 i = 0; i < count && !is.eof(); i++) {
                //read color first
                is.read((char*) &entries[i].color, sizeof(u8));
        
                //check least significant bit whether this has length > 1
                if(entries[i].color & 1) {
                        //length is greater than 1, read from file
                        is.read((char*) &entries[i].length, sizeof(u8));
                } else {
                        //length is 1
                        entries[i].length = 1;
                }
        
                //set least significant bit to 0
                entries[i].color &= 254;
        }
    
        //generate row indices
        u32 entry_i = 0;
        u32 pixel_i = 0;

        for(u32 i = 0; i < height; i++) {
                row_entry[i] = entry_i;
                row_pixel[i] = pixel_i;
                MoveIndexByPixels(width, pixel_i, entry_i);
        }
    
        //close file and set the proper flag
        is.close();
    
        succeeded = true;
}

CProvinceRLE::~CProvinceRLE(void)
{
        if(entries != 0) {
                delete[] entries;
        }
        if(row_entry != 0) {
                delete[] row_entry;
        }
        if(row_pixel != 0) {
                delete[] row_pixel;
        }
}

bool CProvinceRLE::drawEntireTransparent(r2::CSurface& dest, Si32_point& destPoint, u8 transparentKey)
{
        /*if(!succeeded || !dest.IsLocked()) {
                return false;
        }
    
        //check for destination point being within reasonable desination surface bounds
        if(destPoint.x >= i32(dest.getWidth()) || destPoint.y >= i32(dest.getHeight()) ||
                (destPoint.x < -i32(width)) || destPoint.y < -i32(height)) {
                return true;
        }
    
        //initialize loop control variables:
        u32 pixelsToDraw; //number of pixels to draw from each horizontal line
        u32 pixelsToSkip; //number of pixels to skip from each new line
    
        if(destPoint.x < 0) {
                pixelsToSkip = -destPoint.x;
                pixelsToDraw = width - pixelsToSkip > dest.getWidth() ? dest.getWidth() :
                        width - pixelsToSkip;
        } else {
                pixelsToSkip = 0; //draw from start of RLE line offset
                pixelsToDraw = width > dest.getWidth() - destPoint.x ?
                        dest.getWidth() - destPoint.x : width;
        }
    
        //set least significant bit to 0 of transparency key
        transparentKey &= 254;
        
        //entry and pixel indices into RLE data
        u32 entry_i;
        u32 pixel_i; //pixel displacement in entries[entry_i]
    
        //loop through every line of surface by default
        //this loop can also be ended by the RLE reaching the last entry
        for(u32 row = destPoint.y < 0 ? -destPoint.y : 0;
                destPoint.y + row < dest.getHeight() && row < height; row++) {
                
                //prepare for a new line
                MoveIndexToRow(row, pixel_i, entry_i);
                MoveIndexByPixels(pixelsToSkip, pixel_i, entry_i);
                
                //predeclare length
                u32 len;
        
                //draw line (per entry actually)
                for(u32 pixelsDrawn = 0; pixelsDrawn != pixelsToDraw; pixelsDrawn += len) {
                        len = entries[entry_i].length - pixel_i; //adjust for pixel index
                        pixel_i = 0; //set to 0, and will always be unil MoveIndexByPixels is called again
            
                        //draw everything?
                        if(pixelsToDraw - pixelsDrawn < len) {
                                //no, restrict length
                                len = pixelsToDraw - pixelsDrawn;
                        }
            
                        if(entries[entry_i].color != transparentKey) {
                                dest.putPixels(len, destPoint.x + pixelsToSkip + pixelsDrawn,
                                        destPoint.y + row, palette.entries[entries[entry_i].color]);
                        }
            
                        entry_i++;
                }
        }*/
    
        return true;
}

bool CProvinceRLE::drawPartTransparent(r2::CSurface& dest, Si32_point& destPoint, Si32_rect& srcRect,
                u8 transparentKey)
{
        /*if(!succeeded || !dest.IsLocked()) {
                return false;
        }
        
        Si32_rect checkedSrcRect;
        checkedSrcRect.left = srcRect.left < 0 ? 0 : srcRect.left;
        checkedSrcRect.top = srcRect.top < 0 ? 0 : srcRect.top;
        checkedSrcRect.right = srcRect.right > width ? width : srcRect.right;
        checkedSrcRect.bottom = srcRect.bottom > height ? height : srcRect.bottom;
        
        //calculate "real" with and height for the source rectangle
        i32 realWidth = checkedSrcRect.right - checkedSrcRect.left;
        i32 realHeight = checkedSrcRect.bottom - checkedSrcRect.top;
    
        //check for destination point being within reasonable desination surface bounds
        if(destPoint.x >= i32(dest.getWidth()) || destPoint.y >= i32(dest.getHeight()) ||
                (destPoint.x < -realWidth) || destPoint.y < -realHeight) {
                return true;
        }
    
        //initialize loop control variables:
        u32 pixelsToDraw; //number of pixels to draw from each horizontal line
        u32 pixelsToSkip; //number of pixels to skip from each new line
    
        if(destPoint.x < 0) {
                pixelsToSkip = -destPoint.x; //skip additional pixels equal to hidden part
                pixelsToDraw = realWidth - pixelsToSkip > dest.getWidth() ? dest.getWidth() :
                        realWidth - pixelsToSkip;
        } else {
                pixelsToSkip = 0; //draw from start of checkedSrcRect.left
                pixelsToDraw = realWidth > dest.getWidth() - destPoint.x ?
                        dest.getWidth() - destPoint.x : realWidth;
        }
    
        //set least significant bit to 0 of transparency key
        transparentKey &= 254;
        
        //entry and pixel indices into RLE data
        u32 entry_i;
        u32 pixel_i; //pixel displacement in entries[entry_i]
    
        //loop through every line of surface by default
        //this loop can also be ended by the RLE reaching the last entry
        for(u32 row = destPoint.y < 0 ? checkedSrcRect.top - destPoint.y : checkedSrcRect.top;
                destPoint.y + row - checkedSrcRect.top < dest.getHeight() && row < checkedSrcRect.bottom; row++) {
                
                //prepare for a new line
                MoveIndexToRow(row, pixel_i, entry_i);
                MoveIndexByPixels(checkedSrcRect.left, pixel_i, entry_i);
                MoveIndexByPixels(pixelsToSkip, pixel_i, entry_i);
                
                //predeclare length
                u32 len;
        
                //draw line (per entry actually)
                for(u32 pixelsDrawn = 0; pixelsDrawn != pixelsToDraw; pixelsDrawn += len) {
                        len = entries[entry_i].length - pixel_i; //adjust for pixel index
                        pixel_i = 0; //set to 0, and will always be unil MoveIndexByPixels is called again
            
                        //draw everything?
                        if(pixelsToDraw - pixelsDrawn < len) {
                                //no, restrict length
                                len = pixelsToDraw - pixelsDrawn;
                        }
            
                        if(entries[entry_i].color != transparentKey) {
                                dest.putPixels(len, destPoint.x + pixelsToSkip + pixelsDrawn,
                                        destPoint.y + row - checkedSrcRect.top,
                                        palette.entries[entries[entry_i].color]);
                        }
            
                        entry_i++;
                }
        }*/
    
        return true;
}

bool CProvinceRLE::drawPartSubtract(r2::CSurface& dest, Si32_point& destPoint, Si32_rect& srcRect)
{
        /*if(!succeeded || !dest.IsLocked()) {
                return false;
        }
        
        Si32_rect checkedSrcRect;
        checkedSrcRect.left = srcRect.left < 0 ? 0 : srcRect.left;
        checkedSrcRect.top = srcRect.top < 0 ? 0 : srcRect.top;
        checkedSrcRect.right = srcRect.right > width ? width : srcRect.right;
        checkedSrcRect.bottom = srcRect.bottom > height ? height : srcRect.bottom;
        
        //calculate "real" with and height for the source rectangle
        i32 realWidth = checkedSrcRect.right - checkedSrcRect.left;
        i32 realHeight = checkedSrcRect.bottom - checkedSrcRect.top;
    
        //check for destination point being within reasonable desination surface bounds
        if(destPoint.x >= i32(dest.getWidth()) || destPoint.y >= i32(dest.getHeight()) ||
                (destPoint.x < -realWidth) || destPoint.y < -realHeight) {
                return true;
        }
    
        //initialize loop control variables:
        u32 pixelsToDraw; //number of pixels to draw from each horizontal line
        u32 pixelsToSkip; //number of pixels to skip from each new line
    
        if(destPoint.x < 0) {
                pixelsToSkip = -destPoint.x; //skip additional pixels equal to hidden part
                pixelsToDraw = realWidth - pixelsToSkip > dest.getWidth() ? dest.getWidth() :
                        realWidth - pixelsToSkip;
        } else {
                pixelsToSkip = 0; //draw from start of checkedSrcRect.left
                pixelsToDraw = realWidth > dest.getWidth() - destPoint.x ?
                        dest.getWidth() - destPoint.x : realWidth;
        }
        
        //entry and pixel indices into RLE data
        u32 entry_i;
        u32 pixel_i; //pixel displacement in entries[entry_i]
    
        //loop through every line of surface by default
        //this loop can also be ended by the RLE reaching the last entry
        for(u32 row = destPoint.y < 0 ? checkedSrcRect.top - destPoint.y : checkedSrcRect.top;
                destPoint.y + row - checkedSrcRect.top < dest.getHeight() && row < checkedSrcRect.bottom; row++) {
                
                //prepare for a new line
                MoveIndexToRow(row, pixel_i, entry_i);
                MoveIndexByPixels(checkedSrcRect.left, pixel_i, entry_i);
                MoveIndexByPixels(pixelsToSkip, pixel_i, entry_i);
                
                //predeclare length
                u32 len;
        
                //draw line (per entry actually)
                for(u32 pixelsDrawn = 0; pixelsDrawn != pixelsToDraw; pixelsDrawn += len) {
                        len = entries[entry_i].length - pixel_i; //adjust for pixel index
                        pixel_i = 0; //set to 0, and will always be unil MoveIndexByPixels is called again
            
                        //draw everything?
                        if(pixelsToDraw - pixelsDrawn < len) {
                                //no, restrict length
                                len = pixelsToDraw - pixelsDrawn;
                        }
                        
                        i32 x = destPoint.x + pixelsToSkip + pixelsDrawn;
                        i32 y = destPoint.y + row - checkedSrcRect.top;
                        dest.subtractPixels(len, x, y,
                                palette2->entries[entries[entry_i].color].r,
                                palette2->entries[entries[entry_i].color].g,
                                palette2->entries[entries[entry_i].color].b);
            
                        entry_i++;
                }
        }*/
    
        return true;
}

u8 CProvinceRLE::GetPixel(u32 x, u32 y)
{
        u32 pixel_i;
        u32 entry_i;
        MoveIndexToRow(y, pixel_i, entry_i);
        MoveIndexByPixels(x, pixel_i, entry_i);
  
        return entries[entry_i].color; //for now
}

void CProvinceRLE::setPalette(const SPalette& pal, const CSurface& surface)
{
        //palette.map(pal, surface);
        palette2 = &pal;
}

CProvinceRLESet::CProvinceRLESet(const std::string& filepath, prov_id n)
{        
        //error flag, set to false to report error
        succeeded = false;
        
        if(n < 1) {
                //nothing to do
                return;
        }
        
        std::string filename;
    
        //load land RLEs
        for(prov_id i = 0; i < n; i++)
        {
                //generate file name
                u8 buffer[16];
                itoa(i, (char*) buffer, 10);
                std::string number = (char*) buffer;
                filename = filepath + number + ".rlep";
        
                //attempt to load
                CProvinceRLE* rle = new CProvinceRLE(filename);
                if(!rle->hasSucceeded()) {
                        delete rle;
                        succeeded = false;
                        return;
                }
                
                rles.push_back(rle);
        }
        
        rects.resize(n);
    
        std::ifstream is;
    
        //derive path from base directory
        filename = filepath + "Rects.dat";
    
        //open list of land rectangles
        is.open(filename.c_str(), std::ios::binary);
    
        if(!is.is_open())
        {
                LOG_PRINT(filename << " could not be opened!");
                succeeded = false;
                return;
         }
    
        //go through all rects and read
        for(prov_id i = 0; i < n; i++)
        {
                is.read((char*) &rects[i].left, sizeof(i32));
                is.read((char*) &rects[i].top, sizeof(i32));
                is.read((char*) &rects[i].right, sizeof(i32));
                is.read((char*) &rects[i].bottom, sizeof(i32));
        }
    
        is.close();
    
        succeeded = true;
}

CProvinceRLESet::~CProvinceRLESet()
{
        //iterate through entire vector releasing memory
        for(std::vector<CProvinceRLE*>::iterator it = rles.begin(); it != rles.end(); it++) {
                delete *it;
        }
}

CProvinceRLE* CProvinceRLESet::getRLE(prov_id i)
{
    if(i >= 0 && i < rles.size())
    {
        return rles[i];
    }
    else
    {
        return 0;
    }
}

Si32_rect* CProvinceRLESet::getRect(prov_id i)
{
    if(i >= 0 && i < rles.size())
    {
        return &rects[i];
    }
    else
    {
        return 0;
    }
}
