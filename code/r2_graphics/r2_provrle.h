//r2_provrle.h

//This code is published under the General Public License.

#ifndef R2_PROVRLE_H
#define R2_PROVRLE_H

#include <SDL.h>
#include <string>
#include <vector>

#include "..\r2_graphics\r2_video.h"
#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_game\r2_world.h"

class CProvinceRLE
{
protected: //structs
        struct SRLE_Entry
        {
                u8 color;
                u8 length;
        };
protected: //variables
        u32 width;
        u32 height;
        u32 count;
        SRLE_Entry* entries;
        u32* row_entry;
        u8* row_pixel;
        bool succeeded;
        bool isDrawn;
        r2::SPalette palette;
        const r2::SPalette* palette2;
protected: //functions
        bool MoveIndexByPixels(u32 num, u32& pixel_i, u32& entry_i);
        inline void MoveIndexToRow(u32 r, u32& pixel_i, u32& entry_i) {
                entry_i = row_entry[r];
                pixel_i = row_pixel[r];
        }
public:
        //construction/destruction
        CProvinceRLE(const std::string& filename);
        ~CProvinceRLE();
    
        bool hasSucceeded() {return succeeded;}
    
        //!Draws the entire RLE to the destination surface with a transparent key.
        bool drawEntireTransparent(r2::CSurface& dest, Si32_point& destPoint, u8 transparentKey);
        //!Draws a rectangle of pixels in the RLE to the destination surface with a transparent key.
        bool drawPartTransparent(r2::CSurface& dest, Si32_point& destPoint, Si32_rect& srcRect,
                u8 transparentKey);
        bool drawPartSubtract(r2::CSurface& dest, Si32_point& destPoint, Si32_rect& srcRect);
        //!Gets the value of a pixel in the RLE.
        u8 GetPixel(u32 x, u32 y);
    
        //read onlies
        u32 GetWidth() {return width;}
        u32 GetHeight(){return height;}
        u32 GetEntryCount() {return count;}
        
        //drawn state handler
        bool hasBeenDrawn() {return isDrawn;}
        void setDrawn(bool state) {isDrawn = state;}

        void setPalette(const r2::SPalette& pal, const r2::CSurface& surface);
};

//province graphics handling
class CProvinceRLESet
{
protected:
    std::vector<CProvinceRLE*> rles;
    std::vector<Si32_rect> rects;
    bool succeeded;
public:
    //construction/destruction
    CProvinceRLESet(const std::string& filepath, prov_id n);
    ~CProvinceRLESet(void);
    
    bool hasSucceeded() {return succeeded;}
    
    //data access
    CProvinceRLE* getRLE(prov_id i);
    Si32_rect* getRect(prov_id i);
    u16 count() {return rles.size();}
};

#endif //!R2_PROVRLE_H
