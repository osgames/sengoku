//r2_surface.cpp

//This code is published under the General Public License.

#include "..\r2_graphics\r2_surface.h"
#include "..\r2_graphics\r2_video.h"
#include <SDL_image.h>

using namespace r2;

bool CSurface::loadImage(const std::string& file, bool videoFormat, bool alpha)
{
        ASSERT(!loaded);
        
        //yes!!! now supports many formats
        SDL_Surface* bitmap = IMG_Load(file.c_str());
        //SDL_Surface* bitmap = SDL_LoadBMP(file.c_str());
    
        if(bitmap != 0) {
                if(videoFormat) {
                        //convert bitmap to display format with or without alpha
                        if(alpha) {
                                surface = SDL_DisplayFormatAlpha(bitmap);
                        } else {
                                surface = SDL_DisplayFormat(bitmap);
                        }
                        SDL_FreeSurface(bitmap);
                        
                } else {
                        //ignore whether alpha was specified or not - the file should specify that now
                        surface = bitmap;
                }
                
                if(surface == 0) {
                        LOG_ERROR("Failed to convert image format");
                        return false;
                }
                
                if(alpha) {
                        SDL_SetAlpha(surface, SDL_SRCALPHA, 255);
                } else {
                        SDL_SetAlpha(surface, 0, 255);
                }
        } else {
                LOG_ERROR("Failed to load image file " << file);
                return false;
        }
    
        destroyable = true;
        loaded = true;
        colorKey = COLORKEY_NONE;
        lock_refcount = 0;
    
        return true;
}

bool CSurface::blank(u32 width, u32 height, u8 bytesPerPixel, bool videoFormat, bool alpha)
{
        ASSERT(!loaded);
    
        //set flags based on hardware or software residence
        u32 flags = SDL_SWSURFACE;
    
        //use different versions based on whether video's pixel format is wanted
        if(videoFormat) {
                //fetch surface of display for retrival of pixel format
                CSurface screen;
                if(Video_GetScreenSurface(screen)) {
                        //create a surface with what in theory should be the same format as screen first
                        SDL_Surface* tempSurface = SDL_CreateRGBSurface(flags, width, height,
                                screen.surface->format->BitsPerPixel,
                                screen.surface->format->Rmask,
                                screen.surface->format->Gmask,
                                screen.surface->format->Bmask,
                                screen.surface->format->Amask);
                        if(tempSurface == 0) {
                                LOG_ERROR("Error creating intermediate surface");
                                return false;
                        }
                        
                        //convert bitmap to display format with or without alpha specifically
                        if(alpha) {
                                surface = SDL_DisplayFormatAlpha(tempSurface);
                        } else {
                                surface = SDL_DisplayFormat(tempSurface);
                        }
                        SDL_FreeSurface(tempSurface);
                } else {
                        LOG_ERROR("Couldn't access video format");
                        return false;
                }
        } else {
                //attempt creation with alpha in LSB and R, G, B assigned anywhere else
                surface = SDL_CreateRGBSurface(flags, width, height,
                        (alpha ? bytesPerPixel + 1 : bytesPerPixel) * 8,
                        0, 0, 0, alpha ? 0xff : 0);
        }
    
        if(surface == 0) {
                LOG_ERROR("Couldn't create blank surface");
                return false;
        }
    
        destroyable = true;
        colorKey = COLORKEY_NONE;
        lock_refcount = 0;
        loaded = true;
    
        return true;
}

void CSurface::unload()
{
    if(loaded && destroyable)
    {
        //unlock surface
        u32 l = lock_refcount;
        for(u32 i = 0; i < l; i++)
        {
            unlock();
        }
        
        SDL_FreeSurface(surface);
        loaded = false;
    }
}

void CSurface::drawFilledRect(Si32_rect* rc, u8 r, u8 g, u8 b)
{
        ASSERT(loaded);
        
        SDL_Rect sdl_rc;
    
        //calculate color according to surface color format
        u32 color = SDL_MapRGB(surface->format, r, g, b);
    
        if(rc) {
                //fill destination rectangle
                toSDL_Rect(*rc, sdl_rc);
                SDL_FillRect(surface, &sdl_rc, color);
        } else {
                //if destination rectangle isn't specified blit to entire surface
                SDL_FillRect(surface, 0, color);
        }
}

void CSurface::drawHollowRect(Si32_rect& rc, u8 r, u8 g, u8 b, u32 width)
{
        ASSERT(loaded);
        
        if(width == 0) {
                return;
        }
        
        //calculate color according to surface color format
        u32 color = SDL_MapRGB(surface->format, r, g, b);
        
        SDL_Rect sdlRect;
        i32 w, h;
        
        sdlRect.x = rc.left;
        sdlRect.y = rc.top;
        w = rc.right - rc.left - width;
        h = width;
        if(w > 0 && h > 0) {
                sdlRect.w = w;
                sdlRect.h = h;
                SDL_FillRect(surface, &sdlRect, color);
        }
        
        sdlRect.x = rc.right - width;
        sdlRect.y = rc.top;
        w = width;
        h = rc.bottom - rc.top - width;
        if(w > 0 && h > 0) {
                sdlRect.w = w;
                sdlRect.h = h;
                SDL_FillRect(surface, &sdlRect, color);
        }
        
        sdlRect.x = rc.left + width;
        sdlRect.y = rc.bottom - width;
        w = rc.right - rc.left - width;
        h = width;
        if(w > 0 && h > 0) {
                sdlRect.w = w;
                sdlRect.h = h;
                SDL_FillRect(surface, &sdlRect, color);
        }
        
        sdlRect.x = rc.left;
        sdlRect.y = rc.top + width;
        w = width;
        h = rc.bottom - rc.top - width;
        if(w > 0 && h > 0) {
                sdlRect.w = w;
                sdlRect.h = h;
                SDL_FillRect(surface, &sdlRect, color);
        }
}

void CSurface::blit(CSurface& src, const Si32_rect* srcRect, const Si32_rect* destRect)
{
        ASSERT(loaded);
        ASSERT(src.loaded);
        
        if(srcRect != 0) {
                SDL_Rect sdlSrc;
                toSDL_Rect(*srcRect, sdlSrc);
                if(destRect != 0) {
                        SDL_Rect sdlDest;
                        toSDL_Rect(*destRect, sdlDest);
                        ASSERT(SDL_BlitSurface(src.surface, &sdlSrc, surface, &sdlDest) == 0);
                } else {
                        ASSERT(SDL_BlitSurface(src.surface, &sdlSrc, surface, 0) == 0);
                }
        } else {
                if(destRect != 0) {
                        SDL_Rect sdlDest;
                        toSDL_Rect(*destRect, sdlDest);
                        ASSERT(SDL_BlitSurface(src.surface, 0, surface, &sdlDest) == 0);
                } else {
                        ASSERT(SDL_BlitSurface(src.surface, 0, surface, 0) == 0);
                }
        }
        
}

void CSurface::clippedBlit(CSurface& src, const Si32_rect* srcRect, const Si32_rect* destRect,
        const Si32_rect& clipRect)
{
        ASSERT(loaded);
        ASSERT(src.loaded);
        
        Si32_rect actualDest;
        Si32_rect actualSrc; //actual source rectangle, can be clipped
        
        if(destRect == 0) {
                actualDest.left = 0;
                actualDest.top = 0;
                actualDest.right = getWidth();
                actualDest.bottom = getHeight();
        } else {
                actualDest = *destRect;
        }

        if(srcRect == 0) {
                actualSrc.left = 0;
                actualSrc.top = 0;
                actualSrc.right = src.getWidth();
                actualSrc.bottom = src.getHeight();
        } else {
                actualSrc = *srcRect;
        }
        
        //make sure clip rect isn't larger than destination surface
        Si32_rect actualClip;
        actualClip.left = clipRect.left < 0 ? 0 : clipRect.left;
        actualClip.top = clipRect.top < 0 ? 0 : clipRect.top;
        actualClip.right = clipRect.right > getWidth() ? getWidth() : clipRect.right;
        actualClip.bottom = clipRect.bottom > getHeight() ? getHeight() : clipRect.bottom;
        
        //do the clipping of source and destinations
        if(actualDest.left < actualClip.left) {
                actualSrc.left += actualClip.left - actualDest.left;
                actualDest.left = actualClip.left;
        }
        if(actualDest.top < actualClip.top) {
                actualSrc.top += actualClip.top - actualDest.top;
                actualDest.top = actualClip.top;
        }
        if(actualDest.right > actualClip.right) {
                actualSrc.right += actualClip.right - actualDest.right;
                actualDest.right = actualClip.right;
        }
        if(actualDest.bottom > actualClip.bottom) {
                actualSrc.bottom += actualClip.bottom - actualDest.bottom;
                actualDest.bottom = actualClip.bottom;
        }
        
        //only draw if destination & source rectangles are valid
        if(actualDest.left < actualDest.right && actualDest.top < actualDest.bottom
                && actualSrc.left < actualSrc.right && actualSrc.top < actualSrc.bottom) {
                SDL_Rect sdlSrc, sdlDest;
                toSDL_Rect(actualSrc, sdlSrc);
                toSDL_Rect(actualDest, sdlDest);
                ASSERT(SDL_BlitSurface(src.surface, &sdlSrc, surface, &sdlDest) == 0);
        }
}

u32 CSurface::drawText(const char* str, Si32_point& pos, CFont& font, i32 maxWidth)
{
        ASSERT(loaded);
        
  u8* text = (u8*) str;
  i32 lineWidth = 0;
  
  //set the pointer to the initial position
  Si32_point pointer = pos;
  
  //step through all characters in string
  u32 i; //this must be outside for loop to allow return statement outside of loop
  for(i = 0; text[i] != 0; i++)
  {
    u8 c = text[i];
    
    //deal with some special characters first
    if(c == '\n')
    {
      pointer.x = pos.x;
      pointer.y += font.getLineHeight();
      lineWidth = 0;
    }
    //then this is the regular character drawing routine
    else if(font.getChar(c).valid)
    {
      //do we have a maximum width at all:
      if(maxWidth >= 0)
      {
        //we have, make sure we don't exceed it
        if(lineWidth + font.getChar(c).srcRect.right - font.getChar(c).srcRect.left
          > maxWidth)
        {
          return i;
        }
        
        lineWidth += font.getChar(c).advanceX;
      }
      
                //calculate destination rectangle
                Si32_rect destRect;
                destRect.left = pointer.x + font.getChar(c).offset.x;
                destRect.top = pointer.y + font.getChar(c).offset.y;
                destRect.right = destRect.left + font.getChar(c).srcRect.right -
                font.getChar(c).srcRect.left; //destRect.left + width
                destRect.bottom = destRect.top + font.getChar(c).srcRect.bottom -
                font.getChar(c).srcRect.top; //destRect.top + height
      
                //print character
                if(font.getBitmap() != 0) {
                        blit(*font.getBitmap(), &font.getChar(c).srcRect, &destRect);
                }
      
                //move pointer
                pointer.x += font.getChar(c).advanceX;
      
                //TODO: include kerning effects here
                }
        }
  
        return i;
}

u8* CSurface::lock()
{
        ASSERT(loaded);
        
        SDL_LockSurface(surface);
    
        //if we reached here everything is fine
        lock_refcount++;
        return (u8*) surface->pixels;
}

void CSurface::unlock()
{
        ASSERT(loaded);
        
        SDL_UnlockSurface(surface);
    
        //everything is fine
        lock_refcount--;
}

void CSurface::printInfo()
{
        ASSERT(loaded);
        
        LOG_PRINT("");
        LOG_PRINT("Dimensions:");
        LOG_PRINT("Width: " << getWidth());
        LOG_PRINT("Height: " << getHeight());
        LOG_PRINT("");
        LOG_PRINT("Flags:");
        if(surface->flags & SDL_HWSURFACE) {
                LOG_PRINT("Hardware surface");
        } else {
                LOG_PRINT("Software surface");
        }
        if(surface->flags & SDL_ASYNCBLIT) {
                LOG_PRINT("Async blit");
        }
        if(surface->flags & SDL_ANYFORMAT) {
                LOG_PRINT("Any format");
        }
        if(surface->flags & SDL_HWPALETTE) {
                LOG_PRINT("Hardware palette");
        }
        if(surface->flags & SDL_DOUBLEBUF) {
                LOG_PRINT("Double buffered");
        }
        if(surface->flags & SDL_FULLSCREEN) {
                LOG_PRINT("Fullscreen");
        }
        if(surface->flags & SDL_OPENGL) {
                LOG_PRINT("OpenGL context");
        }
        if(surface->flags & SDL_OPENGLBLIT) {
                LOG_PRINT("OpenGL blit");
        }
        if(surface->flags & SDL_RESIZABLE) {
                LOG_PRINT("Resizable");
        }
        if(surface->flags & SDL_HWACCEL) {
                LOG_PRINT("Hardware accelerated");
        }
        if(surface->flags & SDL_SRCCOLORKEY) {
                LOG_PRINT("Source color key");
        }
        if(surface->flags & SDL_RLEACCEL) {
                LOG_PRINT("RLE accelerated");
        }
        if(surface->flags & SDL_SRCALPHA) {
                LOG_PRINT("Source alpha");
        }
        if(surface->flags & SDL_PREALLOC) {
                LOG_PRINT("Preallocated memory");
        }
        LOG_PRINT("");
        LOG_PRINT("Format:");
        LOG_PRINT("BPP: " << (u16) surface->format->BytesPerPixel);
        LOG_PRINT("Mask: (" << (u16) surface->format->Rmask << ", " << (u16) surface->format->Gmask << ", " <<
                (u16) surface->format->Bmask << ", " << (u16) surface->format->Amask << ")");
        LOG_PRINT("Colorkey: " << surface->format->colorkey);
        LOG_PRINT("Alpha: " << (u16) surface->format->alpha);
        LOG_PRINT("");
}

void CSurface::setColorKey(u8 r, u8 g, u8 b)
{
        ASSERT(loaded);
        keyR = r;
        keyG = g;
        keyB = b;
        colorKey = COLORKEY_RGB;
        u32 color = SDL_MapRGB(surface->format, keyR, keyG, keyB);
        SDL_SetColorKey(surface, SDL_SRCCOLORKEY /*| SDL_RLEACCEL*/, color);
}

void CSurface::setColorKey(u8 pal_entry)
{
        ASSERT(loaded);
        keyPal = pal_entry;
        u32 color = u32(keyPal);
        colorKey = COLORKEY_PAL;
        SDL_SetColorKey(surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, color);
}

void CSurface::resetColorKey()
{
        if(colorKey == COLORKEY_RGB) {
                setColorKey(keyR, keyG, keyB);
        } else if(colorKey == COLORKEY_PAL) {
                setColorKey(keyPal);
        }
}

void CSurface::disableColorKey()
{
        ASSERT(loaded);
        SDL_SetColorKey(surface, 0, 0);
        colorKey = COLORKEY_NONE;
}

void CSurface::setPalette(SPalette& pal)
{
        ASSERT(loaded);
        SDL_SetPalette(surface, SDL_LOGPAL, pal.entries, 0, 256);
}

u8 CSurface::getColorKey(u8& comp1, u8& comp2, u8& comp3)
{
        switch(colorKey) {
                case COLORKEY_PAL: {
                        comp1 = keyPal;
                        break;
                } case COLORKEY_RGB: {
                        comp1 = keyR;
                        comp2 = keyG;
                        comp3 = keyB;
                        break;
                }
        }
        
        return colorKey;
}

u32 CSurface::producePixelRGB(u8 r, u8 g, u8 b)
{
        ASSERT(loaded);
        return surface != 0 ? SDL_MapRGB(surface->format, r, g, b) : 0;
}
