//r2_surface.h

//This code is published under the General Public License.

#ifndef R2_SURFACE_H
#define R2_SURFACE_H

#include "..\r2_graphics\r2_palette.h"
#include "..\r2_graphics\r2_font.h"

#define COLORKEY_NONE 0
#define COLORKEY_PAL 1
#define COLORKEY_RGB 2

namespace r2
{

//surface wrapper class, do not call any functions that are API specific
class CSurface
{
//API specific variables, functions, and inner workings...
//...not okay to use elsewhere:
public:
        u32 lock_refcount;
        SDL_Surface* surface;
        u8 keyR;
        u8 keyG;
        u8 keyB;
        u8 keyPal;
        u8 colorKey;
        bool loaded;
        bool destroyable;
//non API specific variable and functions, okay to use elsewhere:
public:
        //Resource management methods:
        //!Loads an image file (rather many formats). Pixel data can be converted to video format, and be forced to/from alphablending.
        bool loadImage(const std::string& file, bool videoFormat, bool alpha);
        //!Allocates memory for a blank surface with a number of bytes per pixel. Can be forced into video format.
        bool blank(u32 width, u32 height, u8 bytesPerPixel, bool videoFormat, bool alpha);
        //!Releases all memory used by this surface.
        void unload();
    
        //Drawing methods:
        //!Fills a destination rectangle with a specific color. If no rect is specified entire screen is filled.
        void drawFilledRect(Si32_rect* rc, u8 r, u8 g, u8 b);
        //!Draws a frame in a destination rectangle with a specific color.
        void drawHollowRect(Si32_rect& rc, u8 r, u8 g, u8 b, u32 width);
        //!Simply blits the contents of a part of a source surface onto this surface.
        void blit(CSurface& src, const Si32_rect* srcRect, const Si32_rect* destRect);
        //!Same as blit() but performs destination clipping.
        void clippedBlit(CSurface& src, const Si32_rect* srcRect, const Si32_rect* destRect,
                const Si32_rect& clipRect);
        //!Draws a string at a position, with a maximum number of characters.
        u32 drawText(const char* str, Si32_point& pos, r2::CFont& font,
                i32 maxWidth = -1); //returns number of characters drawn
        
        //Lock methods:
        u8* lock();
        void unlock();
        bool IsLocked() {return loaded && (lock_refcount != 0);}
        
        //!Prints information about surface.
        void printInfo();
    
        //Color keying:
        void setColorKey(u8 r, u8 g, u8 b);
        void setColorKey(u8 pal_entry);
        void resetColorKey();
        void disableColorKey();
        void setPalette(SPalette& pal);
        //!Returns type of color key. Sets comp1 = palette index if key is paletted, sets (comp1, comp2, comp3) = (R, G, B) if key is color.
        u8 getColorKey(u8& comp1, u8& comp2, u8& comp3);
        //!Creates a u32 pixel from RGB components.
        u32 producePixelRGB(u8 r, u8 g, u8 b);
    
        u32 getWidth() {return surface != 0 ? surface->w : 0;}
        u32 getHeight() {return surface != 0 ? surface->h : 0;}
        
        CSurface() {loaded = false;}
        ~CSurface() {unload();}
        
        //!Gets a pixel at a position in surface, and assumes surface is locked.
        inline u32 getPixel(u32 x, u32 y)
        {
                int bpp = surface->format->BytesPerPixel;
                u8 *p = (u8*) surface->pixels + y * surface->pitch + x * bpp;

                switch(bpp) {
                        case 1: {
                                return *p;
                        } case 2: {
                                return *(u16*)p;
                        } case 3: {
                                #if SDL_BYTEORDER == SDL_BIG_ENDIAN
                                        return p[0] << 16 | p[1] << 8 | p[2];
                                #else
                                        return p[0] | p[1] << 8 | p[2] << 16;
                                #endif
                        } case 4: {
                                return *(u32*)p;
                        } default: {
                                return 0;
                        }
                }
        }

    
        //!Draws many �ixels in a row of the same colour on same scan line
        inline void putPixels(u32 n, i32 x, i32 y, u32 pixel)
        {
                i32 bpp = surface->format->BytesPerPixel;
                u8* p = (u8*) surface->pixels + y * surface->pitch + x * bpp;
                u8* end_p = p + n * bpp; //number of bytes to draw

                switch(bpp) {
                        case 1: {
                                for(; p != end_p; p++) {
                                        *p = (u8) pixel;
                                }
                                break;
                         } case 2: {
                                for(; p != end_p; p += 2) {
                                        *(u16*)p = pixel;
                                }
                                break;
                        } case 3: {
                                for(; p != end_p; p += 3) {
                                        #if SDL_BYTEORDER == SDL_BIG_ENDIAN
                                                p[0] = (pixel >> 16) & 0xff;
                                                p[1] = (pixel >> 8) & 0xff;
                                                p[2] = pixel & 0xff;
                                        #else
                                                p[0] = pixel & 0xff;
                                                p[1] = (pixel >> 8) & 0xff;
                                                p[2] = (pixel >> 16) & 0xff;
                                        #endif
                                }
                                break;
                        } case 4: {
                                for(; p != end_p; p += 4) {
                                        *(u32*)p = pixel;
                                }
                                break;
                        }
                }
        }
        
        //!Subtracts a color from a pixel. Can be 8bit palette, if that's the case, r contains palette entry.
        inline void subtractPixels(u32 n, i32 x, i32 y, u8 r /*also is 8 bit palette*/, u8 g = 0, u8 b = 0)
        {
                i32 bpp = surface->format->BytesPerPixel;
                
                if((bpp == 1 && r <= 1) || (r <= 1 && g <= 1 && b <= 1)) {
                        //the reason I quit here is twofold: 1) this operation degrades pixel quality
                        //for certain truecolor formats, thus introducing a visible border even for values
                        //of r, g, b = 0, 0, 0 distinct from untouched pixels, second it helps to skip this
                        //operation is it's transparent
                        return;
                }
                
                u8 *p = (u8 *)surface->pixels + y * surface->pitch + x * bpp;
                u8 *end_p = p + n * bpp; //number of bytes to draw

                //truecolor, blah
                switch(bpp) {
                case 1:
                        for(; p != end_p; p++) {
                                i16 color = (i16)*p - r;
                                color = color < 0 ? 0 : color;
                                *p = color;
                        }
                        break;
                case 2:
                        for(; p != end_p; p += 2) {
                                u16 color = *(u16*)p;
                                
                                i16 totR = i16(((color & surface->format->Rmask) >> surface->format->Rshift) <<
                                        surface->format->Rloss) - r;
                                i16 totG = i16(((color & surface->format->Gmask) >> surface->format->Gshift) <<
                                        surface->format->Gloss) - g;
                                i16 totB = i16(((color & surface->format->Bmask) >> surface->format->Bshift) <<
                                        surface->format->Bloss) - b;
                                
                                //clamp colors
                                totR = totR < 0 ? 0 : totR;
                                totG = totG < 0 ? 0 : totG;
                                totB = totB < 0 ? 0 : totB;
                                
                                *(u16*)p = ((totR >> surface->format->Rloss) << surface->format->Rshift) |
                                        ((totG >> surface->format->Gloss) << surface->format->Gshift) |
                                        ((totB >> surface->format->Bloss) << surface->format->Bshift);
                        }
                        break;
                case 3:
                        for(; p != end_p; p += 3) {
                                u32 color = *p; //change because this is definately wrong
                                
                                //notice the +1:s below are to shift by an extra bit to divide color by 2
                                i16 totR = i16(((color & surface->format->Rmask) >> surface->format->Rshift) <<
                                        surface->format->Rloss) - r;
                                i16 totG = i16(((color & surface->format->Gmask) >> surface->format->Gshift) <<
                                        surface->format->Gloss) - g;
                                i16 totB = i16(((color & surface->format->Bmask) >> surface->format->Bshift) <<
                                        surface->format->Bloss) - b;
                                
                                //clamp colors
                                totR = totR < 0 ? 0 : totR;
                                totG = totG < 0 ? 0 : totG;
                                totB = totB < 0 ? 0 : totB;
                                
                                #if SDL_BYTEORDER == SDL_BIG_ENDIAN
                                    p[0] = totR;
                                    p[1] = totG;
                                    p[2] = totB;
                                #else
                                    p[0] = totB;
                                    p[1] = totG;
                                    p[2] = totR;
                                #endif
                        }
                        break;
                case 4:
                        for(; p != end_p; p += 4) {
                                u32 color = *(u32*) p;
                                
                                i16 totR = i16(((color & surface->format->Rmask) >> surface->format->Rshift) <<
                                        surface->format->Rloss) - r;
                                i16 totG = i16(((color & surface->format->Gmask) >> surface->format->Gshift) <<
                                        surface->format->Gloss) - g;
                                i16 totB = i16(((color & surface->format->Bmask) >> surface->format->Bshift) <<
                                        surface->format->Bloss) - b;
                                
                                //clamp colors
                                totR = totR < 0 ? 0 : totR;
                                totG = totG < 0 ? 0 : totG;
                                totB = totB < 0 ? 0 : totB;
                                
                                *(u32*)p = ((totR >> surface->format->Rloss) << surface->format->Rshift) |
                                        ((totG >> surface->format->Gloss) << surface->format->Gshift) |
                                        ((totB >> surface->format->Bloss) << surface->format->Bshift);
                        }
                        break;
                }
        }
};

} //!namespace r2

#endif //!R2_SURFACE_H
