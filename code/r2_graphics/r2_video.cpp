//r2_video.cpp

//This code is published under the General Public License.

#include "..\r2_graphics\r2_video.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_errors.h"

using namespace r2;

bool g_video_inited = false;

bool Video_Init(u32 width, u32 height, u32 bpp, bool hardwareStored)
{
        ASSERT(!g_video_inited);

        //try to initialize the video sub system of SDL
        if(SDL_InitSubSystem(SDL_INIT_VIDEO) < 0) {
                LOG_ERROR(ERRORMSG_SDLVIDEO);
                return false; //do not continue
        }
    
        //do things differently depending on whether hardware is desired
        if(hardwareStored) {
                if(SDL_SetVideoMode(width, height, bpp,
                                SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN) < 0) {
                        Log_Print(ERRORMSG_VIDEOMODEHARDWARE);
                        SDL_QuitSubSystem(SDL_INIT_VIDEO);
                        return false;
                }
        } else {
                if(SDL_SetVideoMode(width, height, bpp,
                                SDL_SWSURFACE | SDL_FULLSCREEN) < 0) {
                        Log_Print(ERRORMSG_VIDEOMODESOFTWARE);
                        SDL_QuitSubSystem(SDL_INIT_VIDEO);
                        return false;
                }
        }
    
        //hide Windows cursor
        SDL_ShowCursor(SDL_DISABLE);
    
        g_video_inited = true;
    
        return true;
}

void Video_Exit()
{
        if(g_video_inited) {
                SDL_QuitSubSystem(SDL_INIT_VIDEO);
                g_video_inited = false;
        }
}

bool Video_GetScreenSurface(CSurface& surf)
{
        ASSERT(g_video_inited);
    
        //get front surface and check for failure
        SDL_Surface* screen = SDL_GetVideoSurface();
    
        if(screen != 0) {
                surf.surface = screen;
                surf.destroyable = false;
                surf.loaded = true;
                surf.lock_refcount = 0;
        
                return true;
        } else {
                LOG_ERROR("An attempt to access an uninitialized screen surface was made.");
                return false;
        }
}

bool Video_Flip()
{
        ASSERT(g_video_inited);
    
        SDL_Flip(SDL_GetVideoSurface());
    
        return true; //always true for now
}
