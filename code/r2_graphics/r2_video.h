//r2_video.h - video system which implicitly uses SDL

//This code is published under the General Public License.

#ifndef R2_VIDEO_H
#define R2_VIDEO_H

#include <SDL.h>
#include <iostream>
#include "..\r2_core\r2_res.h"
#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_graphics\r2_font.h"
#include "..\r2_graphics\r2_bitmap.h"
#include "..\r2_graphics\r2_surface.h"
#include <map>


//creation and destruction
bool Video_Init(u32 width, u32 height, u32 bpp, bool hardwareStored);
void Video_Exit(void);

//frame handling
bool Video_GetScreenSurface(r2::CSurface& surf);
bool Video_Flip(void);

struct SRGB
{
        u8 r;
        u8 g;
        u8 b;
        
        SRGB()
        {
                r = g = b = 0;
        }
        
        SRGB(u8 r, u8 g, u8 b)
        {
                SRGB::r = r;
                SRGB::g = g;
                SRGB::b = b;
        }
};

//SDL conversion functions
inline void toSDL_Rect(const Si32_rect& src, SDL_Rect& dest)
{
        dest.x = src.left;
        dest.y = src.top;
        dest.w = src.right - src.left;
        dest.h = src.bottom - src.top;
}

#endif //!R2_VIDEO_H
