//r2_gui.cpp

//This code is published under the General Public License.

#include "r2_gui.h"
#include "..\r2_core\r2_creator.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_game\r2_game.h"
#include "..\r2_core\r2_filepaths.h"
#include "..\r2_gui\r2_provmap.h"
#include "..\r2_graphics\r2_cursor.h"

using namespace r2;

CGUI::CGUI() : rootWindow(new CScreenBackground())
{
        if(rootWindow.get() == 0) {
                LOG_ERROR("Failed to initialize root window. This will almost certainly cause a crash. "
                        "Memory allocation probably is to blaim.");
        }
        inited = false;
}

std::string CGUI::iterateHierarchyToChild(const std::string& idHierarchy, r2::CListTreeNode<CWindow*>*& it)
{
        /*!This function treats a string of the format parent1ID/parent2ID/../parentnID/childID.
        It returns the childID, and uses the parent IDs to advance a reference to a list node.
        If a parent ID isn't found a warning is printed, but operation continues.*/
        
        std::string curr = "";
        u32 j = 0;
        
        for(i32 i = idHierarchy.find("/", 0); i != std::string::npos; i = idHierarchy.find("/", i)) {
                curr = idHierarchy.substr(j, i - j);
                if(curr != "") {
                        CWindow::CStringComparator comp;
                        it = it->findChild(curr, 1, comp);
                        if(it == 0) {
                                LOG_WARNING("Non-existing window id \"" << curr << "\"");
                        }
                }
                j = ++i;
        }
        
        return idHierarchy.substr(j, idHierarchy.size() - j);
}

void CGUI::iterateHierarchy(const std::string& idHierarchy, r2::CListTreeNode<CWindow*>*& it)
{
        /*!This function treats a string of the format parent1ID/parent2ID/../parentnID/childID.
        It advances a reference to a list node.
        If an ID isn't found the function returns false.*/
        
        std::string curr = "";
        u32 j = 0;
        
        for(i32 i = idHierarchy.find("/", 0); i != std::string::npos; i = idHierarchy.find("/", i)) {
                curr = idHierarchy.substr(j, i - j);

                if(curr != "") {
                        CWindow::CStringComparator comp;
                        it = it->findChild(curr, 1, comp);
                        if(it == 0) {
                                LOG_WARNING("Non-existing window id \"" << curr << "\"");
                        }
                }
                j = ++i;
        }
        
        curr = idHierarchy.substr(j, idHierarchy.size() - j);
        if(curr != "") {
                CWindow::CStringComparator comp;
                it = it->findChild(curr, 1, comp);
                if(it == 0) {
                        LOG_WARNING("Non-existing window id \"" << curr << "\"");
                }
        }
}

bool CGUI::init()
{
        ASSERT(!inited);
        
        rootWindow.get()->init("root", luabind::object());
        pointedWindow = 0;
        focusedWindow = 0;
        keyhandler = 0;
        
        return initLua();
}

bool CGUI::initLua()
{
        using namespace luabind;
        
        //private Lua for interface build
        CLua lua;
        if(lua.state() != 0) {
                bindFunctionsWithLua(lua);
                if(!lua.doFile(FILE_SCRIPTS_INTERFACE)) {
                        return false;
                }
                try {
                        call_function<void>(lua.state(), "buildInterface", boost::ref(*this));
                } catch(error& e) {
                        CLua::printError(e);
                        return false;
                }
        } else {
                LOG_ERROR("Error initializing Lua first time.");
                return false;
        }
        
        
        bindFunctionsWithLua(r2::globals.commonLua);
        if(!r2::globals.commonLua.doFile(FILE_SCRIPTS_ACTIONS)) {
                return false;
        }
}

void CGUI::destroy()
{
        if(inited) {
                deleteWindow(rootWindow);
                rootWindow.removeChildren();
                
                inited = false;
        }
}

void CGUI::bindFunctionsWithLua(CLua& l)
{
        using namespace luabind;
        
        if(l.state() == 0) {
                return;
        }
        
        module(l.state())
        [
                class_ <CGUI>("CGUI")
                .def("createWindow", &CGUI::createWindow)
                .def("selectWindow", &CGUI::selectWindow)
                .def("setArea", &CGUI::setArea)
                .def("getWidth", &CGUI::getWidth)
                .def("getHeight", &CGUI::getHeight)
                .def("setBitmap", &CGUI::setBitmap)
                .def("setBitmapArea", &CGUI::setBitmapArea)
                .def("setBitmapRGBColorKey", &CGUI::setBitmapRGBColorKey)
                .def("setBitmapPaletteColorKey", &CGUI::setBitmapPaletteColorKey)
                .def("setRenderMode", &CGUI::setRenderMode)
                .def("setColor", &CGUI::setColor)
                .def("setVisible", &CGUI::setVisible)
                .def("isVisible", &CGUI::isVisible)
                .def("addSprite", &CGUI::addSprite)
                .def("removeSprite", &CGUI::removeSprite)
                .def("animateSprite", &CGUI::animateSprite)
                .def("moveSprite", &CGUI::moveSprite)
                .def("registerAction", &CGUI::registerAction)
                .def("hideScreens", &CGUI::hideScreens)
                .def("setText", &CGUI::setText)
                .def("getText", &CGUI::getText)
                .def("addTextItem", &CGUI::addTextItem)
                .property("itemCount", &CGUI::itemCount)
                .def("setTextItem", &CGUI::setTextItem)
                .def("getTextItem", &CGUI::getTextItem)
                .def("removeItems", &CGUI::removeItems)
                .def("clearItems", &CGUI::clearItems)
                .def("setItemScrolling", &CGUI::setItemScrolling)
                .def("scrollItems", &CGUI::scrollItems)
                .def("calculateVisibleItems", &CGUI::calculateVisibleItems)
                .def("setFont", &CGUI::setFont)
                .def("releaseFont", &CGUI::releaseFont)
                .def("setAsKeyhandler", &CGUI::setAsKeyhandler)
                .def("removeKeyhandler", &CGUI::removeKeyhandler)
                .property("cursorX", &CGUI::getCursorX, &CGUI::setCursorX)
                .property("cursorY", &CGUI::getCursorY, &CGUI::setCursorY)
                .def("forceRedraw", &CGUI::forceRedraw)
                .def("setMapScrolling", &CGUI::setMapScrolling)
                .def("scrollMap", &CGUI::scrollMap)
                .def("setMapMode", &CGUI::setMapMode)
                .def("getMapWidth", &CGUI::getMapWidth)
                .def("getMapHeight", &CGUI::getMapHeight)
                .def("setPalette", &CGUI::setPalette)
                .def("quitASAP", &CGUI::quitASAP)
                .property("screenWidth", &CGUI::screenWidth)
                .property("screenHeight", &CGUI::screenHeight)
                .property("screenBPP", &CGUI::screenBPP)
        ];
}

void CGUI::createWindow(const std::string& idHierarchy, const std::string& type,
        luabind::object const& initTable)
{
        if(R2_REGISTER_EXISTS(CWindow, type)) {
                CListTreeNode<CWindow*>* it = &rootWindow;
                std::string newId = iterateHierarchyToChild(idHierarchy, it);
                if(it != 0) {
                        CWindow* newWindow = R2_CREATE(CWindow, type);
                        ASSERT(newWindow != 0);
                        it->insertChildBack(newWindow);
                        newWindow->init(newId, initTable);
                        pointedWindow = newWindow;
                }
        } else {
                LOG_WARNING("Unknown window class \"" << type << "\"");
                pointedWindow = 0;
        }
}

void CGUI::selectWindow(const std::string& idHierarchy)
{
        CListTreeNode<CWindow*>* it = &rootWindow;
        iterateHierarchy(idHierarchy, it);
        pointedWindow = it != 0 ? it->get() : 0;
}

void CGUI::setArea(i32 left, i32 top, i32 right, i32 bottom)
{
        if(pointedWindow != 0) {
                Si32_rect r(left, top, right, bottom);
                pointedWindow->setArea(r);
        }
}

u32 CGUI::getWidth()
{
        if(pointedWindow != 0) {
                return pointedWindow->getWidth();
        }
        
        return 0;
}

u32 CGUI::getHeight()
{
        if(pointedWindow != 0) {
                return pointedWindow->getHeight();
        }
        
        return 0;
}

void CGUI::setBitmap(const std::string& usage, const std::string& filename, luabind::object table)
{
        if(pointedWindow != 0) {
                pointedWindow->setBitmap(usage, filename, table);
        }
}

void CGUI::setBitmapArea(const std::string& usage, i32 left, i32 top, i32 right, i32 bottom)
{
        if(pointedWindow != 0) {
                Si32_rect r(left, top, right, bottom);
                pointedWindow->setBitmapArea(usage, r);
        }
}


void CGUI::setBitmapRGBColorKey(const std::string& usage, u8 r, u8 g, u8 b)
{
        if(pointedWindow != 0) {
                SRGB rgb(r, g, b);
                pointedWindow->setBitmapColorKey(usage, rgb);
        }
}

void CGUI::setBitmapPaletteColorKey(const std::string& usage, u8 key)
{
        if(pointedWindow != 0) {
                pointedWindow->setBitmapColorKey(usage, key);
        }
}

void CGUI::setRenderMode(const std::string& mode)
{
        if(pointedWindow != 0) {
                pointedWindow->setRenderMode(mode);
        }
}

void CGUI::setColor(const std::string& usage, u8 r, u8 g, u8 b)
{
        if(pointedWindow != 0) {
                SRGB rgb(r, g, b);
                pointedWindow->setColor(usage, rgb);
        }
}

void CGUI::setVisible(bool v)
{
        if(pointedWindow != 0) {
                if(!v && focusedWindow != 0 && pointedWindow == focusedWindow->get()) {
                        focusedWindow = 0;
                }
                pointedWindow->setVisible(v, r2::globals.commonLua);
        }
}

bool CGUI::isVisible()
{
        return pointedWindow != 0 && pointedWindow->isVisible();
}

void CGUI::addSprite(const std::string& id, const std::string& filename,
                const std::string& type, i32 x, i32 y)
{
        if(pointedWindow != 0) {
                Si32_point p(x, y);
                pointedWindow->addSprite(id, filename, type, p);
        }
}

void CGUI::removeSprite(const std::string& id)
{
        if(pointedWindow != 0) {
                pointedWindow->removeSprite(id);
        }
}

void CGUI::animateSprite(const std::string& id, const std::string& animation)
{
        if(pointedWindow != 0) {
                pointedWindow->animateSprite(id, animation);
        }
}

void CGUI::moveSprite(const std::string& id, i32 x, i32 y)
{
        if(pointedWindow != 0) {
                Si32_point p(x, y);
                pointedWindow->moveSprite(id, p);
        }
}

void CGUI::registerAction(const std::string& event, const std::string& action)
{
        if(pointedWindow != 0) {
                pointedWindow->registerAction(event, action);
        }
}

void CGUI::hideScreens()
{
        CListTreeNodeIterator<CWindow*> it(rootWindow);
        while(it.hasNextChild()) {
                CWindow* window = it.nextChild()->get();
                ASSERT(window);
                window->setVisible(false, r2::globals.commonLua);
        }
}

void CGUI::setText(const std::string& str)
{
        if(pointedWindow != 0) {
                CTextBox* tb = dynamic_cast<CTextBox*>(pointedWindow);
                
                if(tb != 0) {
                        tb->setText(str);
                } else {
                        wrongWindowWarning("setText");
                }
        }
}

std::string CGUI::getText()
{
        if(pointedWindow != 0) {
                CTextBox* tb = dynamic_cast<CTextBox*>(pointedWindow);
                
                if(tb != 0) {
                        return tb->getTextCopy();
                } else {
                        wrongWindowWarning("getText");
                }
        }
        
        return "";
}

void CGUI::addTextItem(const std::string& str)
{
        if(pointedWindow != 0) {
                CMultilineLabel* ml = dynamic_cast<CMultilineLabel*>(pointedWindow);
                
                if(ml != 0) {
                        ml->addTextItem(str);
                } else {
                        wrongWindowWarning("addTextItem");
                }
        }
}

u32 CGUI::itemCount() const
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        return list->itemCount();
                }
        }
        
        return 0;
}

void CGUI::setTextItem(const std::string& str, u32 index)
{
        if(pointedWindow != 0) {
                CMultilineLabel* ml = dynamic_cast<CMultilineLabel*>(pointedWindow);
                
                if(ml != 0) {
                        if(index >= 1 && index <= ml->itemCount()) {
                                ml->setTextItem(str, index - 1); //lua to C++ base, 1 to 0
                        } else {
                                LOG_WARNING("setTextItem got invalid index");
                        }
                } else {
                        wrongWindowWarning("setTextItem");
                }
        }
}

std::string CGUI::getTextItem(u32 index)
{
        if(pointedWindow != 0) {
                CMultilineLabel* ml = dynamic_cast<CMultilineLabel*>(pointedWindow);
                
                if(ml != 0) {
                        if(index >= 1 && index <= ml->itemCount()) {
                                return ml->getTextItem(index - 1); //lua to C++ base, 1 to 0
                        } else {
                                LOG_WARNING("getTextItem got invalid index");
                                return "INVALID INDEX";
                        }
                } else {
                        wrongWindowWarning("getTextItem");
                }
        }
        
        return "";
}

void CGUI::removeItems(u32 lowIndex, u32 highIndex)
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        if(lowIndex >= 1 && highIndex >= 1 &&
                        lowIndex <= list->itemCount() && highIndex <= list->itemCount() &&
                        lowIndex <= highIndex) {
                                list->removeItems(lowIndex - 1, highIndex - 1); //we transform from lua
                                        //to C++ indices (base 1 to 0)
                        } else {
                                LOG_WARNING("removeItems got invalid indices");
                        }
                } else {
                        wrongWindowWarning("removeItems");
                }
        }
}

void CGUI::clearItems()
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        list->clearItems();
                } else {
                        wrongWindowWarning("clearItems");
                }
        }
}

void CGUI::scrollItems(i32 amount)
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        list->scrollItems(amount);
                } else {
                        wrongWindowWarning("scrollItems");
                }
        }
}

void CGUI::setItemScrolling(u32 index)
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        //transform index from base 1 to 0 (lua to C++)
                        if(index >= 1) {
                                list->setItemScrolling(index - 1);
                        }
                } else {
                        wrongWindowWarning("setItemScrolling");
                }
        }
}

u32 CGUI::calculateVisibleItems()
{
        if(pointedWindow != 0) {
                IList* list = dynamic_cast<IList*>(pointedWindow);
                
                if(list != 0) {
                        return list->calcVisibleItems();
                } else {
                        wrongWindowWarning("calculateVisibleItems");
                }
        }
        return 0;
}

void CGUI::setFont(const std::string& usage, const std::string& filepath)
{
        if(pointedWindow != 0) {
                IFontOwner* fo = dynamic_cast<IFontOwner*>(pointedWindow);
                
                if(fo != 0) {
                        fo->setFont(usage, filepath);
                } else {
                        wrongWindowWarning("setFont");
                }
        }
}

void CGUI::releaseFont(const std::string& usage)
{
        if(pointedWindow != 0) {
                IFontOwner* fo = dynamic_cast<IFontOwner*>(pointedWindow);
                
                if(fo != 0) {
                        fo->releaseFont(usage);
                } else {
                        wrongWindowWarning("releaseFont");
                }
        }
}

void CGUI::setAsKeyhandler()
{
        if(pointedWindow != 0) {
                keyhandler = pointedWindow;
        }
}

void CGUI::removeKeyhandler()
{
        keyhandler = 0;
}


i32 CGUI::getCursorX() const
{
        return Cursor_GetX();
}

i32 CGUI::getCursorY() const
{
        return Cursor_GetY();
}

void CGUI::setCursorX(i32 x)
{
        Cursor_SetPos(x, Cursor_GetY());
}

void CGUI::setCursorY(i32 y)
{
        Cursor_SetPos(Cursor_GetX(), y);
}

void CGUI::forceRedraw()
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        pm->forceRedraw();
                } else {
                        wrongWindowWarning("forceRedraw");
                }
        }
}

void CGUI::setMapScrolling(f32 x, f32 y)
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        pm->setScrolling(x, y);
                } else {
                        wrongWindowWarning("forceRedraw");
                }
        }
}

void CGUI::scrollMap(f32 x, f32 y)
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        globals.loopDelay = false; //because scrolling needs to be 100% smooth or it looks shit
                        pm->scrollRelative(x, y);
                } else {
                        wrongWindowWarning("forceRedraw");
                }
        }
}

void CGUI::setMapMode(u16 mode)
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        pm->setMapMode(mode);
                } else {
                        wrongWindowWarning("setMapMode");
                }
        }
}

u32 CGUI::getMapWidth()
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        return pm->getMapWidth();
                } else {
                        wrongWindowWarning("getMapWidth");
                }
        }
        
        return 0;
}

u32 CGUI::getMapHeight()
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        return pm->getMapHeight();
                } else {
                        wrongWindowWarning("getMapHeight");
                }
        }
        
        return 0;
}

void CGUI::setPalette(const std::string& palette, prov_id province)
{
        if(pointedWindow != 0) {
                CProvinceMap* pm = dynamic_cast<CProvinceMap*>(pointedWindow);
                
                if(pm != 0) {
                        pm->setPalette(palette, province - 1); //from Lua base 1 to 0
                } else {
                        wrongWindowWarning("setPalette");
                }
        }
}

void CGUI::quitASAP()
{
        r2::globals.running = false;
}

u32 CGUI::screenWidth() const
{
        return globals.screenWidth;
}

u32 CGUI::screenHeight() const
{
        return globals.screenHeight;
}

u32 CGUI::screenBPP() const
{
        return globals.screenBPP;
}

CGUI::~CGUI()
{
        destroy();
}

void CGUI::renderWindow(CListTreeNode<CWindow*>& wnd, CSurface& dest, i32 offsetX, i32 offsetY)
{
        CWindow* theWindow = wnd.get();
        ASSERT(theWindow != 0);
        
        //don't render ourselves or children at all if not visible
        if(!theWindow->isVisible())
        {
                return;
        }
    
        //translate destination rectangle with regards to (x, y) offset
        Si32_rect destRect;
        Si32_rect& area = theWindow->getArea();
        destRect.left = offsetX + area.left;
        destRect.top = offsetY + area.top;
        destRect.right = offsetX + area.right;
        destRect.bottom = offsetY + area.bottom;
  
        theWindow->render(dest, destRect);
        
        CListTreeNodeIterator<CWindow*> it(wnd);
        while(it.hasNextChild()) {
                CListTreeNode<CWindow*>* child = it.nextChild();
                ASSERT(child != 0);
                renderWindow(*child, dest, destRect.left, destRect.top);
        }
}

//!Recursive window click function.
CWindow* CGUI::clickWindow(r2::CListTreeNode<CWindow*>& wnd, u8 button, i32 pointX, i32 pointY, bool down)
{
        CWindow* theWindow = wnd.get();
        ASSERT(theWindow != 0);
        
        //don't click children at all if parent (this) not visible
        if(!theWindow->isVisible())
        {
                return 0;
        }
        
        Si32_rect& area = theWindow->getArea();
        
        CListTreeNodeIterator<CWindow*> it(wnd);
        while(it.hasPrevChild()) {
                CListTreeNode<CWindow*>* child = it.prevChild();
                ASSERT(child != 0);
                CWindow* clk = clickWindow(*child, button, pointX + area.left,
                        pointY + area.top, down);
                if(clk != 0) {
                        return clk;
                }
        }
        
        focusedWindow = 0;
        
        Si32_point p(pointX - area.left, pointY - area.top);
        if(theWindow->checkClick(p, button, down, r2::globals.commonLua, clickedWindow) != 0) {
                
                if(theWindow->isFocusable() && !down) { //CHECK whether !down or down is better choice
                        focusedWindow = &wnd;
                }
                
                return theWindow;
        }
        
        return 0;
}

void CGUI::deleteWindow(r2::CListTreeNode<CWindow*>& wnd)
{
        if(wnd.get() != 0) {
                delete wnd.get();
                wnd.set(0);
        }
        
        CListTreeNodeIterator<CWindow*> it(wnd);
        while(it.hasNextChild())
        {
                CListTreeNode<CWindow*>* child = it.nextChild();
                ASSERT(child != 0);
                deleteWindow(*child);
        }
}

void CGUI::render(CSurface& dest)
{
        renderWindow(rootWindow, dest, 0, 0);
}

void CGUI::updateWindow(r2::CListTreeNode<CWindow*>& wnd, f32 dt)
{
        if(wnd.get() != 0) {
                wnd.get()->update(dt);
        }
        
        CListTreeNodeIterator<CWindow*> it(wnd);
        while(it.hasNextChild())
        {
                CListTreeNode<CWindow*>* child = it.nextChild();
                ASSERT(child != 0);
                updateWindow(*child, dt);
        }
}

void CGUI::update(f32 dt)
{
        updateWindow(rootWindow, dt);
}

void CGUI::mouseClick(const Si32_point& point, u8 button, bool down)
{
        CWindow* clk = clickWindow(rootWindow, button, point.x, point.y, down);
        
        if(down) {
                clickedWindow = clk;
        } else {
                if(clickedWindow != clk && clickedWindow != 0) {
                        clickedWindow->unclick();
                }
                clickedWindow = 0;
        }
}

void CGUI::mouseMove(const Si32_point& point, bool down)
{
        if(clickedWindow != 0 && down) {
                clickedWindow->mouseMove(point);
        }
}

void CGUI::keyPress(u16 keycode, u8 charcode)
{
        if(getFocusedWindow() == 0) {
                if(keyhandler != 0) {
                        keyhandler->keyPress(keycode, charcode, r2::globals.commonLua);
                }
        } else {
                getFocusedWindow()->keyPress(keycode, charcode, r2::globals.commonLua);
        }
}
