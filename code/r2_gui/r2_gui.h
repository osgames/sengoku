//r2_gui.h

//This code is published under the General Public License.

#ifndef R2_GUI_H
#define R2_GUI_H

#include <memory>

#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_tree.h"
#include "..\r2_gui\r2_window.h"
#include "..\r2_core\r2_singleton.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_core\r2_types.h"

namespace r2
{

class CScreenBackground : public CWindow
{
};

class CGUI : public CSingleton<CGUI>
{
friend class CSingleton<CGUI>;
protected: //vars
        r2::CListTreeNode<CWindow*> rootWindow;
        r2::CListTreeNode<CWindow*>* focusedWindow;
        CWindow* keyhandler; //handles keys?
        CWindow* pointedWindow; //for Lua interaction
        CWindow* clickedWindow; //held by mouse down event?
        
        Si32_point cursorPos;
        
        SRGB clearcolor;
        
        //two state variables used to detect mouse ups
        bool mouseLeft;
        bool mouseRight;
        
        bool inited;
protected: //functions
        CGUI(); //constructor is protected according to singleton pattern
        
        //!Inits lua and calls relevant Lua scripts.
        bool initLua();
        
        //!Iterates through parents based on a hierarchy string and returns the child name.
        std::string iterateHierarchyToChild(const std::string& idHierarchy, r2::CListTreeNode<CWindow*>*& it);
        //!Iterates through parents based on a hierarchy string.
        void iterateHierarchy(const std::string& idHierarchy, r2::CListTreeNode<CWindow*>*& it);
        
        //!Recursive window rendering function.
        void renderWindow(r2::CListTreeNode<CWindow*>& wnd, CSurface& dest, i32 offsetX, i32 offsetY);
        
        //!Recursive window click function.
        CWindow* clickWindow(r2::CListTreeNode<CWindow*>& wnd, u8 button, i32 pointX, i32 pointY, bool down);
        
        //!Recursive window deletion function.
        void deleteWindow(r2::CListTreeNode<CWindow*>& wnd);
        
        //!Recursive window update function.
        void updateWindow(r2::CListTreeNode<CWindow*>& wnd, f32 dt);
        
        inline void wrongWindowWarning(const std::string& functionName)
        {
                LOG_WARNING(functionName << " called on wrong window " << pointedWindow->getID());
        }
public:
        ~CGUI();
        
        //!Inits the GUI (loads scripts and so forth).
        bool init();
        //!Destroys the GUI.
        void destroy();
        
        //!Renders the GUI.
        void render(CSurface& dest);
        //!Updates the GUI.
        void update(f32 dt);
        
        //!Mouse clicks.
        void mouseClick(const Si32_point& point, u8 button, bool down);
        //!Mouse movement.
        void mouseMove(const Si32_point& point, bool down);
        
        //!Receives a key press which has a virtual keycode and a translated 32 <= charcode <= 255 Unicode.
        void keyPress(u16 keycode, u8 charcode);
  
        //focus:
        //!Returns the focused window.
        CWindow* getFocusedWindow() {return focusedWindow != 0 ? focusedWindow->get() : 0;}
        void clearFocus() {focusedWindow = 0;}
        
        //!Exposes certain functions to a Lua VM.
        static void bindFunctionsWithLua(CLua& l);
        
        //!Creates a window of a specified class in a specific hierarchy. Lua bound.
        void createWindow(const std::string& idHierarchy, const std::string& type,
                luabind::object const& initTable);
        //!Sets the window pointed to in a specific hierarchy. Lua bound.
        void selectWindow(const std::string& idHierarchy);
        
        //!Sets the LTRB coordinates of a window. Lua bound.
        void setArea(i32 left, i32 top, i32 right, i32 bottom);
        //!Gets the width of window. Lua bound.
        u32 getWidth();
        //!Gets the height of window. Lua bound.
        u32 getHeight();
        
        //!Sets the file resource of a specific window bitmap. Lua bound.
        void setBitmap(const std::string& usage, const std::string& filename, luabind::object table);
        //!Sets the source rectangle of a specific window bitmap. Lua bound.
        void setBitmapArea(const std::string& usage, i32 left, i32 top, i32 right, i32 bottom);
        //!Sets bitmap RGB color key. Lua bound.
        void setBitmapRGBColorKey(const std::string& usage, u8 r, u8 g, u8 b);
        //!Sets bitmap palette color key. Lua bound.
        void setBitmapPaletteColorKey(const std::string& usage, u8 key);
        //!Sets render mode of window. Lua bound.
        void setRenderMode(const std::string& mode);
        //!Sets a color of window:
        void setColor(const std::string& usage, u8 r, u8 g, u8 b);
        
        //!Sets the pointed window visible or not. Lua bound.
        void setVisible(bool v);
        //!Returns whether pointed window is visible. Lua bound.
        bool isVisible();
        
        //!Adds a sprite somewhere to a window. Lua bound.
        void addSprite(const std::string& id, const std::string& filename,
                const std::string& type, i32 x, i32 y);
        //!Removes sprite from window. Lua bound.
        void removeSprite(const std::string& id);
        //!Plays a sprite animation. Lua bound.
        void animateSprite(const std::string& id, const std::string& animation);
        //!Moves a sprite. Lua bound.
        void moveSprite(const std::string& id, i32 x, i32 y);
        
        //!Registers an action function to a window event. Lua bound.
        void registerAction(const std::string& event, const std::string& action);
        
        //!Hides all children of root node. Lua bound.
        void hideScreens();
        
        //!Sets the text of a text box. Lua bound.
        void setText(const std::string& str);
        //!Gets the text of a text box. Lua bound.
        std::string getText();
        
        //!Adds one text line of a multiline label. Lua bound.
        void addTextItem(const std::string& str);
        //!Sets one text line of a multiline label. Lua bound.
        void setTextItem(const std::string& str, u32 index);
        //!Gets one text line of a multiline label. Lua bound.
        std::string getTextItem(u32 index);
        
        //!Returns the number of text lines. Lua bound.
        u32 itemCount() const;
        //!Removes one text text line of a multiline label. Lua bound.
        void removeItems(u32 lowIndex, u32 highIndex);
        //!Removes all lines of a multiline label. Lua bound.
        void clearItems();
        //!Scrolls the display offset of a multiline label. Lua bound. Amount can be negative.
        void scrollItems(i32 amount);
        //!Sets the display offset scrolling to an absolute amount. Lua bound.
        void setItemScrolling(u32 index);
        //!Calculates how many rows can be visible at current height and font. Lua bound.
        u32 calculateVisibleItems();
        
        //!Sets the font for a specified usage. Lua bound.
        void setFont(const std::string& usage, const std::string& filepath);
        //!Releases the font for a specified usage. Lua bound.
        void releaseFont(const std::string& usage);
        
        //!Sets the selected window as keyhandler.
        void setAsKeyhandler();
        //!Clears the keyhandler. Lua bound.
        void removeKeyhandler();
        
        //!Gets cursor position X coordinate. Lua bound.
        i32 getCursorX() const;
        //!Gets cursor position Y coordinate. Lua bound.
        i32 getCursorY() const;
        //!Sets cursor position X coordinate. Lua bound.
        void setCursorX(i32 x);
        //!Sets cursor position Y coordinate. Lua bound.
        void setCursorY(i32 y);
        
        //!Forces the redraw of a map window. Lua bound.
        void forceRedraw();
        //!Sets the map scrolling offset to some absolute coordinates. Lua bound.
        void setMapScrolling(f32 x, f32 y);
        //!Scrolls the map relatively. Lua bound.
        void scrollMap(f32 x, f32 y);
        //Sets the map mode. Lua bound.
        void setMapMode(u16 mode);
        //!Gets map width. Lua bound.
        u32 getMapWidth();
        //!Gets map height. Lua bound.
        u32 getMapHeight();
        //!Sets a province's palette. Lua bound.
        void setPalette(const std::string& palette, prov_id province);
        
        //!Sets the quit flag. Lua bound.
        void quitASAP();
        
        //!Lua bound function which returns global screen width.
        u32 screenWidth() const;
        //!Lua bound function which returns global screen height.
        u32 screenHeight() const;
        //!Lua bound function which returns global screen BPP.
        u32 screenBPP() const;
};

} //end of namespace r2

#endif //!R2_GUI_H
