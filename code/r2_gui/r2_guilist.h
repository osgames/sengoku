//r2_guilist.h

//This code is published under the General Public License.

#ifndef R2_GUILIST_H
#define R2_GUILIST_H

namespace r2 {

//!List control interface. To provide a consistent set of list methods.
class IList
{
public:
        virtual u32 itemCount() = 0;
        virtual void removeItems(u32 indexLow, u32 indexHigh) = 0;
        virtual void clearItems() = 0;
        virtual void scrollItems(i32 relativeOffset) = 0;
        virtual void setItemScrolling(u32 absoluteOffset) = 0;
        virtual u32 calcVisibleItems() = 0;
        virtual void highlightItem(i32 index) = 0;
};

}; //!namespace r2

#endif //!R2_GUILIST_H
