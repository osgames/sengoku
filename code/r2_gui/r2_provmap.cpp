//r2_provmap.cpp

//This code is published under the General Public License.

#include <fstream>
#include <boost\lexical_cast.hpp>
#include "..\r2_gui\r2_provmap.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_gui\r2_sprite.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_game\r2_game.h"

using namespace r2;

void CProvinceMap::init(const std::string& id, luabind::object const& initTable)
{
        using namespace luabind;
        
        CWindow::init(id, initTable);
        
        //some defaults:
        scroll.x = 0.0f;
        scroll.y = 0.0f;
        displayedScroll.x = 0.0f;
        displayedScroll.y = 0.0f;
        mapWidth = 0;
        mapHeight = 0;
        physicalProvinceCount = 0;
        graphicalProvinceCount = 0;
        selStart.x = selEnd.x = 0;
        selStart.y = selEnd.y = 0;
        
        if(initTable && type(initTable) == LUA_TTABLE) {
                if(type(initTable["mapFile"]) == LUA_TSTRING) {
                        loadMapFile(object_cast<std::string>(initTable["mapFile"]));
                }
        }

        //initialize graphical surface (in software for manipulation
        //screen's pixel format)
        if(!surface.blank(getWidth(), getHeight(), 0, true, false)) {
                LOG_ERROR("Failed to create surface.");
                return;
        }
        
        //attempt to load sprites
        townSprites = r2::globals.sprites.bind(FILE_SPRITES_TOWNS, 0);
        armySprites = r2::globals.sprites.bind(FILE_SPRITES_ARMIES, 0);
        
        //register to receive world information
        CGame::getInstance().getWorld().addReceiver(this);
}

void CProvinceMap::destroy()
{
        CGame::getInstance().getWorld().removeReceiver(this);
        
        for(std::vector<SOverlay>::const_iterator it = overlays.begin(); it != overlays.end(); ++it) {
                globals.bitmaps.unbind(it->bmp);
        }
        overlays.clear();
        for(std::vector<SProvGfx>::const_iterator it = graphicalProvs.begin(); it != graphicalProvs.end();
                ++it) {
                globals.bitmaps.unbind(it->bitmap);
        }
        graphicalProvs.clear();
        surface.unload();
        
        r2::globals.sprites.unbind(townSprites);
        r2::globals.sprites.unbind(armySprites);
}

void CProvinceMap::update(f32 dt)
{
        CWindow::update(dt);
        
        //update army sprites
        for(std::map<town_id, CSpriteInstance*>::const_iterator it = towns.begin();
                        it != towns.end(); it++) {
                if(it->second != 0) {
                        it->second->tick(dt);
                }
        }
        
        //update army sprites
        for(std::map<army_id, CSpriteInstance*>::const_iterator it = armies.begin();
                        it != armies.end(); it++) {
                if(it->second != 0) {
                        it->second->tick(dt);
                }
        }
}

void CProvinceMap::receiveMsg(i32 type, i32 id, CMsgSender* sender)
{
        switch(type) {
                case MSG_WORLD_ARMYADDED: {
                        if(armySprites != 0) {
                                armies[id] = new CSpriteInstance("army2", *armySprites);
                                updateArmy(id, * (CWorld*) sender);
                        }
                        break;
                } case MSG_WORLD_ARMYMOVED: {
                        updateArmy(id, * (CWorld*) sender);
                        break;
                } case MSG_WORLD_ARMYREMOVED: {
                        if(armies[id] != 0) {
                                delete armies[id];
                        }
                        armies.erase(id);
                        break;
                } case MSG_WORLD_TOWNADDED: {
                        if(townSprites != 0) {
                                towns[id] = new CSpriteInstance("town", *townSprites);
                                updateTown(id, * (CWorld*) sender);
                        }
                        break;
                } case MSG_WORLD_TOWNREMOVED: {
                        if(towns[id] != 0) {
                                delete towns[id];
                        }
                        towns.erase(id);
                        break;
                }
        }
                        
}

void CProvinceMap::updateArmy(army_id id, r2::CWorld& world)
{
        CSpriteInstance* inst = armies[id];
        if(inst != 0) {
                CWorld::SArmyRender rinfo;
                world.getArmyRenderInfo(id, "", rinfo);
                inst->move(rinfo.mapPos);
        }
}

void CProvinceMap::updateTown(town_id id, r2::CWorld& world)
{
        CSpriteInstance* inst = towns[id];
        if(inst != 0) {
                CWorld::STownRender rinfo;
                world.getTownRenderInfo(id, rinfo);
                inst->move(rinfo.mapPos);
        }
}

bool CProvinceMap::mouseUp(const Si32_point& point, u8 button, r2::CLua& lua)
{
        //selection rectangle constitutes selStart and point (throw away last point only used for drawing)
        TRect<i32> selRect(selStart, point);
        selRect.normalize();
        
        //kill
        selStart.x = selEnd.x = 0;
        selStart.y = selEnd.y = 0;
        
        if(CWindow::mouseUp(point, button, lua)) {
                return true;
        }
        
        //check for new selection
        if(selRect.width() > MAPSEL_TOLERANCE || selRect.height() < MAPSEL_TOLERANCE) {
                //only check for army selection in this mode
                for(std::map<army_id, CSpriteInstance*>::const_iterator it = armies.begin();
                                it != armies.end(); it++) {
                        CSpriteInstance* inst = it->second;
                        TRect<i32> rc = inst->getAbsRect();
                        rc = rc.sub(scroll.cast<i32>()); //translate into viewed space
                        
                        rc = rc.intersect(selRect);
                        if(rc.isNormal()) {
                                //intersection occured
                                
                        }
                }
        } else {
                //check for either army selection, town or province selection
        }
        
        return false;
}

bool CProvinceMap::mouseDown(const Si32_point& point, u8 button, r2::CLua& lua)
{
        selEnd = selStart = point;
        
        return true;
}

void CProvinceMap::unclick()
{
        selStart.x = selEnd.x = 0;
        selStart.y = selEnd.y = 0;
}

void CProvinceMap::mouseMove(const Si32_point& offset)
{
        selEnd.x = selStart.x + offset.x;
        selEnd.y = selStart.y + offset.y;
        
        //limit to actual area
        selEnd.x = selEnd.x > 0 ? selEnd.x : 0;
        selEnd.y = selEnd.y > 0 ? selEnd.y : 0;
        i32 w = getWidth();
        i32 h = getHeight();
        selEnd.x = selEnd.x < w ? selEnd.x : w;
        selEnd.y = selEnd.y < h ? selEnd.y : h;
}

void CProvinceMap::setScrolling(f32 x, f32 y)
{
        scroll.x = x;
        scroll.y = y;
        f32 maxX = mapWidth - (area.right - area.left);
        f32 maxY = mapHeight - (area.bottom - area.top);
        if(scroll.x < 0.0f) {
                scroll.x = 0.0f;
        } else if(scroll.x > maxX) {
                scroll.x = maxX;
        }
        if(scroll.y < 0.0f) {
                scroll.y = 0.0f;
        } else if(scroll.y > maxY) {
                scroll.y = maxY;
        }
}

void CProvinceMap::scrollRelative(f32 x, f32 y)
{
        scroll.x += x;
        scroll.y += y;
        f32 maxX = mapWidth - (area.right - area.left);
        f32 maxY = mapHeight - (area.bottom - area.top);
        if(scroll.x < 0.0f) {
                scroll.x = 0.0f;
        } else if(scroll.x > maxX) {
                scroll.x = maxX;
        }
        if(scroll.y < 0.0f) {
                scroll.y = 0.0f;
        } else if(scroll.y > maxY) {
                scroll.y = maxY;
        }
}

void CProvinceMap::drawBackground(Si32_rect& rc)
{
        Si32_rect rcDest, rcSrc;
  
        //was background bitmap loaded/specified?
        if(backgroundBitmap == 0) {
                //draw ambient background color if it wasn't
                surface.drawFilledRect(&rc, 0, 0, 255);
        } else {
                //it was found, get width and height of background bitmap
                i32 w = backgroundBitmap->getWidth();
                i32 h = backgroundBitmap->getHeight();
    
                //calculate texture offset
                i32 offsetX = ((i32)scroll.x + rc.left) % w;
                i32 offsetY = ((i32)scroll.y + rc.top) % h;
    
                //tiling loops
                for(i32 y = rc.top; y < rc.bottom; y += (rcSrc.bottom - rcSrc.top)) { //per column
                        //calculate top and bottom members of source and dest rects
                        rcSrc.top = 0;
                        rcSrc.bottom = h;
                        rcDest.top = y; //modify
                        rcDest.bottom = y + h;
      
                        //add offset for texture for first drawn rectangle
                        if(y == rc.top) {
                                rcSrc.top = offsetY;
                                rcDest.bottom -= offsetY;
                        }
      
                        //limit height of drawn rectangle so it doesn't go outside the total destination area
                        if(rcDest.bottom > rc.bottom) {
                                rcSrc.bottom -= (rcDest.bottom - rc.bottom);
                                rcDest.bottom = rc.bottom;
                        }
      
                        for(i32 x = rc.left; x < rc.right; x += (rcSrc.right - rcSrc.left)) { //per row
                                //recalculate left and right members of source and dest rects
                                rcSrc.left = 0;
                                rcSrc.right = w;
                                rcDest.left = x; //modify
                                rcDest.right = x + w;
        
                                //add offset for texture for first drawn rectangle
                                if(x == rc.left) {
                                        rcSrc.left = offsetX;
                                        rcDest.right -= offsetX;
                                }
        
                                //limit width of drawn rectangle so it doesn't go outside the total destination area
                                if(rcDest.right > rc.right) {
                                        rcSrc.right -= (rcDest.right - rc.right);
                                        rcDest.right = rc.right;
                                }
    
                                //draw background texture
                                surface.blit(*backgroundBitmap, &rcSrc, &rcDest);
                        }
                }
        }
}

void CProvinceMap::loadMapFile(const std::string& filename)
{
        using namespace luabind;
        CLua lua;
        
        ASSERT(lua.state() != 0);
        
        if(!lua.doFile(filename)) {
                LOG_ERROR("Failed to open map file, map will be uninitialized.");
                return;
        }
        
        //build palettes
        object bp(luabind::globals(lua.state())["builtPalettes"]);
        if(type(bp) == LUA_TTABLE) {
                for(iterator i(bp), end; i != end; ++i) {
                        std::string key = object_cast<std::string>(i.key());
                        if(pals.count(key) == 0) {
                                f32 m1 = type((*i)["m1"]) == LUA_TNUMBER ? object_cast<f32>((*i)["m1"]) : 0.0f;
                                f32 m2 = type((*i)["m2"]) == LUA_TNUMBER ? object_cast<f32>((*i)["m2"]) : 0.0f;
                                f32 m3 = type((*i)["m3"]) == LUA_TNUMBER ? object_cast<f32>((*i)["m3"]) : 0.0f;
                                f32 c1 = type((*i)["c1"]) == LUA_TNUMBER ? object_cast<f32>((*i)["c1"]) : 0.0f;
                                f32 c2 = type((*i)["c2"]) == LUA_TNUMBER ? object_cast<f32>((*i)["c2"]) : 0.0f;
                                f32 c3 = type((*i)["c3"]) == LUA_TNUMBER ? object_cast<f32>((*i)["c3"]) : 0.0f;
                                pals[key].build(m1, c1, m2, c2, m3, c3);
                        }
                }
        }
        
        //general map information
        object map(luabind::globals(lua.state())["map"]);
        if(type(map) == LUA_TTABLE) {
                mapWidth = type(map["width"]) == LUA_TNUMBER ? object_cast<u32>(map["width"]) : 0;
                mapHeight = type(map["height"]) == LUA_TNUMBER ? object_cast<u32>(map["height"]) : 0;
                graphicalProvinceCount = type(map["graphicalProvinceCount"]) == LUA_TNUMBER ?
                        object_cast<prov_id>(map["graphicalProvinceCount"]) : 0;
                physicalProvinceCount = type(map["physicalProvinceCount"]) == LUA_TNUMBER ?
                        object_cast<prov_id>(map["physicalProvinceCount"]) : 0;
                if(type(map["provincePath"]) == LUA_TSTRING) {
                        std::string provincePath = object_cast<std::string>(map["provincePath"]);
                        if(provincePath != "" && *provincePath.rbegin() != '\\' &&
                                        *provincePath.rbegin() != '/') {
                                provincePath += "\\";
                        }
                        loadProvinceGraphics(provincePath);
                }
                if(type(map["overlays"]) == LUA_TTABLE) {
                        for(iterator it(map["overlays"]), end; it != end; ++it) {
                                if(type(*it) == LUA_TTABLE && type((*it)["bitmap"]) == LUA_TSTRING) {
                                        SOverlay o;
                                        o.bmp = r2::globals.bitmaps.bind(object_cast<std::string>
                                                ((*it)["bitmap"]), BITMAP_FLAG_VIDEOFORMAT |
                                                BITMAP_FLAG_ALPHA);
                                        if(o.bmp != 0) {
                                                if(!CLua::readRectangle((*it)["source"], o.src)) {
                                                        o.src.left = 0;
                                                        o.src.top = 0;
                                                        o.src.right = o.bmp->getWidth();
                                                        o.src.bottom = o.bmp->getHeight();
                                                }
                                                if(!CLua::readPoint((*it)["destination"], o.dest)) {
                                                        o.dest.x = 0;
                                                        o.dest.y = 0;
                                                }
                                                overlays.push_back(o);
                                        }
                                }
                        }
                }
        }
        
        //physical to graphical province ID conversion table
        object phys(luabind::globals(lua.state())["physicalProvinces"]);
        if(type(phys) == LUA_TTABLE) {
                for(iterator it(phys), end; it != end; ++it) {
                        if(type(*it) == LUA_TTABLE && type((*it)["id"]) == LUA_TNUMBER &&
                                        type((*it)["graphical"]) == LUA_TTABLE) {
                                prov_id physId = object_cast<prov_id>((*it)["id"]);
                                
                                for(iterator j((*it)["graphical"]); j != end; ++j) {
                                        if(type(*j) == LUA_TNUMBER) {
                                                prov_id gfxId = object_cast<prov_id>(*j);
                                                graphicalToPhys.insert(std::pair<prov_id, prov_id>
                                                        (gfxId, physId));
                                                physToGraphical.insert(std::pair<prov_id, prov_id>
                                                        (physId, gfxId));
                                        }
                                }
                        }
                }
        }
}

void CProvinceMap::loadProvinceGraphics(const std::string& directory)
{
        using namespace luabind;
        
        CLua lua;
        ASSERT(lua.state() != 0);
        
        graphicalProvs.resize(graphicalProvinceCount);
        
        if(!lua.doFile(directory + "Rects.lua")) {
                return;
        }
        
        object rects(luabind::globals(lua.state())["provinceRects"]);
        if(type(rects) == LUA_TTABLE) {
                for(iterator i(rects), end; i != end; ++i) {
                        if(type(i.key()) == LUA_TNUMBER && type(*i) == LUA_TTABLE) {
                                prov_id index = object_cast<prov_id>(i.key()) - 1;
                                if(index >= 0 && index < graphicalProvinceCount) {
                                        Si32_rect rc;
                                        if(CLua::readRectangle(*i, rc)) {
                                                graphicalProvs[index].dest = rc;
                                                graphicalProvs[index].bitmap = r2::globals.bitmaps.bind(
                                                        directory + boost::lexical_cast<std::string>
                                                        (index + 1) + ".bmp", 0);
                                                if(graphicalProvs[index].bitmap != 0) {
                                                        graphicalProvs[index].bitmap->setColorKey(0);
                                                }
                                        }
                                }
                        }
                }
        }
}

void CProvinceMap::redraw(Si32_rect* rc)
{
        Si32_rect destRect;
    
        //check whether we shall redraw entire area or not
        if(rc != 0) {
                destRect = *rc;
        } else {
                //diplayed size, translate for scrolling later
                destRect.left = 0;
                destRect.top = 0;
                destRect.right = getWidth();
                destRect.bottom = getHeight();
        }
    
        //background
        drawBackground(destRect);

        if(mapMode == MAPMODE_PROVINCES) {
                //provinces
                for(std::vector<SProvGfx>::const_iterator it = graphicalProvs.begin();
                        it != graphicalProvs.end(); ++it) {
                        if(it->bitmap != 0) { //can be 0 if loading failed
                                Si32_rect transRect(it->dest.left - (u32) scroll.x,
                                        it->dest.top - (u32) scroll.y, it->dest.right - (u32) scroll.x,
                                        it->dest.bottom - (u32) scroll.y);
                                surface.clippedBlit(*it->bitmap, 0, &transRect, destRect);
                        }
                }
                
                //overlay
                for(std::vector<SOverlay>::const_iterator it = overlays.begin();
                        it != overlays.end(); ++it) {
                        ASSERT(it->bmp != 0);
                        Si32_rect oRect;
                        oRect.left = it->dest.x - (i32) scroll.x;
                        oRect.top = it->dest.y - (i32) scroll.y;
                        oRect.right = oRect.left + (it->src.right - it->src.left);
                        oRect.bottom = oRect.top + (it->src.bottom - it->src.top);
                        surface.clippedBlit(*it->bmp, &it->src, &oRect, destRect);
                }
        }
}

void CProvinceMap::renderSprites(CSurface& dest, Si32_rect& destRect)
{
        Si32_point p(destRect.left - (i32) displayedScroll.x, destRect.top - (i32) displayedScroll.y);
        
        //render towns
        for(std::map<town_id, CSpriteInstance*>::const_iterator it = towns.begin();
                        it != towns.end(); it++) {
                if(it->second != 0) {
                        it->second->render(dest, p, &destRect);
                }
        }
        
        //render armies
        for(std::map<army_id, CSpriteInstance*>::const_iterator it = armies.begin();
                        it != armies.end(); it++) {
                if(it->second != 0) {
                        it->second->render(dest, p, &destRect);
                }
        }
        
        //render regular added sprites on top
        //disabled while I work with adding sprites into core rendering
        /*for(std::map<std::string, CSpriteInstance*>::const_iterator it = sprites.begin(); it != sprites.end();
                        ++it) {
                ASSERT(it->second != 0);
                Si32_point p(destRect.left - (i32) displayedScroll.x, destRect.top - (i32) displayedScroll.y);
                it->second->render(dest, p, &destRect);
        }*/
}

std::string CProvinceMap::clickTestSprites(const Si32_point& testPoint)
{
        //test for any user added sprites on top
        for(std::map<std::string, CSpriteInstance*>::const_reverse_iterator it = sprites.rbegin();
                it != (std::map<std::string, CSpriteInstance*>::const_reverse_iterator) sprites.rend();
                ++it) {
                ASSERT(it->second != 0);
                Si32_point p(testPoint.x + (i32) displayedScroll.x, testPoint.y + (i32) displayedScroll.y);
                if(it->second->isHit(p)) {
                        return it->first;
                }
        }
        
        return "";
}

void CProvinceMap::render(CSurface& dest, Si32_rect& destRect)
{
        /*!It's important to note that this does not call CWindow::render().
        This is because it has no use of default background drawing behaviour.*/
        
        Si32_rect dest_rc, src_rc;
  
        //see whether we need to redraw automatically, important that comparisons are i32
        if((i32) displayedScroll.x != (i32) scroll.x || (i32) displayedScroll.y != (i32) scroll.y) {
                //calculate actual scrolling
                i32 scrollX = (i32)scroll.x - (i32)displayedScroll.x;
                i32 scrollY = (i32)scroll.y - (i32)displayedScroll.y;
    
                //translate the unchanged portion of the surface
                if(scrollX < 0) {
                        src_rc.left = 0;
                        src_rc.right = getWidth() + scrollX;
                        dest_rc.left = -scrollX;
                        dest_rc.right = getWidth();
                } else {
                        src_rc.left = scrollX;
                        src_rc.right = getWidth();
                        dest_rc.left = 0;
                        dest_rc.right = getWidth() - scrollX;
                }
                if(scrollY < 0) {
                        src_rc.top = 0;
                        src_rc.bottom = getHeight() + scrollY;
                        dest_rc.top = -scrollY;
                        dest_rc.bottom = getHeight();
                } else {
                        src_rc.top = scrollY;
                        src_rc.bottom = getHeight();
                        dest_rc.top = 0;
                        dest_rc.bottom = getHeight() - scrollY;
                }
        
                //blit it
                surface.blit(surface, &src_rc, &dest_rc);
   
                //check which rectangles are to be redrawn
                if(scrollX != 0) {
                        //redraw horizontal direction
                        if(scrollX < 0) {
                                dest_rc.left = 0;
                                dest_rc.right = -scrollX;
                        } else {
                                dest_rc.left = getWidth() - scrollX;
                                dest_rc.right = getWidth();
                        }
                        dest_rc.top = 0;
                        dest_rc.bottom = getHeight();
                        redraw(&dest_rc);
      
                        if(scrollY != 0) {
                                //redraw vertical direction
                                if(scrollY < 0) {
                                        dest_rc.top = 0;
                                        dest_rc.bottom = -scrollY;
                                } else {
                                        dest_rc.top = getHeight() - scrollY;
                                        dest_rc.right = getHeight();
                                }
                                if(scrollX < 0) {
                                        dest_rc.left = -scrollX;
                                        dest_rc.right = getWidth();
                                } else {
                                        dest_rc.left = 0;
                                        dest_rc.right = getWidth() - scrollX;
                                }
                                redraw(&dest_rc);
                        }
                } else if(scrollY != 0) {
                        //only redraw vertical direction
                        if(scrollY < 0) {
                                dest_rc.top = 0;
                                dest_rc.bottom = -scrollY;
                        } else {
                                dest_rc.top = getHeight() - scrollY;
                                dest_rc.bottom = getHeight();
                        }
                        dest_rc.left = 0;
                        dest_rc.right = getWidth();
                        redraw(&dest_rc);
                }
        
                //reset displayed scrolling
                displayedScroll.x = scroll.x;
                displayedScroll.y = scroll.y;
        }

        //blit entire map surface to destination
        dest.blit(surface, 0, &destRect); //changed from dest_rc to destRect, if it doesn't work, change back
        
        renderSprites(dest, destRect);
        
        if(!selStart.equals(selEnd)) {
                renderSelectionRect(dest, destRect);
        }
}

void CProvinceMap::renderSelectionRect(CSurface& dest, Si32_rect& destRect)
{
        Si32_rect rect;
        //produce normalized rectangle
        if(selStart.x < selEnd.x) {
                rect.left = destRect.left + selStart.x;
                rect.right = destRect.left + selEnd.x;
        } else {
                rect.left = destRect.left + selEnd.x;
                rect.right = destRect.left + selStart.x;
        }
        if(selStart.y < selEnd.y) {
                rect.top = destRect.top + selStart.y;
                rect.bottom = destRect.top + selEnd.y;
        } else {
                rect.top = destRect.top + selEnd.y;
                rect.bottom = destRect.top + selStart.y;
        }
        
        dest.drawHollowRect(rect, 0, 224, 0, 2);
}

void CProvinceMap::forceRedraw(void)
{
        redraw(0);
  
        //reset displayed scrolling
        displayedScroll.x = scroll.x;
        displayedScroll.y = scroll.y;
}

void CProvinceMap::setPalette(const std::string& name, prov_id prov)
{
        if(prov < 0 && prov >= physicalProvinceCount) {
                LOG_WARNING("Bad physical province ID to setPalette: " << prov);
                return;
        }
        
        std::pair< std::multimap<prov_id, prov_id>::const_iterator,
                std::multimap<prov_id, prov_id>::const_iterator > r = physToGraphical.equal_range(prov);
        for(std::multimap<prov_id, prov_id>::const_iterator it = r.first; it != r.second; ++it) {
                if(pals.count(name) > 0) {
                        if(graphicalProvs[it->second].bitmap != 0) {
                                graphicalProvs[it->second].bitmap->setPalette(pals[name]);
                        }
                        //graphicalProvs->getRLE(it->second)->setPalette(pals[name], surface);
                } else {
                        if(graphicalProvs[it->second].bitmap != 0) {
                                graphicalProvs[it->second].bitmap->setPalette(pals["debugRed"]);
                        }
                        //graphicalProvs->getRLE(it->second)->setPalette(pals["debugRed"], surface);
                }
        }
}

/*i16 CProvinceMap::ProvinceID(u16 set, u32 x, u32 y)
{
  //loop through all rects in the specified set
  for(u32 i = 0; i < provs[set]->Count(); i++)
  {
    //dereference rect and RLE pointers for ease of use
    CProvinceRLE* rle = provs[set]->RLE(i);
    Si32_rect* provRect = provs[set]->Rect(i);
    
    //only test for pixel if coordinates are inside rectangle of province
    //we use the [left, right[ and [top, bottom[ intervals to not exceed 
    //boundaries of the RLE
    if((x >= provRect->left) && (y >= provRect->top) &&
      (x < provRect->right) && (y < provRect->bottom))
    {
      //if the pixel (palette index) is non zero is province was hit
      if(rle->GetPixel(x - provRect->left, y - provRect->top) != 0)
        return i;
    }
  }
  
  return -1;
}*/
