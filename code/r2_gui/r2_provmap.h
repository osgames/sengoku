//r2_provmap.h

//This code is published under the General Public License.

#ifndef R2_PROVMAP_H
#define R2_PROVMAP_H

#include <SDL.h>
#include <string>
#include <vector>
#include <map>
#include <boost\shared_ptr.hpp>
#include "..\r2_graphics\r2_video.h"
#include "..\r2_graphics\r2_provrle.h"
#include "..\r2_core\typedef.h"
#include "..\r2_gui\r2_window.h"
#include "..\r2_gui\r2_sprite.h"
#include "..\r2_game\r2_world.h"
#include "..\r2_core\r2_msgs.h"

#define MAPMODE_NOTHING         0
#define MAPMODE_PROVINCES       1 //provinces + shading
#define MAPMODE_DEBUGSHADING    2 //shading only (for alignment testing)
#define MAPMODE_TERRAIN         3

#define MAPSELTYPE_PROVINCE     1
#define MAPSELTYPE_ARMY         2
#define MAPSELTYPE_TOWN         3

#define MAPSEL_TOLERANCE        2 //tolerance before selection rectange counts - in pixels

namespace r2
{

//!This control shows a map of the game.
class CProvinceMap : public r2::CWindow, public IMsgReceiver
{
protected: //classes
        struct SProvGfx
        {
                CBitmap* bitmap;
                Si32_rect dest;
        };
        struct SOverlay
        {
                Si32_rect src;
                Si32_point dest;
                CBitmap* bmp;
        };
        struct SSelection
        {
                boost::shared_ptr<CSpriteInstance> inst;
                u32 id;
                u8 type;
        };
protected: //variables
        u32 mapWidth;
        u32 mapHeight;
        prov_id graphicalProvinceCount;
        prov_id physicalProvinceCount;
        u16 mapMode;
        
        Sf32_vector scroll;
        Sf32_vector displayedScroll; //displayed scrolled value
        
        Si32_point selStart;
        Si32_point selEnd;
        
        std::map<std::string, SPalette> pals;
        r2::CSurface surface;
        std::vector<SProvGfx> graphicalProvs;
        std::vector<SOverlay> overlays;
        std::multimap<prov_id, prov_id> graphicalToPhys;
        std::multimap<prov_id, prov_id> physToGraphical;
        
        CSpriteSet* townSprites;
        CSpriteSet* armySprites;
        
        std::map<army_id, CSpriteInstance*> armies;
        std::map<town_id, CSpriteInstance*> towns;
        std::list<SSelection> sels;
        
        bool inited;
    
protected: //functions
        //!Draws the tiled background.
        void drawBackground(Si32_rect& rc);
        //!Small help function to load a lua file containing map info.
        void loadMapFile(const std::string& filename);
        //!Loads provinces bitmaps and rectangle file from a directory.
        void CProvinceMap::loadProvinceGraphics(const std::string& directory);
        //!Overshading CWindow::renderSprites.
        void renderSprites(CSurface& dest, Si32_rect& destRect);
        //!Overshading CWindow::clickTestSprites.
        std::string CProvinceMap::clickTestSprites(const Si32_point& testPoint);
        
        //!Updates an army sprite.
        void updateArmy(army_id id, CWorld& world);
        //!Updates a town sprite.
        void updateTown(town_id id, CWorld& world);
        
        //!Overshading IMsgReceiver::receiveMsg.
        void receiveMsg(i32 type, i32 id, CMsgSender* sender);
        
        //!Overshadows CWindow::mouseUp.
        bool mouseUp(const Si32_point& point, u8 button, r2::CLua& lua);
        //!Overshadows CWindow::mouseDown;
        bool mouseDown(const Si32_point& point, u8 button, r2::CLua& lua);
        
        //!Renders unit selection rectangle.
        void renderSelectionRect(CSurface& dest, Si32_rect& destRect);
public:
        //construction/destruction
        void init(const std::string& id, luabind::object const& initTable);
        void destroy();
        
        //!Overshadows CWindow::update.
        void update(f32 dt);
  
        //scrolling (not CProvinceMap's responsibility to check out of bounds
        //or retain scrolling)
        void setScrolling(f32 x, f32 y);
        void scrollRelative(f32 x, f32 y);
        u32 getMapWidth(void) {return mapWidth;}
        u32 getMapHeight(void) {return mapHeight;}
        
        //!Overshadows CWindow::unclick.
        void unclick();
        //!Overshadows CWindow::mouseMove.
        void mouseMove(const Si32_point& offset);
        
        //!Inherited rendering method:
        void render(r2::CSurface& dest, Si32_rect& destRect); //this is actual higher level than Redraw
  
        //internal graphics
        void redraw(Si32_rect* rc);
        void forceRedraw(void); //redraw everything and set state to updated
        void setPalette(const std::string& name, prov_id prov);
        
        //!Sets the map mode. See the MAPMODE_ defines for available modes.
        void setMapMode(u16 newMode) {mapMode = newMode;}
};

} //!namespace r2

#endif //!R2_PROVMAP_H
