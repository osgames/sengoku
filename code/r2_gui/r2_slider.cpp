//r2_slider.cpp

#include "r2_slider.h"

//This code is published under the General Public License.

using namespace r2;

void CSlider::init(const std::string& id, luabind::object const& initTable)
{
        using namespace luabind;
        
        CWindow::init(id, initTable);
        
        slider.value = 0.0;
        slider.min = 0.0;
        slider.max = 0.0;
        slider.smallStep = 0.0;
        slider.bigStep = 0.0;
        slider.bitmap = 0;
        
        if(initTable && type(initTable) == LUA_TTABLE) {
                slider.value =          R2LUA_SAFELOAD(initTable, "initialValue", LUA_TNUMBER, f64, 0.0);
                slider.min =            R2LUA_SAFELOAD(initTable, "min", LUA_TNUMBER, f64, 0.0);
                slider.max =            R2LUA_SAFELOAD(initTable, "max", LUA_TNUMBER, f64, 0.0);
                slider.smallStep =      R2LUA_SAFELOAD(initTable, "smallStep", LUA_TNUMBER, f64, 0.0);
                slider.bigStep =        R2LUA_SAFELOAD(initTable, "bigStep", LUA_TNUMBER, f64, 0.0);
                
        }
}

void CSlider::destroy()
{
        CWindow::destroy();
}

void setBitmap(const std::string& usage, const std::string& filename, luabind::object table)
{
}

void setBitmapArea(const std::string& usage, const Si32_rect& area)
{
}
