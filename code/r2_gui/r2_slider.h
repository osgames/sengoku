//r2_slider.h

//This code is published under the General Public License.

#ifndef R2_SLIDER_H
#define R2_SLIDER_H

#include "r2_window.h"

namespace r2
{

class CSlider: public CWindow
{
protected:
        struct
        {
                TRect<i32> area;
                TVector2D<i32> minPos;
                TVector2D<i32> maxPos;
                f64 value;
                f64 min;
                f64 max;
                f64 smallStep;
                f64 bigStep;
                TRect<i32> bitmapArea;
                CBitmap* bitmap;
        } slider;
public:
        void init(const std::string& id, luabind::object const& initTable);
        void destroy();
        
        //void setBitmap(const std::string& usage, const std::string& filename, luabind::object table);
        //void setBitmapArea(const std::string& usage, const Si32_rect& area);
};

}

#endif //!R2_SLIDER_H
