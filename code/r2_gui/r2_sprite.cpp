//r2_sprite.cpp

//This code is published under the General Public License.

#include "..\r2_gui\r2_sprite.h"
#include "..\r2_core\r2_globals.h"

using namespace r2;

CSpriteInstance::CSpriteInstance(const std::string& type, CSpriteSet& parent)
{
        pos.x = pos.y = 0;
        change(type, parent);
}

void CSpriteInstance::change(const std::string& type, CSpriteSet& parent)
{
        CSpriteInstance::type = type;
        CSpriteInstance::parent = &parent;
        
        playAnimation("");
}

CSpriteInstance::~CSpriteInstance()
{
        
}

void CSpriteInstance::move(const Si32_point& newPos) {
        pos = newPos;
}

void CSpriteInstance::render(CSurface& dest, const Si32_point& destSurfacePoint, const Si32_rect* clipRect)
{
        /*!The clipping rectangle is optional, set pointer to 0 to avoid clipping.*/
        if(currAni != "") {
                CBitmap* bitmap = parent->getBitmap(currAni, currAniDuration);
                const Si32_rect* srcRect = parent->getSourceRect(currAni, currAniDuration);
                Si32_point offset;
                parent->getOffset(offset, currAni, type, currAniDuration);
                
                if(bitmap == 0 || srcRect == 0) {
                        LOG_WARNING("Sprite fails to render for animation with ID: " << currAni);
                        return;
                }
                
                i32 srcWidth = srcRect->right - srcRect->left;
                i32 srcHeight = srcRect->bottom - srcRect->top;
                
                //calculate destination rect
                Si32_rect destRect;
                destRect.left = destSurfacePoint.x - srcWidth / 2 + offset.x + pos.x;
                destRect.top = destSurfacePoint.y - srcHeight / 2 + offset.y + pos.y;
                destRect.right = destRect.left + srcWidth;
                destRect.bottom = destRect.top + srcHeight;
                
                //clip dest rect to specified rectangle (or entire surface) and adjust source rect accordingly
                Si32_rect surfRect;
                if(clipRect == 0) {
                        surfRect.left = 0;
                        surfRect.top = 0,
                        surfRect.right = dest.getWidth();
                        surfRect.bottom = dest.getHeight();
                        clipRect = &surfRect;
                }
                
                dest.clippedBlit(*bitmap, srcRect, &destRect, *clipRect);
        }
}

bool CSpriteInstance::isHit(const Si32_point& testPoint)
{
        if(currAni == "") {
                return false;
        }
        
        ASSERT(parent->getSourceRect(currAni, currAniDuration) != 0);
        
        CBitmap* bitmap = parent->getBitmap(currAni, currAniDuration);
        const Si32_rect* srcRect = parent->getSourceRect(currAni, currAniDuration);
        if(srcRect == 0) {
                LOG_WARNING("Didn't find source rectangle for CSpriteInstance::isHit()");
                return false;
        }
        Si32_point offset;
        parent->getOffset(offset, currAni, type, currAniDuration);
        
        i32 srcWidth = srcRect->right - srcRect->left;
        i32 srcHeight = srcRect->bottom - srcRect->top;
        
        //transform hitpoint to local
        Si32_point p;
        p.x = testPoint.x - pos.x + srcRect->width() / 2 - offset.x;
        p.y = testPoint.y - pos.y + srcRect->height() / 2 - offset.y;
        
        //do intersection test
        if(p.x < 0 || p.y < 0 || p.x >= srcWidth || p.y >= srcHeight) {
                return false;
        }
        
        if(parent->disablePixelTest(type)) {
                return true;
        }
        
        //see if we can do a pixel test
        u8 comp1, comp2, comp3;
        u8 colorKeyType = bitmap->getColorKey(comp1, comp2, comp3);
        
        switch(colorKeyType) {
                case COLORKEY_NONE: {
                        return true;
                } case COLORKEY_PAL: {
                        bool retval = false;
                        
                        if(bitmap->lock() != 0) {
                                retval = u32(comp1) != bitmap->getPixel(p.x, p.y);
                                bitmap->unlock();
                        }
                        return retval;
                } case COLORKEY_RGB: {
                        bool retval = false;
                        
                        if(bitmap->lock() != 0) {
                                retval = bitmap->producePixelRGB(comp1, comp2, comp3) !=
                                        bitmap->getPixel(p.x, p.y);
                                bitmap->unlock();
                        }
                        return retval;
                }
        }
        
        return false;
}

TRect<i32> CSpriteInstance::getAbsRect()
{
        TRect<i32> temp(0, 0, 0, 0);
        
        if(currAni == "") {
                return temp;
        }
        
        const Si32_rect* srcRect = parent->getSourceRect(currAni, currAniDuration);
        if(srcRect == 0) {
                LOG_WARNING("Didn't find source rectangle for CSpriteInstance::getAbsRect()");
                return temp;
        }
        Si32_point offset;
        parent->getOffset(offset, currAni, type, currAniDuration);
        temp.left = -srcRect->width() / 2;
        temp.top = -srcRect->height() / 2;
        temp.right = srcRect->width() / 2;
        temp.bottom = srcRect->height() / 2;
        
        return srcRect->add(offset);
}

void CSpriteInstance::tick(f32 dt)
{
        if(currAni == "") {
                return;
        }
        
        currAniDuration += dt;
        f32 duration = parent->getTotalDuration(currAni);
        if(currAniDuration > duration) {
                currAniDuration -= duration;
                currAni = parent->getFollowUp(currAni);
                if(currAni == "" || !parent->validateAnimation(type, currAni)) {
                        currAni = parent->getDefault(type);
                }
        }
}

void CSpriteInstance::playAnimation(const std::string& id)
{
        /*!Plays a specified animation. If "" or an unknown animation is specified default animation is
        played. If no default animation is available, sprite will not render anything.*/
        
        if(parent->validateAnimation(type, id)) {
                currAni = id;
        } else {
                if(id != "") {
                        LOG_WARNING("Invalid animation ID specified to play: " << id);
                }
                currAni = parent->getDefault(type);
        }
        
        currAniDuration = 0.0f;
}

void CSpriteSet::init(CLua& lua)
{
        using namespace luabind;
        
        ASSERT(lua.state() != 0);
        
        object bitmapT(luabind::globals(lua.state())["bitmaps"]);
        object frameT(luabind::globals(lua.state())["frames"]);
        object aniT(luabind::globals(lua.state())["animations"]);
        object spriteT(luabind::globals(lua.state())["sprites"]);
        
        if(type(bitmapT) != LUA_TTABLE || type(frameT) != LUA_TTABLE ||
                        type(aniT) != LUA_TTABLE || type(spriteT) != LUA_TTABLE) {
                LOG_ERROR("Bitmap, frame, animation or sprite tables missing");
                return;
        }
        
        //bitmaps
        for(iterator i(bitmapT), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type(i.key()) == LUA_TNUMBER &&
                                type((*i)["filename"]) == LUA_TSTRING) {
                        u32 id = object_cast<u32>(i.key());
                        bool alphaBlend = type((*i)["alphaBlend"]) == LUA_TBOOLEAN ? object_cast<bool>
                                ((*i)["alphaBlend"]) : false;
                        CBitmap* bmp = r2::globals.bitmaps.bind(object_cast<std::string>((*i)["filename"]),
                                BITMAP_FLAG_VIDEOFORMAT | (alphaBlend ? BITMAP_FLAG_ALPHA : 0));
                        if(bmp != 0) {
                                bitmaps[id] = bmp;
                                
                                SRGB colorkey;
                                if(CLua::readRGB((*i)["colorKey"], colorkey.r, colorkey.g, colorkey.b)) {
                                        bmp->setColorKey(colorkey.r, colorkey.g, colorkey.b);
                                }
                        }
                }
        }
        
        //frames
        for(iterator i(frameT), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type(i.key()) == LUA_TNUMBER &&
                                type((*i)["bitmap"]) == LUA_TNUMBER) {
                        u32 id = object_cast<u32>(i.key());
                        
                        SFrame* f = new SFrame;
                        ASSERT(f != 0);
                        
                        f->bitmap = object_cast<u32>((*i)["bitmap"]);
                        
                        if(bitmaps.count(f->bitmap) == 0) {
                                LOG_ERROR("Non-existant bitmap index: " << f->bitmap);
                                return;
                        }
                        
                        if(!CLua::readRectangle((*i)["source"], f->src)) {
                                f->src.left = 0;
                                f->src.top = 0;
                                f->src.right = bitmaps[f->bitmap]->getWidth();
                                f->src.bottom = bitmaps[f->bitmap]->getHeight();
                        }
                        if(!CLua::readPoint((*i)["offset"], f->offset)) {
                                f->offset.x = 0;
                                f->offset.y = 0;
                        }
                        
                        frames[id] = f;
                }
        }
        
        //animations
        for(iterator i(aniT), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type((*i)["sequence"]) == LUA_TTABLE) {
                        SAnimation* a = new SAnimation;
                        ASSERT(a != 0);
                        a->totalDuration = 0.0f;
                        
                        for(iterator j((*i)["sequence"]); j != end; ++j) {
                                if(type(*j) == LUA_TTABLE && type((*j)["frame"]) == LUA_TNUMBER) {
                                        SSequenceFrame sf;
                                        sf.frame = object_cast<u32>((*j)["frame"]);
                                        sf.duration = type((*j)["duration"]) == LUA_TNUMBER ?
                                                object_cast<f32>((*j)["duration"]) : 1.0f;
                                        a->frames.push_back(sf);
                                        a->totalDuration += sf.duration;
                                }
                        }
                        
                        a->whenFinishedDo = type((*i)["whenFinishedDo"]) == LUA_TSTRING ?
                                object_cast<std::string>((*i)["whenFinishedDo"]) : "";
                        
                        animations[object_cast<std::string>(i.key())] = a;
                }
        }
        
        //sprites
        for(iterator i(spriteT), end; i != end; ++i) {
                if(type(*i) == LUA_TTABLE && type((*i)["animations"]) == LUA_TTABLE) {
                        SSprite* s = new SSprite;
                        ASSERT(s != 0);
                        
                        for(iterator j((*i)["animations"]); j != end; ++j) {
                                if(type(*j) == LUA_TSTRING) {
                                        s->animations.push_back(object_cast<std::string>(*j));
                                }
                        }
                        
                        if(!CLua::readPoint((*i)["offset"], s->offset)) {
                                s->offset.x = 0;
                                s->offset.y = 0;
                        }
                        
                        s->disablePixelTest = type((*i)["disablePixelTest"]) == LUA_TBOOLEAN ?
                                object_cast<bool>((*i)["disablePixelTest"]) : true;
                        
                        sprites[object_cast<std::string>(i.key())] = s;
                }
        }
}

CSpriteSet::CSpriteSet(const std::string& filename, i32 params)
{
        CLua lua;
        if(!lua.doFile(filename)) {
                return;
        }
        
        init(lua);
}

CSpriteSet::~CSpriteSet()
{
        for(std::map<u32, CBitmap*>::const_iterator it = bitmaps.begin(); it != bitmaps.end(); ++it) {
                globals.bitmaps.unbind(it->second);
        }
        
        for(std::map<u32, SFrame*>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
        
        for(std::map<std::string, SAnimation*>::const_iterator it = animations.begin();
                it != animations.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
        
        for(std::map<std::string, SSprite*>::const_iterator it = sprites.begin(); it != sprites.end(); ++it) {
                if(it->second != 0) {
                        delete it->second;
                }
        }
}

CSpriteSet::SFrame* CSpriteSet::getFrame(const std::string& animation, f32 time)
{
        if(animations.count(animation) == 0) {
                return 0;
        }
        
        SAnimation* a = animations[animation];
        
        if(a->frames.size() == 0) {
                return 0;
        }
        if(time > a->totalDuration) {
                return frames[a->frames.end()->frame]; //default to last frame
        }
        
        f32 sum = 0.0f;
        for(std::vector<SSequenceFrame>::const_iterator it = a->frames.begin(); it != a->frames.end(); ++it) {
                sum += it->duration;
                if(sum > time) {
                        return frames.count(it->frame) > 0 ? frames[it->frame] : 0;
                }
        }
        
        return frames[a->frames.end()->frame]; //default to last frame
}

CBitmap* CSpriteSet::getBitmap(const std::string& animation, f32 time)
{
        SFrame* f = getFrame(animation, time);
        
        if(f == 0) {
                return 0;
        }
        
        u32 bmp = f->bitmap;
        return bitmaps.count(bmp) > 0 ? bitmaps[bmp] : 0;
}

const Si32_rect* CSpriteSet::getSourceRect(const std::string& animation, f32 time)
{
        SFrame* f = getFrame(animation, time);
        
        return f == 0 ? 0 : &f->src;
}

void CSpriteSet::getOffset(Si32_point& offset, const std::string& animation, const std::string& sprite,
        f32 time)
{
        offset.x = 0;
        offset.y = 0;
        
        SFrame* f = getFrame(animation, time);
        
        if(f != 0) {
                offset.x += f->offset.x;
                offset.y += f->offset.y;
        }
        
        std::map<std::string, SSprite*>::iterator spr = sprites.find(sprite);
        if(spr != sprites.end()) {
                offset.x += spr->second->offset.x;
                offset.y += spr->second->offset.y;
        }
}

std::string CSpriteSet::getFollowUp(const std::string& animation)
{
        return animations.count(animation) > 0 ? animations[animation]->whenFinishedDo : "";
}

bool CSpriteSet::validateAnimation(const std::string& sprite, const std::string& animation)
{
        if(sprites.count(sprite) > 0 && animations.count(animation) > 0) {
                //lin. search
                for(std::vector<std::string>::const_iterator it = sprites[sprite]->animations.begin();
                                it != sprites[sprite]->animations.end(); ++it) {
                        if(*it == animation) {
                                return true;
                        }
                }
        }
        
        return false;
}

f32 CSpriteSet::getTotalDuration(const std::string& animation)
{
        return animations.count(animation) > 0 ? animations[animation]->totalDuration : 0.0f;
}

std::string CSpriteSet::getDefault(const std::string& sprite)
{
        return (sprites.count(sprite) > 0 && sprites[sprite]->animations.size() > 0) ?
                *sprites[sprite]->animations.begin() : "";
}

bool CSpriteSet::disablePixelTest(const std::string& sprite)
{
        std::map<std::string, SSprite*>::iterator it = sprites.find(sprite);
        return it == sprites.end() ? true : it->second->disablePixelTest;
}
