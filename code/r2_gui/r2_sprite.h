//r2_sprite.h

//This code is published under the General Public License.

#ifndef R2_SPRITE_H
#define R2_SPRITE_H

#include "..\r2_core\typedef.h"
#include "..\r2_graphics\r2_video.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_core\r2_geometry.h"

namespace r2
{
        
class CSpriteSet;

class CSpriteInstance {
protected:
        std::string type;
        CSpriteSet* parent;
        
        std::string currAni;
        f32 currAniDuration;
        
        Si32_point pos;
public: //methods
        CSpriteInstance(const std::string& type, CSpriteSet& parent);
        ~CSpriteInstance();
        
        //!Changes sprite:
        void change(const std::string& type, CSpriteSet& parent);
        
        //!Moves sprite to new position.
        void move(const Si32_point& newPos);
        
        //!Returns parent sprite set.
        CSpriteSet* getParent() {return parent;}
        
        //!Renders the sprite (current frame) centered to a point at surface. The sprite is clipped by a rect.
        void render(CSurface& dest, const Si32_point& destSurfacePoint, const Si32_rect* clipRect);
        //!Tests whether sprite is hit (if colorkeyed that equals a non-transparent pixel being hit).
        bool isHit(const Si32_point& testPoint);
        //!Updates sprite timing (may change animation and/or frame).
        void tick(f32 dt);
        //!Returns the current frame rectangle translated into final sprite coordinate space.
        TRect<i32> getAbsRect();
        
        //!Plays a specified animation.
        void playAnimation(const std::string& id);
};

class CSpriteSet {
protected: //types
        struct SFrame {
                u32 bitmap;
                Si32_rect src;
                Si32_point offset;
        };
        struct SSequenceFrame {
                u32 frame;
                f32 duration;
        };
        struct SAnimation {
                std::vector<SSequenceFrame> frames;
                std::string whenFinishedDo;
                f32 totalDuration;
        };
        struct SSprite {
                std::vector<std::string> animations;
                Si32_point offset;
                bool disablePixelTest;
        };
protected: //attributes
        std::map<u32, CBitmap*> bitmaps;
        std::map<u32, SFrame*> frames;
        std::map<std::string, SAnimation*> animations;
        std::map<std::string, SSprite*> sprites;
protected: //methods
        void init(CLua& lua);
        
        //!Gets frame for an animation at a certain time.
        SFrame* getFrame(const std::string& animation, f32 time);
public: //methods
        //!Creates a sprite set from a number of tables in a Lua state.
        CSpriteSet(CLua& lua) {init(lua);}
        //!Creates a sprite set from a Lua file.
        CSpriteSet(const std::string& filename, i32 params);
        ~CSpriteSet();
        
        //!Gets the bitmap at a certain duration for an animation.
        CBitmap* getBitmap(const std::string& animation, f32 time);
        //!Gets the source rectangle at a certain duration for an animation.
        const Si32_rect* getSourceRect(const std::string& animation, f32 time);
        //!Gets the offset at a certain duration for an animation.
        void getOffset(Si32_point& offset, const std::string& animation, const std::string& sprite, f32 time);
        //!Gets the follow-up animation for an animation.
        std::string getFollowUp(const std::string& animation);
        //!Checks if an animation belongs to a sprite.
        bool validateAnimation(const std::string& sprite, const std::string& animation);
        //!Gets tota length of an animation.
        f32 getTotalDuration(const std::string& animation);
        //!Gets default animation.
        std::string getDefault(const std::string& sprite);
        //!Returns whether pixel accurate hit tests are disabled.
        bool disablePixelTest(const std::string& sprite);
};

} //!namespace r2

#endif //!R2_SPRITE_H
