//r2_text.cpp

//This code is published under the General Public License.

#include "..\r2_gui\r2_textbox.h"
#include "..\r2_core\r2_gamestate.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_time.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_gui\r2_gui.h"
#include "..\r2_game\r2_game.h"

//add text offsets to destination of any text printed
#define TEXTOFFSET_X 3
#define TEXTOFFSET_Y 2

using namespace r2;

void CMultilineLabel::init(const std::string& id, luabind::object const& initTable)
{
        using namespace luabind;
        
        CWindow::init(id, initTable);
        
        textFont = 0;
        halign = HALIGN_LEFT;
        listOffset = 0;
        highlightedItem = -1;
        selectionAction = "";
        limitWidth = false;
        
        if(initTable && type(initTable) == LUA_TTABLE) {
                if(type(initTable["limitWidth"]) == LUA_TBOOLEAN) {
                        limitWidth = object_cast<bool>(initTable["limitWidth"]);
                }
                if(type(initTable["halign"]) == LUA_TSTRING) {
                        std::string ha = object_cast<std::string>(initTable["halign"]);
                        if(ha == "left") {
                                halign = HALIGN_LEFT;
                        } else if(ha == "center") {
                                halign = HALIGN_CENTER;
                        } else if(ha == "right") {
                                halign = HALIGN_RIGHT;
                        }
                }
        }
}

void CMultilineLabel::destroy()
{
        CWindow::destroy();
        //Res_Unbind(fontpath);
}

void CMultilineLabel::removeItems(u32 indexLow, u32 indexHigh)
{
        ASSERT(indexLow < label.size());
        ASSERT(indexHigh < label.size());
        ASSERT(indexLow <= indexHigh);
        
        highlightedItem = -1;
        
        std::vector<std::string>::iterator it = label.begin();
        for(u32 i = 0; i < indexLow; i++) {
                it++;
        }
        std::vector<std::string>::iterator first = it;
        for(u32 i = indexLow; i <= indexHigh; i++) {
                it++;
        }
        label.erase(first, it);
        
        if(listOffset >= label.size()) {
                listOffset = label.size() - 1 - calcVisibleItems();
                if(listOffset < 0) {
                        listOffset = 0;
                }
        }
}

void CMultilineLabel::clearItems()
{
        label.clear();
        listOffset = 0;
        highlightedItem = -1;
}

u32 CMultilineLabel::calcVisibleItems()
{
        if(textFont != 0) {
                i32 rows = (area.bottom - area.top - TEXTOFFSET_Y * 2) / textFont->getLineHeight();
    
                return rows < 0 ? 0 : rows;
        } else {
                return 0;
        }
}

void CMultilineLabel::highlightItem(i32 index)
{
        highlightedItem = index;
}

void CMultilineLabel::scrollItems(i32 relativeOffset)
{
        listOffset += relativeOffset;
        
        if(listOffset >= (i32) label.size()) {
                listOffset = label.size() - 1;
        }
        if(listOffset < 0) {
                listOffset = 0;
        }
}

void CMultilineLabel::setItemScrolling(u32 absoluteOffset)
{
        listOffset = absoluteOffset;
        
        if(listOffset >= label.size()) {
                listOffset = label.size() - 1;
        }
        if(listOffset < 0) {
                listOffset = 0;
        }
}

void CMultilineLabel::registerAction(const std::string& event, const std::string& action)
{
        if(event == "onSelect") {
                selectionAction = action;
        }
        
        CWindow::registerAction(event, action);
}

void CMultilineLabel::render(CSurface& dest, Si32_rect& destRect)
{
        //utilize regular drawing procedure to draw any background sought
        CWindow::render(dest, destRect);
  
        if(textFont != 0 && label.size() > 0) {
                for(i32 i = listOffset; i < label.size(); i++) {
                        //set up target coordinate based on alignment
                        Si32_point p;
                        p.y = destRect.top + textFont->getLineHeight() * (i - listOffset);
                        switch(halign) {
                                case HALIGN_LEFT: {
                                        p.x = destRect.left;
                                        break;
                                } case HALIGN_CENTER: {
                                        p.x = destRect.left - (textFont->calcStringWidth((char*) label[i].c_str()) / 2);
                                        break;
                                } case HALIGN_RIGHT: {
                                        p.x = destRect.left - textFont->calcStringWidth((char*) label[i].c_str());
                                        break;
                                }
                        }
      
                        p.x += TEXTOFFSET_X;
                        p.y += TEXTOFFSET_Y;
      
                        if(limitWidth) {
                                dest.drawText(label[i].c_str(), p, *textFont, area.right - area.left);
                        } else {
                                dest.drawText(label[i].c_str(), p, *textFont);
                        }
        
                        //draw selection line
                        if(highlightedItem == i) {
                                Si32_rect rc;
                                rc.left = p.x;
                                rc.top = p.y + textFont->getBase();
                                rc.right = rc.left + textFont->calcStringWidth((char*) label[i].c_str());
                                rc.bottom = rc.top + 1;
                                dest.drawFilledRect(&rc, 255, 255, 255);
                                rc.top++;
                                rc.bottom++;
                                dest.drawFilledRect(&rc, 0, 0, 0);
                        }
      
                        //step out if next line would exceeed height
                        if(p.y + textFont->getLineHeight() * 2 > destRect.bottom - TEXTOFFSET_Y) {
                                break;
                        }
                }
        }
}

bool CMultilineLabel::setFont(const std::string& usage, const std::string& filepath)
{
        if(usage == "text") {
                textFont = globals.fonts.bind(filepath, 0);
                return true;
        }
        
        return false;
}

bool CMultilineLabel::releaseFont(const std::string& usage)
{
        if(usage == "text") {
                globals.fonts.unbind(textFont);
                textFont = 0;
                return true;
        }
        
        return false;
}

bool CMultilineLabel::mouseUp(const Si32_point& point, u8 button, CLua& lua)
{
        using namespace luabind;
        
        if(textFont != 0 && selectionAction != "") {
                //check that x and y are within area - TEXTOFFSET edges
                if(point.x >= TEXTOFFSET_X && point.x <= (area.right - area.left - TEXTOFFSET_X) &&
                        point.y >= TEXTOFFSET_Y && point.y <= (area.bottom - area.top - TEXTOFFSET_Y)) {
                        //calculate index for selection
                        i32 index = (point.y - TEXTOFFSET_Y) / textFont->getLineHeight() + listOffset;
      
                        //validate index (make sure it points to a valid element)
                        if(index >= 0 && index < label.size()) {
                                try {
                                        call_function<void>(lua.state(), selectionAction.c_str(),
                                                boost::ref(CGUI::getInstance()),
                                                boost::ref(CGame::getInstance()), index + 1); //to Lua base 1
                                } catch(error& e) {
                                        CLua::printError(e);
                                }
                        }
                        
                        
                }
        }
        
        return CWindow::mouseUp(point, button, lua);
}

void CTextBox::init(const std::string& id, luabind::object const& initTable)
{
        using namespace luabind;
        
        CWindow::init(id, initTable);
        
        text = "";
        textFont = 0;
        offset = 0;
        pointer = 0;
        focusable = true;
        clearOnEnter = false;
        maxLen = -1;
        enterAction = "";
        
        if(initTable && type(initTable) == LUA_TTABLE) {
                if(type(initTable["maxLength"]) == LUA_TNUMBER) {
                        maxLen = object_cast<i32>(initTable["maxLength"]);
                }
                if(type(initTable["clearOnEnter"]) == LUA_TBOOLEAN) {
                        clearOnEnter = object_cast<bool>(initTable["clearOnEnter"]);
                }
        }
}

void CTextBox::destroy()
{
        globals.fonts.unbind(textFont);
        textFont = 0;
        CWindow::destroy();
}

void CTextBox::render(CSurface& dest, Si32_rect& destRect)
{
        //utilize regular drawing procedure to draw any background sought
        CWindow::render(dest, destRect);
  
        if(textFont != 0) {
                //set up target coordinate
                Si32_point p;
                p.x = destRect.left + TEXTOFFSET_X; //adjustment again
                p.y = destRect.top + TEXTOFFSET_Y; //add adjustment always

                //draw only part of string that is pointed to by offset
                dest.drawText(&text.c_str()[offset], p, *textFont,
                        destRect.right - destRect.left - TEXTOFFSET_X * 2);
    
                //should be draw pointer at all considering the time and selection?
                if((((Time_GetTicks() / 500) % 2) == 1) && CGUI::getInstance().getFocusedWindow() == this) {
                        //get length in characters from offset to pointer,
                        //and see if pointer is visible
                        i32 len = pointer - offset;
                        if(len >= 0)
                        {
                                //get width offset into destination rect for pointer
                                i32 pointerX = textFont->calcStringWidth((char*) text.substr(offset,
                                        len).c_str());
       
                                //draw as a white/black 2 pixel wide box
                                Si32_rect rc;
                                rc.left = p.x + pointerX - 1;
                                rc.top = p.y;
                                rc.right = rc.left + 1;
                                rc.bottom = rc.top + textFont->getLineHeight();
                                dest.drawFilledRect(&rc, 255, 255, 255);
                                rc.left++;
                                rc.right++;
                                dest.drawFilledRect(&rc, 0, 0, 0);
                        }
                }
        }
}

void CTextBox::keyPress(u16 keycode, u8 charcode, r2::CLua& lua)
{
        using namespace luabind;
        
        CWindow::keyPress(keycode, charcode, lua);
        
        //keycode
        switch(keycode) {
                case SDLK_ESCAPE: {
                        CGUI::getInstance().clearFocus();
                        break;
                } case SDLK_RETURN:
                case SDLK_KP_ENTER: {
                        if(text != "") {
                                if(enterAction != "") {
                                        try {
                                                call_function<void>(lua.state(), enterAction.c_str(),
                                                        boost::ref(CGUI::getInstance()),
                                                        boost::ref(CGame::getInstance()), text);
                                        } catch(error& e) {
                                                CLua::printError(e);
                                        }
                                }
                                if(clearOnEnter) {
                                        setText("");
                                }
                        
                        }
                        break;
                } case SDLK_BACKSPACE: {
                        //don't erase if pointer is all the way to the left
                        if(pointer > 0) {
                                text.erase(pointer - 1, 1); //simple and good
                                pointer--;
                                limitPointerToText();
                                limitOffsetToPointer();
                        }
                        break;
                } case SDLK_LEFT: {
                        pointer--;
                        limitPointerToText();
                        limitOffsetToPointer();
                        break;
                } case SDLK_RIGHT: {
                        pointer++;
                        limitPointerToText();
                        limitOffsetToPointer();
                        break;
                } case SDLK_HOME: {
                        pointer = 0;
                        offset = 0;
                        break;
                } case SDLK_END: {
                        pointer = text.size();
                        limitOffsetToPointer();
                        break;
                } case SDLK_DELETE: {
                        text = "";
                        offset = 0;
                        pointer = 0;
                        break;
                } default: {
                        //charcode
                        if(charcode != 0 && (text.size() < maxLen || maxLen < 0)) {
                                //build a "fake" string for our input character
                                char str[2];
                                str[0] = charcode;
                                str[1] = 0;
                      
                                text.insert(pointer, str);
                                pointer++; //move pointer to the right of the just added character
                                limitOffsetToPointer();
                        }
                        break;
                }
        }
}

void CTextBox::registerAction(const std::string& event, const std::string& action)
{
        if(event == "onEnter") {
                enterAction = action;
        }
        
        CWindow::registerAction(event, action);
}

void CTextBox::limitPointerToText()
{
        if(pointer < 0) {
                pointer = 0;
        } else if(pointer > text.size()) {
                pointer = text.size();
        }
}

void CTextBox::limitOffsetToPointer()
{
        if(pointer < offset) {
                //keeping the pointer within left side is very simple
                offset = pointer;
        } else {
                //not as simple to keep it within the right side
                //while the pointer is outside the right side, increase the offset
                if(textFont != 0) {
                        while(offset + textFont->calcStringFit((char*) text.substr(offset, pointer -
                                offset).c_str(), area.right - area.left - textFont->getLargestWidth()) < pointer) {
                                offset++;
                        }
                } else {
                        offset = pointer;
                }
        }
}

bool CTextBox::mouseUp(const Si32_point& point, u8 button, r2::CLua& lua)
{
        if(textFont != 0) {
                i32 index = textFont->calcStringIndex(text, offset, area.right - area.left, point.x, false);
                
                if(index >= 0) {
                        pointer = index;
                        limitOffsetToPointer();
                }
        }
        
        return CWindow::mouseUp(point, button, lua);
}

void CTextBox::home()
{
        offset = 0;
        pointer = 0;
}

void CTextBox::setText(const std::string& text)
{
        CTextBox::text = text;
        limitPointerToText();
        limitOffsetToPointer();
}

bool CTextBox::setFont(const std::string& usage, const std::string& filepath)
{
        if(usage == "text") {
                textFont = globals.fonts.bind(filepath, 0);
                return true;
        }
        
        return false;
}

bool CTextBox::releaseFont(const std::string& usage)
{
        if(usage == "text") {
                globals.fonts.unbind(textFont);
                textFont = 0;
                return true;
        }
        
        return false;
}
