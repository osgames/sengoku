//r2_text.h

//This code is published under the General Public License.

//This file contains controls related to displaying and editing text.

#ifndef R2_TEXT_H
#define R2_TEXT_H

#include "..\r2_gui\r2_window.h"
#include "..\r2_core\typedef.h"
#include <string>
#include "..\r2_graphics\r2_font.h"
#include "..\r2_graphics\r2_surface.h"
#include "..\r2_gui\r2_guilist.h"

#define HALIGN_LEFT 0
#define HALIGN_CENTER 1
#define HALIGN_RIGHT 2

namespace r2
{

//!A control which displays multiple lines of text; lines are selectable through mouse.
class CMultilineLabel: public r2::CWindow, public r2::IFontOwner, public r2::IList
{
protected: //attributes
        //data members
        std::vector<std::string> label;
        i32 listOffset;
        i32 highlightedItem;
        u8 halign;
        bool limitWidth; //limit text to width of control?
        
        CFont* textFont;
        std::string fontpath;
        
        std::string selectionAction;
protected: //functions
        //some events
        bool mouseUp(const Si32_point& point, u8 button, CLua& lua);
public: //functions
        void init(const std::string& id, luabind::object const& initTable);
        void destroy();
  
        //class specific list functions
        void addTextItem(const std::string& str) {label.push_back(str);}
        void setTextItem(const std::string& str, u32 index) {label[index] = str;}
        std::string getTextItem(u32 index) {return label[index];}
        
        //methods implemented from IList
        u32 itemCount() {return label.size();}
        void clearItems();
        void removeItems(u32 indexLow, u32 indexHigh);
        void scrollItems(i32 relativeOffset);
        void setItemScrolling(u32 absoluteOffset);
        u32 calcVisibleItems();
        void highlightItem(i32 index);
        
        //!Registers an action with this multiline label. Returns a boolean of whether the action was found.
        void registerAction(const std::string& event, const std::string& action);
  
        //inherited functions
        void render(CSurface& dest, Si32_rect& destRect);
  
        //implemented from IFontOwner
        bool setFont(const std::string& usage, const std::string& filepath);
        bool releaseFont(const std::string& usage);
};

//!A control which displays text and allow editing of said text.
class CTextBox: public r2::CWindow, public r2::IFontOwner
{
protected: //attributes
        std::string text;
        CFont* textFont;
        
        i32 pointer; //where pointer is in string, signed for manipulative reasons
        i32 offset; //offset from where string is shown, signed for manipulative reasons
        i32 maxLen; //maximum length, no maximum if negative
        bool clearOnEnter;
        
        std::string enterAction;
protected: //functions
        //text flow control functions:
        //!The pointer is limited based on the length of the text (basically keeping within array bounds).
        void limitPointerToText();
        //!The offset is limited to where the pointer is.
        void limitOffsetToPointer();
        
        //some events
        bool mouseUp(const Si32_point& point, u8 button, CLua& lua);
public: //functions
        void init(const std::string& id, luabind::object const& initTable);
        void destroy();
        
        //inherited functions
        void render(CSurface& dest, Si32_rect& destRect);
        
        //some events
        void keyPress(u16 keycode, u8 charcode, r2::CLua& lua);
        
        //!Registers an action with this window. Returns a boolean of whether the action was found.
        void registerAction(const std::string& event, const std::string& action);
  
        //class specific functions
        //!Sets text pointer to the leftmost possible.
        void home();
        //!Sets text and make sure pointer and such are within limits.
        void setText(const std::string& text);
        //!Gets copy of text (we don't some outsider to have access).
        std::string getTextCopy() {return text;}
        
        //implemented from IFontOwner
        bool setFont(const std::string& usage, const std::string& filepath);
        bool releaseFont(const std::string& usage);
};

} //end of namespace r2

#endif //!R2_TEXT_H
