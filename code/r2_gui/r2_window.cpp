//r2_window.cpp

//This code is published under the General Public License.

#include "..\r2_gui\r2_window.h"
#include "..\r2_core\r2_gamestate.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_res.h"
#include "..\r2_graphics\r2_cursor.h"
#include "..\r2_core\r2_globals.h"
#include "..\r2_input\r2_input.h"
#include "..\r2_game\r2_game.h"
#include "..\r2_gui\r2_gui.h"

using namespace r2;

//CResourceManager<CBitmap> bitmaps;

bool CWindow::mouseUp(const Si32_point& point, u8 button, r2::CLua& lua)
{
        using namespace luabind;
        
        ASSERT(lua.state() != 0);
        
        if(mouseClickAction != "") {
                try {
                        call_function<void>(lua.state(), mouseClickAction.c_str(),
                                boost::ref(CGUI::getInstance()),
                                boost::ref(CGame::getInstance()), point.x, point.y, button);
                } catch(error& e) {
                        object errorMsg(from_stack(e.state(), -1));
                        LOG_ERROR("Lua error: " << errorMsg);
                }
        }
        
        if(spriteClickAction != "") {
                std::string hitSprite = clickTestSprites(point);
                if(hitSprite != "") {
                        try {
                                call_function<void>(lua.state(), spriteClickAction.c_str(),
                                        boost::ref(CGUI::getInstance()),
                                        boost::ref(CGame::getInstance()), hitSprite, button); //TOCONSIDER: add coords
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                        return true;
                }
        }
        
        return false;
}

void CWindow::init(const std::string& id, luabind::object const& initTable)
{
        using namespace luabind;
        
        ASSERT(!inited);
        
        //set some default properties
        areaSet = false;
        renderMode = WRENDER_NONE;
        enabled = true;
        focusable = false;
        hasColorKey = false;
        CWindow::id = id;
        mouseClickAction = "";
        keyPressAction = "";
        shownAction = "";
        spriteClickAction = "";
        visible = true;
        clickable = true;
        area.left = area.top = area.right = area.bottom = 0;
        
        //look up properties from init table
        if(initTable && type(initTable) == LUA_TTABLE) {
                visible =       R2LUA_SAFELOAD(initTable, "visible", LUA_TBOOLEAN, bool, true);
                clickable =     R2LUA_SAFELOAD(initTable, "clickable", LUA_TBOOLEAN, bool, true);
                
                if(CLua::readRectangle(initTable["initialArea"], area)) {
                        areaSet = true;
                }
        }
        
        inited = true;
}

void CWindow::destroy()
{
        if(!inited) {
                return;
        }
        
        for(std::map<std::string, CSpriteInstance*>::iterator it = sprites.begin(); it != sprites.end();
                ++it) {
                ASSERT(it->second != 0);
                globals.sprites.unbind(it->second->getParent());
                delete it->second;
                it->second = 0; //for safety, we don't know what derived classes may attempt
        }
        
        globals.bitmaps.unbind(backgroundBitmap);
        backgroundBitmap = 0;
}

void CWindow::update(f32 dt)
{
        for(std::map<std::string, CSpriteInstance*>::const_iterator it = sprites.begin(); it != sprites.end();
                ++it) {
                ASSERT(it->second != 0);
                it->second->tick(dt);
        }
}

void CWindow::render(CSurface& dest, Si32_rect& destRect)
{
        //render ourself
        switch(renderMode)
        {
        case WRENDER_SOLID:
                dest.drawFilledRect(&destRect, backColor.r, backColor.g, backColor.b);
                renderSprites(dest, destRect);
                break;
        case WRENDER_BITMAPPED:
                if(backgroundBitmap != 0) {
                        ASSERT(backgroundBitmap != 0);
                        dest.blit(*backgroundBitmap, &backgroundBitmapArea, &destRect);
                }
                renderSprites(dest, destRect);
                break;
        } //if other mode, don't render at all
}

void CWindow::registerAction(const std::string& event, const std::string& action)
{
        if(event == "onClick") {
                mouseClickAction = action;
        } else if(event == "keyPress") {
                keyPressAction = action;
        } else if(event == "shown") {
                shownAction = action;
        } else if(event == "spriteClick") {
                spriteClickAction = action;
        }
}

bool CWindow::checkClick(const Si32_point& point, u8 button, bool down, CLua& lua, CWindow* downWindow)
{
        if(!clickable) {
                return false;
        }
        
        i32 width = area.right - area.left;
        i32 height = area.bottom - area.top;
        
        if(point.x >= 0 && point.x <= width && point.y >= 0 && point.y <= height) {
                if(down) {
                        mouseDown(point, button, lua);
                } else {
                        if(downWindow == this) {
                                mouseUp(point, button, lua);
                        }
                }
                return true;
        }
        
        return false;
}

void CWindow::keyPress(u16 keycode, u8 charcode, r2::CLua& lua)
{
        using namespace luabind;
        
        if(keyPressAction != "") {
                try {
                        call_function<void>(lua.state(), keyPressAction.c_str(), boost::ref(CGUI::getInstance()),
                                boost::ref(CGame::getInstance()), keycode, charcode);
                } catch(error& e) {
                        object errorMsg(from_stack(e.state(), -1));
                        LOG_ERROR("Lua error: " << errorMsg);
                        return;
                }
        }
}

void CWindow::setArea(const Si32_rect& area)
{
        CWindow::area = area;
        areaSet = true;
}

void CWindow::setBitmap(const std::string& usage, const std::string& filename, luabind::object table)
{
        using namespace luabind;
        
        //TODO: implement table check to determine flags
        i32 flags = BITMAP_FLAG_VIDEOFORMAT;
        
        if(table && type(table) == LUA_TTABLE) {
                if(type(table["alphaBlend"]) == LUA_TBOOLEAN) {
                        flags = flags | (object_cast<bool>(table["alphaBlend"]) ? BITMAP_FLAG_ALPHA : 0);
                }
        }
        
        if(usage == "background") {
                r2::globals.bitmaps.unbind(backgroundBitmap);
                backgroundBitmap = r2::globals.bitmaps.bind(filename, flags);
                if(backgroundBitmap != 0) {
                        renderMode = WRENDER_BITMAPPED;
                        if(!areaSet) {
                                //set area implicitly if it wasn't set explicitly
                                backgroundBitmapArea.left = 0;
                                backgroundBitmapArea.top = 0;
                                backgroundBitmapArea.right = backgroundBitmap->getWidth();
                                backgroundBitmapArea.bottom = backgroundBitmap->getHeight();
                        }
                }
        }
}

void CWindow::setBitmapArea(const std::string& usage, const Si32_rect& area)
{
        if(usage == "background") {
                backgroundBitmapArea = area;
        }
}

void CWindow::setBitmapColorKey(const std::string& usage, SRGB& key)
{
        if(usage == "background" && backgroundBitmap != 0) {
                backgroundBitmap->setColorKey(key.r, key.g, key.b);
        }
}

void CWindow::setBitmapColorKey(const std::string& usage, u8 key)
{
        if(usage == "background" && backgroundBitmap != 0) {
                backgroundBitmap->setColorKey(key);
        }
}

void CWindow::setRenderMode(const std::string& mode)
{
        if(mode == "solid") {
                renderMode = WRENDER_SOLID;
        } else if(mode == "bitmap") {
                renderMode = WRENDER_BITMAPPED;
        } else {
                renderMode = WRENDER_NONE;
        }
}

void CWindow::setColor(const std::string& usage, SRGB& rgb)
{
        if(usage == "background") {
                backColor = rgb;
        }
}

void CWindow::setVisible(bool v, CLua& lua)
{
        using namespace luabind;
        
        if(visible != v) {
                visible = v;
                
                if(shownAction != "") {
                        try {
                                call_function<void>(lua.state(), shownAction.c_str(),
                                        boost::ref(CGUI::getInstance()),
                                        boost::ref(CGame::getInstance()), v);
                        } catch(error& e) {
                                CLua::printError(e);
                        }
                }
        }
}

void CWindow::renderSprites(CSurface& dest, Si32_rect& destRect)
{
        for(std::map<std::string, CSpriteInstance*>::const_iterator it = sprites.begin(); it != sprites.end();
                ++it) {
                ASSERT(it->second != 0);
                Si32_point p(destRect.left, destRect.top);
                it->second->render(dest, p, &destRect);
        }
}

std::string CWindow::clickTestSprites(const Si32_point& testPoint)
{
        for(std::map<std::string, CSpriteInstance*>::const_reverse_iterator it = sprites.rbegin();
                it != (std::map<std::string, CSpriteInstance*>::const_reverse_iterator) sprites.rend();
                ++it) {
                ASSERT(it->second != 0);
                if(it->second->isHit(testPoint)) {
                        return it->first;
                }
        }
        
        return "";
}

void CWindow::addSprite(const std::string& id, const std::string& filename,
                const std::string& type, const Si32_point& pos)
{
        CSpriteSet* set = globals.sprites.bind(filename, true);
        
        if(set == 0) {
                LOG_WARNING("Bad sprite set, ignoring addSprite");
                return;
        }
        
        if(sprites.count(id) > 0) {
                ASSERT(sprites[id] != 0);
                globals.sprites.unbind(sprites[id]->getParent());
                sprites[id]->change(type, *set);
                sprites[id]->move(pos);
        } else {
                sprites[id] = new CSpriteInstance(type, *set);
                ASSERT(sprites[id] != 0);
                sprites[id]->move(pos);
        }
}

void CWindow::removeSprite(const std::string& id)
{
        if(sprites.count(id) > 0) {
                ASSERT(sprites[id] != 0);
                globals.sprites.unbind(sprites[id]->getParent());
                delete sprites[id];
                sprites.erase(id);
        }
}

void CWindow::animateSprite(const std::string& id, const std::string& animation)
{
        if(sprites.count(id) > 0) {
                sprites[id]->playAnimation(animation);
        }
}

void CWindow::moveSprite(const std::string& id, const Si32_point& newPos)
{
        if(sprites.count(id) > 0) {
                sprites[id]->move(newPos);
        }
}

u32 CWindow::getWidth()
{
        i32 width = area.right - area.left;
        
        return width < 0 ? width * -1 : width;
}

u32 CWindow::getHeight()
{
        i32 height = area.bottom - area.top;
        
        return height < 0 ? height * -1 : height;
}
