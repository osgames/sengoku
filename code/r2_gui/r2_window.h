//r2_window.h

//This code is published under the General Public License.

/*Changelog:
        2007-09-22: Started The Great Rewrite (TM).*/

#ifndef R2_WINDOW_H
#define R2_WINDOW_H

#include <list>
#include <string>
#include "..\r2_graphics\r2_video.h"
#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_tree.h"
#include "..\r2_core\r2_lua_manager.h"
#include "..\r2_gui\r2_sprite.h"

#define WRENDER_NONE 0
#define WRENDER_SOLID 1
#define WRENDER_BITMAPPED 2

/*Asthetically a window is a rectangular area on screen that can
receive clicks and has a graphical appearance in the same area.
This area is in case of the root window referring to screen space, else the
coordinate space of the parent.*/

namespace r2
{

class CWindow
{
public: //types
        class CWindowComparator : public r2::IComparator<CWindow*, CWindow*>
        {
                i8 compare(CWindow*& lhs, CWindow*& rhs)
                {
                        return lhs->id != rhs->id;
                }
        };
        class CStringComparator : public r2::IComparator<CWindow*, std::string>
        {
                i8 compare(CWindow*& lhs, std::string& rhs)
                {
                        return lhs->id != rhs;
                }
        };
protected: //types
        struct SFloatingText
        {
                f32 duration;
                CFont* font;
                Si32_point pos;
                i32 maxWidth;
                std::string str;
        };
protected: //attributes
        CBitmap* backgroundBitmap;
        Si32_rect backgroundBitmapArea;
        
        SRGB backColor;
        SRGB colorKeyRGB;
        u8 colorKeyPalette;
        bool hasColorKey;
        
        u8 renderMode; //constant
        
        Si32_rect area; //left = x, top = y, right = w + x, bottom = h + y
        bool areaSet; //has area been set explicitly?
        
        std::string id;
        
        std::map<std::string, CSpriteInstance*> sprites;
        
        //events
        std::string mouseClickAction;
        std::string keyPressAction;
        std::string shownAction;
        std::string spriteClickAction;
        
        bool visible; //it makes all children invisible too, automatically inactive
        bool enabled; //does this respond to input?
        bool focusable; //can this window be focused?
        bool clickable; //deafult is true, does this window respond to clicks
        bool inited;
protected:// methods
        //!This function should be called by checkClick when a mouse up on window has been detected.
        virtual bool mouseUp(const Si32_point& point, u8 button, r2::CLua& lua);
        //!This function should be called by checkClick when a mouse down on window has been detected.
        virtual bool mouseDown(const Si32_point& point, u8 button, r2::CLua& lua) {}
        
        //!Function which determines how sprites are drawn.
        virtual void renderSprites(CSurface& dest, Si32_rect& destRect);
        //!Function which determines if a sprite was clicked. Returns empty string if none was clicked.
        virtual std::string clickTestSprites(const Si32_point& testPoint);
public:
        //!The constructor must have no arguments so we can use it for generic registration. See init().
        CWindow() {inited = false;}
        //!The destructor just makes sure destroy() is called.
        ~CWindow() {destroy();}
        
        //!The real initialization method which should be overriden by derived classes as required.
        virtual void init(const std::string& id, luabind::object const& initTable);
        //!Real destruction method.
        virtual void destroy();
        
        //!Gives window processing time.
        virtual void update(f32 dt);
  
        //!Renders the window.
        virtual void render(CSurface& dest, Si32_rect& destRect);
  
        void applyColorKey(); //should we event have this, delete it?
        
        //!Registers an action with this window. Should call super function when inherited.
        virtual void registerAction(const std::string& event, const std::string& action);
        
        //Some events:
        //!Tests whether the window is clicked; it's not to call the click action!
        virtual bool checkClick(const Si32_point& point, u8 button, bool down, r2::CLua& lua,
                CWindow* downWindow);
        //!Called whenever mouse moved from pressed down; coords are relative to position when initially pressed.
        virtual void mouseMove(const Si32_point& point) {}
        //!If some other window got mouse up when this previously had mouse down, this is called.
        virtual void unclick() {}
        //!Deals with keypresses.
        virtual void keyPress(u16 keycode, u8 charcode, r2::CLua& lua);
        
        //!Sets the position and size of the window with a (L, T, R, B) rectangle.
        void setArea(const Si32_rect& area);
        
        //Bitmap operations:
        //!Sets a bitmap of this winodow.
        virtual void setBitmap(const std::string& usage, const std::string& filename, luabind::object table);
        //!Sets the bitmap area of a specific bitmap.
        virtual void setBitmapArea(const std::string& usage, const Si32_rect& area);
        //!Sets the color key of a bitmap to a RGB value.
        virtual void setBitmapColorKey(const std::string& usage, SRGB& key);
        //!Sets the color key of a bitmap to a palette index.
        virtual void setBitmapColorKey(const std::string& usage, u8 key);
        
        //Misc. graphics operations:
        //!Sets the render mode (bitmapped, or solid, etc.).
        void setRenderMode(const std::string& mode);
        //!Sets a color of this window.
        virtual void setColor(const std::string& usage, SRGB& rgb);
        
        //!Shows or hides the window.
        void setVisible(bool v, r2::CLua& lua);
        
        //Sprite handling:
        //!Adds a sprite somewhere in window.
        void addSprite(const std::string& id, const std::string& filename,
                const std::string& type, const Si32_point& pos);
        //!Removes sprite.
        void removeSprite(const std::string& id);
        //!Plays a sprite animation.
        void animateSprite(const std::string& id, const std::string& animation);
        //!Moves a sprite.
        void moveSprite(const std::string& id, const Si32_point& newPos);
        
        //Various get operations:
        //!Is this window visible?
        bool isVisible() {return visible;}
        //!Returns the size + position rectangle.
        Si32_rect& getArea() {return area;}
        //!Gets the ID (name) of the window.
        std::string& getID() {return id;}
        //!Returns if this window is focusable.
        bool isFocusable() {return focusable && enabled;}
        //!Returns if this window is enabled.
        bool isEnabled() {return enabled;}
        //!Returns width.
        u32 getWidth();
        //!Returns height.
        u32 getHeight();
};

} //end of namespace r2

#endif //!R2_WINDOW_H
