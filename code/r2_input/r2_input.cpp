//r2_input.cpp

//This code is published under the General Public License.

#include "..\r2_input\r2_input.h"
#include "..\r2_core\r2_log.h"
#include "..\r2_core\r2_errors.h"

#include <SDL.h>

//SDL vars (and custom)
Si32_point g_mouseMoves;
Uint8 g_mouseButtons;
SDLMod g_keyboardMods;
Uint8* g_keyboardButtons;
u8 g_pressedChar;
u16 g_pressedKey;

bool Input_Init(void)
{ 
  //I am aware this function is very lame...
  //some time there may actually be a need for one though
    
  return true;
}

void Input_Exit(void)
{
}

bool Input_Refresh(void)
{
        //store all information about mouse
        g_mouseButtons = (u32) SDL_GetRelativeMouseState((int*) &g_mouseMoves.x, (int*) &g_mouseMoves.y);
    
        //store keyboard mods
        g_keyboardMods = SDL_GetModState();
}

void Input_MouseMovement(Si32_point& p)
{
        p.x = g_mouseMoves.x;
        p.y = g_mouseMoves.y;
}

bool Input_KeyboardButton(u32 button)
{
        return g_keyboardButtons[button];
}

bool Input_MouseButton(u8 button)
{
        switch(button) {
        case MOUSE_LEFT:
                return g_mouseButtons & SDL_BUTTON(1);
        case MOUSE_MIDDLE:
                return g_mouseButtons & SDL_BUTTON(2);
        case MOUSE_RIGHT:
                return g_mouseButtons & SDL_BUTTON(3);
        default:
                return false;
  }
}

u8 Input_MouseButton2(u8 SDLbutton) //hackish for now
{
        return SDLbutton;
}

bool Input_KeyboardMod(u8 mod)
{
        switch(mod) {
        case KEYMOD_CTRL:
                return g_keyboardMods & KMOD_CTRL;
        case KEYMOD_ALT:
                return g_keyboardMods & KMOD_ALT;
        case KEYMOD_SHIFT:
                return g_keyboardMods & KMOD_SHIFT;
        default:
        return false;
        }
}
