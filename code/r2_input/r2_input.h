//r2_input.h

//This code is published under the General Public License.

//really, this file could have been renamed to "r2_mouse.h"

#ifndef R2_INPUT_H
#define R2_INPUT_H

#include "..\r2_core\typedef.h"

//for the purposes of this implementation
#include <SDL_keysym.h>

#define MOUSE_LEFT 1
#define MOUSE_MIDDLE 2
#define MOUSE_RIGHT 3

#define KEYMOD_CTRL 1
#define KEYMOD_ALT 2
#define KEYMOD_SHIFT 3

bool Input_Init(void);
void Input_Exit(void);

//update function
bool Input_Refresh(void);

//internally handled input
void Input_MouseMovement(Si32_point& p);
bool Input_MouseButton(u8 button); //returns true if pressed
u8 Input_MouseButton2(u8 SDLbutton); //hackish for now
bool Input_KeyboardMod(u8 mod); //returns true if active

#endif //!R2_INPUT_H
