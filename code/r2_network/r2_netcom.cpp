//r2_netcom.cpp

//Corresponding header: r2_netcom.h

#include "..\r2_network\r2_netcom.h"
#include "..\r2_core\r2_log.h"

using namespace r2;

CNetcom* CNetcomHandler::newNetcom()
{
        return new CNetcom();
}

bool CNetcomHandler::initNetcomFromNewPacket(CNetcom& netcom, u16 type, u32 dataSize, bool reliable)
{
        return netcom.initNewPacket(type, dataSize, reliable);
}

bool CNetcomHandler::initNetcomFromPacket(CNetcom& netcom, ENetPacket* packet, u8 channel, u8 user)
{
        return netcom.initFromPacket(packet, channel, user);
}

ENetPacket* CNetcomHandler::getPacket(CNetcom& netcom)
{
        return netcom.getPacket();
}

void CNetcomHandler::setPacket(CNetcom& netcom, ENetPacket* packet)
{
        netcom.setPacket(packet);
}

void CNetcomHandler::setUnwritable(CNetcom& netcom)
{
        netcom.setUnwritable();
}

void CNetcomHandler::setChannel(CNetcom& netcom, u8 ch)
{
        netcom.setChannel(ch);
}

void CNetcomHandler::setUser(CNetcom& netcom, u8 user)
{
        netcom.setUser(user);
}

void CNetcomHandler::destroy()
{
        while(netcoms.size() > 0) {
                CNetcom* netcom = netcoms.front();
                netcoms.pop_front();
                if(netcom != 0) {
                        delete netcom;
                }
        }
        
        events.clear();
}

bool CNetcomHandler::createWritableNetcom(CNetcom** netcom, u8 type, u32 dataSize, bool reliable)
{
        ASSERT(netcom != 0);
        
        *netcom = new CNetcom();
        if(*netcom == 0) {
                return false;
        }
        
        if(!(*netcom)->initNewPacket(type, dataSize, reliable)) {
                delete *netcom;
                *netcom = 0;
                return false;
        }
        
        return true;
}

void CNetcomHandler::destroyNetcom(CNetcom** netcom)
{
        if(netcom != 0 && *netcom != 0) {
                delete *netcom;
                *netcom = 0;
        }
}

void CNetcomHandler::enqueueNetcom(CNetcom** netcom)
{
        if(netcom != 0 && *netcom != 0) {
                netcoms.push_back(*netcom);
                *netcom = 0;
        }
}

void CNetcomHandler::enqueueEvent(const SNetworkEvent& ev)
{
        events.push_back(ev);
}

CNetcom* CNetcomHandler::receiveNextNetcom()
{
        if(netcoms.size() > 0) {
                CNetcom* ret = netcoms.front();
                netcoms.pop_front();
                return ret;
        }
        
        return 0;
}


void CNetcomHandler::nextEvent()
{
        if(events.size() > 0) {
                events.pop_front();
        }
}

SNetworkEvent* CNetcomHandler::currentEvent()
{
        if(events.size() > 0) {
                return &events.front();
        }
        
        return 0;
}

CNetcom* CNetcom::copy()
{
        CNetcom* cpy = new CNetcom();
        ASSERT(cpy);
        cpy->type = type;
        cpy->dataSize = dataSize;
        cpy->dataIndex = dataIndex;
        cpy->channel = channel;
        cpy->user = user;
        cpy->writable = true;
        cpy->packet = enet_packet_copy(packet);
}

void CNetcom::destroy()
{
        if(packet != 0) {
                enet_packet_destroy(packet);
        }
}

bool CNetcom::initNewPacket(u16 type, u32 dataSize, bool reliable)
{
        /*!Initializes this network command with a new ENet packet that can be written to. The size of the packet data is dataSize + sizeof(SNetcomPacketHeader).
        
        Arguments:
                - type: Type of network command.
                - dataSize: Size of command data.
                - reliable: True if command shall be sent in order and with guaranteed arrival, false if otherwise.
        
        DevNote: Check that SNetcomPacketHeader isn't modified by the compiler for alignment purposes, will waste bandwidth...*/
        
        ASSERT(CNetcom::packet == 0);
        
        packet = enet_packet_create(0, sizeof(SNetcomPacketHeader) + dataSize,
                reliable? ENET_PACKET_FLAG_RELIABLE : ENET_PACKET_FLAG_UNSEQUENCED);
        
        if(packet == 0) {
                LOG_ERROR("Failed to create packet.");
                return false;
        }
        
        //write header
        SNetcomPacketHeader* h = (SNetcomPacketHeader*) packet->data;
        h->type = ENET_HOST_TO_NET_16(type);
        
        CNetcom::type = type;
        CNetcom::dataSize = dataSize;
        writable = true;
        rewind();
        
        return true;
}

bool CNetcom::initFromPacket(ENetPacket* packet, u8 channel, u8 user)
{
        /*!Initializes this network command with an existing ENet packet that can be read from.
        
        Arguments:
                - packet: A pointer to the existing packet. Must not be 0.
                - channel: Channel on which this packet was received.
                - user: Number of user who sent the packet.*/
        
        ASSERT(CNetcom::packet == 0);
        ASSERT(packet != 0);
        
        //ensure that the packet contains the header at minimum
        if(packet->dataLength < sizeof(SNetcomPacketHeader)) {
                LOG_WARNING("Size of packet less than header.");
                return false;
        }
        
        //read header
        SNetcomPacketHeader* h = (SNetcomPacketHeader*) packet->data;
        type = ENET_NET_TO_HOST_16(h->type);
                
        CNetcom::packet = packet;
        CNetcom::channel = channel;
        CNetcom::user = user;
        dataSize = packet->dataLength - sizeof(SNetcomPacketHeader);
        writable = false;
        rewind();
        
        return true;
}

void CNetcom::writeByte(u8 byte)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(byte) + dataIndex <= dataSize);
        
        *(u8*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = byte;
        dataIndex += sizeof(byte);
}

void CNetcom::writeByte(i8 byte)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(byte) + dataIndex <= dataSize);
        
        *(i8*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = byte;
        dataIndex += sizeof(byte);
}

void CNetcom::writeShort(u16 word)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(word) + dataIndex <= dataSize);
        
        *(u16*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = ENET_HOST_TO_NET_16(word);
        dataIndex += sizeof(word);
}

void CNetcom::writeShort(i16 word)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(word) + dataIndex <= dataSize);
        
        *(i16*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = ENET_HOST_TO_NET_16(word);
        dataIndex += sizeof(word);
}

void CNetcom::writeLong(u32 dword)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(dword) + dataIndex <= dataSize);
        
        *(u32*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = ENET_HOST_TO_NET_32(dword);
        dataIndex += sizeof(dword);
}

void CNetcom::writeLong(i32 dword)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(sizeof(dword) + dataIndex <= dataSize);
        
        *(i32*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex) = ENET_HOST_TO_NET_32(dword);
        dataIndex += sizeof(dword);
}
        
void CNetcom::writeString(const std::string& str)
{
        ASSERT(writable);
        ASSERT(packet);
        ASSERT(str.size() + 1 + dataIndex <= dataSize);
        
        if(str.size() > 0) {
                memcpy(packet->data + sizeof(SNetcomPacketHeader) + dataIndex, str.c_str(), str.size());
        }
        packet->data[sizeof(SNetcomPacketHeader) + dataIndex + str.size()] = 0;
        dataIndex += str.size() + 1;
}

void CNetcom::readByte(u8& byte)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(u8) + dataIndex <= dataSize);
        
        byte = *(u8*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex);
        dataIndex += sizeof(byte);
}

void CNetcom::readByte(i8& byte)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(i8) + dataIndex <= dataSize);
        
        byte = *(i8*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex);
        dataIndex += sizeof(byte);
}

void CNetcom::readShort(u16& word)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(u16) + dataIndex <= dataSize);
        
        word = ENET_NET_TO_HOST_16(*(u16*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex));
        dataIndex += sizeof(word);
}

void CNetcom::readShort(i16& word)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(i16) + dataIndex <= dataSize);
        
        word = ENET_NET_TO_HOST_16(*(i16*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex));
        dataIndex += sizeof(word);
}

void CNetcom::readLong(u32& dword)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(u32) + dataIndex <= dataSize);
        
        dword = ENET_NET_TO_HOST_32(*(u32*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex));
        dataIndex += sizeof(dword);
}

void CNetcom::readLong(i32& dword)
{
        ASSERT(!writable);
        ASSERT(packet);
        ASSERT(sizeof(i32) + dataIndex <= dataSize);
        
        dword = ENET_NET_TO_HOST_32(*(i32*)(packet->data + sizeof(SNetcomPacketHeader) + dataIndex));
        dataIndex += sizeof(dword);
}
        
void CNetcom::readString(std::string& str)
{
        /*!This function reads a null terminated byte string from the current data position of the packet.
        It also reads the null character, so the total bytes read is readStringLen() + 1.*/
        
        ASSERT(!writable);
        ASSERT(packet);
        
        u32 len = readStringLen() + 1;
        
        ASSERT(len + dataIndex <= dataSize);
        
        str = (const char*) (packet->data + sizeof(SNetcomPacketHeader) + dataIndex);
        dataIndex += len;
}

u32 CNetcom::readStringLen()
{
        return strlen((const char*) packet->data + sizeof(SNetcomPacketHeader) + dataIndex);
}

ENetPacket* enet_packet_copy(ENetPacket* packet)
{
        if(packet == 0) {
                return 0;
        } else {
                return enet_packet_create(packet->data, packet->dataLength, packet->flags);
        }
}
