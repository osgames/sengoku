//r2_netcom.h

//This code is published under the General Public License.

/*Revision history:
2007-09-07: Added Doxygen documentation. Implemented corresponding .cpp file.
2007-09-02: Starting implementing Netcom system and removing older "packets system".
*/

#ifndef R2_NETCOM_H
#define R2_NETCOM_H

#include "..\r2_core\typedef.h"
#include "enet/enet.h"
#include <string>
#include <list>

#define NETEVENTTYPE_NULL                       0
#define NETEVENTTYPE_SERVER_CLIENTCONNECTED     1
#define NETEVENTTYPE_SERVER_CLIENTDISCONNECTED  2
#define NETEVENTTYPE_CLIENT_CONNECTIONFAILED    1
#define NETEVENTTYPE_CLIENT_CONNECTED           2
#define NETEVENTTYPE_CLIENT_DISCONNECTED        3
#define NETEVENTTYPE_CLIENT_OTHERCONNECTED      4
#define NETEVENTTYPE_CLIENT_OTHERDISCONNECTED   5

ENetPacket* enet_packet_copy(ENetPacket* packet);

namespace r2 {

struct SNetworkEvent
{
        u16 type;
        u8 id;
        std::string name;
};

class CNetcom;

class CNetcomHandler
{
protected: //attributes
        std::list<CNetcom*> netcoms;
        std::list<SNetworkEvent> events;
protected: //functions
        //Creates a new netcom.
        CNetcom* newNetcom();
        //!Inits the ENet 'packet' member with relevant data as well as setting attributes itself.
        bool initNetcomFromNewPacket(CNetcom& netcom, u16 type, u32 dataSize, bool reliable);
        //!Inits from an existing ENet packet (for instance, when such a packet was received).
        bool initNetcomFromPacket(CNetcom& netcom, ENetPacket* packet, u8 channel, u8 user);
        
        //!Access the ENet packet as needed.
        ENetPacket* getPacket(CNetcom& netcom);
        //!Set the ENet packet as needed.
        void setPacket(CNetcom& netcom, ENetPacket* packet);
        
        //!Sets Netcom content to be read only.
        void setUnwritable(CNetcom& netcom);
        //!Sets channel.
        void setChannel(CNetcom& netcom, u8 ch);
        //!Sets user netcom was sent from.
        void setUser(CNetcom& netcom, u8 user);
        
        //!Simple adds an event to queue.
        void enqueueEvent(const SNetworkEvent& ev);
public: //functions
        ~CNetcomHandler() {destroy();}
        
        void destroy();
        
        //!Create a new network command owned by this network for sending.
        bool createWritableNetcom(CNetcom** netcom, u8 type, u32 dataSize, bool reliable = true);
        //!Destroy a network command created by createNetcom() or receiveNextNetcom().
        void destroyNetcom(CNetcom** netcom);
        
        //!Simply adds a netcom to queue and transfers responsibility, doesn't do anything with it.
        void enqueueNetcom(CNetcom** netcom);
        
        //!Receive next network command in reception queue. Netcom must be destroyed by caller.
        CNetcom* receiveNextNetcom(); //Netcom sent to client
        u32 countNetcoms() {return netcoms.size();}
        
        //!Moves to next network command in queue. Invalidates any pointers to previous.
        void nextEvent();
        //!Returns network command in front in queue. Returns 0 if empty queue. Should not be deleted by caller!!!
        SNetworkEvent* currentEvent();
};

//!Network command which represents data sent across network, or locally.
class CNetcom
{
        friend class CNetcomHandler;
private:
        //!Contains header data placed in a packet sent over the network. Data is in network order.
        struct SNetcomPacketHeader
        {
                u16 type;
        };
        
        u16 type; //not necessarily of same byte order as in packet (which is network order)
        u32 dataSize;
        u32 dataIndex;
        u8 user;
        ENetPacket* packet;
        u8 channel;
        bool writable;
protected:
        //!Constructor hidden so that only CNetcomHandler can create a CNetcom.
        CNetcom() {packet = 0;}
        
        //!Cleans up.
        void destroy();
        //!Inits the ENet 'packet' member with relevant data as well as setting attributes itself.
        bool initNewPacket(u16 type, u32 dataSize, bool reliable);
        //!Inits from an existing ENet packet (for instance, when such a packet was received).
        bool initFromPacket(ENetPacket* packet, u8 channel, u8 user);
        
        //!Allows CNetwork to access the ENet packet as needed.
        ENetPacket* getPacket() {return packet;}
        //!Allows CNetwork to set the ENet packet as needed.
        void setPacket(ENetPacket* packet) {CNetcom::packet = packet;}
        
        //!Sets the Netcom to be unwritable.
        void setUnwritable() {writable = false;}
        //!Sets channel of Netcom.
        void setChannel(u8 ch) {channel = ch;}
        //!Sets the user Netcom was sent from.
        void setUser(u8 u) {user = u;}
public:
        //!Destructor hidden so that only CNetwork can destroy a CNetcom.
        ~CNetcom() {destroy();}
        
        //!Creates a proper copy, which is writeable regardless of original.
        CNetcom* copy();
        
        //!Sets the data index to 0 to allow writing/reading from 0.
        void rewind() {dataIndex = 0;}
        
        //!Returns command type.
        u16 getType() {return type;}
        //!Calculates number of bytes left of data to write/read.
        i32 bytesLeft() {return dataSize - dataIndex;}
        //!Gets size of data.
        u32 getSize() {return dataSize;}
        //!If this netcom was received, this will return what channel the netcom was received on.
        u8 onChannel() {return channel;}
        //!If this netcom was received on server, this will be from what user this netcom was sent.
        u8 fromUser() {return user;}
        
        //!Writes an unsigned byte to packet in network order.
        void writeByte(u8 byte);
        void writeInteger(u8 byte) {writeByte(byte);}
        //!Writes a signed byte to packet in network order.
        void writeByte(i8 byte);
        void writeInteger(i8 byte) {writeByte(byte);}
        //!Writes an unsigned short to packet in network order.
        void writeShort(u16 word);
        void writeInteger(u16 word) {writeShort(word);}
        //!Writes a signed short to packet in network order.
        void writeShort(i16 word);
        void writeInteger(i16 word) {writeShort(word);}
        //!Writes an unsigned long to packet in network order.
        void writeLong(u32 dword);
        void writeInteger(u32 dword) {writeLong(dword);}
        //!Writes a signed long to packet in network order.
        void writeLong(i32 dword);
        void writeInteger(i32 dword) {writeLong(dword);}
        //!Writes a std::string to packet.
        void writeString(const std::string& str);
        
        //!Reads an unsigned byte from packet into host order.
        void readByte(u8& byte);
        void readInteger(u8& byte) {readByte(byte);}
        //!Reads a signed byte from packet into host order.
        void readByte(i8& byte);
        void readInteger(i8& byte) {readByte(byte);}
        //!Reads an unsigned short from packet into host order.
        void readShort(u16& word);
        void readInteger(u16& word) {readShort(word);}
        //!Reads a signed short from packet into host order.
        void readShort(i16& word);
        void readInteger(i16& word) {readShort(word);}
        //!Reads an unsigned long from packet into host order.
        void readLong(u32& dword);
        void readInteger(u32& dword) {readLong(dword);}
        //!Reads a signed long from packet into host order.
        void readLong(i32& dword);
        void readInteger(i32& dword) {readLong(dword);}
        //!Reads a std::string from packet.
        void readString(std::string& str);
        //!Checks the number of string bytes (excluding null terminating character) of the next string in packet.
        u32 readStringLen();
        
};

} //!namespace r2

#endif //!R2_NETCOM_H
