//r2_network.cpp

//This code is published under the General Public License.

#define CONNECTIONTIMEOUT 5000 //milliseconds
#define AUTHTIMEOUT 5.0f //seconds

#include "..\r2_network\r2_network.h"
#include "..\r2_core\r2_log.h"
#include <memory.h>
#include <boost\lexical_cast.hpp>

using namespace r2;

u16 validatePort(const std::string& port); //used by new code

void CServer::clearUsers()
{
        userCount = 0;
        for(u8 i = 0; i < NETUSER_MAX; i++) {
                users[i].name = "";
                users[i].id = i;
                users[i].connected = false;
                
                userInfo[i].id = i;
                userInfo[i].authed = false;
                userInfo[i].peer = 0;
        }
}

void CServer::authorizeUser(u8 id)
{
        userInfo[id].authed = true;
        
        SNetworkEvent ev;
        ev.type = NETEVENTTYPE_SERVER_CLIENTCONNECTED;
        ev.id = id;
        ev.name = users[id].name;
        events.push_back(ev);
        
        CNetcom* netcom;
        
        //broadcast new user to everyone (including the new user)
        createWritableNetcom(&netcom, NETCOMTYPE_NET_ADDUSER, 1 + (users[id].name.size() + 1));
        netcom->writeInteger(id);
        netcom->writeString(users[id].name);
        sendNetcom(&netcom, 0, 0, NETCHANNEL_NET);
        
        //then send all other users to new user
        for(u8 i = 0; i < NETUSER_MAX; i++) {
                if(users[i].connected && userInfo[i].authed && i != id) {
                        createWritableNetcom(&netcom, NETCOMTYPE_NET_ADDUSER,
                                1 + (users[i].name.size() + 1));
                        netcom->writeInteger(i);
                        netcom->writeString(users[i].name);
                        sendNetcom(&netcom, &id, 1, NETCHANNEL_NET);
                }
        }
}

void CClient::clearUsers()
{
        userCount = 0;
        for(u8 i = 0; i < NETUSER_MAX; i++) {
                users[i].name = "";
                users[i].id = i;
                users[i].connected = false;
        }
}

bool CServer::initSingle(CClient& client)
{
        ASSERT(type == NETWORK_SERVERTYPE_NULL);
        
        type = NETWORK_SERVERTYPE_SINGLE;
        CServer::client = &client;
        
        clearUsers();
        
        return true;
}

bool CClient::initSingle(CServer& server)
{
        ASSERT(type == NETWORK_CLIENTTYPE_NULL);
        
        type = NETWORK_CLIENTTYPE_LOCAL;
        CClient::server = &server;
        
        clearUsers();
        
        userName = "You";
        server.addLocalUser(0, userName);
        
        SNetworkEvent ev;
        ev.type = NETEVENTTYPE_CLIENT_CONNECTED;
        enqueueEvent(ev);
        
        ev.type = NETEVENTTYPE_CLIENT_OTHERCONNECTED;
        ev.id = 0;
        ev.name = userName;
        enqueueEvent(ev);
        
        return true;
}

void CServer::addLocalUser(u8 id, const std::string& name)
{
        if(type == NETWORK_SERVERTYPE_SINGLE && !users[id].connected) {
                users[id].connected = true;
                users[id].name = name;
                userInfo[id].authed = true;
                userCount++;
                
                SNetworkEvent ev;
                ev.type = NETEVENTTYPE_SERVER_CLIENTCONNECTED;
                ev.id = id;
                ev.name = name;
                enqueueEvent(ev);
        }
}

bool CServer::initMulti(const std::string& port, const std::string& password, const std::string& userName)
{
        ASSERT(type == NETWORK_SERVERTYPE_NULL);
        
        //set up address
        ENetAddress addr;
        addr.host = ENET_HOST_ANY;
        addr.port = validatePort(port);
        
        if(addr.port == 0) {
                LOG_ERROR("Invalid port number: '" << port << "'");
                return false;
        }
        
        host = enet_host_create(&addr, NETUSER_MAX, 0, 0);
        
        if(host == 0) {
                LOG_ERROR("Unable to init ENet host.");
                return false;
        }
        
        clearUsers();
        
        CServer::password = password;
        locked = false;
        type = NETWORK_SERVERTYPE_MULTI;
        
        return true;
}

bool CClient::initConnect(const std::string& IPaddress, const std::string& port, const std::string& password,
        const std::string& userName)
{
        ASSERT(type == NETWORK_CLIENTTYPE_NULL);
        
        //set up address
        ENetAddress addr;
        enet_address_set_host(&addr, IPaddress.c_str());
        addr.port = validatePort(port);
        
        if(addr.port == 0) {
                LOG_ERROR("Invalid port number: '" << port << "'");
                return false;
        }
        
        host = enet_host_create(0, 1, 0, 0);
        
        if(host == 0) {
                LOG_ERROR("Unable to init ENet host.");
                return false;
        }
        
        peer = enet_host_connect(host, &addr, NETCHANNEL_COUNT);
        
        if(peer == 0) {
                LOG_ERROR("Unable to init ENet peer.");
                enet_host_destroy(host);
                host = 0;
                return false;
        }
        
        clearUsers();
        
        CClient::password = password;
        CClient::userName = userName;
        connected = false;
        type = NETWORK_CLIENTTYPE_CONNECTED;
        connectionTime = 0.0f;
        
        return true;
}

void CClient::destroy()
{
        CNetcomHandler::destroy();
        
        if(peer != 0) {
                enet_peer_reset(peer);
                peer = 0;
        }
        if(host != 0) {
                enet_host_destroy(host);
                host = 0;
        }
        
        connected = false;
        type = NETWORK_CLIENTTYPE_NULL;
}

void CServer::destroy()
{
        CNetcomHandler::destroy();
        
        if(host != 0) {
                enet_host_destroy(host);
                host = 0;
        }
        
        type = NETWORK_SERVERTYPE_NULL;
}

void CServer::kickUser(u8 id)
{
        if(users[id].connected && id != NETUSER_SERVERLOCAL) {
                if(userInfo[id].peer != 0) {
                        enet_peer_reset(userInfo[id].peer);
                        userInfo[id].peer = 0;
                }
      
                users[id].connected = false;
                users[id].name = "";
                
                userCount--;
    
                //only display quit message if user has been authed
                if(userInfo[id].authed) {
                        SNetworkEvent ev;
                        ev.type = NETEVENTTYPE_SERVER_CLIENTDISCONNECTED;
                        ev.id = id;
                        events.push_back(ev);
                }
        }
}

void CServer::sendNetcom(CNetcom** netcom, u8* toUsers, u8 count, u8 onChannel)
{
        /*!If toUsers == 0 then netcom is sent to all users.*/
        
        ASSERT(isInited());
        
        if(netcom == 0 || *netcom == 0) {
                return;
        }
        ENetPacket* packet = getPacket(**netcom);
        if(packet == 0) {
                destroyNetcom(netcom);
                return;
        }
        
        //determine whether to send to all users or specific
        if(toUsers == 0 && type == NETWORK_SERVERTYPE_MULTI) {
                enet_host_broadcast(host, onChannel, packet);
                        
                setPacket(**netcom, 0); //packet destruction needs to be handled by ENet now
                destroyNetcom(netcom);
        } else if(type == NETWORK_SERVERTYPE_MULTI) {
                bool enetSent = false;
                //step through all recipents
                for(u8 i = 0; i < count; i++) {
                        enetSent = true;
                        enet_peer_send(userInfo[toUsers[i]].peer, onChannel, packet);
                }
                if(enetSent) {
                        setPacket(**netcom, 0); //packet destruction needs to be handled by ENet now
                }
                
                destroyNetcom(netcom);
        } else if(type == NETWORK_CLIENTTYPE_LOCAL) {
                //"fake send" to local client (transfer to received list of client)
                if(toUsers == 0 || (count > 0 && toUsers[0] == 0)) {
                        setChannel(**netcom, onChannel);
                        setUnwritable(**netcom);
                        (*netcom)->rewind();
                        client->enqueueNetcom(netcom);
                }
        } else {
                //even if we can't send netcom we need to destroy it
                destroyNetcom(netcom);
        }
}

void CClient::sendNetcom(CNetcom** netcom, u8 onChannel)
{
        ASSERT(isInited());
        
        if(netcom == 0 || *netcom == 0) {
                return;
        }
        ENetPacket* packet = getPacket(**netcom);
        if(packet == 0) {
                destroyNetcom(netcom);
                return;
        }
        
        if(type == NETWORK_CLIENTTYPE_LOCAL) {
                //"fake send" to local server (transfer to received list)
                setChannel(**netcom, onChannel);
                setUnwritable(**netcom);
                setUser(**netcom, 0);
                (*netcom)->rewind();
                server->enqueueNetcom(netcom);
        } else if(type == NETWORK_CLIENTTYPE_CONNECTED) {
                //we need to send to remote server
                enet_peer_send(peer, onChannel, packet);
                setPacket(**netcom, 0); //packet destruction needs to be handled by ENet now
                destroyNetcom(netcom);
        } else {
                //even if we can't send netcom we need to destroy it
                destroyNetcom(netcom);
        }
}

void CServer::run(f32 dt)
{
        //check for server authorization timeouts
        if(type == NETWORK_SERVERTYPE_MULTI) {
                ENetEvent event; 
                
                enet_host_flush(host);
                
                for(u8 i = 0; i < NETUSER_MAX; i++) {
                        if(users[i].connected && !userInfo[i].authed) {
                                if(userInfo[i].authTime > AUTHTIMEOUT) {
                                        kickUser(i);
                                } else {
                                        userInfo[i].authTime += dt;
                                }
                        }
                }

                while(host != 0 && enet_host_service(host, &event, 0) > 0) {
                        switch(event.type) {
                                case ENET_EVENT_TYPE_CONNECT: {
                                        eventConnect(event);
                                        break;
                                } case ENET_EVENT_TYPE_DISCONNECT: {
                                        eventDisconnect(event);
                                        break;
                                } case ENET_EVENT_TYPE_RECEIVE: {
                                        eventReceive(event);
                                        break;
                                }
                        }
                }
        }
}

void CClient::run(f32 dt)
{
        //check for client connection timmout
        if(type == NETWORK_CLIENTTYPE_CONNECTED) {
                ENetEvent event; 
                
                enet_host_flush(host);
                
                if(connectionTime > CONNECTIONTIMEOUT) {
                        destroy(); //no point in living anymore
                        SNetworkEvent ev;
                        ev.type = NETEVENTTYPE_CLIENT_CONNECTIONFAILED;
                        events.push_back(ev);
                } else {
                        connectionTime += dt;
                }

                while(host != 0 && enet_host_service(host, &event, 0) > 0) {
                        switch(event.type) {
                                case ENET_EVENT_TYPE_CONNECT: {
                                        eventConnect(event);
                                        break;
                                } case ENET_EVENT_TYPE_DISCONNECT: {
                                        eventDisconnect(event);
                                        break;
                                } case ENET_EVENT_TYPE_RECEIVE: {
                                        eventReceive(event);
                                        break;
                                }
                        }
                }   
        }
}

void CServer::eventConnect(ENetEvent& event)
{
        if(userCount < NETUSER_MAX && !locked) {
                //search for a free user slot
                for(u8 i = 0; i < NETUSER_MAX; i++) {
                        //the user must have disconnected for slot to be free
                        if(!users[i].connected) {
                                userCount++;
                                
                                users[i].name = "";
                                users[i].connected = true;
                                
                                userInfo[i].peer = event.peer;
                                userInfo[i].authed = false;
                                userInfo[i].authTime = 0.0f;
                                
                                event.peer->data = (void*) (u32) i; //this field will be remembered
                                
                                //don't issue a connection connection network event because user ain't authed
                                return;
                        }
                }
        }
        
        //we don't have anymore user space or we're locked
        enet_peer_reset(event.peer);
}

void CServer::eventDisconnect(ENetEvent& event)
{
        u8 id = (u8) (u32) event.peer->data;
        
        enet_peer_reset(userInfo[id].peer);
        
        userCount--;
        users[id].connected = false;
        userInfo[id].peer = 0;
        
        //only give notice of disconnection if user had been authorized
        if(userInfo[id].authed) {
                SNetworkEvent ev;
                ev.type = NETEVENTTYPE_SERVER_CLIENTDISCONNECTED;
                ev.id = id;
                events.push_back(ev);
        }
        
        CNetcom* netcom;
        
        createWritableNetcom(&netcom, NETCOMTYPE_NET_REMOVEUSER, 1);
        netcom->writeInteger(id);
        sendNetcom(&netcom, 0, 0, NETCHANNEL_NET);
}

void CServer::eventReceive(ENetEvent& event)
{
        u8 id = (u8) (u32) event.peer->data;
        
        CNetcom* netcom = newNetcom();
        ASSERT(netcom != 0);
        initNetcomFromPacket(*netcom, event.packet, event.channelID, id);
        
        if(event.channelID == NETCHANNEL_NET) {
                switch(netcom->getType()) {
                        case NETCOMTYPE_NET_USERNAME: {
                                if(!userInfo[id].authed) {
                                        std::string newName;
                                        netcom->readString(newName);
                                        if(newName.size() > NETWORK_USERNAME_MAXLEN || newName.size() == 0) {
                                                //kick users with non-valid name lengths
                                                kickUser(id);
                                        } else {
                                                users[id].name = newName;
              
                                                //if PW is zero length the user is immediately authorized
                                                if(password == "") {
                                                        authorizeUser(id);
                                                }
                                        }
                                }
                                break;
                        } case NETCOMTYPE_NET_PASSWORD: {
                                if(!userInfo[id].authed && users[id].name != "") {
                                        std::string logonPassword;
                                        netcom->readString(logonPassword);
              
                                        //do we need to check password at all?
                                        if(password != "") {
                                                if(logonPassword == password) {
                                                        authorizeUser(id);
                                                } else {
                                                        kickUser(id);
                                                }
                                        } else {
                                                authorizeUser(id);
                                        }
                                }
                                break;
                        }
                }
                
                //this is a private message of ours
                destroyNetcom(&netcom);
        } else {
                if(userInfo[id].authed) {
                        netcoms.push_back(netcom);
                } else {
                        //kill messages sent before client is authorized
                        destroyNetcom(&netcom);
                }
        }
}

void CClient::eventConnect(ENetEvent& event)
{
        if(!connected && event.peer == peer) {
                CNetcom* netcom = 0;
    
                //send a packet containing the username
                createWritableNetcom(&netcom, NETCOMTYPE_NET_USERNAME, userName.size() + 1);
                netcom->writeString(userName);
                sendNetcom(&netcom, NETCHANNEL_NET);
    
                //send another packet containing the password (if required)
                if(password != "") {
                        createWritableNetcom(&netcom, NETCOMTYPE_NET_PASSWORD, password.size() + 1);
                        netcom->writeString(password);
                        sendNetcom(&netcom, NETCHANNEL_NET);
                }
    
                enet_host_flush(host);
                
                connected = true;
                
                SNetworkEvent ev;
                ev.type = NETEVENTTYPE_CLIENT_CONNECTED;
                events.push_back(ev);
        } else {
                enet_peer_reset(event.peer);
        }
}

void CClient::eventDisconnect(ENetEvent& event)
{
        if(event.peer != peer) {
                return;
        }
        
        destroy();
        
        SNetworkEvent ev;
        ev.type = NETEVENTTYPE_CLIENT_DISCONNECTED;
        events.push_back(ev);
}
void CClient::eventReceive(ENetEvent& event)
{
        if(event.peer != peer) {
                return;
        }
        
        CNetcom* netcom = newNetcom();
        ASSERT(netcom != 0);
        initNetcomFromPacket(*netcom, event.packet, event.channelID, 0);
        
        if(event.channelID == NETCHANNEL_NET) {
                switch(netcom->getType()) {
                        case NETCOMTYPE_NET_ADDUSER: {
                                u8 id;
                                netcom->readInteger(id);
                                netcom->readString(users[id].name);
                                users[id].connected = true;
                                
                                SNetworkEvent ev;
                                ev.type = NETEVENTTYPE_CLIENT_OTHERCONNECTED;
                                ev.id = id;
                                ev.name = users[id].name;
                                events.push_back(ev);
                                break;
                        } case NETCOMTYPE_NET_REMOVEUSER: {
                                u8 id;
                                netcom->readInteger(id);
                                users[id].connected = false;
                                
                                SNetworkEvent ev;
                                ev.type = NETEVENTTYPE_CLIENT_OTHERDISCONNECTED;
                                ev.id = id;
                                events.push_back(ev);
                                break;
                        }
                }
                destroyNetcom(&netcom);
        } else {
                netcoms.push_back(netcom);
        }
}

u16 validatePort(const std::string& port)
{
        u16 uncheckedPort = 0;
        //make the basic conversion
        try {
                uncheckedPort = boost::lexical_cast<u16>(port);
        } catch(boost::bad_lexical_cast&) {
                return 0;
        }
  
        //ports under 1024 are reserved if I recall correctly (less sure that 1024
        //itself it reserved too but include it for safety)
        
        return uncheckedPort > 1024 ? uncheckedPort : 0;
}

