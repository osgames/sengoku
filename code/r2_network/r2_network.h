//r2_network.h

//This code is published under the General Public License.

/*Revision history:
2007-09-02: Starting implementing Netcom system and removing "packets".
*/

#ifndef R2_NETWORK_H
#define R2_NETWORK_H

#include "enet/enet.h"
#include <string>
#include <list>
#include "..\r2_core\typedef.h"
#include "..\r2_core\r2_msgs.h"
#include "..\r2_network\r2_netcom.h"

#define NETWORK_USERNAME_MAXLEN 31
#define NETWORK_PASSWORD_MAXLEN 31

#define NETUSER_MAX             16 //Enet needs a maximum
#define NETUSER_SERVERLOCAL     0 //local player is always has index 0
#define NETUSER_ALL             255 //means message should be sent to everyone

#define NETCHANNEL_NET          0 //used by the classes below
#define NETCHANNEL_WORLD        1 //world updates from server to client, and world requests from clients to world
#define NETCHANNEL_CHAT         2 //chat messages sent in both directions
#define NETCHANNEL_PAUSE        3 //channel specifically used to communicate a game pause
#define NETCHANNEL_COUNT        4

#define NETWORK_CLIENTTYPE_NULL                 0
#define NETWORK_CLIENTTYPE_LOCAL                1
#define NETWORK_CLIENTTYPE_CONNECTED            2
#define NETWORK_SERVERTYPE_NULL                 0
#define NETWORK_SERVERTYPE_SINGLE               1
#define NETWORK_SERVERTYPE_MULTI                2

#define NETCOMTYPE_NET_PASSWORD                 0
#define NETCOMTYPE_NET_USERNAME                 1
#define NETCOMTYPE_NET_ADDUSER                  2
#define NETCOMTYPE_NET_REMOVEUSER               3

namespace r2 {

struct SUser
{
        //general properties
        std::string name;
        u8 id; //it needs to know about its own index into array = ID
        bool connected;
};

struct SUserServerInfo
{
        ENetPeer* peer;
        f32 authTime;
        u8 id;
        bool authed; //did this client comply with our password?
};

class CClient;

class CServer : public CNetcomHandler
{
private: //attributes
        SUser users[NETUSER_MAX];
        SUserServerInfo userInfo[NETUSER_MAX];
        u8 userCount;
        u8 type; //local or multi
        bool locked;
        std::string password;
        ENetHost* host;
        CClient* client;
private: //functions
        //!Clears all user data to default initialized state (but doesn't perform anything else).
        void clearUsers();
        
        //!Sets a user as authorized, sends info about other clients, and informs other clients, as well as an event.
        void authorizeUser(u8 id);
        
        //!Internal connection event handler for server.
        void eventConnect(ENetEvent& event);
        //!Internal disconnection event handler for server.
        void eventDisconnect(ENetEvent& event);
        //!Internal reception event handler for server.
        void eventReceive(ENetEvent& event);
public:
        CServer() {type = NETWORK_SERVERTYPE_NULL;}
        ~CServer() {destroy();}
        
        bool initSingle(CClient& client);
        bool initMulti(const std::string& port, const std::string& password, const std::string& userName);
        
        void destroy();
        
        //!Performs network tasks.
        void run(f32 dt);
        
        //!Returns number of users connected to server.
        u8 countUsers() {return userCount;}
        
        //!Sends a network command created by createNetcom() to series of clients (or all), and destroys it.
        void sendNetcom(CNetcom** netcom, u8* toUsers, u8 count, u8 onChannel);
        
        //!Forcefully kicks a user from server. Can't kick a server local user.
        void kickUser(u8 id);
        //!Adds a local user bypassing normal procedures (for non-socket use).
        void addLocalUser(u8 id, const std::string& name);
        
        //!Prevents more users from connecting to a multi server.
        void lock() {locked = true;}
        //!Allows more users to connect to a multi server.
        void unlock() {locked = false;}
        //!Toggles between locked and unlocked states.
        void switchLock() {locked = !locked;}
        
        //!Returns if this is inited or has been destroy.
        bool isInited() {return type != NETWORK_SERVERTYPE_NULL;}
        
        bool isUserAuthorized(u8 user) {return users[user].connected && userInfo[user].authed;}
        std::string getUserName(u8 user) {return users[user].name;}
};

class CClient : public CNetcomHandler
{
private:
        SUser users[NETUSER_MAX];
        u8 userCount;
        u8 type; //local or connected
        bool connected;
        f32 connectionTime;
        std::string userName;
        std::string password;
        ENetHost* host;
        ENetPeer* peer;
        CServer* server;
private: //functions
        //!Clears all user data to default initialized state (but doesn't perform anything else).
        void clearUsers();
        
        //!Internal connection event handler for client.
        void eventConnect(ENetEvent& event);
        //!Internal disconnection event handler for client.
        void eventDisconnect(ENetEvent& event);
        //!Internal reception event handler for client.
        void eventReceive(ENetEvent& event);
public:
        CClient() {type = NETWORK_CLIENTTYPE_NULL;}
        ~CClient() {destroy();}
        
        bool initSingle(CServer& server);
        bool initConnect(const std::string& IPaddress, const std::string& port, const std::string& password,
                const std::string& userName);
        
        void destroy();
        
        //!Performs network tasks.
        void run(f32 dt);
        
        //!Sends a network command created by createNetcom() to server, and destroys it.
        void sendNetcom(CNetcom** netcom, u8 onChannel);
        
        //!Returns number of users connected to server.
        u8 countUsers() {return userCount;}
        
        //!Returns if this is inited or has been destroy.
        bool isInited() {return type != NETWORK_CLIENTTYPE_NULL;}
};

} //!namespace r2

#endif //!R2_NETWORK_H
